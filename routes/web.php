<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', function () {
    return view('auth/login');
});
Route::get('/login', 'Auth\LoginController@showLoginForm');
Route::post('/login', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\LoginController@logout');

Route::get('/admin', function () {
    return view('admin_main');
});
Route::get('/btree/admin/get_all_employee', ['uses' => 'EmployeeController@getAll','middleware'=>'btree']);

Route::post('/btree/admin/create_employee', ['uses' => 'EmployeeController@save','middleware'=>'btree']);

Route::post('/btree/admin/get_employee_by_id', ['uses' => 'EmployeeController@getById','middleware'=>'btree']);

Route::put('/btree/admin/update_employee/{id}', ['uses' => 'EmployeeController@update','middleware'=>'btree']);

Route::delete('/btree/admin/delete_employee/{id}', ['uses' => 'EmployeeController@deleteemployee','middleware'=>'btree']);

Route::post('/btree/admin/delete_allemployee', ['uses' => 'EmployeeController@deleteAll','middleware'=>'btree']);

Route::get('/btree/admin/get_employeebysales', ['uses' => 'EmployeeController@employeebysales','middleware'=>'btree']);

Route::get('/btree/admin/search_employee', ['uses' => 'EmployeeController@search','middleware'=>'btree']);
Route::get('/btree/admin/search_trashemployee', ['uses' => 'EmployeeController@searchTrash','middleware'=>'btree']);

Route::post('/btree/admin/delete_selectemployee', ['uses' => 'EmployeeController@deleteSelectEmployee','middleware'=>'btree']);

Route::post('/btree/admin/add_selectemployee', ['uses' => 'EmployeeController@addSelectEmployee','middleware'=>'btree']);



Route::get('/btree/admin/get_all_customer', ['uses' => 'CustomerController@getAll','middleware'=>'btree']);

Route::post('/btree/admin/create_customer', ['uses' => 'CustomerController@save','middleware'=>'btree']);

Route::post('/btree/admin/get_customer_by_id', ['uses' => 'CustomerController@getById','middleware'=>'btree']);

Route::put('/btree/admin/update_customer/{id}', ['uses' => 'CustomerController@update','middleware'=>'btree']);

Route::delete('/btree/admin/delete_customer/{id}', ['uses' => 'CustomerController@deletecustomer','middleware'=>'btree']);

Route::post('/btree/admin/delete_allcustomer', ['uses' => 'CustomerController@deleteAll','middleware'=>'btree']);

Route::get('/btree/admin/get_customerbysales', ['uses' => 'CustomerController@customerbysales','middleware'=>'btree']);

Route::get('/btree/admin/search_customer', ['uses' => 'CustomerController@search','middleware'=>'btree']);

Route::post('/btree/admin/delete_selectcustomer', ['uses' => 'CustomerController@deleteSelectCustomer','middleware'=>'btree']);

Route::post('/btree/admin/add_selectcustomer', ['uses' => 'CustomerController@addSelectCustomer','middleware'=>'btree']);

Route::get('/btree/admin/customer_pointclaim', ['uses' => 'CustomerController@CustomerPointsClaim','middleware'=>'btree']);


Route::post('/btree/admin/create_item', ['uses' => 'ItemController@save','middleware'=>'btree']);

Route::delete('/btree/admin/delete_item/{id}', ['uses' => 'ItemController@deleteitem','middleware'=>'btree']);

Route::post('/btree/admin/delete_allitem', ['uses' => 'ItemController@deleteAll','middleware'=>'btree']);

Route::get('/btree/admin/get_all_item', ['uses' => 'ItemController@getAll','middleware'=>'btree']);

Route::post('/btree/admin/get_item_by_id', ['uses' => 'ItemController@getById','middleware'=>'btree']);

Route::put('/btree/admin/update_item/{id}', ['uses' => 'ItemController@update','middleware'=>'btree']);

Route::get('/btree/admin/search_item', ['uses' => 'ItemController@search','middleware'=>'btree']);

Route::post('/btree/admin/delete_selectitem', ['uses' => 'ItemController@deleteSelectItem','middleware'=>'btree']);

Route::post('/btree/admin/add_selectitem', ['uses' => 'ItemController@addSelectItem','middleware'=>'btree']);


Route::post('/btree/admin/create_itemkit', ['uses' => 'ItemKitController@save','middleware'=>'btree']);

Route::delete('/btree/admin/delete_itemkit/{id}', ['uses' => 'ItemKitController@deleteitem','middleware'=>'btree']);

Route::post('/btree/admin/delete_allitemkit', ['uses' => 'ItemKitController@deleteAll','middleware'=>'btree']);

Route::get('/btree/admin/get_all_itemkit', ['uses' => 'ItemKitController@getAll','middleware'=>'btree']);

Route::post('/btree/admin/get_itemkit_by_id', ['uses' => 'ItemKitController@getById','middleware'=>'btree']);

Route::put('/btree/admin/update_itemkit/{id}', ['uses' => 'ItemKitController@update','middleware'=>'btree']);

Route::get('/btree/admin/search_itemkit', ['uses' => 'ItemKitController@search','middleware'=>'btree']);

Route::post('/btree/admin/delete_selectitemkit', ['uses' => 'ItemKitController@deleteSelectItem','middleware'=>'btree']);

Route::post('/btree/admin/add_selectitemkit', ['uses' => 'ItemKitController@addSelectItem','middleware'=>'btree']);


Route::get('/btree/admin/get_all_supplier', ['uses' => 'SupplierController@getAll','middleware'=>'btree']);

Route::post('/btree/admin/create_supplier', ['uses' => 'SupplierController@save','middleware'=>'btree']);

Route::post('/btree/admin/get_supplier_by_id', ['uses' => 'SupplierController@getById','middleware'=>'btree']);

Route::put('/btree/admin/update_supplier/{id}', ['uses' => 'SupplierController@update','middleware'=>'btree']);

Route::delete('/btree/admin/delete_supplier/{id}', ['uses' => 'SupplierController@deletesupplier','middleware'=>'btree']);

Route::post('/btree/admin/delete_allsupplier', ['uses' => 'SupplierController@deleteAll','middleware'=>'btree']);

Route::get('/btree/admin/get_supplierbysales', ['uses' => 'SupplierController@supplierbysales','middleware'=>'btree']);

Route::get('/btree/admin/search_supplier', ['uses' => 'SupplierController@search','middleware'=>'btree']);

Route::get('/btree/admin/get_supplier_list_all', ['uses' => 'SupplierController@getAllList','middleware'=>'btree']);

Route::post('/btree/admin/delete_selectsupplier', ['uses' => 'SupplierController@deleteSelectSupplier','middleware'=>'btree']);

Route::post('/btree/admin/add_selectsupplier', ['uses' => 'SupplierController@addSelectSupplier','middleware'=>'btree']);



Route::get('/btree/admin/get_salesitem_list', ['uses' => 'SalesController@salebyitem','middleware'=>'btree']);

Route::post('/btree/admin/create_salesitem', ['uses' => 'SalesController@save','middleware'=>'btree']);

Route::post('/btree/admin/return_salesitem', ['uses' => 'SalesController@return','middleware'=>'btree']);

Route::get('/btree/admin/get_suspendedlist', ['uses' => 'SuspendedController@getAllList','middleware'=>'btree']);

Route::get('/btree/admin/get_suspendedlistrender', ['uses' => 'SuspendedController@getAllListRender','middleware'=>'btree']);

Route::post('/btree/admin/create_receivingitem', ['uses' => 'ReceivingController@save','middleware'=>'btree']);

Route::post('/btree/admin/return_receivingitem', ['uses' => 'ReceivingController@return','middleware'=>'btree']);


Route::post('/btree/admin/create_suspendeditem', ['uses' => 'SuspendedController@save','middleware'=>'btree']);



Route::get('/btree/admin/get_all_company', ['uses' => 'CompanyController@getAll','middleware'=>'btree']);

Route::post('/btree/admin/create_company', ['uses' => 'CompanyController@save','middleware'=>'btree']);

Route::post('/btree/admin/get_company_by_id', ['uses' => 'CompanyController@getById','middleware'=>'btree']);

Route::put('/btree/admin/update_company/{id}', ['uses' => 'CompanyController@update','middleware'=>'btree']);

Route::delete('/btree/admin/delete_company/{id}', ['uses' => 'CompanyController@deletecompany','middleware'=>'btree']);

Route::post('/btree/admin/delete_allcompany', ['uses' => 'CompanyController@deleteAll','middleware'=>'btree']);

Route::get('/btree/admin/get_company_list', ['uses' => 'CompanyController@getList','middleware'=>'btree']);

Route::get('/btree/admin/search_company', ['uses' => 'CompanyController@search','middleware'=>'btree']);

Route::post('/btree/admin/delete_selectcompany', ['uses' => 'CompanyController@deleteSelectCompany','middleware'=>'btree']);

Route::post('/btree/admin/add_selectcompany', ['uses' => 'CompanyController@addSelectCompany','middleware'=>'btree']);

Route::get('/btree/admin/get_download_excel', ['uses' => 'ExportController@downloadExcel','middleware'=>'btree']);

Route::get('/btree/admin/get_download_pdf', ['uses' => 'ExportPDFController@downloadPDF','middleware'=>'btree']);


Route::get('/btree/admin/get_all_expense_category', ['uses' => 'ExpenseCategoryController@getAll','middleware'=>'btree']);


Route::post('/btree/admin/create_expense_category', ['uses' => 'ExpenseCategoryController@save','middleware'=>'btree']);


Route::post('/btree/admin/get_expense_category_by_id', ['uses' => 'ExpenseCategoryController@getById','middleware'=>'btree']);


Route::put('/btree/admin/update_expense_category/{id}', ['uses' => 'ExpenseCategoryController@update','middleware'=>'btree']);


Route::delete('/btree/admin/delete_expense_category/{id}', ['uses' => 'ExpenseCategoryController@deleteexpense_category','middleware'=>'btree']);


Route::post('/btree/admin/delete_allexpense_category', ['uses' => 'ExpenseCategoryController@deleteAll','middleware'=>'btree']);


Route::get('/btree/admin/get_expense_category_list', ['uses' => 'ExpenseCategoryController@getList','middleware'=>'btree']);


Route::get('/btree/admin/search_expense_category', ['uses' => 'ExpenseCategoryController@search','middleware'=>'btree']);

Route::get('/btree/admin/get_expense_category_list_all', ['uses' => 'ExpenseCategoryController@getListAll','middleware'=>'btree']);

Route::post('/btree/admin/delete_selectexpense_category', ['uses' => 'ExpenseCategoryController@deleteSelectExpenseCategory','middleware'=>'btree']);

Route::post('/btree/admin/add_selectexpense_category', ['uses' => 'ExpenseCategoryController@addSelectExpenseCategory','middleware'=>'btree']);




Route::get('/btree/admin/get_all_product', ['uses' => 'ProductController@getAll','middleware'=>'btree']);

Route::post('/btree/admin/create_product', ['uses' => 'ProductController@save','middleware'=>'btree']);

Route::post('/btree/admin/get_product_by_id', ['uses' => 'ProductController@getById','middleware'=>'btree']);

Route::put('/btree/admin/update_product/{id}', ['uses' => 'ProductController@update','middleware'=>'btree']);

Route::delete('/btree/admin/delete_product/{id}', ['uses' => 'ProductController@deleteproduct','middleware'=>'btree']);
 
Route::post('/btree/admin/delete_allproduct', ['uses' => 'ProductController@deleteAll','middleware'=>'btree']);
Route::get('/btree/admin/search_product', ['uses' => 'ProductController@search','middleware'=>'btree']);

Route::get('/btree/admin/get_product_list', ['uses' => 'ProductController@getList','middleware'=>'btree']);

Route::post('/btree/admin/delete_selectproduct', ['uses' => 'ProductController@deleteSelectProduct','middleware'=>'btree']);

Route::post('/btree/admin/add_selectproduct', ['uses' => 'ProductController@addSelectProduct','middleware'=>'btree']);



Route::get('/btree/admin/get_all_company_report', ['uses' => 'ReportController@getCompany','middleware'=>'btree']);

Route::get('/btree/admin/get_all_product_report', ['uses' => 'ReportController@getProduct','middleware'=>'btree']);

Route::get('/btree/admin/get_all_customer_report', ['uses' => 'ReportController@getCustomer','middleware'=>'btree']);

Route::get('/btree/admin/get_all_supplier_report', ['uses' => 'ReportController@getSupplier','middleware'=>'btree']);

Route::get('/btree/admin/get_all_item_report', ['uses' => 'ReportController@getItem','middleware'=>'btree']);

Route::get('/btree/admin/get_all_expense_report', ['uses' => 'ReportController@getExpense','middleware'=>'btree']);

Route::get('/btree/admin/get_all_sales_report', ['uses' => 'ReportController@getSales','middleware'=>'btree']);

Route::get('/btree/admin/get_all_suspendedsales_report', ['uses' => 'ReportController@getSuspendedSales','middleware'=>'btree']);

Route::get('/btree/admin/get_all_receiving_report', ['uses' => 'ReportController@getReceiving','middleware'=>'btree']);

Route::get('/btree/admin/get_all_discount_report', ['uses' => 'ReportController@getDiscount','middleware'=>'btree']);

Route::get('/btree/admin/get_all_discountdetails_report', ['uses' => 'ReportController@getdetailsDiscount','middleware'=>'btree']);

Route::get('/btree/admin/get_all_payment_report', ['uses' => 'ReportController@getPayment','middleware'=>'btree']);

Route::get('/btree/admin/get_all_tax_report', ['uses' => 'ReportController@getTax','middleware'=>'btree']);

Route::get('/btree/admin/get_all_employee_report', ['uses' => 'ReportController@getEmployee','middleware'=>'btree']);

Route::get('/btree/admin/get_all_transaction_report', ['uses' => 'ReportController@getTransaction','middleware'=>'btree']);

Route::get('/btree/admin/get_all_itemkits_report', ['uses' => 'ReportController@getItemkits','middleware'=>'btree']);

Route::get('/btree/admin/get_all_commission_report', ['uses' => 'ReportController@getCommission','middleware'=>'btree']);

Route::get('/btree/admin/get_all_detailsales_report', ['uses' => 'ReportController@getDetailSales','middleware'=>'btree']);

Route::get('/btree/admin/get_all_detailsales_item', ['uses' => 'ReportController@getSalesItem','middleware'=>'btree']);

Route::get('/btree/admin/get_all_detailcustomer_report', ['uses' => 'ReportController@getDetailCustomer','middleware'=>'btree']);

Route::get('/btree/admin/get_all_detailsupplier_report', ['uses' => 'ReportController@getDetailSupplier','middleware'=>'btree']);

Route::get('/btree/admin/get_all_detailemployee_report', ['uses' => 'ReportController@getDetailEmployee','middleware'=>'btree']);

Route::get('/btree/admin/get_all_lowinventory', ['uses' => 'ReportController@getLowInventory','middleware'=>'btree']);

Route::get('/btree/admin/get_all_inventorysummary', ['uses' => 'ReportController@getInventorySummary','middleware'=>'btree']);

Route::get('/btree/admin/get_all_deaditem', ['uses' => 'ReportController@getDeadItem','middleware'=>'btree']);


Route::post('/btree/admin/create_expense', ['uses' => 'ExpenseController@save','middleware'=>'btree']);

Route::delete('/btree/admin/delete_expense/{id}', ['uses' => 'ExpenseController@deleteexpense','middleware'=>'btree']);

Route::post('/btree/admin/delete_allexpense', ['uses' => 'ExpenseController@deleteAll','middleware'=>'btree']);

Route::get('/btree/admin/get_all_expense', ['uses' => 'ExpenseController@getAll','middleware'=>'btree']);

Route::post('/btree/admin/get_expense_by_id', ['uses' => 'ExpenseController@getById','middleware'=>'btree']);

Route::put('/btree/admin/update_expense/{id}', ['uses' => 'ExpenseController@update','middleware'=>'btree']);

Route::get('/btree/admin/search_expense', ['uses' => 'ExpenseController@search','middleware'=>'btree']);

Route::post('/btree/admin/delete_selectexpense', ['uses' => 'ExpenseController@deleteSelectExpense','middleware'=>'btree']);

Route::post('/btree/admin/add_selectexpense', ['uses' => 'ExpenseController@addSelectExpense','middleware'=>'btree']);




Route::get('/btree/admin/get_all_location', ['uses' => 'LocationController@getAll','middleware'=>'btree']);

Route::post('/btree/admin/create_location', ['uses' => 'LocationController@save','middleware'=>'btree']);

Route::post('/btree/admin/get_location_by_id', ['uses' => 'LocationController@getById','middleware'=>'btree']);

Route::put('/btree/admin/update_location/{id}', ['uses' => 'LocationController@update','middleware'=>'btree']);

Route::delete('/btree/admin/delete_location/{id}', ['uses' => 'LocationController@deletelocation','middleware'=>'btree']);

Route::post('/btree/admin/delete_alllocation', ['uses' => 'LocationController@deleteAll','middleware'=>'btree']);

Route::get('/btree/admin/search_location', ['uses' => 'LocationController@search','middleware'=>'btree']);

Route::post('/btree/admin/delete_selectlocation', ['uses' => 'LocationController@deleteSelectLocation','middleware'=>'btree']);

Route::post('/btree/admin/add_selectlocation', ['uses' => 'LocationController@addSelectLocation','middleware'=>'btree']);




Route::get('/btree/admin/get_all_companytrash', ['uses' => 'CompanyController@getAlltrash','middleware'=>'btree']);

Route::post('/btree/admin/restore_allcompany', ['uses' => 'CompanyController@storeAll','middleware'=>'btree']);

Route::get('/btree/admin/get_all_producttrash', ['uses' => 'ProductController@getAlltrash','middleware'=>'btree']);

Route::post('/btree/admin/restore_allproduct', ['uses' => 'ProductController@storeAll','middleware'=>'btree']);

Route::get('/btree/admin/get_all_locationtrash', ['uses' => 'LocationController@getAlltrash','middleware'=>'btree']);

Route::post('/btree/admin/restore_alllocation', ['uses' => 'LocationController@storeAll','middleware'=>'btree']);

Route::get('/btree/admin/get_all_itemtrash', ['uses' => 'ItemController@getAlltrash','middleware'=>'btree']);

Route::post('/btree/admin/restore_allitem', ['uses' => 'ItemController@storeAll','middleware'=>'btree']);

Route::get('/btree/admin/get_all_customertrash', ['uses' => 'CustomerController@getAlltrash','middleware'=>'btree']);

Route::post('/btree/admin/restore_allcustomer', ['uses' => 'CustomerController@storeAll','middleware'=>'btree']);

Route::get('/btree/admin/get_all_suppliertrash', ['uses' => 'SupplierController@getAlltrash','middleware'=>'btree']);

Route::post('/btree/admin/restore_allsupplier', ['uses' => 'SupplierController@storeAll','middleware'=>'btree']);

Route::get('/btree/admin/get_all_employeetrash', ['uses' => 'EmployeeController@getAlltrash','middleware'=>'btree']);

Route::post('/btree/admin/restore_allemployee', ['uses' => 'EmployeeController@storeAll','middleware'=>'btree']);

Route::get('/btree/admin/get_all_expense_categorytrash', ['uses' => 'ExpenseCategoryController@getAlltrash','middleware'=>'btree']);

Route::post('/btree/admin/restore_allexpense_category', ['uses' => 'ExpenseCategoryController@storeAll','middleware'=>'btree']);

Route::get('/btree/admin/get_all_expensetrash', ['uses' => 'ExpenseController@getAlltrash','middleware'=>'btree']);

Route::post('/btree/admin/restore_allexpense', ['uses' => 'ExpenseController@storeAll','middleware'=>'btree']);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
