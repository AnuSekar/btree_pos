<?php
namespace App\Requests;

use Illuminate\Http\Request;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Validation\Rule;

class CreateExpenseRequest extends FormRequest
{
    protected $redirect = '/btree/validation-fail';

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [            
            'date' => 'required',
            'amount' => 'required',
            'payment_type' => 'required',
            'expense_description' => 'required',
            'supplier_name' => 'required',
        ];
    }
}

?>
