<?php
namespace App\Requests;

use Illuminate\Http\Request;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Validation\Rule;

class CreateCompanyRequest extends FormRequest
{
    protected $redirect = '/btree/validation-fail';

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [            
             'company_name' => ['required', 
                    Rule::unique('btree_company')->where(function ($query) {
                        $query->where('deleted', 0);
                    })],
            'company_description' => 'required',
        ];
    }
}

?>