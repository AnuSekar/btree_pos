<?php
namespace App\Requests;

use Illuminate\Http\Request;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Validation\Rule;

class CreateCustomerRequest extends FormRequest
{
    protected $redirect = '/btree/validation-fail';

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [

                'first_name' => 'required',
                'last_name' => 'required',
                'phone_number' => Rule::unique('btree_people'),
                'email' => Rule::unique('btree_people'),
                'address_1'  => Rule::unique('btree_people'),
                'city' => 'required',
                'state' => 'required',
                'zip' => 'required',
                'country' => 'required',
            
        ];
    }
}

?>


