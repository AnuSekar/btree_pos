<?php
namespace App\Requests;

use Illuminate\Http\Request;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Validation\Rule;

class CreateItemRequest extends FormRequest
{
    protected $redirect = '/btree/validation-fail';

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [            
            'item_name' => ['required', 
                    Rule::unique('btree_item')->where(function ($query) {
                        $query->where('deleted', 0);
                    })],
            'item_number' => ['required', 
                    Rule::unique('btree_item')->where(function ($query) {
                        $query->where('deleted', 0);
                    })],
            'description' => 'required',
            'cost_price' => 'required',
            'unit_price' => 'required',
            'receiving_quantity' => 'required',
        ];
    }
}

?>
