<?php
namespace App\Requests;

use Illuminate\Http\Request;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Validation\Rule;

class CreateLocationRequest extends FormRequest
{
    protected $redirect = '/btree/validation-fail';

    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [            
            'location_name' => ['required', 
                    Rule::unique('btree_location')->where(function ($query) {
                        $query->where('deleted', 0);
                    })],
        ];
    }

}

?>