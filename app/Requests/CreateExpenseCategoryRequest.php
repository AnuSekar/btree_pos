<?php
namespace App\Requests;

use Illuminate\Http\Request;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Validation\Rule;

class CreateExpenseCategoryRequest extends FormRequest
{
    protected $redirect = '/btree/validation-fail';

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [            
            
            'expense_category_name' => 'required',
            'expense_category_description' => 'required',
        ];
    }
}

?>