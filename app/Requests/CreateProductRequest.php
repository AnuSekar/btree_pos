<?php
namespace App\Requests;

use Illuminate\Http\Request;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Validation\Rule;

class CreateProductRequest extends FormRequest
{
    protected $redirect = '/btree/validation-fail';

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [            
            'product_name' => ['required', 
                    Rule::unique('btree_product')->where(function ($query) {
                        $query->where('deleted', 0);
                    })],
            'product_description' => 'required',
        ];
    }
}

?>