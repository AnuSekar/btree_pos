<?php
namespace App\Repositories;
use App\Models\Location;
use Illuminate\Support\Facades\DB;
use App\Util\BLAlphaNumericCodeGenerator;
use Log;
use App\Response\GlobalResponse;

class LocationRepository
{
     private static  $RECORDS_PER_PAGE =2;

     public function __construct() {
        self::$RECORDS_PER_PAGE = config('blconstants.RECORDS_PER_PAGE');
    }
    public function save(array $data)
    {
         DB::beginTransaction();
         try {
            $location = new  Location;
            $location->fill($data);
            $location->save();
            DB::commit();
        } catch(Exception $e) {
            DB::rollback();
            return GlobalResponse::clientErrorResponse("error");
        }
        
         return GlobalResponse::createResponse($location);
    }

    public function update(array $data){
        DB::beginTransaction();
        try{Log::info($data);
            $location = Location::where("location_id",$data['location_id'])->first(); 
                       
            $location->fill($data);
            $location->save();
            DB::commit();
        }catch(Exception $e) {
            DB::rollback();
            return GlobalResponse::clientErrorResponse("error");
        }
        
         return GlobalResponse::createResponse($location);
    }

    public function deleteAll(){
        try{
            $location = Location::where('deleted', '=', 0)->update(['deleted' => 1]);
             
        }catch(Exception $e){
            return GlobalResponse::clientErrorResponse("error");
        }

         return GlobalResponse::createResponse($location);
    }

    

    public function getAll(){
        try{
            
            $location = DB::table('btree_location as com')
            ->where('com.deleted','=','0')
            ->select('com.*')
            ->Paginate(self::$RECORDS_PER_PAGE);

        }catch(Exception $e){
            return GlobalResponse::clientErrorResponse("error");
        }

         return GlobalResponse::createResponse($location);

    }

    public function getAlltrash(){
        try{
            
            $location = DB::table('btree_location as com')
            ->where('com.deleted','=','1')
            ->select('com.*')
            ->Paginate(self::$RECORDS_PER_PAGE);

        }catch(Exception $e){
            return GlobalResponse::clientErrorResponse("error");
        }

         return GlobalResponse::createResponse($location);

    }


    public function storeAll(){
        try{
            $location = Location::where('deleted', '=', 1)->update(['deleted' => 0]);
             
        }catch(Exception $e){
            return GlobalResponse::clientErrorResponse("error");
        }

         return GlobalResponse::createResponse($location);
    }



    public function getById($data){
        try{
            $location = DB::table('btree_location as com')
            ->where('com.location_id','=',$data)
            ->select('com.*')
            ->get();
           

        }catch(Exception $e){
            return GlobalResponse::clientErrorResponse("error");
        }
        Log::info($location);
        return GlobalResponse::createResponse($location);
    }

    public function deletelocation($data){
        DB::beginTransaction();
      try{
           $location = Location::where('location_id','=',$data)->first();
           if($location->deleted == 1){
            $location->deleted = 0;
            $location->save();
           }
           else{
            $location->deleted = 1;
            $location->save();
           }
           DB::commit();
       }catch(Exception $e){
            DB::rollback();
           return GlobalResponse::clientErrorResponse("error");
       }
        return GlobalResponse::createResponse($location);
   }

   public function search($data){
        try{
             Log::info('Some message here.');

            $sql = " location_name like '%".$data."%' and deleted = 0 ";

             $location = DB::table('btree_location as com')
            ->whereRaw($sql)
            ->Paginate(self::$RECORDS_PER_PAGE);
            Log::info($location);
        }catch(Exception $e){
           return GlobalResponse::clientErrorResponse("error");
        }

         return GlobalResponse::createResponse($location);

    }
     public function getList(){
        try{
            
            $location = DB::table('btree_location as com')
            ->where('com.deleted','=','0')
            ->select('com.*')
            ->get();
            
        }catch(Exception $e){
            return GlobalResponse::clientErrorResponse("error");
        }

         return GlobalResponse::createResponse($location);
  }  

  public function deleteSelectLocation(array $data){
    DB::beginTransaction();
      try{

        foreach ($data as $value) {
            $location = Location::where('location_id','=',$value)->first();
            $location->deleted = 1;
            $location->save();
        }
        DB::commit();
       }catch(Exception $e){
            DB::rollback();
           return GlobalResponse::clientErrorResponse("error");
       }
       
        return GlobalResponse::createResponse("success");
   }

   public function addSelectLocation(array $data){
    DB::beginTransaction();
      try{

        foreach ($data as $value) {
            $location = Location::where('location_id','=',$value)->first();
            $location->deleted = 0;
            $location->save();
        }
        DB::commit();
       }catch(Exception $e){
            DB::rollback();
           return GlobalResponse::clientErrorResponse("error");
       }
       
        return GlobalResponse::createResponse("success");
   }




} ?>

