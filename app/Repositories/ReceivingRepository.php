<?php
namespace App\Repositories;
use App\Models\Item;
use App\Models\Item_Tax;
use App\Models\Receiving;
use App\Models\ReceivingItem;
use App\Models\ItemQuantity;
use App\Models\Transaction;
use Illuminate\Support\Facades\DB;
use App\Util\BLAlphaNumericCodeGenerator;
use Log;

class ReceivingRepository
{
     private static  $RECORDS_PER_PAGE =2;

     public function __construct() {
        self::$RECORDS_PER_PAGE = config('blconstants.RECORDS_PER_PAGE');
    }
    public function save(array $datas)
    {
         DB::beginTransaction();
         try {
            Log::info($datas);
            if($datas['supplier'] != ''){
                $datas['supplier_id'] = $datas['supplier'];
            }
            
            $datas['employee_id'] = 1;
            $datas['reference'] = 1;
            $receiving = new Receiving();
            $receiving->fill($datas);
            $receiving->save();
            $x =1;
            foreach($datas['item'] as $data){
                Log::info($data);
                $item = Item::where("item_id",$data['item_id'])->first(); 
                $receivingitem = new ReceivingItem();
                $receivingitem['receiving_id'] = $receiving->receiving_id;
                $receivingitem['item_id'] = $data['item_id'];
                $receivingitem['line'] = $x;
                $receivingitem['quantity_purchased'] = $data['item_quantity'];
                $receivingitem['item_cost_price'] = $item['cost_price'];
                $receivingitem['item_unit_price'] = $item['unit_price'];
                $receivingitem['receiving_quantity'] = $data['item_quantity'];
                if( $data['item_discount'] != ''){
                    $receivingitem['discount_percent'] = $data['item_discount'];
                }
                $receivingitem['item_location'] = 1;
                $receivingitem->save();

                $trans_data = new Transaction();
                $trans_data['trans_items'] = $data['item_id'];
                $trans_data['trans_user'] = 1;
                $trans_data['trans_location'] = 1;
                $trans_data['trans_inventory'] = $data['item_quantity'];
                $trans_data->save();

                $quantity= ItemQuantity::where("item_id",$data['item_id'])->first();
                $itemquantity= $quantity['quantity']+$data['item_quantity'];
                $receivingquantity = ItemQuantity::where("item_id",$data['item_id'])->update(['quantity' => $itemquantity]);
                $x++;
 
            }
              
           
        } catch(Exception $e) {
            DB::commit();
            throw $e;
        }
        DB::rollback();
        return $receiving;
    }

    public function return(array $datas)
    {
         try {
            Log::info($datas);
            if($datas['supplier'] != ''){
                $datas['supplier_id'] = $datas['supplier'];
            }
            
            $datas['employee_id'] = 1;
            $datas['reference'] = 1;
            $receiving = new Receiving();
            $receiving->fill($datas);
            $receiving->save();
            $x =1;
            foreach($datas['item'] as $data){
                Log::info($data);
                $item = Item::where("item_id",$data['item_id'])->first(); 
                $receivingitem = new ReceivingItem();
                $receivingitem['receiving_id'] = $receiving->receiving_id;
                $receivingitem['item_id'] = $data['item_id'];
                $receivingitem['line'] = $x;
                $receivingitem['quantity_purchased'] = -$data['item_quantity'];
                $receivingitem['item_cost_price'] = $item['cost_price'];
                $receivingitem['item_unit_price'] = $item['unit_price'];
                $receivingitem['receiving_quantity'] = $item['receiving_quantity'];
                if( $data['item_discount'] != ''){
                    $receivingitem['discount_percent'] = $data['item_discount'];
                }
                $receivingitem['item_location'] = 1;
                $receivingitem->save();

                $trans_data = new Transaction();
                $trans_data['trans_items'] = $data['item_id'];
                $trans_data['trans_user'] = 1;
                $trans_data['trans_location'] = 1;
                $trans_data['trans_inventory'] = -$data['item_quantity'];
                $trans_data->save();

                $quantity= ItemQuantity::where("item_id",$data['item_id'])->first();
                $itemquantity= $quantity['quantity']-$data['item_quantity'];
                $receivingquantity = ItemQuantity::where("item_id",$data['item_id'])->update(['quantity' => $itemquantity]);
                $x++;
 
            }
              
           
        } catch(Exception $e) {

            throw $e;
        }
       // return $sales;
    }
} ?>

