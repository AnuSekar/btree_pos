<?php
namespace App\Repositories;
use App\Models\Employee;
use App\Models\Person;
use Illuminate\Support\Facades\DB;
use App\Util\BLAlphaNumericCodeGenerator;
use Log;
use App\Response\GlobalResponse;

class EmployeeRepository
{
     private static  $RECORDS_PER_PAGE =2;

     public function __construct() {
        self::$RECORDS_PER_PAGE = config('blconstants.RECORDS_PER_PAGE');
    }
    public function save(array $data)
    {
         DB::beginTransaction();
         try {
            $person = new Person;
            $person->fill($data);
            $person->save();
            $employee = new Employee;
            $data['person_id'] = $person->person_id;
            $data['password'] = bcrypt($data['password']);
            Log::info($data['password']);
            $employee->fill($data);
            $employee->save();
            DB::commit();
        } catch(Exception $e) {
            DB::rollback();
            throw GlobalResponse::clientErrorResponse("error");
        }
        
        return GlobalResponse::createResponse($employee);
    }

    public function update(array $data){
        DB::beginTransaction();
        try{
             $employee = Employee::where("person_id",$data['person_id'])->first(); 
            
            if (is_null($employee)){
                return "failed";
            }
            $person = Person::where("person_id",$data['person_id'])->first();
            $person->fill($data);
            $person->save();
            
            $employee->fill($data);
            $employee->save();
            DB::commit();

        }catch(Exception $e) {
            DB::rollback();
            throw GlobalResponse::clientErrorResponse("error");
        }
        
        return GlobalResponse::createResponse($employee);
    }


    public function deleteAll(){
        DB::beginTransaction();
        try{
            $msg = Employee::where('deleted', '=', 0)->update(['deleted' => 1]);
            DB::commit(); 
        }catch(Exception $e) {
            DB::rollback();
            throw GlobalResponse::clientErrorResponse("error");
        }
        
        return GlobalResponse::createResponse($msg);
    }
    

    public function getAll(){
        try{
            
            $employee = DB::table('btree_people as ppl')
            ->leftjoin('btree_employees as emp','ppl.person_id','=','emp.person_id')
            ->where('emp.deleted','=','0')
            ->select('ppl.*','emp.*')
            ->Paginate(self::$RECORDS_PER_PAGE);
            if (is_null($employee))
            {
                return "failed";
            }

        }catch(Exception $e){
            throw GlobalResponse::clientErrorResponse("error");;
        }

        return GlobalResponse::createResponse($employee);

    }

    public function storeAll(){
        try{
            $msg = Employee::where('deleted', '=', 1)->update(['deleted' => 0]);
             
        }catch(Exception $e){
            throw GlobalResponse::clientErrorResponse("error");
        }

        return GlobalResponse::createResponse($msg);
    }

    public function getAlltrash(){
        try{
            
            $employee = DB::table('btree_people as ppl')
            ->leftjoin('btree_employees as cus','ppl.person_id','=','cus.person_id')
            ->where('cus.deleted','=','1')
            ->select('ppl.*','cus.*')
            ->Paginate(self::$RECORDS_PER_PAGE);
            Log::info($employee );
            if (is_null($employee))
            {
                return "failed";
            }

        }catch(Exception $e){
            throw GlobalResponse::clientErrorResponse("error");
        }

        return GlobalResponse::createResponse($employee);

    }

    public function getById(array $data){
        try{
            $employee = DB::table('btree_people as ppl')
            ->where('ppl.person_id','=',$data['person_id'])
            ->join('btree_employees as emp','ppl.person_id','=','emp.person_id')
            ->select('ppl.*','emp.*')
            ->get();
            if (is_null($employee)){
                return "failed";
            }

        }catch(Exception $e){
            throw GlobalResponse::clientErrorResponse("error");
        }

        return GlobalResponse::createResponse($employee);
    }

    public function deleteemployee($data){
        DB::beginTransaction();
      try{

           $employee = Employee::where('person_id','=',$data)->first();
           if($employee->deleted == 1){
            $employee->deleted = 0;
            $employee->save();
           }
           else{
            $employee->deleted = 1;
            $employee->save();
           }
           DB::commit();
       }catch(Exception $e) {
            DB::rollback();
            throw GlobalResponse::clientErrorResponse("error");
        }
        
        return GlobalResponse::createResponse($employee);
    }
   public function search($data){
        try{
             Log::info('Some message here.');
             Log::info($data);
           $sql = "   ( ppl.first_name like '%".$data."%' or ppl.last_name like '%".$data."%' or ppl.phone_number like '%".$data."%' ) and emp.deleted = 0 and ppl.person_id = emp.person_id";

             $employee = DB::table('btree_people as ppl')
            ->leftjoin('btree_employees as emp','ppl.person_id','=','emp.person_id')
            ->whereRaw($sql)
            ->Paginate(self::$RECORDS_PER_PAGE);
            Log::info($employee);
            if (is_null($employee))
            {
                return "failed";
            }

        }catch(Exception $e){
            throw GlobalResponse::clientErrorResponse("error");
        }

        return GlobalResponse::createResponse($employee);

    }
    public function employeebysales($data){
        try{
            Log::info($data);
            $employee = DB::select("SELECT ppl.person_id,ppl.first_name,ppl.first_name as label,ppl.last_name from btree_employees as emp, btree_people as ppl where  ppl.person_id = emp.person_id  and emp.deleted = 0 and (ppl.first_name like '%".$data ."%' or ppl.last_name like '%".$data ."%' )");
            if (is_null($employee))
            {
                return "failed";
            }

        }catch(Exception $e){
            throw GlobalResponse::clientErrorResponse("error");
        }

        return GlobalResponse::createResponse($employee);

    }

    public function searchTrash($data){
        try{
             Log::info('Some message here.');
             Log::info($data);
           $sql = "   ( ppl.first_name like '%".$data."%' or ppl.last_name like '%".$data."%' or ppl.phone_number like '%".$data."%' ) and emp.deleted = 1 and ppl.person_id = emp.person_id";

             $employee = DB::table('btree_people as ppl')
            ->leftjoin('btree_employees as emp','ppl.person_id','=','emp.person_id')
            ->whereRaw($sql)
            ->Paginate(self::$RECORDS_PER_PAGE);
            Log::info($employee);
            if (is_null($employee))
            {
                return "failed";
            }

        }catch(Exception $e){
            throw GlobalResponse::clientErrorResponse("error");
        }

        return GlobalResponse::createResponse($employee);

    }

    public function deleteSelectEmployee(array $data){
        DB::beginTransaction();
      try{

        foreach ($data as $value) {
            $employee = Employee::where('person_id','=',$value)->first();
            $employee->deleted = 1;
            $employee->save();
        }
        DB::commit();
       }catch(Exception $e){
        DB::rollback();
           return GlobalResponse::clientErrorResponse("error");
       }
       
        return GlobalResponse::createResponse("success");
   }

   public function addSelectEmployee(array $data){
    DB::beginTransaction();
      try{

        foreach ($data as $value) {
            $employee = Employee::where('person_id','=',$value)->first();
            $employee->deleted = 0;
            $employee->save();
        }
        DB::commit();
       }catch(Exception $e){
        DB::rollback();
           return GlobalResponse::clientErrorResponse("error");
       }
       
        return GlobalResponse::createResponse("success");
   }


} ?>

