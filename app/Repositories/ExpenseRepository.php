<?php
namespace App\Repositories;
use App\Models\Expense;
use Illuminate\Support\Facades\DB;
use App\Util\BLAlphaNumericCodeGenerator;
use Log;
use App\Response\GlobalResponse;

class ExpenseRepository
{
     private static  $RECORDS_PER_PAGE =2;

     public function __construct() {
        self::$RECORDS_PER_PAGE = config('blconstants.RECORDS_PER_PAGE');
    }
    public function save(array $data)
    {    DB::beginTransaction();
         try {
            $expense = new  Expense;
            $expense->fill($data);
            $expense->save();
           DB::commit();
        } catch(Exception $e) {
            DB::rollback();
            return GlobalResponse::clientErrorResponse("error");
        }
        
         return GlobalResponse::createResponse($expense);
    }

    public function update(array $data){
      DB::beginTransaction();
        try{Log::info($data);
            $expense = Expense::where("expense_id",$data['expense_id'])->first(); 
                       
            $expense->fill($data);
            $expense->save();
            DB::commit();
        }catch(Exception $e) {
            DB::rollback();
            return GlobalResponse::clientErrorResponse("error");
        }
        
         return GlobalResponse::createResponse($expense);
    }

    public function deleteAll(){
        try{
            $msg = Expense::where('deleted', '=', 0)->update(['deleted' => 1]);
             
        }catch(Exception $e){
            throw $e;
        }

        return $msg;
    }

    public function storeAll(){
        try{
            $msg = Expense::where('deleted', '=', 1)->update(['deleted' => 0]);
             
        }catch(Exception $e){
            throw $e;
        }

        return $msg;
    }

    public function getAll(){
        try{
            
            $expense = DB::table('btree_expenses as ex')
            ->leftjoin('btree_expense_category as exc','ex.expense_category_id','=','exc.expense_category_id')
            ->where('ex.deleted','=','0')
            ->select('ex.*','exc.expense_category_name')
            ->Paginate(self::$RECORDS_PER_PAGE);
            if (is_null($expense))
            {
                return "failed";
            }

        }catch(Exception $e){
            throw $e;
        }

        return $expense;

    }

    public function getAlltrash(){
        try{
            
            $expense = DB::table('btree_expenses as ex')
            ->leftjoin('btree_expense_category as exc','ex.expense_category_id','=','exc.expense_category_id')
            ->where('ex.deleted','=','1')
            ->select('ex.*','exc.expense_category_name')
            ->Paginate(self::$RECORDS_PER_PAGE);
            if (is_null($expense))
            {
                return "failed";
            }

        }catch(Exception $e){
            throw $e;
        }

        return $expense;

    }


    public function getById($data){
        try{
            $expense = DB::table('btree_expenses as com')
            ->where('com.expense_id','=',$data)
            ->select('com.*')
            ->get();
           

        }catch(Exception $e){
            return GlobalResponse::clientErrorResponse("error");
        }
        Log::info($expense);
        return GlobalResponse::createResponse($expense);
    }

    public function deleteexpense($data){
      DB::beginTransaction();
      try{
           $expense = Company::where('expense_id','=',$data)->first();
           if($expense->deleted==1){
            $expense->deleted = 0;
            $expense->save();
           }
           else{
            $expense->deleted = 1;
            $expense->save();
           }
           DB::commit();
       }catch(Exception $e) {
            DB::rollback();
            return GlobalResponse::clientErrorResponse("error");
        }
        
         return GlobalResponse::createResponse($expense);
    }

   public function search($data){
        try{
             Log::info('Some message here.');

            $sql = " expense_category_name like '%".$data."%' and deleted = 0 ";

             $expense = DB::table('btree_expenses as it')
            ->whereRaw($sql)
            ->simplePaginate(self::$RECORDS_PER_PAGE);
            if (is_null($expense))
            {
                return "failed";
            }

        }catch(Exception $e){
            throw $e;
        }

        return $expense;

    }

    public function deleteSelectExpense(array $data){
      DB::beginTransaction();
      try{

        foreach ($data as $value) {
            $expense = Expense::where('expense_id','=',$value)->first();
            $expense->deleted = 1;
            $expense->save();
        }
        DB::commit();
       }catch(Exception $e){
           DB::rollback();
           return GlobalResponse::clientErrorResponse("error");
       }
        
        return GlobalResponse::createResponse("success");
   }

   public function addSelectExpense(array $data){
      DB::beginTransaction();
      try{

        foreach ($data as $value) {
            $expense = Expense::where('expense_id','=',$value)->first();
            $expense->deleted = 0;
            $expense->save();
        }
        DB::commit();
       }catch(Exception $e){
           DB::rollback();
           return GlobalResponse::clientErrorResponse("error");
       }
        
        return GlobalResponse::createResponse("success");
   }




} ?>
