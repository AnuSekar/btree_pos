<?php
namespace App\Repositories;
use App\Models\ItemKit;
use App\Models\ItemKitItem;
use Illuminate\Support\Facades\DB;
use App\Util\BLAlphaNumericCodeGenerator;
use App\Response\GlobalResponse;
use Log;

class ItemKitRepository
{
     private static  $RECORDS_PER_PAGE =2;

     public function __construct() {
        self::$RECORDS_PER_PAGE = config('blconstants.RECORDS_PER_PAGE');
    }
    public function save(array $data)
    {
         DB::beginTransaction();
         try {
            $itemkit = new ItemKit();
            $itemkit['name']=$data['item_kit_name'];
            $itemkit['discount'] = $data['item_kit_discount'];
            $itemkit->save();
            $item_kit_id = $itemkit->item_kit_id;
            $x = 0;
            foreach($data['itemarray'] as $datas){
            $itemkititem = new ItemKitItem();
            $itemkititem['item_kit_id'] =$item_kit_id;
            $itemkititem['item_id'] = $datas['item_id'];
            $itemkititem['kit_sequence'] = $x;
            $itemkititem['quantity'] = $datas['item_quantity'];
            $itemkititem->save();
            $x++;
            }
            
           
            DB::commit(); 
           
        } catch(Exception $e) {
             DB::rollback();
            throw $e;
        }
         
        return $itemkit;
    }

    public function update(array $data){
        DB::beginTransaction();
        try{
            Log::info($data);
            $itemkit = ItemKit::where("item_kit_id",$data['item_kit_id'])->first();
            $itemkit['name']=$data['item_kit_name'];
            $itemkit['discount'] = $data['item_kit_discount'];
            $itemkit->save();
            $item_kit_id = $itemkit->item_kit_id;
            $x = 0;
            $itemkititem = ItemKitItem::where("item_kit_id",$data['item_kit_id'])->delete();
            foreach($data['itemarray'] as $datas){
            $itemkititem = new ItemKitItem();
            $itemkititem['item_kit_id'] =$item_kit_id;
            $itemkititem['item_id'] = $datas['item_id'];
            $itemkititem['kit_sequence'] = $x;
            $itemkititem['quantity'] = $datas['item_quantity'];
            $itemkititem->save();
            $x++;
            }
            if (is_null($itemkit)){
                return "failed";
            }
            DB::commit();
        }catch(Exception $e){
             DB::rollback();
            throw $e;
        }
        
        return $itemkit;
    }


    public function deleteAll(){
        try{
            $msg = Item::where('deleted', '=', 0)->update(['deleted' => 1]);
             
        }catch(Exception $e){
            throw $e;
        }

        return $msg;
    }

    public function storeAll(){
        try{
            $msg = Item::where('deleted', '=', 1)->update(['deleted' => 0]);
             
        }catch(Exception $e){
            throw $e;
        }

        return $msg;
    }

    public function getAll(){
        try{
            
            $item = DB::table('btree_item_kits as it')
            ->select('it.name as item_kit_name','it.discount as item_kit_discount',
                'it.item_kit_id')
            ->Paginate(self::$RECORDS_PER_PAGE);
            if (is_null($item))
            {
                return "failed";
            }

        }catch(Exception $e){
            throw $e;
        }

        return $item;

    }

    public function getAlltrash(){
        try{
            
            $item = DB::table('btree_item as it')
            ->join('btree_item_taxes as iit','it.item_id','=','iit.item_id')
            ->join('btree_company as com','com.company_id','=','it.company_id')
            ->join('btree_product as pro','pro.product_id','=','it.product_id')
            ->leftjoin('btree_people as ppl','it.supplier_id','=','ppl.person_id')
            ->select('it.*','com.company_name','pro.product_name')
            ->selectRaw("GROUP_CONCAT(iit.tax_name) as taxname,GROUP_CONCAT(iit.percent) as percent,CONCAT(ppl.first_name , ppl.last_name) as supplier_name")
            ->where('it.deleted','=','1')
            ->groupBy('it.item_id')
            ->Paginate(self::$RECORDS_PER_PAGE);
            if (is_null($item))
            {
                return "failed";
            }

        }catch(Exception $e){
            throw $e;
        }

        return $item;

    }

    public function getById(array $data){
        try{
            Log::info($data['id']);
            $item = DB::select("select GROUP_CONCAT(iti.item_id) as item_id,it.name as item_kit_name , it.discount as item_kit_discount,GROUP_CONCAT(i.item_name) as item_name,GROUP_CONCAT(iti.quantity) as quantity from btree_item_kits as it,btree_item_kit_items as iti, btree_item as i where it.item_kit_id =".$data['id']." and it.item_kit_id = iti.item_kit_id and i.item_id = iti.item_id");
            Log::info($item);

            if (is_null($item)){
                return "failed";
            }

        }catch(Exception $e){
            throw $e;
        }

        return $item;
    }

    public function deleteitem($data){
        DB::beginTransaction();
      try{
           $item = Item::where('item_id','=',$data)->first();
           if($item->deleted==1){
            $item->deleted = 0;
            $item->save();
           }
           else{
            $item->deleted = 1;
            $item->save();
           }
           DB::commit();
       }catch(Exception $e){
         DB::rollback();
           throw $e;
       }
       
       return $item;
   }



   public function search($data){
        try{
             Log::info('Some message here.');

            $sql = " item_name like '%".$data."%' and deleted = 0 ";

             $item = DB::table('btree_item as it')
            ->whereRaw($sql)
            ->simplePaginate(self::$RECORDS_PER_PAGE);
            // $item = item::whereRaw($sql)->get();
          //  $item = DB::Select($sql);
            Log::info($item);
            if (is_null($item))
            {
                return "failed";
            }

        }catch(Exception $e){
            throw $e;
        }

        return $item;

    }

    public function deleteSelectItem(array $data){
        DB::beginTransaction();
      try{

        foreach ($data as $value) {
            $item = Item::where('item_id','=',$value)->first();
            $item->deleted = 1;
            $item->save();
        }
        DB::commit();
       }catch(Exception $e){
         DB::rollback();
           return GlobalResponse::clientErrorResponse("error");
       }
       
        return GlobalResponse::createResponse("success");
   }

   public function addSelectItem(array $data){
    DB::beginTransaction();
      try{

        foreach ($data as $value) {
            $item = Item::where('item_id','=',$value)->first();
            $item->deleted = 0;
            $item->save();
        }
        DB::commit();
       }catch(Exception $e){
         DB::rollback();
           return GlobalResponse::clientErrorResponse("error");
       }
       
        return GlobalResponse::createResponse("success");
   }



} ?>

