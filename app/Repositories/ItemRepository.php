<?php
namespace App\Repositories;
use App\Models\Item;
use App\Models\Item_Tax;
use App\Models\ItemQuantity;
use Illuminate\Support\Facades\DB;
use App\Util\BLAlphaNumericCodeGenerator;
use App\Response\GlobalResponse;
use Log;

class ItemRepository
{
     private static  $RECORDS_PER_PAGE =2;

     public function __construct() {
        self::$RECORDS_PER_PAGE = config('blconstants.RECORDS_PER_PAGE');
    }
    public function save(array $data)
    {
         DB::beginTransaction();
         try {
            $item = new Item;
            $item->fill($data);
            $item->save();
            $item_id = $item->item_id;
            
             if(array_key_exists('tax1',$data)){
            $tax1 = $data['tax1'];
            $tax1['item_id'] = $item->item_id;
            $item_taxes1 = new  Item_Tax;
            $item_taxes1->fill($tax1);
            $item_taxes1->save();
            }
            if(array_key_exists('tax2',$data)){
            $tax2 = $data['tax2'];
            $tax2['item_id'] = $item->item_id;
            $item_taxes2 = new  Item_Tax;
            $item_taxes2->fill($tax2);
            $item_taxes2->save();
            }
            if(array_key_exists('tax3',$data)){
            $tax3 = $data['tax3'];
            $tax3['item_id'] = $item->item_id;
            $item_taxes3 = new  Item_Tax;
            $item_taxes3->fill($tax3);
            $item_taxes3->save();
            }
            $itemquantity = new ItemQuantity();
            $itemquantity['item_id']=$item->item_id;
            $itemquantity['location_id']='1';
            $itemquantity['quantity']='0';
            $itemquantity->save();
            DB::commit(); 
           
        } catch(Exception $e) {
             DB::rollback();
            throw $e;
        }
         
        return $item;
    }

    public function update(array $data){
        DB::beginTransaction();
        try{
            Log::info($data);
            $item =Item::where("item_id",$data['item_id'])->first();
            $item->fill($data);
            $item->save();
            $item_taxes = Item_Tax::where("item_id",$data['item_id'])->delete();
            if(array_key_exists('tax1',$data)){
            $tax1 = $data['tax1'];
            $tax1['item_id'] = $item->item_id;
            $item_taxes1 = new  Item_Tax;
            $item_taxes1->fill($tax1);
            $item_taxes1->save();
            }
            if(array_key_exists('tax2',$data)){
            $tax2 = $data['tax2'];
            $tax2['item_id'] = $item->item_id;
            $item_taxes2 = new  Item_Tax;
            $item_taxes2->fill($tax2);
            $item_taxes2->save();
            }
            if(array_key_exists('tax3',$data)){
            $tax3 = $data['tax3'];
            $tax3['item_id'] = $item->item_id;
            $item_taxes3 = new  Item_Tax;
            $item_taxes3->fill($tax3);
            $item_taxes3->save();
            }

            if (is_null($item)){
                return "failed";
            }
            DB::commit();
        }catch(Exception $e){
             DB::rollback();
            throw $e;
        }
        
        return $item;
    }


    public function deleteAll(){
        try{
            $msg = Item::where('deleted', '=', 0)->update(['deleted' => 1]);
             
        }catch(Exception $e){
            throw $e;
        }

        return $msg;
    }

    public function storeAll(){
        try{
            $msg = Item::where('deleted', '=', 1)->update(['deleted' => 0]);
             
        }catch(Exception $e){
            throw $e;
        }

        return $msg;
    }

    public function getAll(){
        try{
            
            $item = DB::table('btree_item as it')
            ->join('btree_item_taxes as iit','it.item_id','=','iit.item_id')
            ->join('btree_company as com','com.company_id','=','it.company_id')
            ->join('btree_product as pro','pro.product_id','=','it.product_id')
            ->leftjoin('btree_people as ppl','it.supplier_id','=','ppl.person_id')
            ->select('it.*','com.company_name','pro.product_name')
            ->selectRaw("GROUP_CONCAT(iit.tax_name) as taxname,GROUP_CONCAT(iit.percent) as percent,CONCAT(ppl.first_name , ppl.last_name) as supplier_name")
            ->where('it.deleted','=','0')
            ->groupBy('it.item_id')
            ->Paginate(self::$RECORDS_PER_PAGE);
            if (is_null($item))
            {
                return "failed";
            }

        }catch(Exception $e){
            throw $e;
        }

        return $item;

    }

    public function getAlltrash(){
        try{
            
            $item = DB::table('btree_item as it')
            ->join('btree_item_taxes as iit','it.item_id','=','iit.item_id')
            ->join('btree_company as com','com.company_id','=','it.company_id')
            ->join('btree_product as pro','pro.product_id','=','it.product_id')
            ->leftjoin('btree_people as ppl','it.supplier_id','=','ppl.person_id')
            ->select('it.*','com.company_name','pro.product_name')
            ->selectRaw("GROUP_CONCAT(iit.tax_name) as taxname,GROUP_CONCAT(iit.percent) as percent,CONCAT(ppl.first_name , ppl.last_name) as supplier_name")
            ->where('it.deleted','=','1')
            ->groupBy('it.item_id')
            ->Paginate(self::$RECORDS_PER_PAGE);
            if (is_null($item))
            {
                return "failed";
            }

        }catch(Exception $e){
            throw $e;
        }

        return $item;

    }

    public function getById(array $data){
        try{
            // $item = DB::table('btree_item as it')
            // ->where('it.item_id','=',$data['item_id'])
            // ->join('btree_item_taxes as iit','it.item_id','=','iit.item_id')
            // ->select(DB::raw('group_concat(iit.tax_name) taxname'))
            // ->groupBy('it.item_id')
            // ->get();

            $item = DB::select("SELECT Distinct it.*, group_concat(iit.tax_name) as taxname , group_concat(iit.percent) as percent FROM btree_item as it,btree_item_taxes as iit WHERE it.deleted = 0 and it.item_id = iit.item_id and it.item_id= " .$data['item_id']. " group by it.item_id");


            if (is_null($item)){
                return "failed";
            }

        }catch(Exception $e){
            throw $e;
        }

        return $item;
    }

    public function deleteitem($data){
        DB::beginTransaction();
      try{
           $item = Item::where('item_id','=',$data)->first();
           if($item->deleted==1){
            $item->deleted = 0;
            $item->save();
           }
           else{
            $item->deleted = 1;
            $item->save();
           }
           DB::commit();
       }catch(Exception $e){
         DB::rollback();
           throw $e;
       }
       
       return $item;
   }



   public function search($data){
        try{
             Log::info('Some message here.');

            $sql = " item_name like '%".$data."%' and deleted = 0 ";

             $item = DB::table('btree_item as it')
            ->whereRaw($sql)
            ->simplePaginate(self::$RECORDS_PER_PAGE);
            // $item = item::whereRaw($sql)->get();
          //  $item = DB::Select($sql);
            Log::info($item);
            if (is_null($item))
            {
                return "failed";
            }

        }catch(Exception $e){
            throw $e;
        }

        return $item;

    }

    public function deleteSelectItem(array $data){
        DB::beginTransaction();
      try{

        foreach ($data as $value) {
            $item = Item::where('item_id','=',$value)->first();
            $item->deleted = 1;
            $item->save();
        }
        DB::commit();
       }catch(Exception $e){
         DB::rollback();
           return GlobalResponse::clientErrorResponse("error");
       }
       
        return GlobalResponse::createResponse("success");
   }

   public function addSelectItem(array $data){
    DB::beginTransaction();
      try{

        foreach ($data as $value) {
            $item = Item::where('item_id','=',$value)->first();
            $item->deleted = 0;
            $item->save();
        }
        DB::commit();
       }catch(Exception $e){
         DB::rollback();
           return GlobalResponse::clientErrorResponse("error");
       }
       
        return GlobalResponse::createResponse("success");
   }



} ?>

