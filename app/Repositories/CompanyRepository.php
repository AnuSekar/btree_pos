<?php
namespace App\Repositories;
use App\Models\Company;
use Illuminate\Support\Facades\DB;
use App\Util\BLAlphaNumericCodeGenerator;
use Log;
use App\Response\GlobalResponse;

class CompanyRepository
{
     private static  $RECORDS_PER_PAGE =2;

     public function __construct() {
        self::$RECORDS_PER_PAGE = config('blconstants.RECORDS_PER_PAGE');
    }
    public function save(array $data)
    {
        DB::beginTransaction();
        try {
            $company = new  Company;
            $company->fill($data);
            $company->save();
            DB::commit();
           
        } catch(Exception $e) {
            DB::rollback();
            return GlobalResponse::clientErrorResponse("error");
        }
        
        return GlobalResponse::createResponse($company);
    }

    public function update(array $data){
        DB::beginTransaction();
        try{
            $company = Company::where("company_id",$data['company_id'])->first(); 
                       
            $company->fill($data);
            $company->save();
            DB::commit();

        }catch(Exception $e){
            DB::rollback();
            return GlobalResponse::clientErrorResponse("error");
        }
         
         return GlobalResponse::createResponse($company);
    }


    public function deleteAll(){
        DB::beginTransaction();
        try{
            $company = Company::where('deleted', '=', 0)->update(['deleted' => 1]);
            DB::commit();
        }catch(Exception $e){
            DB::rollback();
            return GlobalResponse::clientErrorResponse("error");
        }
         
         return GlobalResponse::createResponse($company);
    }


    public function storeAll(){
        try{
            $company = Company::where('deleted', '=', 1)->update(['deleted' => 0]);
             
        }catch(Exception $e){
            return GlobalResponse::clientErrorResponse("error");
        }

         return GlobalResponse::createResponse($company);
    }


    public function getAll(){
        try{
            
            $company = DB::table('btree_company as com')
            ->where('com.deleted','=','0')
            ->select('com.*')
            ->Paginate(self::$RECORDS_PER_PAGE);

        }catch(Exception $e){
            return GlobalResponse::clientErrorResponse("error");
        }

         return GlobalResponse::createResponse($company);

    }

    public function getAlltrash(){
        try{
            
            $company = DB::table('btree_company as com')
            ->where('com.deleted','=','1')
            ->select('com.*')
            ->Paginate(self::$RECORDS_PER_PAGE);

        }catch(Exception $e){
            return GlobalResponse::clientErrorResponse("error");
        }

         return GlobalResponse::createResponse($company);

    }

    public function getById($data){
        try{
            $company = DB::table('btree_company as com')
            ->where('com.company_id','=',$data)
            ->select('com.*')
            ->get();
           

        }catch(Exception $e){
            return GlobalResponse::clientErrorResponse("error");
        }
        Log::info($company);
        return GlobalResponse::createResponse($company);
    }

    public function deletecompany($data){
         DB::beginTransaction();
      try{
           $company = Company::where('company_id','=',$data)->first();
           if($company->deleted==1){
            $company->deleted = 0;
            $company->save();
           }
           else{
            $company->deleted = 1;
            $company->save();
           }
           DB::commit();
       }catch(Exception $e){
            DB::rollback();
           return GlobalResponse::clientErrorResponse("error");
       }
       
        return GlobalResponse::createResponse($company);
   }

   public function search($data){
        try{
             Log::info('Some message here.');

            $sql = " company_name like '%".$data."%' and deleted = 0 ";

             $company = DB::table('btree_company as com')
            ->whereRaw($sql)
            ->Paginate(self::$RECORDS_PER_PAGE);
            Log::info($company);
        }catch(Exception $e){
           return GlobalResponse::clientErrorResponse("error");
        }

         return GlobalResponse::createResponse($company);

    }

     public function getList(){
        try{
            
            $company = DB::table('btree_company as com')
            ->where('com.deleted','=','0')
            ->select('com.*')
            ->get();
            
        }catch(Exception $e){
            return GlobalResponse::clientErrorResponse("error");
        }

         return GlobalResponse::createResponse($company);
  }  
   public function deleteSelectCompany(array $data){
    DB::beginTransaction();
      try{

        foreach ($data as $value) {
            $company = Company::where('company_id','=',$value)->first();
            $company->deleted = 1;
            $company->save();
        }
        DB::commit();
       }catch(Exception $e){
        DB::rollback();
           return GlobalResponse::clientErrorResponse("error");
       }
       
        return GlobalResponse::createResponse("success");
   }

   public function addSelectCompany(array $data){
    DB::beginTransaction();
      try{

        foreach ($data as $value) {
            $company = Company::where('company_id','=',$value)->first();
            $company->deleted = 0;
            $company->save();
        }
        DB::commit();
    }catch(Exception $e){
        DB::rollback();
           return GlobalResponse::clientErrorResponse("error");
       }
       
        return GlobalResponse::createResponse("success");
   }
       

} ?>

