<?php
namespace App\Repositories;
use Illuminate\Support\Facades\DB;
use App\Util\BLAlphaNumericCodeGenerator;
use Log;
use Carbon\Carbon;
use App\Response\GlobalResponse;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;

class ReportRepository
{
     private static  $RECORDS_PER_PAGE =2;

     public function __construct() {
        self::$RECORDS_PER_PAGE = config('blconstants.RECORDS_PER_PAGE');
    }

    public function getReport($tmp)
    {
      $date = new Carbon;
      Log::info(date("Y-m-d"));
          $transtype = '';
          if($tmp['transtype'] == 1){
            $transtype = '';
          }else if($tmp['transtype'] == 2){
            $transtype = " and sa.sales_type=1 ";
          }else if($tmp['transtype'] == 3){
            $transtype = " and sa.sales_type=2 ";
          }
          if($tmp['type'] == 1){
            $datesymbol = '>=';
            $datetime = Carbon::today();
          }else if ($tmp['type'] == 2){
            $datesymbol = '=';
            $datetime = Carbon::yesterday();
          }else if ($tmp['type'] == 3){
            $datesymbol = '>';
            $datetime = $date->subDays(7);
          }else if ($tmp['type'] == 4){
            $date = new Carbon;
            $datesymbol = '>';
            $datetime = $date->subDays(30);
          }else if ($tmp['type'] == 5){
            $datesymbol = '>';
            $datetime = $date->subDays(90);
          }else if ($tmp['type'] == 6){
            $datesymbol = '>';
            $datetime = $date->subDays(180);
          }else if ($tmp['type'] == 7){
            $datesymbol = '>';
            $datetime = $date->subYear();
          }else if ($tmp['type'] == 8){
            $start_date = $date->startOfWeek();
            $end_date = $date->copy()->endOfWeek();
          }else if ($tmp['type'] == 9){
            $start_date = $date->startOfMonth();
            $end_date = $date->copy()->endOfMonth();
          }else if ($tmp['type'] == 10){
            $start_date = $date->startOfYear();
            $end_date = $date->copy()->endOfYear();
          }else if ($tmp['type'] == 11){
            $start_date = $tmp['start_date'];
            $end_date = $tmp['end_date'];
          }
        
          if($tmp['type'] == 11 || $tmp['type'] == 10 || $tmp['type'] == 9 || $tmp['type'] ==8)
          {
            return array($start_date,$end_date,$transtype);
          }
          else
          {
            return array($datesymbol,$datetime,$transtype);
          }

    }

    public function getCompany(array $data)
    {
      Log::info($data['reporttype']);
      try{
          if($data['type'] == 11 || $data['type'] == 10 || $data['type'] == 9 || $data['type'] ==8){
              list($start_date,$end_date,$transtype)=$this->getReport($data);
              $company = DB::select("select * from (select com.company_id, com.company_name,it.item_id,sa.sale_id, sum(sit.quantity_purchased) as quantity_purchased, sum(sit.item_cost_price * sit.quantity_purchased *(1-sit.discount_percent/100)) as sales_amount from btree_company as com inner join btree_item as it on com.company_id = it.company_id inner join btree_sales_items as sit on it.item_id = sit.item_id inner join btree_sales as sa on sit.sales_id = sa.sale_id where (sa.sale_time between '".$start_date."' and '".$end_date."') and com.deleted = 0 ".$transtype." group by com.company_id) as d join (select com.company_id,sitt.sale_id,sitt.item_id,sum(sitt.item_tax_amount) as tax_amount from btree_sales_items_taxes as sitt, btree_sales as sa,btree_item as it,btree_company as com where sitt.sale_id = sa.sale_id  and com.company_id = it.company_id and it.item_id = sitt.item_id and com.deleted = 0 and (sa.sale_time between '".$start_date."' and '".$end_date."' ) ".$transtype." group by com.company_id) as s on d.company_id = s.company_id");

          }else{
              list($datesymbol,$datetime,$transtype)=$this->getReport($data);
              $company = DB::select("select * from (select com.company_id, com.company_name,it.item_id,sa.sale_id, sum(sit.quantity_purchased) as quantity_purchased, sum(sit.item_cost_price * sit.quantity_purchased *(1-sit.discount_percent/100)) as sales_amount from btree_company as com inner join btree_item as it on com.company_id = it.company_id inner join btree_sales_items as sit on it.item_id = sit.item_id inner join btree_sales as sa on sit.sales_id = sa.sale_id where Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' and com.deleted = 0 ".$transtype." group by com.company_id) as d join (select com.company_id,sitt.sale_id,sitt.item_id,sum(sitt.item_tax_amount) as tax_amount from btree_sales_items_taxes as sitt, btree_sales as sa,btree_item as it,btree_company as com where sitt.sale_id = sa.sale_id and com.company_id = it.company_id and it.item_id = sitt.item_id and com.deleted = 0  and  Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by com.company_id) as s on d.company_id = s.company_id");

        }

        } catch(Exception $e) {

            return GlobalResponse::clientErrorResponse("error");
        }
        if($data['reporttype'] == 'normal'){
          Log::info($company);
            $company = $this->arrayPaginator($company, $data);   
           return GlobalResponse::createResponse($company);
          }
        else{
            return $company;
        }

    }
 
    public function getProduct(array $data)
    {  
      try{
          if($data['type'] == 11 || $data['type'] == 10 || $data['type'] == 9 || $data['type'] ==8){
            list($start_date,$end_date,$transtype)=$this->getReport($data);
            $product = DB::select("select * from (select pro.product_id, pro.product_name,it.item_id,sa.sale_id, sum(sit.quantity_purchased) as quantity_purchased, sum(sit.item_cost_price * sit.quantity_purchased *(1-sit.discount_percent/100)) as sales_amount from btree_product as pro inner join btree_item as it on pro.product_id = it.product_id inner join btree_sales_items as sit on it.item_id = sit.item_id inner join btree_sales as sa on sit.sales_id = sa.sale_id where (sa.sale_time between '".$start_date."' and '".$end_date."') and pro.deleted = 0 ".$transtype." group by pro.product_id) as d join (select pro.product_id,sitt.sale_id,sitt.item_id,sum(sitt.item_tax_amount) as tax_amount from btree_sales_items_taxes as sitt, btree_sales as sa,btree_item as it,btree_product as pro where sitt.sale_id = sa.sale_id and pro.product_id = it.product_id and it.item_id = sitt.item_id and pro.deleted = 0  and (sa.sale_time between '".$start_date."' and '".$end_date."' ) ".$transtype." group by pro.product_id) as s on d.product_id = s.product_id");
          }else{
            list($datesymbol,$datetime,$transtype)=$this->getReport($data);
            $product = DB::select("select * from (select pro.product_id, pro.product_name,it.item_id,sa.sale_id, sum(sit.quantity_purchased) as quantity_purchased, sum(sit.item_cost_price * sit.quantity_purchased *(1-sit.discount_percent/100)) as sales_amount from btree_product as pro inner join btree_item as it on pro.product_id = it.product_id inner join btree_sales_items as sit on it.item_id = sit.item_id inner join btree_sales as sa on sit.sales_id = sa.sale_id where Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' and pro.deleted = 0 ".$transtype." group by pro.product_id) as d join (select pro.product_id,sitt.sale_id,sitt.item_id,sum(sitt.item_tax_amount) as tax_amount from btree_sales_items_taxes as sitt, btree_sales as sa,btree_item as it,btree_product as pro where sitt.sale_id = sa.sale_id and pro.product_id = it.product_id and it.item_id = sitt.item_id and pro.deleted = 0 and Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by pro.product_id) as s on d.product_id = s.product_id");
        }
        } catch(Exception $e) {

            return GlobalResponse::clientErrorResponse("error");
        }
        
        if($data['reporttype'] == 'normal'){
         $product = $this->arrayPaginator($product, $data);
         return GlobalResponse::createResponse($product);
        }
        else{
          return $product;
        }
    }

    public function getCustomer(array $data)
    {
      Log::info($data['type']);
      try{
          if($data['type'] == 11 || $data['type'] == 10 || $data['type'] == 9 || $data['type'] ==8){
            list($start_date,$end_date,$transtype)=$this->getReport($data);
            $customer = DB::select("select * from (select cus.person_id,CONCAT(ppl.first_name,' ',ppl.last_name)as name,sa.sale_id,count(sa.sale_id)as count, sum(sit.item_cost_price * sit.quantity_purchased *(1-sit.discount_percent/100)) as sales_amount,sum(sit.item_unit_price * sit.quantity_purchased) as sales_whole_amount from btree_customers as cus inner join btree_people as ppl on cus.person_id = ppl.person_id inner join btree_sales as sa on cus.person_id = sa.customer_id inner join btree_sales_items as sit on sa.sale_id = sit.sales_id  where cus.deleted = 0  and (sa.sale_time between '".$start_date."' and '".$end_date."') ".$transtype." group by cus.person_id) as d join (select cus.person_id,sitt.sale_id, sitt.item_id,sum(sitt.item_tax_amount) as tax_amount from btree_sales_items_taxes as sitt, btree_sales as sa,btree_customers as cus where sitt.sale_id = sa.sale_id and cus.person_id = sa.customer_id and cus.deleted = 0  and (sa.sale_time between '".$start_date."' and '".$end_date."' ) ".$transtype." group by cus.person_id) as s on d.person_id = s.person_id");
            
          }else{
            list($datesymbol,$datetime,$transtype)=$this->getReport($data);
            $customer = DB::select("select * from (select cus.person_id,CONCAT(ppl.first_name,' ',ppl.last_name)as name,sa.sale_id,count(sa.sale_id)as count, sum(sit.item_cost_price * sit.quantity_purchased *(1-sit.discount_percent/100)) as sales_amount,sum(sit.item_unit_price * sit.quantity_purchased) as sales_whole_amount from btree_customers as cus inner join btree_people as ppl on cus.person_id = ppl.person_id inner join btree_sales as sa on cus.person_id = sa.customer_id inner join btree_sales_items as sit on sa.sale_id = sit.sales_id  where cus.deleted = 0  and Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by cus.person_id) as d join (select cus.person_id,sitt.sale_id, sitt.item_id,sum(sitt.item_tax_amount) as tax_amount from btree_sales_items_taxes as sitt, btree_sales as sa,btree_customers as cus where sitt.sale_id = sa.sale_id and cus.person_id = sa.customer_id and cus.deleted = 0 and Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by cus.person_id) as s on d.person_id = s.person_id");
        }
        } catch(Exception $e) {

            return GlobalResponse::clientErrorResponse("error");
        }

        if($data['reporttype'] == 'normal'){
         $customer = $this->arrayPaginator($customer, $data);
         return GlobalResponse::createResponse($customer);
        }
        else{
          return $customer;
        }
    }
          
    public function getSupplier(array $data)
    {
      try{
          if($data['type'] == 11 || $data['type'] == 10 || $data['type'] == 9 || $data['type'] ==8){
            list($start_date,$end_date,$transtype)=$this->getReport($data);
            $supplier = DB::select("select * from (select sup.person_id,CONCAT(ppl.first_name,' ',ppl.last_name)as name,sa.sale_id,count(sa.sale_id)as count, sum(sit.item_cost_price * sit.quantity_purchased *(1-sit.discount_percent/100)) as sales_amount,sum(sit.item_unit_price * sit.quantity_purchased) as sales_whole_amount from btree_suppliers as sup inner join btree_people as ppl on sup.person_id = ppl.person_id inner join btree_item as it on it.supplier_id = sup.person_id inner join btree_sales_items as sit on it.item_id = sit.item_id inner join btree_sales as sa on sit.sales_id = sa.sale_id where sup.deleted = 0 and (sa.sale_time between '".$start_date."' and '".$end_date."') ".$transtype." group by sup.person_id) as d join (select sup.person_id,sitt.sale_id, sitt.item_id,sum(sitt.item_tax_amount) as tax_amount from btree_sales_items_taxes as sitt, btree_sales as sa,btree_suppliers as sup, btree_item as it where sitt.sale_id = sa.sale_id and sup.person_id = it.supplier_id and sup.deleted = 0 and it.item_id = sitt.item_id  and (sa.sale_time between '".$start_date."' and '".$end_date."' ) ".$transtype." group by sup.person_id) as s on d.person_id = s.person_id");
            
            }else{
              list($datesymbol,$datetime,$transtype)=$this->getReport($data);
              $supplier = DB::select("select * from (select sup.person_id,CONCAT(ppl.first_name,' ',ppl.last_name)as name,sa.sale_id,count(sa.sale_id)as count, sum(sit.item_cost_price * sit.quantity_purchased *(1-sit.discount_percent/100)) as sales_amount,sum(sit.item_unit_price * sit.quantity_purchased) as sales_whole_amount from btree_suppliers as sup inner join btree_people as ppl on sup.person_id = ppl.person_id inner join btree_item as it on it.supplier_id = sup.person_id inner join btree_sales_items as sit on it.item_id = sit.item_id inner join btree_sales as sa on sit.sales_id = sa.sale_id where sup.deleted = 0 and Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by sup.person_id) as d join (select sup.person_id,sitt.sale_id, sitt.item_id,sum(sitt.item_tax_amount) as tax_amount from btree_sales_items_taxes as sitt, btree_sales as sa,btree_suppliers as sup, btree_item as it where sitt.sale_id = sa.sale_id and sup.person_id = it.supplier_id and it.item_id = sitt.item_id and sup.deleted = 0 and Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by sup.person_id) as s on d.person_id = s.person_id");
        }
        } catch(Exception $e) {

            return GlobalResponse::clientErrorResponse("error");
        }
        if($data['reporttype'] == 'normal'){
         $supplier = $this->arrayPaginator($supplier, $data);
         return GlobalResponse::createResponse($supplier);
        }
        else{
          return $supplier;
        }
    }      
         
    public function getItem(array $data)
    {  
     try{
          if($data['type'] == 11 || $data['type'] == 10 || $data['type'] == 9 || $data['type'] ==8){
            list($start_date,$end_date,$transtype)=$this->getReport($data);
            $item = DB::select("select * from (select it.item_id,it.item_name,sa.sale_id, sum(sit.quantity_purchased) as quantity_purchased, sum(sit.item_cost_price * sit.quantity_purchased *(1-sit.discount_percent/100)) as sales_amount ,sum(sit.item_unit_price * sit.quantity_purchased) as sales_whole_amount from btree_item as it inner join btree_sales_items as sit on it.item_id = sit.item_id inner join btree_sales as sa on sit.sales_id = sa.sale_id where it.deleted = 0 and (sa.sale_time between '".$start_date."' and '".$end_date."') ".$transtype." group by it.item_id) as d join (select sitt.sale_id,sitt.item_id,sum(sitt.item_tax_amount) as tax_amount from btree_sales_items_taxes as sitt, btree_sales as sa, btree_item as it where sitt.sale_id = sa.sale_id and it.item_id = sitt.item_id and it.deleted = 0 and (sa.sale_time between '".$start_date."' and '".$end_date."' ) ".$transtype." group by it.item_id) as s on d.item_id = s.item_id");
          }else{
            list($datesymbol,$datetime,$transtype)=$this->getReport($data);
            $item = DB::select("select * from (select it.item_id,it.item_name,sa.sale_id, sum(sit.quantity_purchased) as quantity_purchased, sum(sit.item_cost_price * sit.quantity_purchased *(1-sit.discount_percent/100)) as sales_amount,sum(sit.item_unit_price * sit.quantity_purchased) as sales_whole_amount from btree_item as it inner join btree_sales_items as sit on it.item_id = sit.item_id inner join btree_sales as sa on sit.sales_id = sa.sale_id where it.deleted = 0 and Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by it.item_id) as d join (select sitt.sale_id,sitt.item_id,sum(sitt.item_tax_amount) as tax_amount from btree_sales_items_taxes as sitt, btree_sales as sa, btree_item as it where sitt.sale_id = sa.sale_id and it.item_id = sitt.item_id and it.deleted = 0 and Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by it.item_id) as s on d.item_id = s.item_id");
        }
        } catch(Exception $e) {

            return GlobalResponse::clientErrorResponse("error");
        }

        if($data['reporttype'] == 'normal'){
         $item = $this->arrayPaginator($item, $data);
         return GlobalResponse::createResponse($item);
        }
        else{
          return $item;
        }
    }
          
    public function getExpense(array $data)
    {
      Log::info($data);
      $data['transtype']='';
      try{
          if($data['type'] == 11 || $data['type'] == 10 || $data['type'] == 9 || $data['type'] ==8){
            list($start_date,$end_date,$transtype)=$this->getReport($data);
          $expense = DB::select("select exc.expense_category_id, exc.expense_category_name,sum(ex.amount) as amount, count(ex.expense_category_id) as count from btree_expense_category as exc inner join btree_expenses as ex on exc.expense_category_id = ex.expense_category_id where Date(ex.date between '".$start_date."' and '".$end_date."') group by ex.expense_category_id");
          }else{
            list($datesymbol,$datetime,$transtype)=$this->getReport($data);
             $expense = DB::select("select exc.expense_category_id, exc.expense_category_name,sum(ex.amount) as amount, count(ex.expense_category_id) as count from btree_expense_category as exc inner join btree_expenses as ex on exc.expense_category_id = ex.expense_category_id where Date(ex.date) ".$datesymbol." '".$datetime->toDateString()."' group by ex.expense_category_id");
          }
            $expense = $this->arrayPaginator($expense, $data);
          Log::info($expense);
        } catch(Exception $e) {

            return GlobalResponse::clientErrorResponse("error");
        }
         return GlobalResponse::createResponse($expense);
    }

    public function getSales(array $data)
    {
      try{
          if($data['type'] == 11 || $data['type'] == 10 || $data['type'] == 9 || $data['type'] ==8){
            list($start_date,$end_date,$transtype)=$this->getReport($data);
            $sales = DB::select("select * from (select Date(sa.sale_time) as sale_time,count(sa.sale_id) as count,sa.sale_id,sum(sit.item_cost_price * sit.quantity_purchased *(1-sit.discount_percent/100)) as sales_amount,sum(sit.item_unit_price * quantity_purchased) as sales_whole_amount from btree_sales  as sa inner join btree_sales_items as sit on sit.sales_id = sa.sale_id where (sa.sale_time between '".$start_date."' and '".$end_date."') ".$transtype." group by Date(sa.sale_time)) as d join (select sitt.sale_id,sum(sitt.item_tax_amount) as tax_amount from btree_sales_items_taxes as sitt, btree_sales as sa where sitt.sale_id = sa.sale_id and (sa.sale_time between '".$start_date."' and '".$end_date."' ) ".$transtype." group by Date(sa.sale_time)) as s on d.sale_id = s.sale_id");
          }else{
            list($datesymbol,$datetime,$transtype)=$this->getReport($data);
            $sales = DB::select("select * from (select Date(sa.sale_time) as sale_time,sa.sale_id,count(sa.sale_id) as count, sum(sit.item_cost_price * sit.quantity_purchased *(1-sit.discount_percent/100)) as sales_amount,sum(sit.item_unit_price * quantity_purchased) as sales_whole_amount from btree_sales  as sa inner join btree_sales_items as sit on sit.sales_id = sa.sale_id where Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by Date(sa.sale_time)) as d join (select sitt.sale_id,sum(sitt.item_tax_amount) as tax_amount from btree_sales_items_taxes as sitt, btree_sales as sa where sitt.sale_id = sa.sale_id and  Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by Date(sa.sale_time)) as s on d.sale_id = s.sale_id");
        }
        } catch(Exception $e) {
            return GlobalResponse::clientErrorResponse("error");
        }
        if($data['reporttype'] == 'normal'){
         $sales = $this->arrayPaginator($sales, $data);
         return GlobalResponse::createResponse($sales);
        }
        else{
          return $sales;
        }
    }

    public function getTransaction(array $data)
    {
      try{
          if($data['type'] == 11 || $data['type'] == 10 || $data['type'] == 9 || $data['type'] ==8){
            list($start_date,$end_date,$transtype)=$this->getReport($data);
            $sales = DB::select("select * from (select Date(sa.sale_time) as sale_time,count(sa.sale_id) as count,sa.sale_id,sum(sit.item_cost_price * sit.quantity_purchased *(1-sit.discount_percent/100)) as sales_amount,sum(sit.item_unit_price * quantity_purchased) as sales_whole_amount from btree_sales  as sa inner join btree_sales_items as sit on sit.sales_id = sa.sale_id where (sa.sale_time between '".$start_date."' and '".$end_date."') ".$transtype." group by Date(sa.sale_time)) as d join (select sitt.sale_id,sum(sitt.item_tax_amount) as tax_amount from btree_sales_items_taxes as sitt, btree_sales as sa where sitt.sale_id = sa.sale_id and (sa.sale_time between '".$start_date."' and '".$end_date."' ) ".$transtype." group by Date(sa.sale_time)) as s on d.sale_id = s.sale_id");
          }else{
            list($datesymbol,$datetime,$transtype)=$this->getReport($data);
            $sales = DB::select("select * from (select Date(sa.sale_time) as sale_time,sa.sale_id,count(sa.sale_id) as count, sum(sit.item_cost_price * sit.quantity_purchased *(1-sit.discount_percent/100)) as sales_amount,sum(sit.item_unit_price * quantity_purchased) as sales_whole_amount from btree_sales  as sa inner join btree_sales_items as sit on sit.sales_id = sa.sale_id where Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by Date(sa.sale_time)) as d join (select sitt.sale_id,sum(sitt.item_tax_amount) as tax_amount from btree_sales_items_taxes as sitt, btree_sales as sa where sitt.sale_id = sa.sale_id and  Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by Date(sa.sale_time)) as s on d.sale_id = s.sale_id");
        }
            $sales = $this->arrayPaginator($sales, $data);
        } catch(Exception $e) {

            return GlobalResponse::clientErrorResponse("error");
        }
         return GlobalResponse::createResponse($sales);
    }

    public function getSuspendedSales(array $data)
    {  
      try{
          if($data['type'] == 11 || $data['type'] == 10 || $data['type'] == 9 || $data['type'] ==8){
           list($start_date,$end_date,$transtype)=$this->getReport($data);
           $sales = DB::select("select * from (select Date(sa.sale_time) as sale_time,sa.sale_id, sa.sale_id as sale_ids, CONCAT(ppl.first_name,' ',ppl.last_name) as soldby, sum(sit.item_cost_price * sit.quantity_purchased *(1-sit.discount_percent/100)) as sales_amount,SUM(sit.quantity_purchased) as item_purchase from btree_sales  as sa inner join btree_sales_items as sit on sit.sales_id = sa.sale_id inner join btree_employees as emp on emp.person_id = sa.employee_id inner join btree_people as ppl on ppl.person_id = emp.person_id where sa.sales_status = 1 and  (sa.sale_time between '".$start_date."' and '".$end_date."') ".$transtype." group by sa.sale_id) as d join (select sitt.sale_id,sum(sitt.item_tax_amount) as tax_amount from btree_sales_items_taxes as sitt, btree_sales as sa where sa.sales_status = 1 and sitt.sale_id = sa.sale_id and  (sa.sale_time between '".$start_date."' and '".$end_date."') ".$transtype." group by sa.sale_id) as s on d.sale_id = s.sale_id left join (select CONCAT(ppl.first_name,' ',ppl.last_name) as soldto, sa.sale_id from btree_people as ppl, btree_customers  as cus, btree_sales as sa where sa.sales_status = 1 and ppl.person_id = cus.person_id and sa.customer_id = cus.person_id and  (sa.sale_time between '".$start_date."' and '".$end_date."') ".$transtype." group by sa.sale_id) as e on e.sale_id = s.sale_id");
            
          }else{
            list($datesymbol,$datetime,$transtype)=$this->getReport($data);
            $sales = DB::select("select * from (select Date(sa.sale_time) as sale_time,sa.sale_id, sa.sale_id as sale_ids, CONCAT(ppl.first_name,' ',ppl.last_name) as soldby,sum(sit.item_cost_price * sit.quantity_purchased *(1-sit.discount_percent/100)) as sales_amount,SUM(sit.quantity_purchased) as item_purchase from btree_sales  as sa inner join btree_sales_items as sit on sit.sales_id = sa.sale_id inner join btree_employees as emp on emp.person_id = sa.employee_id inner join btree_people as ppl on ppl.person_id = emp.person_id where sa.sales_status = 1 and Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by sa.sale_id) as d join (select sitt.sale_id,sum(sitt.item_tax_amount) as tax_amount from btree_sales_items_taxes as sitt, btree_sales as sa where sa.sales_status = 1 and sitt.sale_id = sa.sale_id and  Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by sa.sale_id) as s on d.sale_id = s.sale_id left join (select CONCAT(ppl.first_name,' ',ppl.last_name) as soldto, sa.sale_id from btree_people as ppl, btree_customers  as cus, btree_sales as sa where sa.sales_status = 1 and ppl.person_id = cus.person_id and sa.customer_id = cus.person_id and  Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by sa.sale_id) as e on e.sale_id = s.sale_id");

        } 
         $sales = $this->arrayPaginator($sales, $data);
          Log::info($sales);
        } catch(Exception $e) {

            return GlobalResponse::clientErrorResponse("error");
        }
         return GlobalResponse::createResponse($sales);
    }

    public function getReceiving(array $data)
    {  
      try{
          if($data['type'] == 11 || $data['type'] == 10 || $data['type'] == 9 || $data['type'] ==8){
            $receiving = DB::table('btree_receivings as rec')
            ->join('btree_receiving_items as rit','rit.receiving_id','=','rec.receiving_id')
            ->join('btree_item as it','rit.item_id','=','it.item_id')
            ->join('btree_company as com','it.company_id','=','com.company_id')
            ->select('com.company_name', 'rec.receiving_id','it.item_id', 'it.item_name','rit.receiving_quantity', 'rit.item_id','rec.receiving_time','rec.payment_type',DB::raw('sum(rit.item_cost_price) as item_cost_price'), DB::raw('sum(rit.receiving_quantity) as receiving_quantity'))
           ->whereBetween('rec.receiving_time', array($datetime, $datetime1))
            ->groupby('rec.receiving_id')
            ->Paginate(self::$RECORDS_PER_PAGE);
          }else{
            $receiving = DB::table('btree_receivings as rec')
            ->join('btree_receiving_items as rit','rit.receiving_id','=','rec.receiving_id')
            ->join('btree_item as it','rit.item_id','=','it.item_id')
            ->join('btree_company as com','it.company_id','=','com.company_id')
            ->select('com.company_name', 'rec.receiving_id', 'it.item_id', 'it.item_name', 'rit.item_id','rec.receiving_time','rit.receiving_quantity','rec.payment_type',DB::raw('sum(rit.item_cost_price) as item_cost_price'), DB::raw('sum(rit.receiving_quantity) as receiving_quantity'))
             ->where('rec.receiving_time', $datesymbol,$datetime  )
            ->groupby('rec.receiving_id')
            ->Paginate(self::$RECORDS_PER_PAGE);
          }
            
          Log::info($receiving);
        } catch(Exception $e) {

            return GlobalResponse::clientErrorResponse("error");
        }
         return GlobalResponse::createResponse($receiving);
    }

    public function getDiscount(array $data)
    {  
      try{
          if($data['type'] == 11 || $data['type'] == 10 || $data['type'] == 9 || $data['type'] ==8){
           list($start_date,$end_date,$transtype)=$this->getReport($data);
            $discount = DB::select("select Max(sit.discount_percent) as discount_percent,count(*) AS count from btree_sales_items as sit,btree_sales as sa where sa.sale_id = sit.sales_id and sit.discount_percent > 0 and (sa.sale_time between '".$start_date."' and '".$end_date."') ".$transtype." group by sit.discount_percent");
          }else{
             list($datesymbol,$datetime,$transtype)=$this->getReport($data);
            $discount = DB::select("select Max(sit.discount_percent) as discount_percent,count(*) AS count from btree_sales_items as sit,btree_sales as sa where sa.sale_id = sit.sales_id and sit.discount_percent > 0 and Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by sit.discount_percent");
          }
          $discount = $this->arrayPaginator($discount, $data);
          Log::info($discount);
        } catch(Exception $e) {

            return GlobalResponse::clientErrorResponse("error");
        }
         return GlobalResponse::createResponse($discount);
    }

  public function getdetailsDiscount(array $data)
      {  
        Log::info($data);
        try{
            if($data['type'] == 11 || $data['type'] == 10 || $data['type'] == 9 || $data['type'] ==8){
             list($start_date,$end_date,$transtype)=$this->getReport($data);
              $discount = DB::select("select * from (select Date(sa.sale_time) as sale_time,sa.sale_id, sa.sale_id as sale_ids, CONCAT(ppl.first_name,' ',ppl.last_name) as soldby,sp.payment_type, sum(sit.item_cost_price * sit.quantity_purchased *(1-sit.discount_percent/100)) as sales_amount,SUM(sit.quantity_purchased) as item_purchase from btree_sales  as sa inner join btree_sales_items as sit on sit.sales_id = sa.sale_id inner join btree_employees as emp on emp.person_id = sa.employee_id inner join btree_people as ppl on ppl.person_id = emp.person_id inner join btree_sales_payments as sp on sp.sale_id = sa.sale_id where sit.discount_percent = ".$data['discountrate']." and (sa.sale_time between '".$start_date."' and '".$end_date."') ".$transtype." group by sa.sale_id) as d join (select sitt.sale_id,sum(sitt.item_tax_amount) as tax_amount from btree_sales_items_taxes as sitt, btree_sales as sa where sitt.sale_id = sa.sale_id and  (sa.sale_time between '".$start_date."' and '".$end_date."') ".$transtype." group by sa.sale_id) as s on d.sale_id = s.sale_id left join (select CONCAT(ppl.first_name,' ',ppl.last_name) as soldto, sa.sale_id from btree_people as ppl, btree_customers  as cus, btree_sales as sa where ppl.person_id = cus.person_id and sa.customer_id = cus.person_id and  (sa.sale_time between '".$start_date."' and '".$end_date."') ".$transtype." group by sa.sale_id) as e on e.sale_id = s.sale_id");
            
            }else{
               list($datesymbol,$datetime,$transtype)=$this->getReport($data);
               $discount = DB::select("select * from (select Date(sa.sale_time) as sale_time,sa.sale_id, sa.sale_id as sale_ids, CONCAT(ppl.first_name,' ',ppl.last_name) as soldby,sp.payment_type,sum(sit.item_cost_price * sit.quantity_purchased *(1-sit.discount_percent/100)) as sales_amount,SUM(sit.quantity_purchased) as item_purchase from btree_sales  as sa inner join btree_sales_items as sit on sit.sales_id = sa.sale_id inner join btree_employees as emp on emp.person_id = sa.employee_id inner join btree_people as ppl on ppl.person_id = emp.person_id inner join btree_sales_payments as sp on sp.sale_id = sa.sale_id where sit.discount_percent = ".$data['discountrate']." and  Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by sa.sale_id) as d join (select sitt.sale_id,sum(sitt.item_tax_amount) as tax_amount from btree_sales_items_taxes as sitt, btree_sales as sa where sitt.sale_id = sa.sale_id and  Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by sa.sale_id) as s on d.sale_id = s.sale_id left join (select CONCAT(ppl.first_name,' ',ppl.last_name) as soldto, sa.sale_id from btree_people as ppl, btree_customers  as cus, btree_sales as sa where ppl.person_id = cus.person_id and sa.customer_id = cus.person_id and  Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by sa.sale_id) as e on e.sale_id = s.sale_id");

            }
            $discount = $this->arrayPaginator($discount, $data);
            Log::info($discount);
          } catch(Exception $e) {

              return GlobalResponse::clientErrorResponse("error");
          }
           return GlobalResponse::createResponse($discount);
      }
    public function getPayment(array $data)
    {  
     try{
          if($data['type'] == 11 || $data['type'] == 10 || $data['type'] == 9 || $data['type'] ==8){
            list($start_date,$end_date,$transtype)=$this->getReport($data);
            $payment = DB::select("select sum(sp.payment_amount) as sales_amount,count(sp.sale_id) as count,sp.payment_type from btree_sales_payments as sp,btree_sales as sa where sa.sale_id = sp.sale_id and (sa.sale_time between '".$start_date."' and '".$end_date."') ".$transtype." group by sp.payment_type");
          }else{
            list($datesymbol,$datetime,$transtype)=$this->getReport($data);
            $payment = DB::select("select sum(sp.payment_amount) as sales_amount,count(sp.sale_id) as count,sp.payment_type from btree_sales_payments as sp inner join btree_sales as sa on sa.sale_id = sp.sale_id where Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by sp.payment_type");
        }
            $payment = $this->arrayPaginator($payment, $data);
          Log::info($payment);
        } catch(Exception $e) {

            return GlobalResponse::clientErrorResponse("error");
        }
         return GlobalResponse::createResponse($payment);
    }

    public function getTax(array $data)
    { 
      try{
          if($data['type'] == 11 || $data['type'] == 10 || $data['type'] == 9 || $data['type'] ==8){
            list($start_date,$end_date,$transtype)=$this->getReport($data);
             $tax = DB::select("select sum(st.sales_tax_amount) as tax_amount, sum(st.sales_tax_basis) as sales_amount,st.name,st.tax_rate, count(st.sales_id) as count from btree_sales_tax as st,btree_sales as sa where sa.sale_id = st.sales_id and (sa.sale_time between '".$start_date."' and '".$end_date."') ".$transtype." group by st.tax_group");
          }else{
            list($datesymbol,$datetime,$transtype)=$this->getReport($data);
            $tax = DB::select("select sum(st.sales_tax_amount) as tax_amount, sum(st.sales_tax_basis) as sales_amount,st.name,st.tax_rate , count(st.sales_id) as count from btree_sales_tax as st inner join btree_sales as sa on sa.sale_id = st.sales_id where Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by st.tax_group");
        }
            $tax = $this->arrayPaginator($tax, $data);
          Log::info($tax);
        } catch(Exception $e) {

            return GlobalResponse::clientErrorResponse("error");
        }
         return GlobalResponse::createResponse($tax);
    }

    public function getEmployee(array $data)
    {  
      try{
          if($data['type'] == 11 || $data['type'] == 10 || $data['type'] == 9 || $data['type'] ==8){
            list($start_date,$end_date,$transtype)=$this->getReport($data);
            $employee = DB::select("select * from (select emp.person_id,CONCAT(ppl.first_name,' ',ppl.last_name)as name,sa.sale_id,count(sa.sale_id)as count, sum(sit.item_cost_price * sit.quantity_purchased *(1-sit.discount_percent/100)) as sales_amount,sum(sit.item_unit_price * sit.quantity_purchased) as sales_whole_amount from btree_employees as emp inner join btree_people as ppl on emp.person_id = ppl.person_id inner join btree_sales as sa on emp.person_id = sa.employee_id inner join btree_sales_items as sit on sa.sale_id = sit.sales_id  where emp.deleted = 0 and (sa.sale_time between '".$start_date."' and '".$end_date."') ".$transtype." group by emp.person_id) as d join (select emp.person_id,sitt.sale_id, sitt.item_id,sum(sitt.item_tax_amount) as tax_amount from btree_sales_items_taxes as sitt, btree_sales as sa,btree_employees as emp where sitt.sale_id = sa.sale_id and emp.deleted = 0  and emp.person_id = sa.employee_id and (sa.sale_time between '".$start_date."' and '".$end_date."' ) ".$transtype." group by emp.person_id) as s on d.person_id = s.person_id");
            
          }else{
            list($datesymbol,$datetime,$transtype)=$this->getReport($data);
            $employee = DB::select("select * from (select emp.person_id,CONCAT(ppl.first_name,' ',ppl.last_name)as name,sa.sale_id,count(sa.sale_id)as count, sum(sit.item_cost_price * sit.quantity_purchased *(1-sit.discount_percent/100)) as sales_amount,sum(sit.item_unit_price * sit.quantity_purchased) as sales_whole_amount from btree_employees as emp inner join btree_people as ppl on emp.person_id = ppl.person_id inner join btree_sales as sa on emp.person_id = sa.employee_id inner join btree_sales_items as sit on sa.sale_id = sit.sales_id  where emp.deleted = 0 and Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by emp.person_id) as d join (select emp.person_id,sitt.sale_id, sitt.item_id,sum(sitt.item_tax_amount) as tax_amount from btree_sales_items_taxes as sitt, btree_sales as sa,btree_employees as emp where sitt.sale_id = sa.sale_id and emp.person_id = sa.employee_id and emp.deleted = 0 and Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by emp.person_id) as s on d.person_id = s.person_id");

        }
        } catch(Exception $e) {

            return GlobalResponse::clientErrorResponse("error");
        }
        if($data['reporttype'] == 'normal'){
         $employee = $this->arrayPaginator($employee, $data);
         return GlobalResponse::createResponse($employee);
        }
        else{
          return $employee;
        }

    }

    public function getDetailSales(array $data)
    {  
      try{
          if($data['type'] == 11 || $data['type'] == 10 || $data['type'] == 9 || $data['type'] ==8){
            list($start_date,$end_date,$transtype)=$this->getReport($data);
            $sales = DB::select("select * from (select Date(sa.sale_time) as sale_time,sa.sale_id, sa.sale_id as sale_ids, CONCAT(ppl.first_name,' ',ppl.last_name) as soldby,sp.payment_type, sum(sit.item_cost_price * sit.quantity_purchased *(1-sit.discount_percent/100)) as sales_amount,SUM(sit.quantity_purchased) as item_purchase from btree_sales  as sa inner join btree_sales_items as sit on sit.sales_id = sa.sale_id inner join btree_employees as emp on emp.person_id = sa.employee_id inner join btree_people as ppl on ppl.person_id = emp.person_id inner join btree_sales_payments as sp on sp.sale_id = sa.sale_id where (sa.sale_time between '".$start_date."' and '".$end_date."') ".$transtype." group by sa.sale_id) as d join (select sitt.sale_id,sum(sitt.item_tax_amount) as tax_amount from btree_sales_items_taxes as sitt, btree_sales as sa where sitt.sale_id = sa.sale_id and  (sa.sale_time between '".$start_date."' and '".$end_date."') ".$transtype." group by sa.sale_id) as s on d.sale_id = s.sale_id left join (select CONCAT(ppl.first_name,' ',ppl.last_name) as soldto, sa.sale_id from btree_people as ppl, btree_customers  as cus, btree_sales as sa where ppl.person_id = cus.person_id and sa.customer_id = cus.person_id and  (sa.sale_time between '".$start_date."' and '".$end_date."') ".$transtype." group by sa.sale_id) as e on e.sale_id = s.sale_id");
            
          }else{
            list($datesymbol,$datetime,$transtype)=$this->getReport($data);
            $sales = DB::select("select * from (select Date(sa.sale_time) as sale_time,sa.sale_id, sa.sale_id as sale_ids, CONCAT(ppl.first_name,' ',ppl.last_name) as soldby,sp.payment_type,sum(sit.item_cost_price * sit.quantity_purchased *(1-sit.discount_percent/100)) as sales_amount,SUM(sit.quantity_purchased) as item_purchase from btree_sales  as sa inner join btree_sales_items as sit on sit.sales_id = sa.sale_id inner join btree_employees as emp on emp.person_id = sa.employee_id inner join btree_people as ppl on ppl.person_id = emp.person_id inner join btree_sales_payments as sp on sp.sale_id = sa.sale_id where Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by sa.sale_id) as d join (select sitt.sale_id,sum(sitt.item_tax_amount) as tax_amount from btree_sales_items_taxes as sitt, btree_sales as sa where sitt.sale_id = sa.sale_id and  Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by sa.sale_id) as s on d.sale_id = s.sale_id left join (select CONCAT(ppl.first_name,' ',ppl.last_name) as soldto, sa.sale_id from btree_people as ppl, btree_customers  as cus, btree_sales as sa where ppl.person_id = cus.person_id and sa.customer_id = cus.person_id and  Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by sa.sale_id) as e on e.sale_id = s.sale_id");

        }
            $sales = $this->arrayPaginator($sales, $data);
            Log::info($sales);
        } catch(Exception $e) {

            return GlobalResponse::clientErrorResponse("error");
        }
         return GlobalResponse::createResponse($sales);
    }

    public function getDetailCustomer(array $data)
    {  
      try{
          if($data['type'] == 11 || $data['type'] == 10 || $data['type'] == 9 || $data['type'] ==8){
            list($start_date,$end_date,$transtype)=$this->getReport($data);
            $sales = DB::select("select * from (select Date(sa.sale_time) as sale_time,sa.sale_id, sa.sale_id as sale_ids, CONCAT(ppl.first_name,' ',ppl.last_name) as soldby,sp.payment_type, sum(sit.item_cost_price * sit.quantity_purchased *(1-sit.discount_percent/100)) as sales_amount,SUM(sit.quantity_purchased) as item_purchase from btree_sales  as sa inner join btree_sales_items as sit on sit.sales_id = sa.sale_id inner join btree_employees as emp on emp.person_id = sa.employee_id inner join btree_people as ppl on ppl.person_id = emp.person_id inner join btree_sales_payments as sp on sp.sale_id = sa.sale_id where (sa.sale_time between '".$start_date."' and '".$end_date."') and sa.customer_id =".$data['personid']." ".$transtype." group by sa.sale_id) as d join (select sitt.sale_id,sum(sitt.item_tax_amount) as tax_amount from btree_sales_items_taxes as sitt, btree_sales as sa where sitt.sale_id = sa.sale_id and  (sa.sale_time between '".$start_date."' and '".$end_date."') ".$transtype." group by sa.sale_id) as s on d.sale_id = s.sale_id left join (select CONCAT(ppl.first_name,' ',ppl.last_name) as soldto, sa.sale_id from btree_people as ppl, btree_customers  as cus, btree_sales as sa where ppl.person_id = cus.person_id and sa.customer_id =".$data['personid']." and sa.customer_id = cus.person_id and  (sa.sale_time between '".$start_date."' and '".$end_date."') ".$transtype." group by sa.sale_id) as e on e.sale_id = s.sale_id");
            
          }else{
            list($datesymbol,$datetime,$transtype)=$this->getReport($data);
            $sales = DB::select("select * from (select Date(sa.sale_time) as sale_time,sa.sale_id, sa.sale_id as sale_ids, CONCAT(ppl.first_name,' ',ppl.last_name) as soldby,sp.payment_type,sum(sit.item_cost_price * sit.quantity_purchased *(1-sit.discount_percent/100)) as sales_amount,SUM(sit.quantity_purchased) as item_purchase from btree_sales  as sa inner join btree_sales_items as sit on sit.sales_id = sa.sale_id inner join btree_employees as emp on emp.person_id = sa.employee_id inner join btree_people as ppl on ppl.person_id = emp.person_id inner join btree_sales_payments as sp on sp.sale_id = sa.sale_id where sa.customer_id =".$data['personid']."  and Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by sa.sale_id) as d join (select sitt.sale_id,sum(sitt.item_tax_amount) as tax_amount from btree_sales_items_taxes as sitt, btree_sales as sa where sitt.sale_id = sa.sale_id and  Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by sa.sale_id) as s on d.sale_id = s.sale_id left join (select CONCAT(ppl.first_name,' ',ppl.last_name) as soldto, sa.sale_id from btree_people as ppl, btree_customers  as cus, btree_sales as sa where ppl.person_id = cus.person_id and sa.customer_id =".$data['personid']."  and sa.customer_id = cus.person_id and  Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by sa.sale_id) as e on e.sale_id = s.sale_id");

        }
            $sales = $this->arrayPaginator($sales, $data);
            Log::info($sales);
        } catch(Exception $e) {

            return GlobalResponse::clientErrorResponse("error");
        }
         return GlobalResponse::createResponse($sales);
    }

    public function getDetailEmployee(array $data)
    {  
      try{
          if($data['type'] == 11 || $data['type'] == 10 || $data['type'] == 9 || $data['type'] ==8){
            list($start_date,$end_date,$transtype)=$this->getReport($data);
            $sales = DB::select("select * from (select Date(sa.sale_time) as sale_time,sa.sale_id, sa.sale_id as sale_ids, CONCAT(ppl.first_name,' ',ppl.last_name) as soldby,sp.payment_type, sum(sit.item_cost_price * sit.quantity_purchased *(1-sit.discount_percent/100)) as sales_amount,SUM(sit.quantity_purchased) as item_purchase from btree_sales  as sa inner join btree_sales_items as sit on sit.sales_id = sa.sale_id inner join btree_employees as emp on emp.person_id = sa.employee_id inner join btree_people as ppl on ppl.person_id = emp.person_id inner join btree_sales_payments as sp on sp.sale_id = sa.sale_id where (sa.sale_time between '".$start_date."' and '".$end_date."') ".$transtype." and sa.employee_id =".$data['personid']." group by sa.sale_id) as d join (select sitt.sale_id,sum(sitt.item_tax_amount) as tax_amount from btree_sales_items_taxes as sitt, btree_sales as sa where sitt.sale_id = sa.sale_id and  (sa.sale_time between '".$start_date."' and '".$end_date."') ".$transtype." group by sa.sale_id) as s on d.sale_id = s.sale_id left join (select CONCAT(ppl.first_name,' ',ppl.last_name) as soldto, sa.sale_id from btree_people as ppl, btree_customers  as cus, btree_sales as sa where ppl.person_id = cus.person_id and sa.employee_id =".$data['personid']." and sa.customer_id = cus.person_id and  (sa.sale_time between '".$start_date."' and '".$end_date."') ".$transtype." group by sa.sale_id) as e on e.sale_id = s.sale_id");
            
          }else{
            list($datesymbol,$datetime,$transtype)=$this->getReport($data);
            $sales = DB::select("select * from (select Date(sa.sale_time) as sale_time,sa.sale_id, sa.sale_id as sale_ids, CONCAT(ppl.first_name,' ',ppl.last_name) as soldby,sp.payment_type,sum(sit.item_cost_price * sit.quantity_purchased *(1-sit.discount_percent/100)) as sales_amount,SUM(sit.quantity_purchased) as item_purchase from btree_sales  as sa inner join btree_sales_items as sit on sit.sales_id = sa.sale_id inner join btree_employees as emp on emp.person_id = sa.employee_id inner join btree_people as ppl on ppl.person_id = emp.person_id inner join btree_sales_payments as sp on sp.sale_id = sa.sale_id where sa.employee_id =".$data['personid']."  and Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by sa.sale_id) as d join (select sitt.sale_id,sum(sitt.item_tax_amount) as tax_amount from btree_sales_items_taxes as sitt, btree_sales as sa where sitt.sale_id = sa.sale_id and  Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by sa.sale_id) as s on d.sale_id = s.sale_id left join (select CONCAT(ppl.first_name,' ',ppl.last_name) as soldto, sa.sale_id from btree_people as ppl, btree_customers  as cus, btree_sales as sa where ppl.person_id = cus.person_id and sa.employee_id =".$data['personid']."  and sa.customer_id = cus.person_id and  Date(sa.sale_time) ".$datesymbol." '".$datetime->toDateString()."' ".$transtype." group by sa.sale_id) as e on e.sale_id = s.sale_id");

        }
            $sales = $this->arrayPaginator($sales, $data);
            Log::info($sales);
        } catch(Exception $e) {

            return GlobalResponse::clientErrorResponse("error");
        }
         return GlobalResponse::createResponse($sales);
    }



    public function getSalesItem(array $data){
      Log::info($data['sale_id']);
      
      $sales = DB::select("select * from (select it.item_id,(sit.item_cost_price * sit.quantity_purchased *(1-sit.discount_percent/100)) as sales_amount,sit.quantity_purchased,it.item_name from btree_sales_items as sit inner join btree_item as it on it.item_id = sit.item_id inner join btree_sales as sa on sa.sale_id = sit.sales_id  where sa.sales_status = ".$data['sales_status']." and sit.sales_id = ".$data['sale_id']."  group by it.item_id) as d join (select it.item_id,sitt.sale_id,sum(sitt.item_tax_amount) as tax_amount from btree_sales_items_taxes as sitt, btree_sales as sa,btree_item as it where sitt.sale_id = ".$data['sale_id']." and sa.sale_id = sitt.sale_id and sa.sales_status = ".$data['sales_status']." and  sitt.item_id = it.item_id group by it.item_id) as s on d.item_id = s.item_id");
       return GlobalResponse::createResponse($sales);

    }
    public function getLowInventory(array $data){
      
      $inventory = DB::select("select it.item_name, it.item_number, it.reorder_level,inv.quantity from btree_item_quantities as inv , btree_item as it where it.item_id = inv.item_id and it.deleted = 0 and inv.quantity <= it.reorder_level group by it.item_name");
      $inventory = $this->arrayPaginator($inventory, $data);
       return GlobalResponse::createResponse($inventory);

    }

    public function getInventorySummary(array $data){
      Log::info($data);
      if($data['type'] == 1){
        $transtype = '';
      }elseif ($data['type'] == 2) {
        $transtype = 'and inv.quantity <= 0';
      }elseif ($data['type'] == 3) {
        $transtype = 'and inv.quantity > 0';
      }
      $inventory = DB::select("select it.item_name, it.item_number, it.reorder_level,inv.quantity,
      it.cost_price,it.unit_price,(it.cost_price * inv.quantity) as sales_total_value from btree_item_quantities as inv , btree_item as it where it.item_id = inv.item_id and it.deleted = 0  ".$transtype."  group by it.item_name");
      $inventory = $this->arrayPaginator($inventory, $data);
       return GlobalResponse::createResponse($inventory);

    }

    
    public function getCommission(array $data)
    {  
      try{
          
            $commission = DB::select("SELECT CONCAT(ppl.first_name,' ',ppl.last_name) as name, sum(cuspts.points_earned) as earned, sum(clm.used) as claimpts FROM btree_customer_pts_claim as clm , btree_customer_point as cuspts ,btree_customers as cus, btree_people as ppl where cus.person_id = cuspts.customer_id and cus.person_id = clm.customer_id and cus.person_id = ppl.person_id");

        
            $commission = $this->arrayPaginator($commission, $data);
            Log::info($commission);
        } catch(Exception $e) {

            return GlobalResponse::clientErrorResponse("error");
        }
         return GlobalResponse::createResponse($commission);
    }

    public function getDeadItem(array $data)
    {  
      try{
          if($data['type'] == 11 || $data['type'] == 10 || $data['type'] == 9 || $data['type'] ==8){
            list($start_date,$end_date,$transtype)=$this->getReport($data);
            $inventory = DB::select("select * from (select it.item_id,it.item_name,it.item_number,it.cost_price, it.unit_price from btree_item as it group by it.item_id) as d left join (select sit.item_id from btree_sales_items as sit, btree_sales as sa WHERE sa.sale_id = sit.sales_id and (sa.sale_time between '".$start_date."' and '".$end_date."') group by sit.item_id) as s on s.item_id = d.item_id where s.item_id IS NOT NULL");
            
          }else{
            list($datesymbol,$datetime,$transtype)=$this->getReport($data);
            $inventory = DB::select("select * from (select it.item_id,it.item_name,it.item_number,it.cost_price, it.unit_price,inv.quantity from btree_item as it, btree_item_quantities as inv where inv.item_id = it.item_id group by it.item_id) as d left join (select sit.item_id from btree_sales_items as sit, btree_sales as sa WHERE sa.sale_id = sit.sales_id and sa.sale_time > '".$datetime->toDateString()."'  group by sit.item_id ) as s on s.item_id = d.item_id where s.item_id IS NOT NULL");        }
            $inventory = $this->arrayPaginator($inventory, $data);
            Log::info($inventory);
        } catch(Exception $e) {

            return GlobalResponse::clientErrorResponse("error");
        }
         return GlobalResponse::createResponse($inventory);
    }


    public function arrayPaginator($array, $request)
    {
        $page = Input::get('page', 1);
        $perPage = Input::get('per_page', 1);
        $offset = ($page * $perPage) - $perPage;

        return new LengthAwarePaginator(array_slice($array, $offset, $perPage), count($array), $perPage, $page);
    }
// select com.company_id,per_pagecom.company_name,it.item_name,it.item_id,sum(sit.item_cost_price),sum(sit.item_unit_price) from btree_company as com, btree_sales_items as sit, btree_item as it where it.item_id = sit.item_id and it.company_id = com.company_id group by com.company_id
} ?>
