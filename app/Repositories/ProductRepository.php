<?php
namespace App\Repositories;
use App\Models\Product;
use Illuminate\Support\Facades\DB;
use App\Util\BLAlphaNumericCodeGenerator;
use Log;
use App\Response\GlobalResponse;

class ProductRepository
{
     private static  $RECORDS_PER_PAGE =2;

     public function __construct() {
        self::$RECORDS_PER_PAGE = config('blconstants.RECORDS_PER_PAGE');
    }
    public function save(array $data)
    {
        DB::beginTransaction();
         try {
          Log::info($data);
            $product = new  Product;
            $product->fill($data);
            $product->save();
            DB::commit();
        } catch(Exception $e) {
            DB::rollback();
            return GlobalResponse::clientErrorResponse("error");
        }
        
         return GlobalResponse::createResponse($product);
    }

    public function update(array $data){
        DB::beginTransaction();
        try{
             $product = product::where("product_id",$data['product_id'])->first();          
            $product->fill($data);
            $product->save();
            DB::commit();
        }catch(Exception $e) {
            DB::rollback();
            return GlobalResponse::clientErrorResponse("error");
        }
        
         return GlobalResponse::createResponse($product);
    }


    public function deleteAll(){
        try{
            $product = product::where('deleted', '=', 0)->update(['deleted' => 1]);
             
        }catch(Exception $e){
            return GlobalResponse::clientErrorResponse("error");
        }

         return GlobalResponse::createResponse($product);
    }
    

    public function getAll(){
        try{
            
            $product = DB::table('btree_product as com')
            ->where('com.deleted','=','0')
            ->select('com.*')
            ->Paginate(self::$RECORDS_PER_PAGE);
            if (is_null($product))
            {
                return "failed";
            }

        }catch(Exception $e){
            return GlobalResponse::clientErrorResponse("error");
        }

         return GlobalResponse::createResponse($product);
    }

    public function getAlltrash(){
        try{
            
            $product = DB::table('btree_product as com')
            ->where('com.deleted','=','1')
            ->select('com.*')
            ->Paginate(self::$RECORDS_PER_PAGE);

        }catch(Exception $e){
            return GlobalResponse::clientErrorResponse("error");
        }

         return GlobalResponse::createResponse($product);

    }


    public function storeAll(){
        try{
            $product = Product::where('deleted', '=', 1)->update(['deleted' => 0]);
             
        }catch(Exception $e){
            return GlobalResponse::clientErrorResponse("error");
        }

         return GlobalResponse::createResponse($product);
    }


    public function getById(array $data){
        try{
            $product = DB::table('btree_product as com')
            ->where('com.product_id','=',$data['product_id'])
            ->select('com.*')
            ->get();
            if (is_null($product)){
                return "failed";
             }

        }catch(Exception $e){
            return GlobalResponse::clientErrorResponse("error");
        }

         return GlobalResponse::createResponse($product);
    }

    public function deleteproduct($data){
        DB::beginTransaction();
      try{
           $product = Product::where('product_id','=',$data)->first();
           if($product->deleted == 1){
            $product->deleted = 0;
           $product->save();
           }
           else{
            $product->deleted = 1;
           $product->save();
           }
           DB::commit();
       }catch(Exception $e) {
            DB::rollback();
            return GlobalResponse::clientErrorResponse("error");
        }
        
         return GlobalResponse::createResponse($product);
    }

   public function search($data){
        try{
             Log::info('Some message here.');

            $sql = " product_name like '%".$data."%' and deleted = 0 ";

             $product = DB::table('btree_product as pro')
            ->whereRaw($sql)
            ->simplePaginate(self::$RECORDS_PER_PAGE);
            // $item = item::whereRaw($sql)->get();
          //  $item = DB::Select($sql);
            Log::info($product);
            if (is_null($product))
            {
                return "failed";
            }

        }catch(Exception $e){
            return GlobalResponse::clientErrorResponse("error");
        }

         return GlobalResponse::createResponse($product);
    }
     public function getList(){
       try{
            
            $product = DB::table('btree_product as com')
            ->where('com.deleted','=','0')
            ->select('com.*')
            ->get();
            if (is_null($product))
            {
                return "failed";
            }

        }catch(Exception $e){
         return GlobalResponse::clientErrorResponse("error");
        }

         return GlobalResponse::createResponse($product);
    }
    public function deleteSelectProduct(array $data){
        DB::beginTransaction();
      try{

        foreach ($data as $value) {
            $product = Product::where('product_id','=',$value)->first();
            $product->deleted = 1;
            $product->save();
        }
        DB::commit();
       }catch(Exception $e){
            DB::rollback();
           return GlobalResponse::clientErrorResponse("error");
       }
       
        return GlobalResponse::createResponse("success");
   }

   public function addSelectProduct(array $data){
    DB::beginTransaction();
      try{

        foreach ($data as $value) {
            $product = Product::where('product_id','=',$value)->first();
            $product->deleted = 0;
            $product->save();
        }
        DB::commit();
       }catch(Exception $e){
            DB::rollback();
           return GlobalResponse::clientErrorResponse("error");
       }
       
        return GlobalResponse::createResponse("success");
   }


} ?>

