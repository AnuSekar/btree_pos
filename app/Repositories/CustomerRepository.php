<?php
namespace App\Repositories;
use App\Models\Customer;
use App\Models\Person;
use App\Models\CustomerPointClaim;
use Illuminate\Support\Facades\DB;
use App\Util\BLAlphaNumericCodeGenerator;
use Log;
use App\Response\GlobalResponse;

class CustomerRepository
{
     private static  $RECORDS_PER_PAGE =2;

     public function __construct() {
        self::$RECORDS_PER_PAGE = config('blconstants.RECORDS_PER_PAGE');
    }
    public function save(array $data)
    {
        DB::beginTransaction();
         try {
            $person = new Person;
            $person->fill($data);
            $person->save();
            $customer = new  Customer;
            $customer['person_id'] = $person->person_id;
            $customer->fill($data);
            $customer->save();
            DB::commit();
           
        } catch(Exception $e) {
            DB::rollback();
            throw GlobalResponse::clientErrorResponse("error");
        }
        
        return GlobalResponse::createResponse($customer);
    }

    public function update(array $data){
        DB::beginTransaction();
        try{
             $customer = Customer::where("person_id",$data['person_id'])->first(); 
            
            if (is_null($customer)){
                return "failed";
            }
            $person = Person::where("person_id",$data['person_id'])->first();
            $person->fill($data);
            $person->save();
            
            $customer->fill($data);
            $customer->save();
            DB::commit();

        }catch(Exception $e){
          DB::rollback();
            throw GlobalResponse::clientErrorResponse("error");
        }
        
        return GlobalResponse::createResponse($customer);
    }


    public function deleteAll(){
      DB::beginTransaction();
        try{
            $msg = Customer::where('deleted', '=', 0)->update(['deleted' => 1]);
            DB::commit(); 
        }catch(Exception $e){
          DB::rollback();
            throw GlobalResponse::clientErrorResponse("error");
        }
        
        return GlobalResponse::createResponse($msg);
    }

    public function storeAll(){
        try{
            $msg = Customer::where('deleted', '=', 1)->update(['deleted' => 0]);
             
        }catch(Exception $e){
            throw GlobalResponse::clientErrorResponse("error");
        }

        return GlobalResponse::createResponse($msg);
    }

    

    public function getAll(){
        try{
            
            $customer = DB::table('btree_people as ppl')
            ->leftjoin('btree_customers as cus','ppl.person_id','=','cus.person_id')
            ->where('cus.deleted','=','0')
            ->select('ppl.*','cus.*')
            ->Paginate(self::$RECORDS_PER_PAGE);
            if (is_null($customer))
            {
                return "failed";
            }

        }catch(Exception $e){
            throw GlobalResponse::clientErrorResponse("error");
        }

        return GlobalResponse::createResponse($customer);

    }

    public function getAlltrash(){
        try{
            
            $customer = DB::table('btree_people as ppl')
            ->leftjoin('btree_customers as cus','ppl.person_id','=','cus.person_id')
            ->where('cus.deleted','=','1')
            ->select('ppl.*','cus.*')
            ->Paginate(self::$RECORDS_PER_PAGE);
            if (is_null($customer))
            {
                return "failed";
            }

        }catch(Exception $e){
            throw GlobalResponse::clientErrorResponse("error");
        }

        return GlobalResponse::createResponse($customer);

    }


    public function getById(array $data){
        try{
            $customer = DB::table('btree_people as ppl')
            ->where('ppl.person_id','=',$data['person_id'])
            ->join('btree_customers as cus','ppl.person_id','=','cus.person_id')
            ->select('ppl.*','cus.*')
            ->get();
            if (is_null($customer)){
                return "failed";
            }

        }catch(Exception $e){
            throw GlobalResponse::clientErrorResponse("error");
        }

        return GlobalResponse::createResponse($customer);
    }

    public function deletecustomer($data){
      DB::beginTransaction();
      try{

           $customer = Customer::where('person_id','=',$data)->first();
           if($customer->deleted == 1){
            $customer->deleted = 0;
            $customer->save();
           }
           else{
            $customer->deleted = 1;
            $customer->save();
           }
           DB::commit();
       }catch(Exception $e){
          DB::rollback();
           throw GlobalResponse::clientErrorResponse("error");
       }
       
       return GlobalResponse::createResponse($customer);
   }

   public function search($data){
        try{
             Log::info('Some message here.');

            $sql = "  ( ppl.first_name like '%".$data."%' or ppl.last_name like '%".$data."%' or ppl.phone_number like '%".$data."%' ) and cus.deleted = 0 and ppl.person_id = cus.person_id";

             $customer = DB::table('btree_people as ppl')
            ->leftjoin('btree_customers as cus','ppl.person_id','=','cus.person_id')
            ->whereRaw($sql)
            ->simplePaginate(self::$RECORDS_PER_PAGE);
             // $customer = customer::whereRaw($sql)->get();
            if (is_null($customer))
            {
                return "failed";
            }

        }catch(Exception $e){
            throw GlobalResponse::clientErrorResponse("error");
        }

        return GlobalResponse::createResponse($customer);

    }

    public function customerbysales($data){
        try{
            Log::info($data);
            $customer = DB::select("SELECT ppl.person_id,ppl.first_name,ppl.first_name as label,ppl.last_name from btree_customers as cus, btree_people as ppl where  ppl.person_id = cus.person_id  and cus.deleted = 0 and (ppl.first_name like '%".$data ."%' or ppl.last_name like '%".$data ."%' )");
            if (is_null($customer))
            {
                return "failed";
            }

        }catch(Exception $e){
            throw GlobalResponse::clientErrorResponse("error");
        }

        return GlobalResponse::createResponse($customer);

    }

    public function deleteSelectCustomer(array $data){
      DB::beginTransaction();
      try{

        foreach ($data as $value) {
            $customer = Customer::where('person_id','=',$value)->first();
            $customer->deleted = 1;
            $customer->save();
        }
        DB::commit();
       }catch(Exception $e){
        DB::rollback();
           return GlobalResponse::clientErrorResponse("error");
       }
       
        return GlobalResponse::createResponse("success");
   }

   public function addSelectCustomer(array $data){
    DB::beginTransaction();
      try{

        foreach ($data as $value) {
            $customer = Customer::where('person_id','=',$value)->first();
            $customer->deleted = 0;
            $customer->save();
        }
        DB::commit();
       }catch(Exception $e){
        DB::rollback();
           return GlobalResponse::clientErrorResponse("error");
       }
       
        return GlobalResponse::createResponse("success");
   }

   public function CustomerPointsClaim(array $data){
      Log::info($data);
      $customer =Customer::where("person_id",$data['person_id'])->first();
      $customer['points'] = $customer['points'] - $data['claimpts'];
      $customer->save();

      $CustomerPointClaim = new CustomerPointClaim;
      $CustomerPointClaim['customer_id'] = $data['person_id'];
      $CustomerPointClaim['used'] = $data['claimpts'];
      $CustomerPointClaim->save();
      
        return GlobalResponse::createResponse("success");
                
   }


} ?>

