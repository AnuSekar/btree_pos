<?php
namespace App\Repositories;
use App\Models\Item;
use App\Models\Item_Tax;
use App\Models\Sales;
use App\Models\SalesItem;
use App\Models\SalesItemTax;
use App\Models\SalesTax;
use App\Models\CustomerPoints;
use Illuminate\Support\Facades\DB;
use App\Response\GlobalResponse;
use Log;

class SuspendedRepository
{
     private static  $RECORDS_PER_PAGE =2;

     public function __construct() {
        self::$RECORDS_PER_PAGE = config('blconstants.RECORDS_PER_PAGE');
    }
    public function save(array $datas)
    {
         try {
            Log::info($datas);
            if(!is_null($datas['update']) && $datas['update'] == 'updatesales'){
                return $this->update($datas);

            }
            $sales = new Sales();
            //$sales->fill($datas);
            $sales['employee_id'] = 1; 
            $sales['invoice_number'] = 1;
            $sales['sales_status'] = 1;
            $sales['sales_type'] = $datas['sales_type'];
            if($datas['customer'] != ''){
                $sales['customer_id'] = $datas['customer'];
            }
            $sales->save();
            $x =1;
            $taxsale =$datas['sales_tax'];
            Log::info($taxsale);
            $subtotal = 0;
            foreach($datas['item'] as $data){
                Log::info($data);
                $item = Item::where("item_id",$data['item_id'])->first(); 
                $salesitem = new SalesItem();
                
                $salesitem['sales_id'] = $sales->sale_id;
                $salesitem['item_id'] = $data['item_id'];
                $salesitem['line'] = $x;
                $salesitem['quantity_purchased'] = $data['item_quantity'];
                $salesitem['item_cost_price'] = $item['cost_price'];
                $salesitem['item_unit_price'] = $item['unit_price'];
                if( $data['item_discount'] != ''){
                    $salesitem['discount_percent'] = $data['item_discount'];
                    $discount= (($item['cost_price']*$data['item_discount'])/100);
                    $discountprice = $item['cost_price'] - $discount;
                    $totalprice = $discountprice * $data['item_quantity'];
                }else{
                    $totalprice = $item['cost_price']*$data['item_quantity'];
                }
                $salesitem['item_location'] = 1;
                $salesitem->save();
                $x++;
                $salestax['sales_id']=$sales->sale_id;


                $itemtax = Item_Tax::where("item_id",$data['item_id'])->get();
                $subtotal = $subtotal + $totalprice;
                foreach($itemtax as $tax){
                    $salesitemtax = new SalesItemTax();
                    $salesitemtax['percent']=$tax->percent;
                    $salesitemtax['name']=$tax->tax_name;
                    $taxgrpname = $tax->tax_name.' '.$tax->percent;
                    $taxsale[$taxgrpname] = $taxsale[$taxgrpname]+ (($totalprice * $tax->percent)/100);
                    Log::info($taxsale);
                    $salesitemtax['item_id']=$data['item_id'];
                    $salesitemtax['sale_id']=$sales->sale_id;
                    $salesitemtax['line']=$salesitem->line;
                    $salesitemtax['item_tax_amount']=(($totalprice * $tax->percent)/100);
                    $salesitemtax->save();
                }

            }
                $y=0;
                foreach ($taxsale as $key => $value) {
                    $salestax = new SalesTax();
                    $salestax['sales_id']=$sales->sale_id; 
                    $splittax = explode(" ",$key);
                    $salestax['tax_group'] = $key;
                    $salestax['sales_tax_basis']= $subtotal;
                    $salestax['sales_tax_amount'] = $value;
                    $salestax['print_sequence']= $y;
                    $salestax['name'] = $splittax[0];
                    $salestax['tax_rate'] = $splittax[1];
                    $salestax->save();
                    $y++;
                }

       
        } catch(Exception $e) {

            throw $e;
        }
       return $sales;


}
    
    public function update(array $datas)
    {
         try {
            Log::info($datas);
            $salesid = $datas['salesid'];
            SalesItem::where("sales_id",$datas['salesid'])->delete();
            SalesItemTax::where("sale_id",$datas['salesid'])->delete();
            SalesTax::where("sales_id",$datas['salesid'])->delete();

            $x =1;
            $taxsale =$datas['sales_tax'];
            Log::info($taxsale);
            $subtotal = 0;
            foreach($datas['item'] as $data){
                Log::info($data);
                $item = Item::where("item_id",$data['item_id'])->first(); 
                $salesitem = new SalesItem();
                
                $salesitem['sales_id'] = $salesid;
                $salesitem['item_id'] = $data['item_id'];
                $salesitem['line'] = $x;
                $salesitem['quantity_purchased'] = $data['item_quantity'];
                $salesitem['item_cost_price'] = $item['cost_price'];
                $salesitem['item_unit_price'] = $item['unit_price'];
                if( $data['item_discount'] != ''){
                    $salesitem['discount_percent'] = $data['item_discount'];
                    $discount= (($item['cost_price']*$data['item_discount'])/100);
                    $discountprice = $item['cost_price'] - $discount;
                    $totalprice = $discountprice * $data['item_quantity'];
                }else{
                    $totalprice = $item['cost_price']*$data['item_quantity'];
                }
                $salesitem['item_location'] = 1;
                $salesitem->save();
                $x++;
                $salestax['sales_id']=$salesid;

                $itemtax = Item_Tax::where("item_id",$data['item_id'])->get();
                $subtotal = $subtotal + $totalprice;
                foreach($itemtax as $tax){
                    $salesitemtax = new SalesItemTax();
                    $salesitemtax['percent']=$tax->percent;
                    $salesitemtax['name']=$tax->tax_name;
                    $taxgrpname = $tax->tax_name.' '.$tax->percent;
                    $taxsale[$taxgrpname] = $taxsale[$taxgrpname]+ (($totalprice * $tax->percent)/100);
                    Log::info($taxsale);
                    $salesitemtax['item_id']=$data['item_id'];
                    $salesitemtax['sale_id']=$salesid;
                    $salesitemtax['line']=$salesitem->line;
                    $salesitemtax['item_tax_amount']=(($totalprice * $tax->percent)/100);
                    $salesitemtax->save();
                }

            }
                $y=0;
                foreach ($taxsale as $key => $value) {
                    $salestax = new SalesTax();
                    $salestax['sales_id']=$salesid; 
                    $splittax = explode(" ",$key);
                    $salestax['tax_group'] = $key;
                    $salestax['sales_tax_basis']= $subtotal;
                    $salestax['sales_tax_amount'] = $value;
                    $salestax['print_sequence']= $y;
                    $salestax['name'] = $splittax[0];
                    $salestax['tax_rate'] = $splittax[1];
                    $salestax->save();
                    $y++;
                }

       
        } catch(Exception $e) {

            throw $e;
        }
       return GlobalResponse::createResponse("success");  


} 


    public function getAllList(){
        try{
            

            $supplier = DB::select("select * from (select Date(sa.sale_time) as date,sa.sale_id,sa.sale_id as sales_id, sa.customer_id from btree_sales  as sa where sa.sales_status = 1) as s left join (select CONCAT(ppl.first_name,' ',ppl.last_name) as customer_name, sa.sale_id from btree_people as ppl, btree_customers  as cus,btree_sales as sa where sa.sales_status = 1 and ppl.person_id = cus.person_id and sa.customer_id = cus.person_id ) as e on e.sale_id = s.sale_id");

            if (is_null($supplier))
            {
                return "failed";
            }

        }catch(Exception $e){
            throw GlobalResponse::clientErrorResponse("error");
        }

        return GlobalResponse::createResponse($supplier);  

    }

    public function getAllListRender(array $data){
        try{
            
            Log::info($data);
            $supplier = DB::select("select * from (select sa.sale_id,sa.customer_id,sit.item_id,it.item_name,sit.item_cost_price as cost_price, sit.item_unit_price as unit_price,sit.discount_percent as discount, SUM(iit.percent) as percent,group_concat(iit.tax_name,' ',iit.percent) as tax_group, sit.quantity_purchased as quantity from btree_sales  as sa, btree_sales_items as sit,btree_item_taxes as iit,btree_item as it where sit.sales_id = sa.sale_id and sale_id = ".$data['id']." and sit.item_id = iit.item_id and it.item_id = sit.item_id) as s left join (select CONCAT(ppl.first_name,' ',ppl.last_name) as customer_name, sa.sale_id from btree_people as ppl, btree_customers  as cus,btree_sales as sa where sa.sale_id = ".$data['id']." and ppl.person_id = cus.person_id and sa.customer_id = cus.person_id ) as e on e.sale_id = s.sale_id");

            if (is_null($supplier))
            {
                return "failed";
            }

        }catch(Exception $e){
            throw GlobalResponse::clientErrorResponse("error");
        }

        return GlobalResponse::createResponse($supplier);  

    }


    

}?>

