<?php
namespace App\Repositories;
use App\Models\ExpenseCategory;
use Illuminate\Support\Facades\DB;
use App\Util\BLAlphaNumericCodeGenerator;
use Log;
use App\Response\GlobalResponse;

class ExpenseCategoryRepository
{
     private static  $RECORDS_PER_PAGE =2;

     public function __construct() {
        self::$RECORDS_PER_PAGE = config('blconstants.RECORDS_PER_PAGE');
    }
    public function save(array $data)
    {    DB::beginTransaction();
         try {
            $expense_category = new  ExpenseCategory;
            $expense_category->fill($data);
            $expense_category->save();
            DB::commit();
        } catch(Exception $e) {
            DB::rollback();
            return GlobalResponse::clientErrorResponse("error");
        }
        
         return GlobalResponse::createResponse($expense_category);
    }

    public function update(array $data){
        DB::beginTransaction();
        try{Log::info($data);
            $expense_category = ExpenseCategory::where("expense_category_id",$data['expense_category_id'])->first(); 
                       
            $expense_category->fill($data);
            $expense_category->save();
            DB::commit();
        }catch(Exception $e) {
            DB::rollback();
            return GlobalResponse::clientErrorResponse("error");
        }
        
         return GlobalResponse::createResponse($expense_category);
    }


    public function deleteAll(){
        DB::beginTransaction();
        try{
            $expense_category = ExpenseCategory::where('deleted', '=', 0)->update(['deleted' => 1]);
             DB::commit();
        }catch(Exception $e) {
            DB::rollback();
            return GlobalResponse::clientErrorResponse("error");
        }
        
         return GlobalResponse::createResponse($expense_category);
    }

    public function storeAll(){
        try{
            $expense_category = ExpenseCategory::where('deleted', '=', 1)->update(['deleted' => 0]);
             
        }catch(Exception $e){
            return GlobalResponse::clientErrorResponse("error");
        }

         return GlobalResponse::createResponse($expense_category);
    }


    public function getAll(){
        try{
            
            $expense_category = DB::table('btree_expense_category as com')
            ->where('com.deleted','=','0')
            ->select('com.*')
            ->Paginate(self::$RECORDS_PER_PAGE);

        }catch(Exception $e){
            return GlobalResponse::clientErrorResponse("error");
        }

         return GlobalResponse::createResponse($expense_category);

    }

    public function getAlltrash(){
        try{
            
            $expense_category = DB::table('btree_expense_category as com')
            ->where('com.deleted','=','1')
            ->select('com.*')
            ->Paginate(self::$RECORDS_PER_PAGE);

        }catch(Exception $e){
            return GlobalResponse::clientErrorResponse("error");
        }

         return GlobalResponse::createResponse($expense_category);

    }

    public function getById($data){
        try{
            $expense_category = DB::table('btree_expense_category as com')
            ->where('com.expense_category_id','=',$data)
            ->select('com.*')
            ->get();
           

        }catch(Exception $e){
            return GlobalResponse::clientErrorResponse("error");
        }
        Log::info($expense_category);
        return GlobalResponse::createResponse($expense_category);
    }

    public function deleteexpense_category($data){
        DB::beginTransaction();
      try{
           $expense_category = ExpenseCategory::where('expense_category_id','=',$data)->first();
           if($expense_category->deleted==1){
            $expense_category->deleted = 0;
            $expense_category->save();
           }
           else{
            $expense_category->deleted = 1;
            $expense_category->save();
           }
           DB::commit();
       }catch(Exception $e) {
            DB::rollback();
            return GlobalResponse::clientErrorResponse("error");
        }
        
         return GlobalResponse::createResponse($expense_category);
    }

   public function search($data){
        try{
             Log::info('Some message here.');

            $sql = " expense_category_name like '%".$data."%' and deleted = 0 ";

             $expense_category = DB::table('btree_expense_category as com')
            ->whereRaw($sql)
            ->Paginate(self::$RECORDS_PER_PAGE);
            Log::info($expense_category);
        }catch(Exception $e){
           return GlobalResponse::clientErrorResponse("error");
        }

         return GlobalResponse::createResponse($expense_category);

    }

     public function getList(){
        try{
            
            $expense_category = DB::table('btree_expense_category as com')
            ->where('com.deleted','=','0')
            ->select('com.*')
            ->get();
            
        }catch(Exception $e){
            return GlobalResponse::clientErrorResponse("error");
        }

         return GlobalResponse::createResponse($expense_category);
  }  

      public function getAllList(){
        try{
            

            $expense = DB::table('btree_expense_category as exp')
            //->leftjoin('btree_expense_category as exc', 'exp.expense_category_id','=','exc.expense_category_id')
            ->where('exp.deleted','=','0')
            ->select('exp.expense_category_id')
            ->get();
            if (is_null($expense))
            {
                return "failed";
            }

        }catch(Exception $e){
            throw GlobalResponse::clientErrorResponse("error");
        }

        return GlobalResponse::createResponse($expense);  

    }

    public function deleteSelectExpenseCategory(array $data){
        DB::beginTransaction();
      try{

        foreach ($data as $value) {
            $expense_category = ExpenseCategory::where('expense_category_id','=',$value)->first();
            $expense_category->deleted = 1;
            $expense_category->save();
        }
        DB::commit();
       }catch(Exception $e){
            DB::rollback();
            return GlobalResponse::clientErrorResponse("error");
       }
        
        return GlobalResponse::createResponse("success");
   }

   public function addSelectExpenseCategory(array $data){
    DB::beginTransaction();
      try{

        foreach ($data as $value) {
            $expense_category = ExpenseCategory::where('expense_category_id','=',$value)->first();
            $expense_category->deleted = 0;
            $expense_category->save();
        }
        DB::commit();
       }catch(Exception $e){
            DB::rollback();
            return GlobalResponse::clientErrorResponse("error");
       }
        
        return GlobalResponse::createResponse("success");
   }




} ?>

