<?php
namespace App\Repositories;
use App\Models\Supplier;
use App\Models\Person;
use Illuminate\Support\Facades\DB;
use App\Util\BLAlphaNumericCodeGenerator;
use Log;
use App\Response\GlobalResponse;

class SupplierRepository
{
     private static  $RECORDS_PER_PAGE =2;

     public function __construct() {
        self::$RECORDS_PER_PAGE = config('blconstants.RECORDS_PER_PAGE');
    }
    public function save(array $data)
    {
         DB::beginTransaction();
         try {
            $person = new Person;
            $person->fill($data);
            $person->save();
            $supplier = new  Supplier;
            $supplier['person_id'] = $person->person_id;
            $supplier->fill($data);
            $supplier->save();
            DB::commit();
        } catch(Exception $e) {
            DB::rollback();
            throw GlobalResponse::clientErrorResponse("error");
        }
        
        return GlobalResponse::createResponse($supplier);  
    }

    public function update(array $data){
        DB::beginTransaction();
        try{
             $supplier = Supplier::where("person_id",$data['person_id'])->first(); 
            
            if (is_null($supplier)){
                return "failed";
            }
            $person = Person::where("person_id",$data['person_id'])->first();
            $person->fill($data);
            $person->save();
            
            $supplier->fill($data);
            $supplier->save();
            DB::commit();
        }catch(Exception $e) {
            DB::rollback();
            throw GlobalResponse::clientErrorResponse("error");
        }
        
        return GlobalResponse::createResponse($supplier);  
    }


    public function deleteAll(){
        try{
            $msg = Supplier::where('deleted', '=', 0)->update(['deleted' => 1]);
             
        }catch(Exception $e){
            throw GlobalResponse::clientErrorResponse("error");
        }

        return GlobalResponse::createResponse($msg);  
    }

    public function storeAll(){
        try{
            $msg = Supplier::where('deleted', '=', 1)->update(['deleted' => 0]);
             
        }catch(Exception $e){
            throw GlobalResponse::clientErrorResponse("error");
        }

        return GlobalResponse::createResponse($msg);
    }

    public function getAlltrash(){
        try{
            
            $supplier = DB::table('btree_people as ppl')
            ->leftjoin('btree_suppliers as cus','ppl.person_id','=','cus.person_id')
            ->where('cus.deleted','=','1')
            ->select('ppl.*','cus.*')
            ->Paginate(self::$RECORDS_PER_PAGE);
            if (is_null($supplier))
            {
                return "failed";
            }

        }catch(Exception $e){
            throw GlobalResponse::clientErrorResponse("error");
        }

        return GlobalResponse::createResponse($supplier);

    }
 

    public function getAll(){
        try{
            

            $supplier = DB::table('btree_people as ppl')
            ->leftjoin('btree_suppliers as sup','ppl.person_id','=','sup.person_id')
            ->where('sup.deleted','=','0')
            ->select('ppl.*','sup.*')
            ->Paginate(self::$RECORDS_PER_PAGE);
            if (is_null($supplier))
            {
                return "failed";
            }

        }catch(Exception $e){
            throw GlobalResponse::clientErrorResponse("error");
        }

        return GlobalResponse::createResponse($supplier);  

    }

    public function getById(array $data){
        try{
           
            $supplier = DB::table('btree_people as ppl')
            //->where('ppl.person_id','=',$data['person_id'])
            ->join('btree_suppliers as sup','ppl.person_id','=','sup.person_id')
            ->select('ppl.*','sup.*')
            ->get();

            if (is_null($supplier)){
                return "failed";
            }

        }catch(Exception $e){
            throw GlobalResponse::clientErrorResponse("error");
        }

        return GlobalResponse::createResponse($supplier);  
    }

    public function deletesupplier($data){
        DB::beginTransaction();
      try{
           $supplier = Supplier::where('person_id','=',$data)->first();
           if($supplier->deleted == 1){
            $supplier->deleted = 0;
           $supplier->save();
           }
           else{
            $supplier->deleted = 1;
           $supplier->save();
           }        
           DB::commit();
       }catch(Exception $e) {
            DB::rollback();
            throw GlobalResponse::clientErrorResponse("error");
        }
        
        return GlobalResponse::createResponse($supplier);  
    }


   public function search($data){
        try{
             Log::info('Some message here.');

            $sql = "   ( ppl.first_name like '%".$data."%' or ppl.last_name like '%".$data."%' or ppl.phone_number like '%".$data."%' ) and sup.deleted = 0 and ppl.person_id = sup.person_id";

             $supplier = DB::table('btree_people as ppl')
            ->leftjoin('btree_suppliers as sup','ppl.person_id','=','sup.person_id')
            ->whereRaw($sql)
            ->Paginate(self::$RECORDS_PER_PAGE);
             // $supplier = supplier::whereRaw($sql)->get();
            if (is_null($supplier))
            {
                return "failed";
            }

        }catch(Exception $e){
            throw GlobalResponse::clientErrorResponse("error");
        }

        return GlobalResponse::createResponse($supplier);  

    }

    public function getList($data){
       try{
           
        
           $supplier = DB::select("SELECT ppl.person_id,ppl.first_name,ppl.first_name as label,ppl.last_name from btree_suppliers as sup, btree_people as ppl where  ppl.person_id = sup.person_id  and sup.deleted = 0 and (ppl.first_name like '%".$data ."%' or ppl.last_name like '%".$data ."%' )");
          

           if (is_null($supplier))
           {
               return "failed";
           }       

         }
       catch(Exception $e){
           throw GlobalResponse::clientErrorResponse("error");
       }        
       return GlobalResponse::createResponse($supplier);  
  }  


    public function getAllList(){
        try{
            

            $supplier = DB::table('btree_people as ppl')
            ->leftjoin('btree_suppliers as sup','ppl.person_id','=','sup.person_id')
            ->where('sup.deleted','=','0')
            ->select('ppl.person_id')
            ->selectRaw('CONCAT(ppl.first_name , ppl.last_name) as supplier_name')
            ->get();
            if (is_null($supplier))
            {
                return "failed";
            }

        }catch(Exception $e){
            throw GlobalResponse::clientErrorResponse("error");
        }

        return GlobalResponse::createResponse($supplier);  

    }

    public function deleteSelectSupplier(array $data){
        DB::beginTransaction();
      try{

        foreach ($data as $value) {
            $supplier = Supplier::where('person_id','=',$value)->first();
            $supplier->deleted = 1;
            $supplier->save();
        }
        DB::commit();
       }catch(Exception $e){
        DB::rollback();
           return GlobalResponse::clientErrorResponse("error");
       }
       
        return GlobalResponse::createResponse("success");
   }

   public function addSelectSupplier(array $data){
    DB::beginTransaction();
      try{

        foreach ($data as $value) {
            $supplier = Supplier::where('person_id','=',$value)->first();
            $supplier->deleted = 0;
            $supplier->save();
        }
        DB::commit();
       }catch(Exception $e){
        DB::rollback();
           return GlobalResponse::clientErrorResponse("error");
       }
       
        return GlobalResponse::createResponse("success");
   }


} ?>

