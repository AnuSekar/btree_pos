<?php
namespace App\Repositories;
use App\Models\Item;
use App\Models\Item_Tax;
use App\Models\Sales;
use App\Models\SalesItem;
use App\Models\SalesItemTax;
use App\Models\SalesTax;
use App\Models\SalesPayment;
use App\Models\ItemQuantity;
use App\Models\Transaction;
use App\Models\CustomerPoints;
use App\Models\Customer;
use Illuminate\Support\Facades\DB;
use App\Util\BLAlphaNumericCodeGenerator;
use Log;
use App\Response\GlobalResponse;

class SalesRepository
{
     private static  $RECORDS_PER_PAGE =2;

     public function __construct() {
        self::$RECORDS_PER_PAGE = config('blconstants.RECORDS_PER_PAGE');
    }
    public function save(array $datas)
    {
            DB::beginTransaction();
         try {
            Log::info($datas);
            $sales = new Sales();
            $sales['employee_id'] = 1; 
            $sales['invoice_number'] = 1;
            $sales['sales_status'] = 0;
            $sales['sales_type'] = $datas['sales_type'];
            if($datas['customer'] != ''){
                $sales['customer_id'] = $datas['customer'];
            }
            $sales->save();
            $x =1;
            $points = 0;
            $taxsale =$datas['sales_tax'];
            Log::info($taxsale);
            $subtotal = 0;
            foreach($datas['item'] as $data){
                Log::info($data);
                $item = Item::where("item_id",$data['item_id'])->first(); 
                $salesitem = new SalesItem();
                
                $salesitem['sales_id'] = $sales->sale_id;
                $salesitem['item_id'] = $data['item_id'];
                $salesitem['line'] = $x;
                $salesitem['quantity_purchased'] = $data['item_quantity'];
                $salesitem['item_cost_price'] = $item['cost_price'];
                $points = $points + (($item['commission'] * $item['cost_price'] )/100);
                $salesitem['item_unit_price'] = $item['unit_price'];
                if( $data['item_discount'] != ''){
                    $salesitem['discount_percent'] = $data['item_discount'];
                    $discount= (($item['cost_price']*$data['item_discount'])/100);
                    $discountprice = $item['cost_price'] - $discount;
                    $totalprice = $discountprice * $data['item_quantity'];
                }else{
                    $totalprice = $item['cost_price']*$data['item_quantity'];
                }
                $salesitem['item_location'] = 1;
                $salesitem->save();
                $x++;
                $salestax['sales_id']=$sales->sale_id;

                $quantity= ItemQuantity::where("item_id",$data['item_id'])->first();
                $itemquantity= $quantity['quantity']-$data['item_quantity'];
                $salesquantity = ItemQuantity::where("item_id",$data['item_id'])->update(['quantity' => $itemquantity]);
                $trans_data = new Transaction();
                $trans_data['trans_items'] = $data['item_id'];
                $trans_data['trans_user'] = 1;
                $trans_data['trans_location'] = 1;
                $trans_data['trans_inventory'] = -$data['item_quantity'];
                $trans_data->save();

                $itemtax = Item_Tax::where("item_id",$data['item_id'])->get();
                $subtotal = $subtotal + $totalprice;
                foreach($itemtax as $tax){
                    $salesitemtax = new SalesItemTax();
                    $salesitemtax['percent']=$tax->percent;
                    $salesitemtax['name']=$tax->tax_name;
                    $taxgrpname = $tax->tax_name.' '.$tax->percent;
                    $taxsale[$taxgrpname] = $taxsale[$taxgrpname]+ (($totalprice * $tax->percent)/100);
                    Log::info($taxsale);
                    $salesitemtax['item_id']=$data['item_id'];
                    $salesitemtax['sale_id']=$sales->sale_id;
                    $salesitemtax['line']=$salesitem->line;
                    $salesitemtax['item_tax_amount']=(($totalprice * $tax->percent)/100);
                    $salesitemtax->save();
                }

            }
                $salespayment = new SalesPayment();
                $salespayment['sale_id'] = $sales->sale_id;
                $salespayment['payment_type'] =$datas['paymenttype'];
                $salespayment['payment_amount'] =$totalprice;
                $salespayment->save();
                $y=0;
                foreach ($taxsale as $key => $value) {
                    $salestax = new SalesTax();
                    $salestax['sales_id']=$sales->sale_id; 
                    $splittax = explode(" ",$key);
                    $salestax['tax_group'] = $key;
                    $salestax['sales_tax_basis']= $subtotal;
                    $salestax['sales_tax_amount'] = $value;
                    $salestax['print_sequence']= $y;
                    $salestax['name'] = $splittax[0];
                    $salestax['tax_rate'] = $splittax[1];
                    $salestax->save();
                    $y++;
                }

                if($datas['customer'] != ''){
                $customerpoints = new CustomerPoints();
                $customerpoints['sales_id'] = $sales->sale_id;
                $customerpoints['customer_id'] =$datas['customer'];
                $customerpoints['points_earned'] =$points;
                $customerpoints->save();

                $customer =Customer::where("person_id",$datas['customer'])->first();
                $customer['points'] = $customer['points'] + $points;
                $customer->save();
                }
           DB::commit();
        } catch(Exception $e) {
            DB::rollback();
             return GlobalResponse::clientErrorResponse("error");
        }
        
       return GlobalResponse::createResponse("success");
    }

    public function return(array $datas)
    {
        DB::beginTransaction();
        try {
          $sales = new Sales();
            //$sales->fill($datas);
            $sales['employee_id'] = 1; 
            $sales['invoice_number'] = 1;
            $sales['sales_status'] = 0;
            if($datas['customer'] != ''){
                $sales['customer_id'] = $datas['customer'];
            }
            $sales['sales_type'] = $datas['sales_type'];
            $sales->save();
            $x =1;
            $points = 0;
            $taxsale =$datas['sales_tax'];
            Log::info($taxsale);
            $subtotal = 0;
            foreach($datas['item'] as $data){
                Log::info($data);
                $item = Item::where("item_id",$data['item_id'])->first(); 
                $salesitem = new SalesItem();
                
                $salesitem['sales_id'] = $sales->sale_id;
                $salesitem['item_id'] = $data['item_id'];
                $salesitem['line'] = $x;
                $salesitem['discount_percent'] = $data['item_discount'];
                $salesitem['quantity_purchased'] = -$data['item_quantity'];
                $salesitem['item_cost_price'] = $item['cost_price'];
                $points = $points + (($item['commission'] * $item['cost_price'] )/100);
                $salesitem['item_unit_price'] = $item['unit_price'];
                if( $data['item_discount'] != ''){
                    $salesitem['discount_percent'] = $data['item_discount'];
                    $discount= (($item['cost_price']*$data['item_discount'])/100);
                    $discountprice = $item['cost_price'] - $discount;
                    $totalprice = $discountprice * $data['item_quantity'];
                }else{
                    $totalprice = $item['cost_price']*$data['item_quantity'];
                }
                $salesitem['item_location'] = 1;
                $salesitem->save();
                $x++;
                $salestax['sales_id']=$sales->sale_id;

                $quantity= ItemQuantity::where("item_id",$data['item_id'])->first();
                $itemquantity= $quantity['quantity']-$data['item_quantity'];
                $salesquantity = ItemQuantity::where("item_id",$data['item_id'])->update(['quantity' => $itemquantity]);
                $trans_data = new Transaction();
                $trans_data['trans_items'] = $data['item_id'];
                $trans_data['trans_user'] = 1;
                $trans_data['trans_location'] = 1;
                $trans_data['trans_inventory'] = $data['item_quantity'];
                $trans_data->save();

                $itemtax = Item_Tax::where("item_id",$data['item_id'])->get();
                $subtotal = $subtotal + $totalprice;
                foreach($itemtax as $tax){
                    $salesitemtax = new SalesItemTax();
                    $salesitemtax['percent']=$tax->percent;
                    $salesitemtax['name']=$tax->tax_name;
                    $taxgrpname = $tax->tax_name.' '.$tax->percent;
                    $taxsale[$taxgrpname] = $taxsale[$taxgrpname]+ (($totalprice * $tax->percent)/100);
                    Log::info($taxsale);
                    $salesitemtax['item_id']=$data['item_id'];
                    $salesitemtax['sale_id']=$sales->sale_id;
                    $salesitemtax['line']=$salesitem->line;
                    $salesitemtax['item_tax_amount']=-(($totalprice * $tax->percent)/100);
                    $salesitemtax->save();
                }

            }
                $salespayment = new SalesPayment();
                $salespayment['sale_id'] = $sales->sale_id;
                $salespayment['payment_type'] =$datas['paymenttype'];
                $salespayment['payment_amount'] = -$totalprice ;
                $salespayment->save();
                $y=0;
                foreach ($taxsale as $key => $value) {
                    $salestax = new SalesTax();
                    $salestax['sales_id']=$sales->sale_id; 
                    $splittax = explode(" ",$key);
                    $salestax['tax_group'] = $key;
                    $salestax['sales_tax_basis']= -$subtotal;
                    $salestax['sales_tax_amount'] = -$value;
                    $salestax['print_sequence']= $y;
                    $salestax['name'] = $splittax[0];
                    $salestax['tax_rate'] = $splittax[1];
                    $salestax->save();
                    $y++;
                }

                if($datas['customer'] != ''){
                $customerpoints = new CustomerPoints();
                $customerpoints['sales_id'] = $sales->sale_id;
                $customerpoints['customer_id'] =$datas['customer_id'];
                $customerpoints['points_earned'] = - $points;
                $customerpoints->save();

                 $customer =Customer::where("person_id",$datas['customer'])->first();
                $customer['points'] = $customer['points'] - $points;
                $customer->save();
               
                }
         DB::commit();  
        } catch(Exception $e) {
            DB::rollback();
             return GlobalResponse::clientErrorResponse("error");
        }
        
       return GlobalResponse::createResponse("success");
    }

    
    public function deleteAll(){
        try{
            $msg = sales::where('deleted', '=', 0)->update(['deleted' => 1]);
             
        }catch(Exception $e){
            throw $e;
        }

        return $msg;
    }

    

    public function getAll(){
        try{
            
            $sales = DB::table('btree_sales as it')
            ->join('btree_sales_taxes as iit','it.sales_id','=','iit.sales_id')
            ->distinct()
            ->select('it.*','it.*')
            ->where('it.deleted','=','0')
            ->simplePaginate(self::$RECORDS_PER_PAGE);
            if (is_null($sales))
            {
                return "failed";
            }

        }catch(Exception $e){
            throw $e;
        }

        return $sales;

    }

    public function getById(array $data){
        try{
            // $sales = DB::table('btree_sales as it')
            // ->where('it.sales_id','=',$data['sales_id'])
            // ->join('btree_sales_taxes as iit','it.sales_id','=','iit.sales_id')
            // ->select(DB::raw('group_concat(iit.tax_name) taxname'))
            // ->groupBy('it.sales_id')
            // ->get();

            $sales = DB::select("SELECT Distinct it.*, group_concat(iit.tax_name) as taxname , group_concat(iit.percent) as percent FROM btree_sales as it,btree_sales_taxes as iit WHERE it.deleted = 0 and it.sales_id = iit.sales_id and it.sales_id= " .$data['sales_id']. " group by it.sales_id");


            if (is_null($sales)){
                return "failed";
            }

        }catch(Exception $e){
            throw $e;
        }

        return $sales;
    }

    public function deletesales(array $data){
      try{

            Log::info("deleted the sales >>>>>>>>>>>>>>");
           $sales = sales::where('sales_id',$data['sales_id'])->first();
           $sales->deleted = 1;
           $sales->save();
       }catch(Exception $e){
           throw $e;
       }
       return $sales;
   }

   public function search($data){
        try{
             Log::info('Some message here.');

            $sql = " sales_name like '%".$data."%' and deleted = 0 ";
             Log::info($sql);

             $sales = DB::table('btree_sales as it')
            ->whereRaw($sql)
            ->simplePaginate(self::$RECORDS_PER_PAGE);
            // $sales = sales::whereRaw($sql)->get();
          //  $sales = DB::Select($sql);
            Log::info($sales);
            if (is_null($sales))
            {
                return "failed";
            }

        }catch(Exception $e){
            throw $e;
        }

        return $sales;

    }

    public function salebyitem($data){
        try{

            $sales = DB::select("SELECT it.item_name,it.item_name as label,it.item_id,it.cost_price,it.unit_price,group_concat(tax_name,' ',percent) as tax_group,SUM(percent) as percent from btree_item as it, btree_item_taxes as iit where it.item_id = iit.item_id and it.deleted = 0 and it.item_name like '%".$data ."%' group by it.item_id");

            $kitsales = DB::select("SELECT * from (SELECT  ikt.name as label,ikt.item_kit_id as id,it.item_name, it.cost_price from btree_item_kits as ikt left join btree_item_kit_items as iki on ikt.item_kit_id = iki.item_kit_id left join btree_item as it on it.item_id = iki.item_id where ikt.name like '%".$data ."%' ) as d left join (SELECT ikt.item_kit_id as id, group_concat(tax_name,' ',percent) as tax_group ,iki.item_id,SUM(percent) as percent ,it.item_name, it.cost_price from btree_item_kits as ikt left join btree_item_kit_items as iki on ikt.item_kit_id = iki.item_kit_id left join btree_item_taxes as iit on iit.item_id = iki.item_id left join btree_item as it on it.item_id = iki.item_id where ikt.name like '%".$data ."%' group by iki.item_id,ikt.item_kit_id) as s on s.id = d.id GROUP By d.id,s.item_id");

            //SELECT * from (SELECT  ikt.name as label,ikt.item_kit_id as id from btree_item_kits as ikt left join btree_item_kit_items as iki on ikt.item_kit_id = iki.item_kit_id left join btree_item as it on it.item_id = iki.item_id ) as d left join (SELECT ikt.item_kit_id as id, group_concat(tax_name,' ',percent) as tax_group ,iki.item_id,SUM(percent) as percent from btree_item_kits as ikt left join btree_item_kit_items as iki on ikt.item_kit_id = iki.item_kit_id left join btree_item_taxes as iit on iit.item_id = iki.item_id group by iki.item_id,ikt.item_kit_id) as s on s.id = d.id GROUP By d.id,s.item_id

           // Log::info($kitsales);
           $sales = array_merge($sales,$kitsales );
            Log::info($sales);
            if (is_null($sales))
            {
                return "failed";
            }

        }catch(Exception $e){
            throw $e;
        }

        return $sales;

    }



} ?>

