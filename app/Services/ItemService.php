<?php
namespace App\Services;
use App\Repositories\ItemRepository;

class ItemService{

    private $itemRepository;

    public function __construct(ItemRepository $itemRepository) {
         $this->itemRepository = $itemRepository;
     }

    public function save(array $data){

        $result = $this->itemRepository->save($data);
        return $result;
    }

    public function update(array $data){
        $result = $this->itemRepository->update($data);
        return $result;
    }

    public function deleteAll(){
         $result = $this->itemRepository->deleteAll();
        return $result;
    }

    public function storeAll(){
         $result = $this->itemRepository->storeAll();
        return $result;
    }


    public function getAll(){
        return $this->itemRepository->getAll();
    }

   public function getAlltrash(){
        return $this->itemRepository->getAlltrash();
   }

    public function getById($data){
        $result = $this->itemRepository->getById($data);
        return $result;
    }

    public function deleteitem($data){
         $result = $this->itemRepository->deleteitem($data);
        return $result;
    }
    public function search($data){
        return $this->itemRepository->search($data);
   }

   public function deleteSelectItem(array $data){
        return $this->itemRepository->deleteSelectItem($data);
   }
   
   public function addSelectItem(array $data){
        return $this->itemRepository->addSelectItem($data);
   }
  
}

?>
