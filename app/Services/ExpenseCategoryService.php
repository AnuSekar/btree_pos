<?php
namespace App\Services;
use App\Repositories\ExpenseCategoryRepository;

class ExpenseCategoryService{

    private $expense_categoryRepository;

    public function __construct(ExpenseCategoryRepository $expense_categoryRepository) {
         $this->expense_categoryRepository = $expense_categoryRepository;
     }

    public function save(array $data){

        $result = $this->expense_categoryRepository->save($data);
        return $result;
    }

    public function update(array $data){
        $result = $this->expense_categoryRepository->update($data);
        return $result;
    }

    public function deleteAll(){
         $result = $this->expense_categoryRepository->deleteAll();
        return $result;
    }

    public function storeAll(){
         $result = $this->expense_categoryRepository->storeAll();
        return $result;
    }


    public function getAll(){
        return $this->expense_categoryRepository->getAll();
   }

   public function getAlltrash(){
        return $this->expense_categoryRepository->getAlltrash();
   }

    public function getById($data){
        $result = $this->expense_categoryRepository->getById($data);
        return $result;
    }

    public function deleteexpense_category($data){
         $result = $this->expense_categoryRepository->deleteexpense_category($data);
        return $result;
    }
    public function search($data){
        return $this->expense_categoryRepository->search($data);
   }
   public function Trashsearch($data){
        return $this->expense_categoryRepository->Trashsearch($data);
   }
   public function getList(){
        return $this->expense_categoryRepository->getList();
   }
   public function getAllList(){
        return $this->expense_categoryRepository->getAllList();
   }
   public function deleteSelectExpenseCategory(array $data){
        return $this->expense_categoryRepository->deleteSelectExpenseCategory($data);
   }
   public function addSelectExpenseCategory(array $data){
        return $this->expense_categoryRepository->addSelectExpenseCategory($data);
   }
}

?>
