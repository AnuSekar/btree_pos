<?php
namespace App\Services;
use App\Repositories\EmployeeRepository;

class EmployeeService{

    private $employeeRepository;

    public function __construct(EmployeeRepository $employeeRepository) {
         $this->employeeRepository = $employeeRepository;
     }

    public function save(array $data){

        $result = $this->employeeRepository->save($data);
        return $result;
    }

    public function update(array $data){
        $result = $this->employeeRepository->update($data);
        return $result;
    }

    public function deleteAll(){
         $result = $this->employeeRepository->deleteAll();
        return $result;
    }


    public function getAll(){
        return $this->employeeRepository->getAll();
   }

    public function getAlltrash(){
       $employee = $this->employeeRepository->getAlltrash();
       return $employee;
    }

   
   public function storeAll(){
         $result = $this->employeeRepository->storeAll();
        return $result;
    }

    public function getById($data){
        $result = $this->employeeRepository->getById($data);
        return $result;
    }
    public function employeebysales($data){
        $result = $this->employeeRepository->employeebysales($data);
        return $result;
    }

    public function deleteemployee($data){
         $result = $this->employeeRepository->deleteemployee($data);
        return $result;
    }
    public function search($data){
        return $this->employeeRepository->search($data);
   }
   public function searchTrash($data){
        return $this->employeeRepository->searchTrash($data);
   }
   public function deleteSelectEmployee(array $data){
        return $this->employeeRepository->deleteSelectEmployee($data);
   }
   public function addSelectEmployee(array $data){
        return $this->employeeRepository->addSelectEmployee($data);
   }
  
}

?>