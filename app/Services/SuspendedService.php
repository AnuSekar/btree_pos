<?php
namespace App\Services;
use App\Repositories\SuspendedRepository;

class SuspendedService{

    private $salesRepository;

    public function __construct(SuspendedRepository $suspendedRepository) {
         $this->suspendedRepository = $suspendedRepository;
     }

    public function save(array $data){

        $result = $this->suspendedRepository->save($data);
        return $result;
    }

    public function getAllList(){

        $result = $this->suspendedRepository->getAllList();
        return $result;
    }

    public function getAllListRender($data){

        $result = $this->suspendedRepository->getAllListRender($data);
        return $result;
    }
   
}

?>
