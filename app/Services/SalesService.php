<?php
namespace App\Services;
use App\Repositories\SalesRepository;

class SalesService{

    private $salesRepository;

    public function __construct(SalesRepository $salesRepository) {
         $this->salesRepository = $salesRepository;
     }

    public function save(array $data){

        $result = $this->salesRepository->save($data);
        return $result;
    }

     public function return(array $data){

        $result = $this->salesRepository->return($data);
        return $result;
    }

    public function deleteAll(){
         $result = $this->salesRepository->deleteAll();
        return $result;
    }


    public function getAll(){
        return $this->salesRepository->getAll();
   }

    public function getById($data){
        $result = $this->salesRepository->getById($data);
        return $result;
    }

    public function deletesales($data){
         $result = $this->salesRepository->deletesales($data);
        return $result;
    }
    public function search($data){
        return $this->salesRepository->search($data);
   }

   public function salebyitem($data){
        return $this->salesRepository->salebyitem($data);

        
   }
  
}

?>
