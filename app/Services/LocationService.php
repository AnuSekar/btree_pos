<?php
namespace App\Services;
use App\Repositories\LocationRepository;

class LocationService{

    private $locationRepository;

    public function __construct(LocationRepository $locationRepository) {
         $this->locationRepository = $locationRepository;
     }

    public function save(array $data){

        $result = $this->locationRepository->save($data);
        return $result;
    }

    public function update(array $data){
        $result = $this->locationRepository->update($data);
        return $result;
    }

    public function deleteAll(){
         $result = $this->locationRepository->deleteAll();
        return $result;
    }


    public function getAll(){
        return $this->locationRepository->getAll();
   }

   public function getAlltrash(){
       $location = $this->locationRepository->getAlltrash();
       return $location;
    }

   
   public function storeAll(){
         $result = $this->locationRepository->storeAll();
        return $result;
    }


    public function getById($data){
        $result = $this->locationRepository->getById($data);
        return $result;
    }

    public function deletelocation($data){
         $result = $this->locationRepository->deletelocation($data);
        return $result;
    }
    public function search($data){
        return $this->locationRepository->search($data);
   }
   public function getList(){
        return $this->locationRepository->getList();
   }
   public function deleteSelectLocation(array $data){
        return $this->locationRepository->deleteSelectLocation($data);
   }
   public function addSelectLocation(array $data){
        return $this->locationRepository->addSelectLocation($data);
   }
}

?>
