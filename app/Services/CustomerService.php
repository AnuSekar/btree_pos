<?php
namespace App\Services;
use App\Repositories\CustomerRepository;

class CustomerService{

    private $customerRepository;

    public function __construct(CustomerRepository $customerRepository) {
         $this->customerRepository = $customerRepository;
     }

    public function save(array $data){

        $result = $this->customerRepository->save($data);
        return $result;
    }

    public function update(array $data){
        $result = $this->customerRepository->update($data);
        return $result;
    }

    public function deleteAll(){
         $result = $this->customerRepository->deleteAll();
        return $result;

    }


    public function getAll(){
        return $this->customerRepository->getAll();
   }

    public function getById($data){
        $result = $this->customerRepository->getById($data);
        return $result;
    }

    public function getAlltrash(){
       $customer = $this->customerRepository->getAlltrash();
       return $customer;
    }

   
   public function storeAll(){
         $result = $this->customerRepository->storeAll();
        return $result;
    }


    public function deletecustomer($data){
         $result = $this->customerRepository->deletecustomer($data);
        return $result;
    }
    public function search($data){
        return $this->customerRepository->search($data);
   }
    public function customerbysales($data){
        return $this->customerRepository->customerbysales($data);

        
   }
   public function deleteSelectCustomer(array $data){
        return $this->customerRepository->deleteSelectCustomer($data);
   }
   public function addSelectCustomer(array $data){
        return $this->customerRepository->addSelectCustomer($data);
   }
   public function CustomerPointsClaim(array $data){
        return $this->customerRepository->CustomerPointsClaim($data);
   }
}

?>
