<?php
namespace App\Services;
use App\Repositories\ExpenseRepository;

class ExpenseService{

    private $expenseRepository;

    public function __construct(ExpenseRepository $expenseRepository) {
         $this->expenseRepository = $expenseRepository;
     }

    public function save(array $data){

        $result = $this->expenseRepository->save($data);
        return $result;
    }

    public function update(array $data){
        $result = $this->expenseRepository->update($data);
        return $result;
    }

    public function deleteAll(){
         $result = $this->expenseRepository->deleteAll();
        return $result;
    }

    public function storeAll(){
         $result = $this->expenseRepository->storeAll();
        return $result;
    }

    public function getAll(){
        return $this->expenseRepository->getAll();
   }

   public function getAlltrash(){
        return $this->expenseRepository->getAlltrash();
   }

    public function getById($data){
        $result = $this->expenseRepository->getById($data);
        return $result;
    }

    public function deleteexpense($data){
         $result = $this->expenseRepository->deleteexpense($data);
        return $result;
    }
    public function search($data){
        return $this->expenseRepository->search($data);
   }
   
   public function deleteSelectExpense(array $data){
        return $this->expenseRepository->deleteSelectExpense($data);
   }
   public function addSelectExpense(array $data){
        return $this->expenseRepository->addSelectExpense($data);
   }
  
}

?>
