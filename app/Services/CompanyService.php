<?php
namespace App\Services;
use App\Repositories\CompanyRepository;

class CompanyService{

    private $companyRepository;

    public function __construct(CompanyRepository $companyRepository) {
         $this->companyRepository = $companyRepository;
     }

    public function save(array $data){

        $result = $this->companyRepository->save($data);
        return $result;
    }

    public function update(array $data){
        $result = $this->companyRepository->update($data);
        return $result;
    }

    public function deleteAll(){
         $result = $this->companyRepository->deleteAll();
        return $result;
    }

    public function storeAll(){
         $result = $this->companyRepository->storeAll();
        return $result;
    }


    public function getAll(){
        return $this->companyRepository->getAll();
   }

   public function getAlltrash(){
        return $this->companyRepository->getAlltrash();
   }

    public function getById($data){
        $result = $this->companyRepository->getById($data);
        return $result;
    }

    public function deletecompany($data){
         $result = $this->companyRepository->deletecompany($data);
        return $result;
    }
    public function search($data){
        return $this->companyRepository->search($data);
   }
   public function Trashsearch($data){
        return $this->companyRepository->Trashsearch($data);
   }
   public function getList(){
        return $this->companyRepository->getList();
   }
    public function deleteSelectCompany(array $data){
        return $this->companyRepository->deleteSelectCompany($data);
   }
   public function addSelectCompany(array $data){
        return $this->companyRepository->addSelectCompany($data);
   }
   
}

?>
