<?php
namespace App\Services;
use App\Repositories\ProductRepository;

class productService{

    private $productRepository;

    public function __construct(ProductRepository $productRepository) {
         $this->productRepository = $productRepository;
     }

    public function save(array $data){

        $result = $this->productRepository->save($data);
        return $result;
    }

    public function update(array $data){
        $result = $this->productRepository->update($data);
        return $result;
    }

    public function deleteAll(){
         $result = $this->productRepository->deleteAll();
        return $result;
    }


    public function getAll(){
        return $this->productRepository->getAll();
   }
   public function storeAll(){
         $result = $this->productRepository->storeAll();
        return $result;
    }

    public function getAlltrash(){
        return $this->productRepository->getAlltrash();
   }


    public function getById($data){
        $result = $this->productRepository->getById($data);
        return $result;
    }

    public function deleteproduct($data){
         $result = $this->productRepository->deleteproduct($data);
        return $result;
    }
    public function search($data){
        return $this->productRepository->search($data);
   }
   public function getList(){
        return $this->productRepository->getList();
   }
   public function deleteSelectProduct(array $data){
        return $this->productRepository->deleteSelectProduct($data);
   }
   public function addSelectProduct(array $data){
        return $this->productRepository->addSelectProduct($data);
   }
}

?>
