<?php
namespace App\Services;
use App\Repositories\SupplierRepository;

class SupplierService{

    private $supplierRepository;

    public function __construct(SupplierRepository $supplierRepository) {
         $this->supplierRepository = $supplierRepository;
     }

    public function save(array $data){

        $result = $this->supplierRepository->save($data);
        return $result;
    }

    public function update(array $data){
        $result = $this->supplierRepository->update($data);
        return $result;
    }

    public function deleteAll(){
         $result = $this->supplierRepository->deleteAll();
        return $result;
    }


    public function getAll(){
        return $this->supplierRepository->getAll();
   }

    public function getAlltrash(){
       $supplier = $this->supplierRepository->getAlltrash();
       return $supplier;
    }

   
   public function storeAll(){
         $result = $this->supplierRepository->storeAll();
        return $result;
    }

    public function getById($data){
        $result = $this->supplierRepository->getById($data);
        return $result;
    }

    public function deletesupplier($data){
         $result = $this->supplierRepository->deletesupplier($data);
        return $result;
    }
    public function search($data){
        return $this->supplierRepository->search($data);
   }
   public function getList($data){
        return $this->supplierRepository->getList($data);
   }

   
    public function getAllList(){
        return $this->supplierRepository->getAllList();
   }

   public function deleteSelectSupplier(array $data){
        return $this->supplierRepository->deleteSelectSupplier($data);
   }
   public function addSelectSupplier(array $data){
        return $this->supplierRepository->addSelectSupplier($data);
   }
  
}

?>
