<?php
namespace App\Services;
use App\Repositories\ReceivingRepository;

class ReceivingService{

    private $salesRepository;

    public function __construct(ReceivingRepository $receivingRepository) {
         $this->receivingRepository = $receivingRepository;
     }

    public function save(array $data){

        $result = $this->receivingRepository->save($data);
        return $result;
    }

    public function return(array $data){

        $result = $this->receivingRepository->return($data);
        return $result;
    }
   
}

?>
