<?php
namespace App\Services;
use App\Repositories\ReportRepository;

class ReportService{


    public function __construct(ReportRepository $reportRepository) {
         $this->reportRepository = $reportRepository;
     }

    public function getCompany(array $data){

        $result = $this->reportRepository->getCompany($data);
        return $result;
    }

    public function getProduct(array $data){

        $result = $this->reportRepository->getProduct($data);
        return $result;
    }

    public function getCustomer(array $data){

        $result = $this->reportRepository->getCustomer($data);
        return $result;
    }

    public function getSupplier(array $data){

        $result = $this->reportRepository->getSupplier($data);
        return $result;
    }

    public function getItem(array $data){

        $result = $this->reportRepository->getItem($data);
        return $result;
    }

    public function getExpense(array $data){

        $result = $this->reportRepository->getExpense($data);
        return $result;
    }

    public function getSales(array $data){

        $result = $this->reportRepository->getSales($data);
        return $result;
    }

    public function getSuspendedSales(array $data){

        $result = $this->reportRepository->getSuspendedSales($data);
        return $result;
    }

    public function getReceiving(array $data){

        $result = $this->reportRepository->getReceiving($data);
        return $result;
    }

    public function getDiscount(array $data){

        $result = $this->reportRepository->getDiscount($data);
        return $result;
    }

    public function getPayment(array $data){

        $result = $this->reportRepository->getPayment($data);
        return $result;
    }

    public function getTax(array $data){

        $result = $this->reportRepository->getTax($data);
        return $result;
    }
    public function getEmployee(array $data){

        $result = $this->reportRepository->getEmployee($data);
        return $result;
    }
    public function getDetailSales(array $data){

        $result = $this->reportRepository->getDetailSales($data);
        return $result;
    }
    public function getSalesItem(array $data){

        $result = $this->reportRepository->getSalesItem($data);
        return $result;
    }
    public function getdetailsDiscount(array $data){

        $result = $this->reportRepository->getdetailsDiscount($data);
        return $result;
    }
    public function getDetailSupplier(array $data){

        $result = $this->reportRepository->getDetailSupplier($data);
        return $result;
    }

    public function getDetailEmployee(array $data){

        $result = $this->reportRepository->getDetailEmployee($data);
        return $result;
    }

    public function getDetailCustomer(array $data){

        $result = $this->reportRepository->getDetailCustomer($data);
        return $result;
    }

    public function getTransaction(array $data){

        $result = $this->reportRepository->getTransaction($data);
        return $result;
    }

    public function getItemkits(array $data){

        $result = $this->reportRepository->getItemkits($data);
        return $result;
    }

    public function getCommission(array $data){

        $result = $this->reportRepository->getCommission($data);
        return $result;
    }

    public function getLowInventory(array $data){

        $result = $this->reportRepository->getLowInventory($data);
        return $result;
    }

    public function getInventorySummary(array $data){

        $result = $this->reportRepository->getInventorySummary($data);
        return $result;
    }

    public function getDeadItem(array $data){

        $result = $this->reportRepository->getDeadItem($data);
        return $result;
    }
    
}

?>
