<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ReportService;
use Log;
class ReportController extends Controller
{

    public function __construct(ReportService $reportService) {
        $this->reportService = $reportService;
    }

    public function getCompany(Request $request){
        $report = $this->reportService->getCompany($request->all());
        return $report;
    }

    public function getProduct(Request $request){
        $report = $this->reportService->getProduct($request->all());
        return $report;
    }

    public function getCustomer(Request $request){
        $report = $this->reportService->getCustomer($request->all());
        return $report;
    }

    public function getSupplier(Request $request){
        $report = $this->reportService->getSupplier($request->all());
        return $report;
    }

    public function getItem(Request $request){
        $report = $this->reportService->getItem($request->all());
        return $report;
    }

    public function getExpense(Request $request){
        $report = $this->reportService->getExpense($request->all());
        return $report;
    }

    public function getSales(Request $request){
        $report = $this->reportService->getSales($request->all());
        return $report;
    }

    public function getSuspendedSales(Request $request){
        $report = $this->reportService->getSuspendedSales($request->all());
        return $report;
    }

    public function getReceiving(Request $request){
        $report = $this->reportService->getReceiving($request->all());
        return $report;
    }

    public function getDiscount(Request $request){
        $report = $this->reportService->getDiscount($request->all());
        return $report;
    }

    public function getPayment(Request $request){
        $report = $this->reportService->getPayment($request->all());
        return $report;
    }
    public function getTax(Request $request){
        $report = $this->reportService->getTax($request->all());
        return $report;
    }
    public function getEmployee(Request $request){
        $report = $this->reportService->getEmployee($request->all());
        return $report;
    }
    public function getDetailSales(Request $request){
        $report = $this->reportService->getDetailSales($request->all());
        return $report;
    }
    public function getSalesItem(Request $request){
        Log::info($request);
        $report = $this->reportService->getSalesItem($request->all());
        return $report;
    }
    public function getdetailsDiscount(Request $request){
        $report = $this->reportService->getdetailsDiscount($request->all());
        return $report;
    }
    public function getDetailEmployee(Request $request){
        $report = $this->reportService->getDetailEmployee($request->all());
        return $report;
    }

    public function getDetailCustomer(Request $request){
        $report = $this->reportService->getDetailCustomer($request->all());
        return $report;
    }

    public function getDetailSupplier(Request $request){
        $report = $this->reportService->getDetailSupplier($request->all());
        return $report;
    }

    public function getItemkits(Request $request){
        $report = $this->reportService->getItemkits($request->all());
        return $report;
    }

    public function getCommission(Request $request){
        $report = $this->reportService->getCommission($request->all());
        return $report;
    }

    public function getTransaction(Request $request){
        $report = $this->reportService->getTransaction($request->all());
        return $report;
    }

    public function getLowInventory(Request $request){
        $report = $this->reportService->getLowInventory($request->all());
        return $report;
    }

    public function getInventorySummary(Request $request){
        $report = $this->reportService->getInventorySummary($request->all());
        return $report;
    }

    public function getDeadItem(Request $request){
        $report = $this->reportService->getDeadItem($request->all());
        return $report;
    }    


    
} 