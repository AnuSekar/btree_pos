<?php
namespace App\Http\Controllers;
use App\Requests\CreateSalesRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ItemKitService;
use Log;
class ItemKitController extends Controller
{

	private $itemService;

    public function __construct(ItemKitService $itemService) {
        $this->itemService = $itemService;
    }

    public function save(CreateSalesRequest $request)
    {

        try{
        
        $item = $this->itemService->save( $request->json()->all());

        }catch(Exception $e) {
            throw $e;
        }

        return $item;

        
    }

    public function update(Request $request)
    {
        //$item = $request->person_id;
    	$item = $this->itemService->update($request->json()->all());

        return $item;

    }


    public function deleteAll(Request $request)
    {
        $item = $this->itemService->deleteAll();
        return $item;
       
    }

     public function storeAll(Request $request)
    {
        $item = $this->itemService->storeAll();
        return $item;
       
    }

     public function getAll(){
    	$item = $this->itemService->getAll();
        return $item;
    }

    public function getAlltrash(){
       $item = $this->itemService->getAlltrash();
       return $item;
    }

     public function getById(Request $request){
    	$item = $this->itemService->getById($request->all());
        return $item;
    }
    
    public function deleteitem($id)
    {
        $item = $this->itemService->deleteitem($id);
        return $item;
       
    }
    public function search(Request $request){
        $item = $this->itemService->search($request->get("item_name"));
        return $item;
    }
    
    public function deleteSelectItem(Request $request){
        $item = $this->itemService->deleteSelectItem($request->json()->all());
        return $item;
    }

    public function addSelectItem(Request $request){
        $item = $this->itemService->addSelectItem($request->json()->all());
        return $item;
    }
    

}    
