<?php
namespace App\Http\Controllers;
use App\Requests\CreateEmployeeRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\EmployeeService;
use Log;
class EmployeeController extends Controller
{

	private $employeeService;

    public function __construct(EmployeeService $employeeService) {
        $this->employeeService = $employeeService;
    }

    public function save(CreateEmployeeRequest $request)
    {

        try{

        $employee = $this->employeeService->save( $request->json()->all());

        }catch(Exception $e) {
            throw $e;
        }

        return $employee; 


        
    }
 
    public function update(Request $request)
    {
        //$employee = $request->person_id;
    	$employee = $this->employeeService->update($request->json()->all());

        return $employee;

    }


    public function deleteAll(Request $request)
    {
        $employee = $this->employeeService->deleteAll();
        return $employee;
       
    }

     public function getAll(){
    	$employee = $this->employeeService->getAll();
       return $employee;
    }

     public function getById($id){
    	$employee = $this->employeeService->getById($id);
        return $employee;
    }
    
    public function storeAll()
    {
        $employee = $this->employeeService->storeAll();
        return $employee;
       
    }

    public function getAlltrash(){
       $employee = $this->employeeService->getAlltrash();
       return $employee;
    }   
   
    
    public function deleteemployee($id)
    {
        $employee = $this->employeeService->deleteemployee($id);
        return $employee;
       
    }
    public function search(Request $request){
        $employee = $this->employeeService->search($request->get("employee_name"));
        return $employee;
    }
    public function employeebysales(Request $request){
        $customer = $this->employeeService->employeebysales($request->get("searchemployee"));
        return $customer;
    }
    public function searchTrash(Request $request){
        $employee = $this->employeeService->searchTrash($request->get("employee_name"));
        return $employee;
    }
    public function deleteSelectEmployee(Request $request){
        $employee = $this->employeeService->deleteSelectEmployee($request->json()->all());
        return $employee;
    }
    public function addSelectEmployee(Request $request){
        $employee = $this->employeeService->addSelectEmployee($request->json()->all());
        return $employee;
    }
    


}    