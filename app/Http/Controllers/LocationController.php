<?php
namespace App\Http\Controllers;
use App\Requests\CreateLocationRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\LocationService;
use Log;
class LocationController extends Controller
{

  private $locationService;

    public function __construct(LocationService $locationService) {
        $this->locationService = $locationService;
    }

    public function save(CreateLocationRequest $request)
    {

        try{
        
        $location = $this->locationService->save( $request->json()->all());

        }catch(Exception $e) {
            throw $e;
        }

        return $location;

        
    }

    public function update(Request $request)
    {
      $location = $this->locationService->update($request->json()->all());

        return $location;

    }


    public function deleteAll(Request $request)
    {
        $location = $this->locationService->deleteAll();
        return $location;
       
    }

     public function getAll(Request $request){
       $location = $this->locationService->getAll();
       return $location;
    }

    public function storeAll()
    {
        $location = $this->locationService->storeAll();
        return $location;
       
    }

    public function getAlltrash(){
       $location = $this->locationService->getAlltrash();
       return $location;
    }

     public function getById($id){
        
      $location = $this->locationService->getById($id);
        return $location;
    }

   
    
    public function deletelocation($id)
    {   
        $location = $this->locationService->deletelocation($id);
        return $location;
       
    }
    public function search(Request $request){
        $location = $this->locationService->search($request->get("location_name"));
        return $location;
    }
    public function getList(){
        $location = $this->locationService->getList();
        return $location;
    }
    public function deleteSelectLocation(Request $request){
        $location = $this->locationService->deleteSelectLocation($request->json()->all());
        return $location;
    }
    public function addSelectLocation(Request $request){
        $location = $this->locationService->addSelectLocation($request->json()->all());
        return $location;
    }


}    
