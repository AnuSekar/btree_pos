<?php
namespace App\Http\Controllers;
use App\Requests\CreateCompanyRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\CompanyService;
use Log;
class CompanyController extends Controller
{

	private $companyService;

    public function __construct(CompanyService $companyService) {
        $this->companyService = $companyService;
    }

   public function save(CreateCompanyRequest $request)
    {

        try{
        
        $company = $this->companyService->save( $request->json()->all());

        }catch(Exception $e) {
            throw $e;
        }

        return $company;

        
    }
    
    public function update(Request $request)
    {
    	$company = $this->companyService->update($request->json()->all());

        return $company;

    }


    public function deleteAll(Request $request)
    {
        $company = $this->companyService->deleteAll();
        return $company;
       
    }

    public function storeAll(Request $request)
    {
        $company = $this->companyService->storeAll();
        return $company;
       
    }


     public function getAll(){
       $company = $this->companyService->getAll();
       return $company;
    }

    public function getAlltrash(){
       $company = $this->companyService->getAlltrash();
       return $company;
    }

     public function getById($id){
        
    	$company = $this->companyService->getById($id);
        return $company;
    }

   
    
    public function deletecompany($id)
    {   
        $company = $this->companyService->deletecompany($id);
        return $company;
       
    }
    public function search(Request $request){
        $company = $this->companyService->search($request->get("company_name"));
        return $company;
    }
    public function getList(){
        $company = $this->companyService->getList();
        return $company;
    }
    public function deleteSelectCompany(Request $request){
        $company = $this->companyService->deleteSelectCompany($request->json()->all());
        return $company;
    }
    public function addSelectCompany(Request $request){
        $company = $this->companyService->addSelectCompany($request->json()->all());
        return $company;
    }
       

}    
