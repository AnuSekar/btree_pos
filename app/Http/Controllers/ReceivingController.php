<?php
namespace App\Http\Controllers;
use App\Requests\CreateReceivingRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ReceivingService;
use Log;
class ReceivingController extends Controller
{

	private $receivingService;

    public function __construct(ReceivingService $receivingService) {
        $this->receivingService = $receivingService;
    }

    public function save(CreateReceivingRequest $request)
    {

        try{
        $receiving = $this->receivingService->save( $request->json()->all());

        }catch(Exception $e) {
            throw $e;
        }

        return $receiving;

        
    }

    public function return(CreateReceivingRequest $request)
    {

        try{
        $receiving = $this->receivingService->return( $request->json()->all());

        }catch(Exception $e) {
            throw $e;
        }

        return $receiving;

        
    }

}    
