<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\User;
use Hash;
use Auth;
use Redirect;
use Session;
use Validator;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\DB;
use Log;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('admin.login');
    }

    public function login(Request $request) {
        Log::info($request);
        if (Auth::attempt ([ 
                'username' => $request->get ( 'email' ),'password' => $request->get ( 'password' ) ])) {
             $user = DB::table('btree_employees')
            ->where('username','=',$request->get ( 'email' ))
            ->select('person_id')
            ->first();
             $user = (array)$user;
            $request->session()->put('person_id', $user['person_id']);  
            //Log::info($user);
            return redirect('/admin');
        } else {
            Session::flash ( 'message', "Invalid Credentials , Please try again." );
            return Redirect::back ();
        }
    }

    public function logout() {
       // Auth::logout ();
        $request->session()->forget('person_id');
        return redirect('/');
    }
}
