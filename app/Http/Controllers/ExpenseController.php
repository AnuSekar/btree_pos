<?php
namespace App\Http\Controllers;
use App\Requests\CreateExpenseRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ExpenseService;
class ExpenseController extends Controller
{

    private $expenseService;

    public function __construct(ExpenseService $expenseService) {
        $this->expenseService = $expenseService;
    }

    public function save(CreateExpenseRequest $request)
    {

        try{
        
        $expense = $this->expenseService->save( $request->json()->all());

        }catch(Exception $e) {
            throw $e;
        }

        return $expense;

        
    }

    public function update(Request $request)
    {
        $expense = $this->expenseService->update($request->json()->all());
        return $expense;
    }

    public function deleteAll(Request $request)
    {
        $expense = $this->expenseService->deleteAll();
        return $expense;
       
    }

    public function storeAll(Request $request)
    {
        $expense = $this->expenseService->storeAll();
        return $expense;
       
    }

     public function getAll(){
        $expense = $this->expenseService->getAll();
        return $expense;
    }

    public function getAlltrash(){
       $expense = $this->expenseService->getAlltrash();
       return $expense;
    }

     public function getById(Request $request){
        $expense = $this->expenseService->getById($request->json()->all());
        return $expense;
    }
    
    public function deleteexpense($id)
    {
        $expense = $this->expenseService->deleteexpense($id);
        return $expense;
       
    }
    public function search(Request $request){
        $expense = $this->expenseService->search($request->get("expense_name"));
        return $expense;
    }
    public function deleteSelectExpense(Request $request){
        $expense = $this->expenseService->deleteSelectExpense($request->json()->all());
        return $expense;
    }
    public function addSelectExpense(Request $request){
        $expense = $this->expenseService->addSelectExpense($request->json()->all());
        return $expense;
    }
    


}    
