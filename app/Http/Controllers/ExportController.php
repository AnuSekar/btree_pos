<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Util\BLAlphaNumericCodeGenerator;
use Log;
use App\Services\ReportService;
use Illuminate\Http\Request;
use App\Response\GlobalResponse;
use Excel;
class ExportController extends ReportController
{
   public function __construct(ReportService $reportService) {
        $this->reportService = $reportService;
    }
   public function downloadExcel(Request $request){
         $filename = "";
         $sheetname = "";
         $filetype = "csv";
         $exportvalue = "" ;
         Log::info($request);
        if($request['exporttype'] == 'companypage'){
            $filename = "company";  
            $sheetname = "company sheet";
            $filetype = "csv";
            $exportvalue = DB::table('btree_company as com')->where('com.deleted','=','0')->get();
            $exportvalue = $exportvalue->map(function ($item){
            return get_object_vars($item);
        });
        }
        else if($request['exporttype'] == "company_report"){
            $filename = "company_report";
            $sheetname = "company sheet";
            $filetype = "csv";

            $exportvalue =$this->reportService->getCompany($request->all());
            $exportvalue = array_map(function ($value) {
            return (array)$value;
              }, $exportvalue);

        } 

        else if($request['exporttype'] == 'productpage'){
            $filename = "product";
            $sheetname = "product sheet";
            $filetype = "csv";
            $exportvalue = DB::table('btree_product as pro')->where('pro.deleted','=','0')->get();
            $exportvalue = $exportvalue->map(function ($item){
            return get_object_vars($item);
        });
        }
        else if($request['exporttype'] == "product_report"){
            $filename = "product_report";
            $sheetname = "product sheet";
            $filetype = "csv";

            $exportvalue =$this->reportService->getProduct($request->all());
            $exportvalue = array_map(function ($value) {
            return (array)$value;
              }, $exportvalue);

        }  
        
        else if($request['exporttype'] == 'employeepage'){
            $filename = "employee";
            $sheetname = "employee sheet";
            $filetype = "csv";
            $exportvalue = DB::select("select ppl.first_name, ppl.last_name, ppl.email, ppl.phone_number, ppl.address_1, ppl.city, ppl.state from btree_people as ppl , btree_employees as emp where emp.person_id=ppl.person_id and emp.deleted = 0 group by emp.person_id"); 
            $exportvalue = array_map(function ($value) {
            return (array)$value;
              }, $exportvalue);
        }
        else if($request['exporttype'] == "employee_report"){
            $filename = "employee_report";
            $sheetname = "employee sheet";
            $filetype = "csv";

            $exportvalue =$this->reportService->getEmployee($request->all());
            $exportvalue = array_map(function ($value) {
            return (array)$value;
              }, $exportvalue);

        }  

        else if($request['exporttype'] == 'customerpage'){
            $filename = "customer";
            $sheetname = "customer sheet";
            $filetype = "csv";
            $exportvalue = DB::select("select ppl.first_name, ppl.last_name, ppl.email, ppl.phone_number, ppl.address_1, ppl.city, ppl.state from btree_people as ppl , btree_customers as cus where cus.person_id=ppl.person_id and cus.deleted = 0 group by cus.person_id"); 
            $exportvalue = array_map(function ($value) {
            return (array)$value;
              }, $exportvalue);
        }
        else if($request['exporttype'] == "customer_report"){
            $filename = "customer_report";
            $sheetname = "customer sheet";
            $filetype = "csv";

            $exportvalue =$this->reportService->getCustomer($request->all());
            $exportvalue = array_map(function ($value) {
            return (array)$value;
              }, $exportvalue);

        }

        else if($request['exporttype'] == 'supplierpage'){
            $filename = "supplier";
            $sheetname = "supplier sheet";
            $filetype = "csv";
            $exportvalue = DB::select("select ppl.first_name, ppl.last_name, ppl.email, ppl.phone_number, ppl.address_1, ppl.city, ppl.state from btree_people as ppl , btree_suppliers as sup where sup.person_id=ppl.person_id and sup.deleted = 0 group by sup.person_id"); 
            $exportvalue = array_map(function ($value) {
            return (array)$value;
              }, $exportvalue);
        }
        else if($request['exporttype'] == "supplier_report"){
            $filename = "supplier_report";
            $sheetname = "supplier sheet";
            $filetype = "csv";

            $exportvalue =$this->reportService->getSupplier($request->all());
            $exportvalue = array_map(function ($value) {
            return (array)$value;
              }, $exportvalue);

        } 

        else if($request['exporttype'] == 'itempage'){
            $filename = "item";
            $sheetname = "item sheet";
            $filetype = "csv";
            $exportvalue = DB::select("select it.item_name, it.item_number, com.company_name, pro.product_name, ppl.first_name as supplier, it.cost_price from btree_item as it , btree_company as com, btree_product as pro, btree_people as ppl where it.company_id=com.company_id and it.product_id=pro.product_id and it.supplier_id=ppl.person_id and it.deleted = 0 group by it.item_id"); 
            $exportvalue = array_map(function ($value) {
            return (array)$value;
              }, $exportvalue);
        }
        else if($request['exporttype'] == "item_report"){
            $filename = "item_report";
            $sheetname = "item sheet";
            $filetype = "csv";

            $exportvalue =$this->reportService->getItem($request->all());
            $exportvalue = array_map(function ($value) {
            return (array)$value;
              }, $exportvalue);

        } 

        else if($request['exporttype'] == "sale_report"){
            $filename = "sale_report";
            $sheetname = "sale sheet";
            $filetype = "csv";

            $exportvalue =$this->reportService->getSales($request->all());
            $exportvalue = array_map(function ($value) {
            return (array)$value;
              }, $exportvalue);

        } 
        
        return \Excel::create($filename, function($excel) use ($exportvalue,$sheetname) {
            $excel->sheet($sheetname, function($sheet) use ($exportvalue)
            {
                $sheet->fromArray($exportvalue);
            });
        })->export($filetype);
    }

    }


 ?>

