<?php
namespace App\Http\Controllers;
use App\Requests\CreateCustomerRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\CustomerService;
use Log;
class CustomerController extends Controller
{

	private $customerService;

    public function __construct(CustomerService $customerService) {
        $this->customerService = $customerService;
    }

    public function save(CreateCustomerRequest $request)
    {

        try{
        
        $customer = $this->customerService->save( $request->json()->all());

        }catch(Exception $e) {
            throw $e;
        }

        return $customer;

        
    }

    public function update(Request $request)
    {
        //$customer = $request->person_id;
    	$customer = $this->customerService->update($request->json()->all());

        return $customer;

    }


    public function deleteAll(Request $request)
    {
        $customer = $this->customerService->deleteAll();
        return $customer;
       
    }

     public function getAll(){
       $customer = $this->customerService->getAll();
       return $customer;
    }

     public function getById($id){
    	$customer = $this->customerService->getById($id);
        return $customer;
    }

    public function storeAll()
    {
        $customer = $this->customerService->storeAll();
        return $customer;
       
    }

    public function getAlltrash(){
       $customer = $this->customerService->getAlltrash();
       return $customer;
    }   
    
    public function deletecustomer($id)
    {
        $customer = $this->customerService->deletecustomer($id);
        return $customer;
       
    }
    public function search(Request $request){
        $customer = $this->customerService->search($request->get("customer_name"));
        return $customer;
    }
    public function customerbysales(Request $request){
        $customer = $this->customerService->customerbysales($request->get("searchcustomer"));
        return $customer;
    }
    public function deleteSelectCustomer(Request $request){
        $customer = $this->customerService->deleteSelectCustomer($request->json()->all());
        return $customer;
    }
    public function addSelectCustomer(Request $request){
        $customer = $this->customerService->addSelectCustomer($request->json()->all());
        return $customer;
    }
    
    public function CustomerPointsClaim(Request $request){
        $customer = $this->customerService->CustomerPointsClaim($request->all());
        return $customer;
    }


}    
