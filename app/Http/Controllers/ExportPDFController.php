<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Util\BLAlphaNumericCodeGenerator;
use Log;
use Illuminate\Http\Request;
use App\Services\ReportService;
use App\Response\GlobalResponse;
use Dompdf\Dompdf;

class ExportPDFController extends ReportController
{
    public function __construct(ReportService $reportService) {
        $this->reportService = $reportService;
    }
     public function downloadPDF(Request $request){
         $filename = "";
         $sheetname = "";
         $filetype = "pdf";
         $exportvalue = "" ;
         Log::info($request);
         $dompdf = new Dompdf();

         if($request['exporttype'] == 'companypage'){
            $exportvalue = DB::table('btree_company as com')
            ->where('com.deleted','=','0')
            ->get();
            $exportvalue = $exportvalue->map(function ($item){
            return get_object_vars($item);
            });
            $html = '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                <h1>Company</h1>
                <table class="table table-bordered">
                <tr>
                <th>Company_id</th><th>Company Name</th><th>Company Description</th>
                </tr>';
        
            foreach ($exportvalue as $value) {
            $id = (string)$value['company_id'];
            $html .= '<tr>';
            $html .= ('<td>'.$id .'</td><td>'.$value['company_name'].'</td><td>'.$value['company_description'].'</td>');
            $html .= '</tr>';
            }      
            $html.='</table>';
            $dompdf->loadHtml($html);
            $dompdf->render();
            $dompdf->stream();

         }else if($request['exporttype'] == 'productpage'){
            $exportvalue = DB::table('btree_product as com')
            ->where('com.deleted','=','0')
            ->get();
            $exportvalue = $exportvalue->map(function ($item){
            return get_object_vars($item);
            });
            $html = '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                <h1>Product</h1>
                <table class="table table-bordered">
                <tr>
                <th>Product_id</th><th>Product Name</th><th>Product Description</th>
                </tr>';
        
            foreach ($exportvalue as $value) {
            $id = (string)$value['product_id'];
            $html .= '<tr>';
            $html .= ('<td>'.$id .'</td><td>'.$value['product_name'].'</td><td>'.$value['product_description'].'</td>');
            $html .= '</tr>';
            }     
            $html.='</table>';
            $dompdf->loadHtml($html);
            $dompdf->render();
            $dompdf->stream();

         }else if($request['exporttype'] == 'employeepage'){
            $exportvalue = DB::select("select ppl.first_name, ppl.last_name, ppl.email, ppl.phone_number, ppl.address_1, ppl.city, ppl.state, emp.person_id from btree_people as ppl , btree_employees as emp where emp.person_id=ppl.person_id and emp.deleted = 0 group by emp.person_id"); 
            $exportvalue = array_map(function ($value) {
            return (array)$value;
              }, $exportvalue);
            $html = '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                <h1>Employee</h1>
                <table class="table table-bordered">
                <tr>
                <th>Employee_id</th><th>First Name</th><th>Last Name</th><th>Address</th><th>Phone Number</th><th>Email</th>
                </tr>';
        
            foreach ($exportvalue as $value) {
            $id = (string)$value['person_id'];
            $html .= '<tr>';
            $html .= ('<td>'.$id .'</td><td>'.$value['first_name'].'</td><td>'.$value['last_name'].'</td><td>'.$value['address_1'].'</td><td>'.$value['phone_number'].'</td><td>'.$value['email'].'</td>');
            $html .= '</tr>';
            }     
            $html.='</table>';
            $dompdf->loadHtml($html);
            $dompdf->render();
            $dompdf->stream();
            
         }else if($request['exporttype'] == 'customerpage'){
            $exportvalue = DB::select("select ppl.first_name, ppl.last_name, ppl.email, ppl.phone_number, ppl.address_1, ppl.city, ppl.state, cus.person_id from btree_people as ppl , btree_customers as cus where cus.person_id=ppl.person_id and cus.deleted = 0 group by cus.person_id"); 
            $exportvalue = array_map(function ($value) {
            return (array)$value;
              }, $exportvalue);
            $html = '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                <h1>Customer</h1>
                <table class="table table-bordered">
                <tr>
                <th>Customer_id</th><th>First Name</th><th>Last Name</th><th>Address</th><th>Phone Number</th><th>Email</th>
                </tr>';
        
            foreach ($exportvalue as $value) {
            $id = (string)$value['person_id'];
            $html .= '<tr>';
            $html .= ('<td>'.$id .'</td><td>'.$value['first_name'].'</td><td>'.$value['last_name'].'</td><td>'.$value['address_1'].'</td><td>'.$value['phone_number'].'</td><td>'.$value['email'].'</td>');
            $html .= '</tr>';
            }     
            $html.='</table>';
            $dompdf->loadHtml($html);
            $dompdf->render();
            $dompdf->stream();

         }else if($request['exporttype'] == 'supplierpage'){
            $exportvalue = DB::select("select ppl.first_name, ppl.last_name, ppl.email, ppl.phone_number, ppl.address_1, ppl.city, ppl.state, sup.person_id from btree_people as ppl , btree_suppliers as sup where sup.person_id=ppl.person_id and sup.deleted = 0 group by sup.person_id"); 
            $exportvalue = array_map(function ($value) {
            return (array)$value;
              }, $exportvalue);
            $html = '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                <h1>Supplier</h1>
                <table class="table table-bordered">
                <tr>
                <th>Supplier_id</th><th>First Name</th><th>Last Name</th><th>Address</th><th>Phone Number</th><th>Email</th>
                </tr>';
        
            foreach ($exportvalue as $value) {
            $id = (string)$value['person_id'];
            $html .= '<tr>';
            $html .= ('<td>'.$id .'</td><td>'.$value['first_name'].'</td><td>'.$value['last_name'].'</td><td>'.$value['address_1'].'</td><td>'.$value['phone_number'].'</td><td>'.$value['email'].'</td>');
            $html .= '</tr>';
            }     
            $html.='</table>';
            $dompdf->loadHtml($html);
            $dompdf->render();
            $dompdf->stream();

        }else if($request['exporttype'] == 'itempage'){
            $exportvalue = DB::select("select it.item_name, it.item_number, com.company_name, pro.product_name, ppl.first_name as supplier, it.cost_price, it.item_id from btree_item as it , btree_company as com, btree_product as pro, btree_people as ppl where it.company_id=com.company_id and it.product_id=pro.product_id and it.supplier_id=ppl.person_id and it.deleted = 0 group by it.item_id"); 
            $exportvalue = array_map(function ($value) {
            return (array)$value;
              }, $exportvalue);
            $html = '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                <h1>Item</h1>
                <table class="table table-bordered">
                <tr>
                <th>Item_id</th><th>Item Name</th><th>Company Name</th><th>Product Name</th><th>Supplier Name</th><th>Cost Price</th>
                </tr>';
        
            foreach ($exportvalue as $value) {
            $id = (string)$value['item_id'];
            $html .= '<tr>';
            $html .= ('<td>'.$id .'</td><td>'.$value['item_name'].'</td><td>'.$value['company_name'].'</td><td>'.$value['product_name'].'</td><td>'.$value['supplier'].'</td><td>'.$value['cost_price'].'</td>');
            $html .= '</tr>';
            }     
            $html.='</table>';
            $dompdf->loadHtml($html);
            $dompdf->render();
            $dompdf->stream();

        }else if($request['exporttype'] == 'company_report'){
            $exportvalue =$this->reportService->getCompany($request->all());
            $exportvalue = array_map(function ($value) {
            return (array)$value;
              }, $exportvalue);
            $html = '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                <h1>Company Report</h1>
                <table class="table table-bordered">
                <tr>
                <th>Company_id</th><th>Company Name</th><th>Item Sold</th><th>Total</th>
                </tr>';
        
            foreach ($exportvalue as $value) {
            $id = (string)$value['company_id'];
            $html .= '<tr>';
            $html .= ('<td>'.$id .'</td><td>'.$value['company_name'].'</td><td>'.$value['quantity_purchased'].'</td><td>'.$value['sales_amount'].'</td>');
            $html .= '</tr>';
            }      
            $html.='</table>';
            $dompdf->loadHtml($html);
            $dompdf->render();
            $dompdf->stream();

         }else if($request['exporttype'] == 'product_report'){
            $exportvalue =$this->reportService->getProduct($request->all());
            $exportvalue = array_map(function ($value) {
            return (array)$value;
              }, $exportvalue);
            $html = '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                <h1>Product Report</h1>
                <table class="table table-bordered">
                <tr>
                <th>Product_id</th><th>Product Name</th><th>Item Sold</th><th>Total</th>
                </tr>';
        
            foreach ($exportvalue as $value) {
            $id = (string)$value['product_id'];
            $html .= '<tr>';
            $html .= ('<td>'.$id .'</td><td>'.$value['product_name'].'</td><td>'.$value['quantity_purchased'].'</td><td>'.$value['sales_amount'].'</td>');
            $html .= '</tr>';
            }      
            $html.='</table>';
            $dompdf->loadHtml($html);
            $dompdf->render();
            $dompdf->stream();

         }else if($request['exporttype'] == 'employee_report'){
            $exportvalue =$this->reportService->getEmployee($request->all());
            $exportvalue = array_map(function ($value) {
            return (array)$value;
              }, $exportvalue);
            $html = '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                <h1>Employee Report</h1>
                <table class="table table-bordered">
                <tr>
                <th>Employee_id</th><th>Employee Name</th><th>Item Sold</th><th>Total</th>
                </tr>';
        
            foreach ($exportvalue as $value) {
            $id = (string)$value['person_id'];
            $html .= '<tr>';
            $html .= ('<td>'.$id .'</td><td>'.$value['name'].'</td><td>'.$value['count'].'</td><td>'.$value['sales_amount'].'</td>');
            $html .= '</tr>';
            }      
            $html.='</table>';
            $dompdf->loadHtml($html);
            $dompdf->render();
            $dompdf->stream();

         }else if($request['exporttype'] == 'customer_report'){
            $exportvalue =$this->reportService->getCustomer($request->all());
            $exportvalue = array_map(function ($value) {
            return (array)$value;
              }, $exportvalue);
            $html = '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                <h1>Customer Report</h1>
                <table class="table table-bordered">
                <tr>
                <th>Customer_id</th><th>Customer Name</th><th>Item Sold</th><th>Total</th>
                </tr>';
        
            foreach ($exportvalue as $value) {
            $id = (string)$value['person_id'];
            $html .= '<tr>';
            $html .= ('<td>'.$id .'</td><td>'.$value['name'].'</td><td>'.$value['count'].'</td><td>'.$value['sales_amount'].'</td>');
            $html .= '</tr>';
            }      
            $html.='</table>';
            $dompdf->loadHtml($html);
            $dompdf->render();
            $dompdf->stream();

         }else if($request['exporttype'] == 'supplier_report'){
            $exportvalue =$this->reportService->getSupplier($request->all());
            $exportvalue = array_map(function ($value) {
            return (array)$value;
              }, $exportvalue);
            $html = '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                <h1>Supplier Report</h1>
                <table class="table table-bordered">
                <tr>
                <th>Supplier_id</th><th>Supplier Name</th><th>Item Sold</th><th>Total</th>
                </tr>';
        
            foreach ($exportvalue as $value) {
            $id = (string)$value['person_id'];
            $html .= '<tr>';
            $html .= ('<td>'.$id .'</td><td>'.$value['name'].'</td><td>'.$value['count'].'</td><td>'.$value['sales_amount'].'</td>');
            $html .= '</tr>';
            }      
            $html.='</table>';
            $dompdf->loadHtml($html);
            $dompdf->render();
            $dompdf->stream();

         }else if($request['exporttype'] == 'item_report'){
            $exportvalue =$this->reportService->getItem($request->all());
            $exportvalue = array_map(function ($value) {
            return (array)$value;
              }, $exportvalue);
            $html = '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                <h1>Item Report</h1>
                <table class="table table-bordered">
                <tr>
                <th>Item_id</th><th>Item Name</th><th>Item Sold</th><th>Total Amount</th>
                </tr>';
        
            foreach ($exportvalue as $value) {
            $id = (string)$value['item_id'];
            $html .= '<tr>';
            $html .= ('<td>'.$id .'</td><td>'.$value['item_name'].'</td><td>'.$value['quantity_purchased'].'</td><td>'.$value['sales_amount'].'</td>');
            $html .= '</tr>';
            }      
            $html.='</table>';
            $dompdf->loadHtml($html);
            $dompdf->render();
            $dompdf->stream();

         }else if($request['exporttype'] == 'sale_report'){
            $exportvalue =$this->reportService->getSales($request->all());
            $exportvalue = array_map(function ($value) {
            return (array)$value;
              }, $exportvalue);
            $html = '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                <h1>Sales Report</h1>
                <table class="table table-bordered">
                <tr>
                <th>Sale_id</th><th>Date</th><th>Items Sold</th><th>Total</th><th>Sub Total</th><th>Tax</th>
                </tr>';
        
            foreach ($exportvalue as $value) {
            $id = (string)$value['sale_id'];
            $html .= '<tr>';
            $html .= ('<td>'.$id .'</td><td>'.$value['sale_time'].'</td><td>'.$value['count'].'</td><td>'.$value['sales_whole_amount'].'</td><td>'.$value['sales_amount'].'</td><td>'.$value['tax_amount'].'</td>');
            $html .= '</tr>';
            }      
            $html.='</table>';
            $dompdf->loadHtml($html);
            $dompdf->render();
            $dompdf->stream();

         }
    }

}
 ?>

