<?php
namespace App\Http\Controllers;
use App\Requests\CreateProductRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ProductService;
use Log;
class ProductController extends Controller
{

	private $productService;

    public function __construct(ProductService $productService) {
        $this->productService = $productService;
    }

    public function save(CreateProductRequest $request)
    {

        try{
        
        $product = $this->productService->save( $request->json()->all());

        }catch(Exception $e) {
            throw $e;
        }

        return $product;

        
    }

    public function update(Request $request)
    {
    	$product = $this->productService->update($request->json()->all());

        return $product;

    }


    public function deleteAll(Request $request)
    {
        $product = $this->productService->deleteAll();
        return $product;
       
    }

    public function storeAll(Request $request)
    {
        $product = $this->productService->storeAll();
        return $product;
       
    }


    public function getAlltrash(){
       $product = $this->productService->getAlltrash();
       return $product;
    }


     public function getAll(){
       $product = $this->productService->getAll();
       return $product;
    }

     public function getById($id){
    	$product = $this->productService->getById($id);
        return $product;
    }
    
    public function deleteproduct($id)
    {
        $product = $this->productService->deleteproduct($id);
        return $product;
       
    }
    public function search(Request $request){
        $product = $this->productService->search($request->get("product_name"));
        return $product;
    }
    public function getList(){
        $product = $this->productService->getList();
        return $product;
    }
    public function deleteSelectProduct(Request $request){
        $product = $this->productService->deleteSelectProduct($request->json()->all());
        return $product;
    }
    public function addSelectProduct(Request $request){
        $product = $this->productService->addSelectProduct($request->json()->all());
        return $product;
    }
    


}    
