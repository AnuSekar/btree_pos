<?php
namespace App\Http\Controllers;
use App\Requests\CreateSuspendedRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\SuspendedService;
use Log;
class SuspendedController extends Controller
{

	private $receivingService;

    public function __construct(SuspendedService $suspendedService) {
        $this->suspendedService = $suspendedService;
    }

    public function save(CreateSuspendedRequest $request)
    {

        try{
        $suspended = $this->suspendedService->save( $request->json()->all());

        }catch(Exception $e) {
            throw $e;
        }

        return $suspended;   
    }

    public function getAllList(){
       $supplier = $this->suspendedService->getAllList();
       return $supplier;
    }

    public function getAllListRender(Request $request){
       $supplier = $this->suspendedService->getAllListRender($request->all());
       return $supplier;
    }
    


}    
