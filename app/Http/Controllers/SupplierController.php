<?php
namespace App\Http\Controllers;
use App\Requests\CreateSupplierRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\SupplierService;
use Log;
class SupplierController extends Controller
{

	private $supplierService;

    public function __construct(SupplierService $supplierService) {
        $this->supplierService = $supplierService;
    }

    public function save(CreateSupplierRequest $request)
    {

        try{
        
        $supplier = $this->supplierService->save( $request->json()->all());

        }catch(Exception $e) {
            throw $e;
        }

        return $supplier;

        
    }

    public function update(Request $request)
    {
        //$supplier = $request->person_id;
    	$supplier = $this->supplierService->update($request->json()->all());

        return $supplier;

    }


    public function deleteAll(Request $request)
    {
        $supplier = $this->supplierService->deleteAll();
        return $supplier;
       
    }

     public function getAll(){
       $supplier = $this->supplierService->getAll();
       return $supplier;
    }

     public function getById($id){
    	$supplier = $this->supplierService->getById($id);
        return $supplier;
    }

    public function storeAll()
    {
        $supplier = $this->supplierService->storeAll();
        return $supplier;
       
    }

    public function getAlltrash(){
       $supplier = $this->supplierService->getAlltrash();
       return $supplier;
    }   
       
    
    public function deletesupplier($id)
    {
        $supplier = $this->supplierService->deletesupplier($id);
        return $supplier;
       
    }
    public function search(Request $request){
        $supplier = $this->supplierService->search($request->get("supplier_name"));
        return response()->json($supplier, 201);
    }

    public function getList(Request $request){
        $supplier = $this->supplierService->getList($request->get("searchsupplier"));
        return response()->json($supplier, 201);
    }

    public function getAllList(){
       $supplier = $this->supplierService->getAllList();
       return $supplier;
    }
    public function deleteSelectSupplier(Request $request){
        $supplier = $this->supplierService->deleteSelectSupplier($request->json()->all());
        return $supplier;
    }
    public function addSelectSupplier(Request $request){
        $supplier = $this->supplierService->addSelectSupplier($request->json()->all());
        return $supplier;
    }
    


}    
