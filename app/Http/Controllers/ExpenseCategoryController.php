<?php
namespace App\Http\Controllers;
use App\Requests\CreateExpenseCategoryRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ExpenseCategoryService;
use Log;
class ExpenseCategoryController extends Controller
{

  private $expense_categoryService;

    public function __construct(ExpenseCategoryService $expense_categoryService) {
        $this->expense_categoryService = $expense_categoryService;
    }

   public function save(CreateExpenseCategoryRequest $request)
    {

        try{
        
        $expense_category = $this->expense_categoryService->save( $request->json()->all());

        }catch(Exception $e) {
            throw $e;
        }

        return $expense_category;

        
    }
    
    public function update(Request $request)
    {
      $expense_category = $this->expense_categoryService->update($request->json()->all());

        return $expense_category;

    }


    public function deleteAll(Request $request)
    {
        $expense_category = $this->expense_categoryService->deleteAll();
        return $expense_category;
       
    }

    public function storeAll(Request $request)
    {
        $expense_category = $this->expense_categoryService->storeAll();
        return $expense_category;
       
    }


     public function getAll(){
       $expense_category = $this->expense_categoryService->getAll();
       return $expense_category;
    }

    public function getAlltrash(){
       $expense_category = $this->expense_categoryService->getAlltrash();
       return $expense_category;
    }

     public function getById($id){
        
      $expense_category = $this->expense_categoryService->getById($id);
        return $expense_category;
    }

   
    
    public function deleteexpense_category($id)
    {   
        $expense_category = $this->expense_categoryService->deleteexpense_category($id);
        return $expense_category;
       
    }
    public function search(Request $request){
        $expense_category = $this->expense_categoryService->search($request->get("expense_category_name"));
        return $expense_category;
    }


    public function getList(){
        $expense_category = $this->expense_categoryService->getList();
        return $expense_category;
    }

    public function getAllList(){
       $expense_category = $this->expense_categoryService->getAllList();
       return $expense_category;
    }
    public function deleteSelectExpenseCategory(Request $request){
        $expense_category = $this->expense_categoryService->deleteSelectExpenseCategory($request->json()->all());
        return $expense_category;
    }
    public function addSelectExpenseCategory(Request $request){
        $expense_category = $this->expense_categoryService->addSelectExpenseCategory($request->json()->all());
        return $expense_category;
    }
   


}    
