<?php
namespace App\Http\Controllers;
use App\Requests\CreateSalesRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\SalesService;
use Log;
class SalesController extends Controller
{

	private $saleService;

    public function __construct(SalesService $salesService) {
        $this->salesService = $salesService;
    }

    public function save(CreateSalesRequest $request)
    {

        try{
        $sales = $this->salesService->save( $request->json()->all());

        }catch(Exception $e) {
            throw $e;
        }

        return $sales;

        
    }

    public function return(CreateSalesRequest $request)
    {

        try{
        $sales = $this->salesService->return( $request->json()->all());

        }catch(Exception $e) {
            throw $e;
        }

        return $sales;

        
    }

    public function deleteAll(Request $request)
    {
        $sales = $this->salesService->deleteAll();
        return response()->json($sales, 201);
       
    }

     public function getAll(){
    	$sales = $this->salesService->getAll();
        return response()->json($sales, 201);
    }

     public function getById(Request $request){
    	$sales = $this->salesService->getById($request->json()->all());
        return response()->json($sales, 201);
    }

   
    
    public function deletesales(Request $request)
    {
        $sales = $this->salesService->deletesales($request->json()->all());
        return response()->json($sales, 201);
       
    }
    public function search(Request $request){
        $sales = $this->salesService->search($request->get("sales_name"));
        return response()->json($sales, 201);
    }

     public function salebyitem(Request $request){
        $sales = $this->salesService->salebyitem($request->get('searchitem'));

        return response()->json($sales, 201);
    }
}    
