<?php
namespace App\Http\Middleware;

use Closure;

class BtreeMiddleware
{
    public function handle($request, Closure $next)
    {
		
		$user = $request->session()->get('person_id');
		if(!is_null($user)){
			return $next($request);
		}else{
			return response('/admin', 201);
		
		}

	}
}

?>