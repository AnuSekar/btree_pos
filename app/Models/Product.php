<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $table = "btree_product";

    protected $primaryKey = 'product_id';

    public $timestamps = false;
 
    protected $fillable = [
      'product_id', 'product_name','product_description','deleted'
       ];

}
 
?>
