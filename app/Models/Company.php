<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{

    protected $table = "btree_company";

    protected $primaryKey = 'company_id';

    public $timestamps = false;
 
    protected $fillable = [
      'company_id','company_description','company_name','deleted'
       ];

}
 
?>
