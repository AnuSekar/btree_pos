<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{

    protected $table = "btree_employees";

        protected $primaryKey = 'person_id';

    public $timestamps = false;
 
    protected $fillable = [
      'person_id', 'username','password','deleted','hash_version'
       ];


}

 
?>