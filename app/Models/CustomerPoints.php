<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class CustomerPoints extends Model
{

    protected $table = "btree_customer_point";

    protected $primaryKey = 'points_id';

    public $timestamps = false;
 
    protected $fillable = [
      'points_id','customer_id','sales_id','points_earned'
       ];

}
 
?>
