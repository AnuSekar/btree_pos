<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class SalesTax extends Model
{

    protected $table = "btree_sales_tax";

    protected $primaryKey = 'sale_id';

    public $timestamps = false;
 
    protected $fillable = [
      'sale_id', 'tax_group','sales_tax_basis','sales_tax_amount','print_sequence','name','tax_rate'
       ];

}
 
?>