<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ExpenseCategory extends Model
{

    protected $table = "btree_expense_category";

    protected $primaryKey = 'expense_category_id';

    public $timestamps = false;
 
    protected $fillable = [
      'expense_category_id','expense_category_description','expense_category_name','deleted'
       ];

}
 
?>
