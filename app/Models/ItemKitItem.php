<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ItemKitItem extends Model
{

    protected $table = "btree_item_kit_items";

    protected $primaryKey = 'item_id';

    public $timestamps = false;
 
    protected $fillable = [
      'item_id','item_kit_id', 'quantity','kit_sequence'
       ];

}
 
?>
