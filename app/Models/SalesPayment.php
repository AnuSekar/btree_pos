<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class SalesPayment extends Model
{

    protected $table = "btree_sales_payments";

    protected $primaryKey = 'sale_id';

    public $timestamps = false;
 
    protected $fillable = [
      'sale_id', 'payment_type','payment_amount'
       ];

}
 
?>