<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Person extends Model
{

    protected $table = "btree_people";

    protected $primaryKey = 'person_id';

    public $timestamps = false;
 
    protected $fillable = [
       'first_name','last_name','gender','phone_number','email','address_1','address_2','city','state','zip','country','person_id'
       ];

}
 
?>