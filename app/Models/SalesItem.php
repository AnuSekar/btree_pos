<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class SalesItem extends Model
{

    protected $table = "btree_sales_items";

    protected $primaryKey = 'sales_id';

    public $timestamps = false;
 
    protected $fillable = [
      'sales_id', 'item_id','serialnumber','line','quantity_purchased','item_cost_price','item_unit_price','discount_percent','item_location'
       ];

}
 
?>