<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class CustomerPointClaim extends Model
{

    protected $table = "btree_customer_pts_claim";

    protected $primaryKey = 'claim_id';

    public $timestamps = false;
 
    protected $fillable = [
      'claim_id','customer_id','date','date'
       ];

}
 
?>
