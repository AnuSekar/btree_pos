<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{

    protected $table = "btree_expenses";

    protected $primaryKey = 'expense_id';

    public $timestamps = false;
 
    protected $fillable = [
      'expense_id','date','amount','payment_type','expense_category_id','expense_description','employee_id','deleted','supplier_name' ];
       

}
 
?>
