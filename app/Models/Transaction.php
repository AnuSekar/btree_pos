<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{

    protected $table = "btree_inventory";

    protected $primaryKey = 'trans_id';

    public $timestamps = false;
 
    protected $fillable = [
      'trans_items','trans_user','trans_date','trans_location','trans_inventory'
       ];

}
 
?>
