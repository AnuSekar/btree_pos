<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{

    protected $table = "btree_item";

    protected $primaryKey = 'item_id';

    public $timestamps = false;
 
    protected $fillable = [
      'item_name', 'company_id','product_id','supplier_id','item_number','deleted','description','cost_price','unit_price','reorder_level','receiving_quantity','pic_id','allow_alt_description','is_serialized','commission'
       ];

}
 
?>

	