<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ItemQuantity extends Model
{

    protected $table = "btree_item_quantities";

    protected $primaryKey = 'item_id';

    public $timestamps = false;
 
    protected $fillable = [
      'item_id', 'location_id','quantity'
       ];

}
 
?>
