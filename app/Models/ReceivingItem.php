<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ReceivingItem extends Model
{

    protected $table = "btree_receiving_items";

    protected $primaryKey = 'receiving_id';

    public $timestamps = false;
 
    protected $fillable = [
      'receiving_id', 'item_id','serialnumber','line','quantity_purchased','item_cost_price','item_unit_price','discount_percent','item_location','receiving_quantity'
       ];

}
 
?>
