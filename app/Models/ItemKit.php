<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ItemKit extends Model
{

    protected $table = "btree_item_kits";

    protected $primaryKey = 'item_kit_id';

    public $timestamps = false;
 
    protected $fillable = [
      'name','discount'
       ];

}
 
?>

	