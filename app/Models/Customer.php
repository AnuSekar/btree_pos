<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{

    protected $table = "btree_customers";

    protected $primaryKey = 'person_id';

    public $timestamps = false;
 
    protected $fillable = [
      'person_id', 'company_name','account_number','deleted','taxable','discount_percent'
       ];

}
 
?>
