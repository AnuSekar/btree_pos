<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Receiving extends Model
{

    protected $table = "btree_receivings";

    protected $primaryKey = 'receiving_id';

    public $timestamps = false;
 
    protected $fillable = [
      'receiving_id', 'receiving_time','supplier_id','employee_id','payment_type',
     ' reference'
       ];

}
 
?>
