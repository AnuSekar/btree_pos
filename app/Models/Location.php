<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{

    protected $table = "btree_location";

    protected $primaryKey = 'location_id';

    public $timestamps = false;
 
    protected $fillable = [
      'location_id','location_name','deleted'
       ];

}
 
?>
