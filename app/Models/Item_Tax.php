<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Item_Tax extends Model
{

    protected $table = "btree_item_taxes";

    protected $primaryKey = 'item_id';

    public $timestamps = false;
 
    protected $fillable = [
      'item_id','tax_name', 'percent'
       ];

}
 
?>

