<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class SalesItemTax extends Model
{

    protected $table = "btree_sales_items_taxes";

    protected $primaryKey = 'sale_id';

    public $timestamps = false;
 
    protected $fillable = [
      'sale_id', 'item_id','name','line','percent','item_tax_amount'
       ];

}
 
?>