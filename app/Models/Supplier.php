<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{

    protected $table = "btree_suppliers";

    protected $primaryKey = 'person_id';

    public $timestamps = false;
 
    protected $fillable = [
      'person_id', 'company_name','agency_name','deleted','account_number'
       ];

}
 
?>

