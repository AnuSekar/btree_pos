<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{

    protected $table = "btree_sales";

    protected $primaryKey = 'sale_id';

    public $timestamps = false;
 
    protected $fillable = [
      'sale_id', 'sale_time','customer_id','employee_id','invoice_number','sales_type','sales_status'
       ];

}
 
?>