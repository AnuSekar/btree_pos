var admin = admin || {};
admin.SalesItemCollection = Backbone.Collection.extend({
    model: admin.SalesItemModel,
	url: '/btree/admin/get_salesitem_list',

	    parse : function(response){
    	return response;  
   }  
});	

admin.SuspendedSalesItemCollection = Backbone.Collection.extend({
    model: admin.SuspendedSalesItemModel,
	url: '/btree/admin/get_suspendedlist',

	    parse : function(response){
    	return response;  
   }  
});	


admin.SuspendedSalesRenderItemCollection = Backbone.Collection.extend({
    model: admin.SuspendedSalesItemModel,
  url: '/btree/admin/get_suspendedlistrender',

      parse : function(response){
      return response;  
   }  
}); 
