var admin = admin || {};
admin.CompanyCollection = Backbone.PageableCollection.extend({
  model: admin.CompanyModel,
  url: '/btree/admin/get_all_company',
  mode: "server",
      state: {
          pageSize: 5,
          sortKey: "updated",
          order: 1
        },

        // You can remap the query parameters from `state` keys from
        // the default to those your server supports
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
         // get the state from Github's search API result
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },

        // get the actual records
        parseRecords: function (resp, options) {
          return resp.data;
        }
}); 


admin.CompanyTrashCollection = Backbone.PageableCollection.extend({
  model: admin.CompanyModel,
  url: '/btree/admin/get_all_companytrash',
  mode: "server",
      state: {
          pageSize: 5,
          sortKey: "updated",
          order: 1
        },

        // You can remap the query parameters from `state` keys from
        // the default to those your server supports
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
         // get the state from Github's search API result
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },

        // get the actual records
        parseRecords: function (resp, options) {
          return resp.data;
        }
}); 


