var admin = admin || {};
admin.ItemCollection = Backbone.PageableCollection.extend({
    model: admin.ItemModel,
  url: '/btree/admin/get_all_item',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },

        // You can remap the query parameters from `state` keys from
        // the default to those your server supports
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
         // get the state from Github's search API result
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },

        // get the actual records
        parseRecords: function (resp, options) {
          return resp.data;
        }
}); 

admin.ItemTrashCollection = Backbone.PageableCollection.extend({
  model: admin.ItemModel,
  url: '/btree/admin/get_all_itemtrash',
  mode: "server",
      state: {
          pageSize: 5,
          sortKey: "updated",
          order: 1
        },

        // You can remap the query parameters from `state` keys from
        // the default to those your server supports
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
         // get the state from Github's search API result
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },

        // get the actual records
        parseRecords: function (resp, options) {
          return resp.data;
        }
}); 



admin.ItemKitCollection = Backbone.PageableCollection.extend({
    model: admin.ItemKitModel,
  url: '/btree/admin/get_all_itemkit',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
         // get the state from Github's search API result
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },

        // get the actual records
        parseRecords: function (resp, options) {
          return resp.data;
        }
}); 