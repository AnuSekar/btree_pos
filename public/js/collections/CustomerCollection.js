 var admin = admin || {};
admin.CustomerCollection = Backbone.PageableCollection.extend({
    model: admin.CustomerModel,
	url: '/btree/admin/get_all_customer',

	    mode: "server",
      state: {
    	  pageSize: 5,
          sortKey: "updated",
          order: 1
        },

        // You can remap the query parameters from `state` keys from
        // the default to those your server supports
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
         // get the state from Github's search API result
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },

        // get the actual records
        parseRecords: function (resp, options) {
          return resp.data;
   }  
});	

admin.CustomerTrashCollection = Backbone.PageableCollection.extend({
    model: admin.CustomerModel,
  url: '/btree/admin/get_all_customertrash',

      mode: "server",
      state: {
        pageSize: 5,
          sortKey: "updated",
          order: 1
        },

        // You can remap the query parameters from `state` keys from
        // the default to those your server supports
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
         // get the state from Github's search API result
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },

        // get the actual records
        parseRecords: function (resp, options) {
          return resp.data;
   }  
}); 

