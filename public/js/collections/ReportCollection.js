var admin = admin || {};
admin.CompanyReportCollection = Backbone.PageableCollection.extend({
    model: admin.CompanyReportModel,
	url: '/btree/admin/get_all_company_report',
	mode: "server",
	    state: {
		pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp.data;
        }
}); 

admin.CompanyGraphReportCollection = Backbone.PageableCollection.extend({
    model: admin.CompanyReportModel,
  url: '/btree/admin/get_all_company_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp;
        }
}); 

admin.ProductReportCollection = Backbone.PageableCollection.extend({
    model: admin.ProductReportModel,
  url: '/btree/admin/get_all_product_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },      
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp.data;
        }
}); 

admin.ProductGraphReportCollection = Backbone.PageableCollection.extend({
    model: admin.ProductReportModel,
  url: '/btree/admin/get_all_product_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },      
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp;
        }
}); 

admin.CustomerReportCollection = Backbone.PageableCollection.extend({
    model: admin.CustomerReportModel,
  url: '/btree/admin/get_all_customer_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp.data;
        }
}); 

admin.CustomerDetailsReportCollection = Backbone.PageableCollection.extend({
    model: admin.CustomerDetailsReportModel,
  url: '/btree/admin/get_all_detailcustomer_report',
  mode: "server",
      state: {
          pageSize: 5,
          sortKey: "updated",
          order: 1,

        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp.data;
        }
});

admin.CustomerGraphReportCollection = Backbone.PageableCollection.extend({
    model: admin.CustomerReportModel,
  url: '/btree/admin/get_all_customer_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp;
        }
}); 

admin.SupplierReportCollection = Backbone.PageableCollection.extend({
    model: admin.SupplierReportModel,
  url: '/btree/admin/get_all_supplier_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp.data;
        }
}); 

admin.SupplierDetailsReportCollection = Backbone.PageableCollection.extend({
    model: admin.SupplierDetailsReportModel,
  url: '/btree/admin/get_all_detailsupplier_report',
  mode: "server",
      state: {
          pageSize: 5,
          sortKey: "updated",
          order: 1,

        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp.data;
        }
});

admin.SupplierGraphReportCollection = Backbone.PageableCollection.extend({
    model: admin.SupplierReportModel,
  url: '/btree/admin/get_all_supplier_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp;
        }
}); 


admin.ItemReportCollection = Backbone.PageableCollection.extend({
    model: admin.ItemReportModel,
  url: '/btree/admin/get_all_item_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp.data;
        }
}); 

admin.ItemGraphReportCollection = Backbone.PageableCollection.extend({
    model: admin.ItemReportModel,
  url: '/btree/admin/get_all_item_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp;
        }
}); 


admin.ExpenseCategoryReportCollection = Backbone.PageableCollection.extend({
    model: admin.ExpenseCategoryReportModel,
  url: '/btree/admin/get_all_expense_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp.data;
        }
});

admin.ExpenseGraphReportCollection = Backbone.PageableCollection.extend({
    model: admin.ExpenseCategoryReportModel,
  url: '/btree/admin/get_all_expense_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp;
        }
});

admin.SalesReportCollection = Backbone.PageableCollection.extend({
    model: admin.SalesReportModel,
  url: '/btree/admin/get_all_sales_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp.data;
        }
});

admin.SalesDetailsReportCollection = Backbone.PageableCollection.extend({
    model: admin.SalesDetailsReportModel,
  url: '/btree/admin/get_all_detailsales_report',
  mode: "server",
      state: {
          pageSize: 5,
          sortKey: "updated",
          order: 1,

        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp.data;
        }
});

admin.SalesDetailItemReportCollection = Backbone.Collection.extend({
    model: admin.SalesDetailsItemReportModel,
    url: '/btree/admin/get_all_detailsales_item',
    parse : function(response){
      this.current_page = response.current_page;
        return response;  
   }  
});

admin.ClaimPointsReportCollection = Backbone.Collection.extend({
    model: admin.SalesDetailsItemReportModel,
    url: '/btree/admin/customer_pointclaim',
    parse : function(response){
      this.current_page = response.current_page;
        return response;  
   }  
});

admin.SalesGraphReportCollection = Backbone.PageableCollection.extend({
    model: admin.SalesReportModel,
  url: '/btree/admin/get_all_sales_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp;
        }
});

admin.SuspendedSalesReportCollection = Backbone.PageableCollection.extend({
    model: admin.SuspendedSalesReportModel,
  url: '/btree/admin/get_all_suspendedsales_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp.data;
        }
});

admin.SuspendedSalesGraphReportCollection = Backbone.PageableCollection.extend({
    model: admin.SuspendedSalesReportModel,
  url: '/btree/admin/get_all_suspendedsales_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp;
        }
});


admin.ReceivingReportCollection = Backbone.PageableCollection.extend({
    model: admin.ReceivingReportModel,
  url: '/btree/admin/get_all_receiving_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp.data;
        }
});

admin.ReceivingGraphReportCollection = Backbone.PageableCollection.extend({
    model: admin.ReceivingReportModel,
  url: '/btree/admin/get_all_receiving_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp;
        }
});

admin.DiscountReportCollection = Backbone.PageableCollection.extend({
    model: admin.DiscountReportModel,
  url: '/btree/admin/get_all_discount_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp.data;
        }
}); 

admin.DiscountDetailsReportCollection = Backbone.PageableCollection.extend({
    model: admin.DiscountDetailsReportModel,
  url: '/btree/admin/get_all_discountdetails_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp.data;
        }
});

admin.DiscountGraphReportCollection = Backbone.PageableCollection.extend({
    model: admin.DiscountReportModel,
  url: '/btree/admin/get_all_discount_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp;
        }
}); 

admin.PaymentReportCollection = Backbone.PageableCollection.extend({
    model: admin.PaymentReportModel,
  url: '/btree/admin/get_all_payment_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp.data;
        }
}); 

admin.TaxReportCollection = Backbone.PageableCollection.extend({
    model: admin.TaxReportModel,
  url: '/btree/admin/get_all_tax_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp.data;
        }
}); 

admin.TaxGraphReportCollection = ackbone.PageableCollection.extend({
    model: admin.PaymentReportModel,
  url: '/btree/admin/get_all_payment_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp;
        }
}); 

admin.EmployeeReportCollection = Backbone.PageableCollection.extend({
    model: admin.EmployeeReportModel,
  url: '/btree/admin/get_all_employee_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp.data;
        }
}); 

admin.EmployeeDetailsReportCollection = Backbone.PageableCollection.extend({
    model: admin.EmployeeDetailsReportModel,
  url: '/btree/admin/get_all_detailemployee_report',
  mode: "server",
      state: {
          pageSize: 5,
          sortKey: "updated",
          order: 1,

        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp.data;
        }
});

admin.EmployeeGraphReportCollection = Backbone.PageableCollection.extend({
    model: admin.EmployeeReportModel,
  url: '/btree/admin/get_all_employee_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp;
        }
}); 

admin.ItemkitsReportCollection = Backbone.PageableCollection.extend({
    model: admin.ItemkitsReportModel,
  url: '/btree/admin/get_all_itemkits_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp.data;
        }
}); 

admin.ItemkitsGraphReportCollection = Backbone.PageableCollection.extend({
    model: admin.ItemkitsReportModel,
  url: '/btree/admin/get_all_itemkits_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp;
        }
});

admin.TransactionReportCollection = Backbone.PageableCollection.extend({
    model: admin.InventoryReportModel,
  url: '/btree/admin/get_all_transaction_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp.data;
        }
}); 

admin.TransactionGraphReportCollection = Backbone.PageableCollection.extend({
    model: admin.InventoryReportModel,
  url: '/btree/admin/get_all_transaction_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp;
        }
});  


admin.CommissionReportCollection = Backbone.PageableCollection.extend({
    model: admin.CommissionReportModel,
  url: '/btree/admin/get_all_commission_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp.data;
        }
}); 

admin.CommissionGraphReportCollection = Backbone.PageableCollection.extend({
    model: admin.CommissionReportModel,
  url: '/btree/admin/get_all_commission_report',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp;
        }
}); 

admin.LowInventoryReportCollection = Backbone.PageableCollection.extend({
    model: admin.InventoryReportModel,
  url: '/btree/admin/get_all_lowinventory',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp.data;
        }
}); 

admin.InventorySummaryReportCollection = Backbone.PageableCollection.extend({
    model: admin.InventoryReportModel,
  url: '/btree/admin/get_all_inventorysummary',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp.data;
        }
}); 


admin.DeadItemReportCollection = Backbone.PageableCollection.extend({
    model: admin.InventoryReportModel,
  url: '/btree/admin/get_all_deaditem',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },
        parseRecords: function (resp, options) {
          return resp.data;
        }
}); 
