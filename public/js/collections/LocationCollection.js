var admin = admin || {};
admin.LocationCollection = Backbone.PageableCollection.extend({
    model: admin.LocationModel,
  url: '/btree/admin/get_all_location',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },

        // You can remap the query parameters from `state` keys from
        // the default to those your server supports
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
         // get the state from Github's search API result
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },

        // get the actual records
        parseRecords: function (resp, options) {
          return resp.data;
        }
}); 

admin.LocationTrashCollection = Backbone.PageableCollection.extend({
    model: admin.LocationModel,
  url: '/btree/admin/get_all_locationtrash',
  mode: "server",
      state: {
    pageSize: 5,
          sortKey: "updated",
          order: 1
        },

        // You can remap the query parameters from `state` keys from
        // the default to those your server supports
        queryParams: {
          totalPages: null,
          totalRecords: null,
          sortKey: "sort"
        },
         // get the state from Github's search API result
        parseState: function (resp, queryParams, state, options) {
          return {totalRecords: resp.total};
        },

        // get the actual records
        parseRecords: function (resp, options) {
          return resp.data;
        }
}); 



