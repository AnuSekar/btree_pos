var admin = admin || {};

admin.MgntProductItemView = Backbone.View.extend({

    tagName: 'option',

    initialize: function(){
      console.log('init');
      _.bindAll(this, 'render');
    },

    render: function(){
       console.log("veiws :: " + this.model.get('product_name'));
      $(this.el).attr('value',
      this.model.get('product_id')).html(this.model.get('product_name'));
      return this;
    }

});
