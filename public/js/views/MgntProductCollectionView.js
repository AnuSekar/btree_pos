var admin = admin || {};

admin.MgntProductCollectionView = Backbone.View.extend({

	initialize: function(options) {
    	console.log('Initialize MgntProductCollectionView');
    	console.log('faqCollection');
	    	console.log(options.proCollection);
			this.collection = new admin.ProductCollection(options.proCollection);

		_.bindAll(this, "renderProduct");
		_.bindAll(this, "render");
		this.render();
	},

	events:{
	},

	render: function() {
    	console.log('Rendering Product collection');
    	if( this.collection )
    	{    		
    	   // this.$el.html( '<option value="">Select</option>' );
			   this.collection.each(function(item) {
		    	  console.log(item.get('product_id'));
		        console.log(item.get('product_name'));
				this.renderProduct(item);
			}, this );
		}
    	return this;
	},

	renderProduct: function( item ) {
		var proItemView = new admin.MgntProductItemView({
			model: item
		});
    	this.$el.append( proItemView.render().el );
	},

});
