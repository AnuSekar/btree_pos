var admin = admin || {};

admin.ExpensePageView = Backbone.View.extend({
    template: $( '#expensePageTpl' ).html(),
  initialize: function() {
    appRouter.paginationtype = "";
    this.render();    
  },

  events:{
       'click #create_expense' : 'expenseCreate',
       'click #expense_deleteall'    :'restoreordeleteAll',
       'click #expense_deleteselect'    :'addordeleteSelect',
       'input #expense_search'  : 'expenseSearch',
       'click #expense_restoreall'    :'restoreordeleteAll',
       'click #expense_addselect'    :'addordeleteSelect',
  },

  render: function() {          
      var _self = this;
      this.$el.html(this.template);
      $("#switch1").switchbutton({
      checkedLabel: 'YES',
      uncheckedLabel: 'NO',
      classes: 'ui-switchbutton-ios5'
    }).change(function(e){
     var toggledel = $(this).prop("checked") ? "checked" : "unchecked";
      _self.toggle(toggledel);
    });
    
    var toggleval = $("#switch1").prop("checked") ? "checked" : "unchecked";
    this.toggle(toggleval);
    return this;
  },
  toggle:function(toggledel){
  console.log(toggledel);
  if(toggledel == 'checked'){
    this.expenseListView = new admin.ExpenseTableView({el: $( '#expense-list'),
      mode:'normal'});
     $('#create_expense').show();
     $('#expense_deleteselect').show();
     $('#expense_deleteall').show();
     $('#expense_search').show();
     $('#expense_search').val("");
     $('#expense_restoreall').hide();
     $('#expense_addselect').hide();
     this.adddeletelabel = "Delete Selected Expense";
     this.adddeletetext = "Do You want to Delete Selected expense???";
     this.adddeleteurl = "/btree/admin/delete_selectexpense";
     this.searchurl = "/btree/admin/search_expense";
     this.restoredeletelabel = "Delete All Expense";
     this.restoredeletetext = "Do You want to Delete all this expense???";
     this.restoredeleteurl = "/btree/admin/delete_allexpense";
  }else if(toggledel == 'unchecked'){
    this.expenseListView = new admin.ExpenseTableView({el: $( '#expense-list' ),
      mode:'trash'});
     $('#create_expense').hide();
     $('#expense_deleteselect').hide();
     $('#expense_deleteall').hide();
     $('#expense_search').show();
     $('#expense_search').val("");
     $('#expense_restoreall').show();
     $('#expense_addselect').show();
     this.adddeletelabel = "Restore Selected Expense";
     this.adddeletetext = "Do You want to Restore Selected expense???";
     this.adddeleteurl = "/btree/admin/add_selectexpense";
     this.searchurl = "/btree/admin/search_trashexpense";
     this.restoredeletelabel = "Restore All Expense";
     this.restoredeletetext = "Do You want to Restore all this expense???";
     this.restoredeleteurl = "/btree/admin/restore_allexpense";
  }
  },

  expenseCreate:function(e){
    e.preventDefault();
        var self = this;
    var currentView = new admin.ExpenseFormPageView({
                template: _.template($('#expenseCreateFormTpl').html()),
                mode: 'create'
            }); 
    var expensemodal= new BackboneBootstrapModals.ConfirmationModal({
          label: "Create Expense",
          text: currentView.el,
          onConfirm: function(e) {
            console.log("action confirmed");
            return self.rendersaveexpense();
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    expensemodal.render();
  },

  restoreordeleteAll:function(e)
  {
          e.preventDefault();
        var self = this;
        var expenseselect= new BackboneBootstrapModals.ConfirmationModal({
          label: self.restoredeletelabel,
          text: self.restoredeletetext,
          onConfirm: function(e) {
             $.ajax({
                    type: 'POST',
                    url: self.restoredeleteurl,
                     contentType:'application/json',
                    cache:false,
                    success: function(data) {
                      self.expenseListView.removeoraddAll();
                      $( "div.success").html("All expense are Deleted Successfully.");
                      $( "div.success" ).fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );   
                    },
                    error: function(data) {
                      
                    }
             });
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    expenseselect.render();   
  },

  
  expenseSearch:function () {
  
    console.log("expenseSearch");
    $('#findStatus').html("");
     //$('#findStatus').html(alert("No Records Found"));
    var search_expense = $('#expense_search').val();
    var data = {};
    self=this;
    if(search_expense.length >=1 )
    {
        console.log("more 2");
        data.expense_name = search_expense;

        console.log(" search data "+JSON.stringify(data));

        this.expenseListView.collection.fetch({url:'/btree/admin/search_expense',reset: true, data:data, processData: true,
            success: function (coll) {
                $( '#expensetable' ).empty();
                $( '#expensetable' ).unbind();
                $( '#paginator' ).empty();
                $( '#paginator' ).unbind();                         
                self.expenseListView.render();
            },
            error: function(err) {
                $( "div.failure").html(" Error while searching the expense . Contact Administrator.");
                $( "div.failure" ).fadeIn( 300 ).delay( 3500 ).fadeOut( 800 );                      
            }

        });
        $("#expense_load_more").show();     
    }
    if(search_expense.length == 0)
    {            
        console.log("empty");
        skip = 0;
         this.expenseListView.collection.fetch({reset: true, data, processData: true,
            success: function (coll) {
                $( '#expensetable' ).empty();
                $( '#expensetable' ).unbind();
                $( '#paginator' ).empty();
                $( '#paginator' ).unbind();                     
                self.expenseListView.render();   
            },       

        }); 
        $("#expense_load_more").show();         
    }
},

rendersaveexpense:function(){
  var self = this;
 var expensemodel = new admin.ExpenseModel();
 var expensedata = { expense_category_id:$('#expense_category_name').val(),
 expense_description:$.trim($('#expense_description').val()),
 amount:$.trim($('#amount').val()),
 payment_type:$.trim($('#paymenttype').val()),
 supplier_name:$.trim($('#supplier_name').val())
};
expensemodel.set(expensedata);
if(expensemodel.isValid()){
  expensemodel.save(null,{ 
    success: function () {
      $( "div.success").html("Expense Successfully Created.");
        self.render();
        return true;
    },
    error: function (model, error) {
        return false;
    }});

}else{
  _.each(expensemodel.validationError, function (error) {
       $('#' + error.name).text(error.message);
    });
return false;
}
 return false;
},

addordeleteSelect:function(e){
  e.preventDefault();
  var self = this;
  var selectedModels = appRouter.expensegrid.getSelectedModels();
  console.log(selectedModels);
  var selectexpense = [];
for(i=0;i<selectedModels.length;i++){
  selectexpense.push(selectedModels[i].attributes.expense_id);
}
    var expenseselect= new BackboneBootstrapModals.ConfirmationModal({
          label: self.adddeletelabel,
          text: self.adddeletetext,
          onConfirm: function(e) {
            $('#expensetable').empty();
            $('#expensetable').unbind();
            $( '#paginator' ).empty();
            $( '#paginator' ).unbind(); 
            $.ajax({
              type:'POST',
              url:self.adddeleteurl,
              data:JSON.stringify(selectexpense),
              contentType:'application/json',
              cache:false,
              success:function(){
                self.render();
              },
              error:function(){
                return false;
              }
            });
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    expenseselect.render();
},
});
