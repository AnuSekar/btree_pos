var admin = admin || {};

admin.SaleAddItemPageView = Backbone.View.extend({

      initialize:function(options){
        this.template = options.template;
        this.model = options.model;
        this.mode = options.mode;
        this.price = 0;
        this.render();
       
      },
      events:{
        'input #quantity' : 'renderQuantity',
        'input #discount' : 'renderDiscount',
        'input #price' : 'renderPrice'
      },

      render: function() {

        console.log(this.model.attributes);
        // var tax =  parseFloat(this.model.attributes.percent);
        // var cost_price =  parseFloat(this.model.attributes.cost_price);
        // cost_price = cost_price (1-parseFloat(this.model.attributes.discount)/100);
        // var taxamount = (tax/100)*cost_price;
        // var price =  cost_price + taxamount ; 
        // console.log(price);
        // this.model.set({total_price:price.toFixed(3)});
        
          console.log("inside",i);
          this.$el.append(this.template(this.model.toJSON()));
        this.renderTotalCalculation(this.model.attributes.item_id);
  
                console.log(appRouter.sales_item);
         //this.renderSubTotal();
        return this;
      },

      renderQuantity:function(e){
        console.log($(e.target).val());
        quantity = parseFloat($(e.target).val());
        var iid = parseInt($(e.target).attr('iid'));
        var item_model = appRouter.sales_item.where({item_id:iid})[0];
        item_model.set({"quantity":quantity});
        console.log(appRouter.sales_item);
        this.renderTotalCalculation(iid);

      },

      renderDiscount:function(e){
        console.log($(e.target).val());
        discount = parseFloat($(e.target).val());
        var iid = parseInt($(e.target).attr('iid'));
        if(isNaN(discount)){
         discount = 0;
        }
        var item_model = appRouter.sales_item.where({item_id:iid})[0];
        item_model.set({"discount":discount});
        console.log(appRouter.sales_item);
        this.renderTotalCalculation(iid);
      },

      renderPrice:function(e){
        console.log($(e.target).val());
        price = parseFloat($(e.target).val());
        var iid = parseInt($(e.target).attr('iid'));
        var item_model = appRouter.sales_item.where({item_id:iid})[0];
        item_model.set({"cost_price":price});
        console.log(appRouter.sales_item);
        this.renderTotalCalculation(iid);
        
        

      },
    renderTotalCalculation:function(iid,quantity,discount){
        
        var item_model = appRouter.sales_item.where({item_id:iid});
        console.log(item_model);
        var price = parseFloat(item_model[0].attributes.cost_price);
        var quantity = parseFloat(item_model[0].attributes.quantity);
        var discount = parseFloat(item_model[0].attributes.discount);
        if(isNaN(discount)){
         discount = 0;
        }
        var taxPrice = price * (1-discount/100);
        var taxtotal = (parseFloat(item_model[0].attributes.percent)/100)*taxPrice;
        var itemprice = taxtotal + taxPrice;
        var  totalprice = itemprice * quantity;
        console.log(quantity,iid,discount,taxPrice);
        console.log(totalprice.toFixed(3));

        $('#'+iid).find('#price_item').text(totalprice.toFixed(3));
        this.renderSubTotal();
        

    },
    renderSubTotal:function(){
      $('#sales_sub_total').val("");
      var tax_gto = appRouter.sales_item.groupBy( function(model){
        return model.get('tax_group');
      });
      var sst=0;
      appRouter.sales_item.each(function(model){
        var price = parseFloat(model.get('cost_price'));
        var discount = parseFloat(model.get('discount'));
        var priquantity = parseFloat(model.get('quantity'));
        var totalprice = priquantity * price * (1-discount/100 );
        console.log(totalprice);
        sst=sst+totalprice;
      });
      var taxgp1 ;
      var arr = [];
      var taxgpspl;
      var taxpri;
      var totalpri;
      var taxquan;
      var taxgpspl1;
      var taxdis;
      var taxafdis;
      var totatax;
      for(listtax in tax_gto){
        console.log(tax_gto[listtax]);
       for(i=0;i< tax_gto[listtax].length;i++){
        taxgp = tax_gto[listtax][i].attributes.tax_group;
        taxpri = parseFloat(tax_gto[listtax][i].attributes.cost_price);
        taxquan = parseFloat(tax_gto[listtax][i].attributes.quantity) ;
        console.log(taxquan);
        taxafdis = taxpri * (1-parseFloat(tax_gto[listtax][i].attributes.discount)/100);
        console.log(taxafdis);
        taxgpspl = taxgp.split(',');
        console.log(taxgpspl);
        for(j=0;j<taxgpspl.length;j++){
          taxgpspl1 = taxgpspl[j].split(" ");
          console.log((parseFloat(taxgpspl1[1])/100));
          totatax = (parseFloat(taxgpspl1[1])/100) * taxafdis * taxquan;
          arr.push({name : taxgpspl[j], price : totatax });
        }
        console.log(arr);
       }
       var groups = {};
       var salesgroups = {};
        for(k=0;k<arr.length;k++){
          var groupName = arr[k].name;
          console.log(groupName);
          if (!groups[groupName]) {
           groups[groupName] = 0;
           salesgroups[groupName] = 0;
          }
          groups[groupName] =parseFloat(groups[groupName])+arr[k].price;
        }
      }
       $('#tax_amount_list').text("");
      Object.keys(groups).forEach(function(key) {
        console.log('Key : ' + key + ', Value : ' + groups[key])
         $('#tax_amount_list').append(key +"% :"+groups[key].toFixed(3)+"<br>");
      });
      appRouter.sales_tax = salesgroups;
     
      $('#sales_sub_total').val(sst);
      this.renderTotal();
    },
    renderTotal:function(){
      $('#sales_total').val("");
      var st = 0;
      $('#sales_item_list tr').find('#price_item').each(function(){
        st = st + parseFloat($(this).text());
      });
        console.log(st);
          $('#sales_total').val(st);
    },


});
