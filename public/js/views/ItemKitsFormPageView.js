var admin = admin || {};

admin.ItemKitsFormPageView = Backbone.View.extend({

initialize: function(options) {
      this.mode = options.mode;
      this.template = options.template;
      this.id = options.id;
      this.itemkit = '';
      this.itemsearch = new admin.SalesItemCollection();
      var id = {};
      id.id = this.id;
      var self = this;
      console.log ('mode: ' + this.mode + ' model: ' + this.model);
      if (new String(this.mode).valueOf() == new String('create').valueOf()) {
     this.render(); 
      } else {   //edit
        $.when(
        $.ajax({
            url: "/btree/admin/get_itemkit_by_id",
            type: "POST",
            contentType:'application/json',
            data:JSON.stringify(id),
            cache:false,
            success: function(data) {
               console.log(data);
               self.itemkit = data[0];
            },
            error: function(data) {
               errorhandle(data);

            }
          })
          ).done(function(){
          self.render();

          })
       }         
      
     },

    events: {
    'input #add_item' : 'AddItem',
    'click #del-item' : 'deleteItem'
  },

    render: function() {
    if (new String(this.mode).valueOf() == new String('create').valueOf()) {
      var self = this;
      this.$el.html(this.template);
      
      } else { //edit
        this.$el.html(this.template({item_kit_name : this.itemkit.item_kit_name,
          item_kit_discount: this.itemkit.item_kit_discount}));
        console.log(this.itemkit.item_name);
          var itemname = this.itemkit.item_name.split(',');
          var quantity = this.itemkit.quantity.split(',');
          var item_id = this.itemkit.item_id.split(',');
          lihtml = '';
        for(i=0;i<itemname.length;i++){
            lihtml+= '<tr id=a'+item_id[i]+' iit='+item_id[i]+'><td><a id="del-item" data-toggle="tooltip" data-placement="top" title="Delete" val="'+item_id[i]+'"><i val="'+item_id[i]+'" class="glyphicon glyphicon-minus-sign"></i></a></td>'+'<td><td>'+ itemname[i]+'</td>'+'<td> <input type="text" id="quantity" value="'+quantity[i]+'"/> </td>';
        }
         $('#list_item_kit').append(lihtml);
    }           
        return this;
  },
  AddItem:function(){
    var self = this;
    var searchdata = $('#add_item').val();
          var data = {};
          if(searchdata.length > 0){
          data.searchitem = searchdata;
          $.when(
            this.itemsearch.fetch({
            url:"/btree/admin/get_salesitem_list",
            reset:true,
            data:data,
            contentType: "application/json",
         })
          ).done(function(){
          var itemNames = self.itemsearch.map(function (model) {
          return _.pick(model.toJSON(), ["item_name", "item_number","label","item_id"]);
            });
          console.log(itemNames);
            $("#add_item").autocomplete({ 
            source : itemNames,
            minLength : 0,
            select: function(event, ui){ 
              var selectedModel =self.itemsearch.where({item_name: ui.item.value})[0];
              console.log("inside",selectedModel);
              var id = selectedModel.attributes.item_id;
              lihtml = '<tr id=a'+id+' iit='+id+'><td><a id="del-item" data-toggle="tooltip" data-placement="top" title="Delete" val="'+id+'"><i val="'+id+'" class="glyphicon glyphicon-minus-sign"></i></a></td>'+'<td><td>'+ selectedModel.attributes.item_name+'</td>'+'<td> <input type="text" id="quantity" /> </td>';
              $('#list_item_kit').append(lihtml);
              }
          })
       .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
          console.log("inside");
          return $( "<li>" )
            .append( "<a>" + item.item_name + 
                "<br><span style='font-size: 80%;'>Desc: " + item.item_number + "</span></a>")
            .appendTo( ul );
    };
        })
    }     
  },
  deleteItem:function(e){

     var id =  $(e.target).attr('val');
     console.log(id);
     $('#a'+id).remove();
  }
});
