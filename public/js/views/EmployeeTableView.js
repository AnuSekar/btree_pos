var admin = admin || {};

admin.EmployeeTableView = Backbone.View.extend({
  
  initialize: function(options) {
      console.log('Initialize employeeTableView');
    var self = this;
        this.pageNo = 1;
       limit =2;
        this.recIndex = 1;
        this.options = options;
      console.log(options.mode);
       $('#managementtable').empty();
      $('#managementtable').unbind();
      $( '#paginator' ).empty();
      $( '#paginator' ).unbind(); 
      if(options.mode == 'normal'){
        this.collection = new admin.EmployeeCollection();    
      }else if(options.mode == 'trash'){
        this.collection = new admin.EmployeeTrashCollection();    
      }
      this.collection.fetch({              
        success:function(data){
          console.log("success");

          console.log(JSON.stringify(data));
          self.render();


        },
        error:function(data){
          /*try{
            var errData = JSON.parse(data.responseText);
            if ( errData.errCode == 550) {
              window.location.href = '/sessionExpired';
          } else {
            if (errData.errMsg.length > 0) {
                var failureMsg = errData.errMsg;  
              } else {
                  var failureMsg = "Error occurred while fetching the employee Table View. Please Contact Administrator.";  
              }
              $( "div.failure").html(failureMsg);
                    $( "div.failure" ).fadeIn( 300 ).delay( 3500 ).fadeOut( 800 );        
           }
          }catch(e){
          window.location.href = '/sessionExpired';
        }*/
        }
      }); 
  },

  render: function() {

    var self = this;
     if(this.options.mode == 'normal'){
        this.edittemplate = '<span style="display:inline-block; width: 20px;"><a id="editemployee" data-toggle="tooltip" data-placement="top" title="Edit">'+
      '<i class="glyphicon glyphicon-pencil"></i></a></span>'+
      '<span style="display:inline-block; width: 20px;"><a id="deleteemployee" data-toggle="tooltip" data-placement="top" title="Delete">'+
      '<i class="glyphicon glyphicon-minus-sign"></i></a></span>';    
      this.edittext = 'Delete';
      }else if(this.options.mode == 'trash'){
        this.edittemplate = '<span style="display:inline-block; width: 20px;"><a id="deleteemployee" data-toggle="tooltip" data-placement="top" title="Delete">'+
      '<i class="glyphicon glyphicon-minus-sign"></i></a></span>';  
      this.edittext = 'Restore';
      }
    var DeleteCell = Backgrid.Cell.extend({
    template: _.template(self.edittemplate),
    events: {
      "click #editemployee": "editRow",
      "click #deleteemployee": "deleteRow",
    },
    deleteRow: function (e) {
      console.log("Hello");
      e.preventDefault();
      modelvalue = this.model;
      var employeemodal= new BackboneBootstrapModals.ConfirmationModal({
          label: "Delete Employee",
          text: "Are You want to Delete this employee?",
          onConfirm: function(e) {
            e.preventDefault();
            var employeemodal = modelvalue;
            employeemodal.destroy({
                success: function (model, respose, options) {
                    console.log("The model has deleted the server");
                    $('#managementtable').empty();
                    $('#managementtable').unbind();
                    $( '#paginator' ).empty();
                    $( '#paginator' ).unbind(); 
                    appRouter.currentView.Search();
                    return true;
                },
                error: function (model, xhr, options) {
                    console.log("Something went wrong while deleting the model");
                    return false;
                }
            });
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
   employeemodal.render();
    },
    editRow:function(e){
      var self= this;
       console.log("Hello",this.model);
      e.preventDefault();
      var currentView = new admin.FormPageView({
                template: _.template($('#employeeEditFormTpl').html()),
                mode: 'edit',
                model:self.model.attributes
            }); 
    var employeemodal= new BackboneBootstrapModals.ConfirmationModal({
          label: "Edit Employee",
          text: currentView.el,
          onConfirm: function(e) {
            console.log("action confirmed");
            return self.rendereditemployee();
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    employeemodal.render();
    },
    rendereditemployee:function(){
      var self = this;
     var employeemodel = new admin.EmployeeModel();
     var employeedata = { person_id:self.model.attributes.person_id,
      first_name:$.trim($('#employee_first_name').val()),
  last_name:$.trim($('#employee_last_name').val()),
  phone_number:$.trim($('#employee_phone_number').val()),
  email:$.trim($('#employee_email').val()),
  address_1:$.trim($('#employee_address_1').val()),
  address_2:$.trim($('#employee_address_2').val()),
  city:$.trim($('#employee_city').val()),
  state:$.trim($('#employee_state').val()),
  zip:$.trim($('#employee_zip').val()),
  country:$.trim($('#employee_country').val()),
  username:$.trim($('#employee_username').val()),
  password:$.trim($('#employee_password').val())
     };
    employeemodel.set(employeedata);
    if(employeemodel.isValid()){
      employeemodel.save(null,{ 
        success: function () {
          $('#managementtable').empty();
          $('#managementtable').unbind();
          $( '#paginator' ).empty();
          $( '#paginator' ).unbind(); 
          appRouter.currentView.Search();
            console.log('success!');
            
        },
        error: function (model, error) {
            console.log(error);
            return false;
        }});
    }else{
      _.each(employeemodel.validationError, function (error) {
           $('#' + error.name).text(error.message);
        });
    return false;
    }
     return true;
    },
      
    render: function () {
      this.$el.html(this.template());
      this.delegateEvents();
      return this;
    }

});
    
    var columns = [{
    name: "", 
    cell: "select-row",
    headerCell: "select-all",

  }, 
    {
    name: "first_name",
    label: "First Name",
     editable: false,
    cell: "string" 
  },
  {
    name: "last_name",
    label: "Last Name",
     editable: false,
    cell: "string" 
  },
  {
    name: "username",
    label: "User Name",
     editable: false,
    cell: "string" 
  },  
  {
    name: "email",
    label: "Email",
     editable: false,
    cell: "string" 
  }, {
    name: "phone_number",
    label: "Phone Number",
     editable: false,
    cell: "string" 
  }, 
  {
    name: "address_1",
    label: "Address",
     editable: false,
    cell: "string" 
  }, 
  {
        name: "delete",
        label: "Action",
        cell: DeleteCell
    }];

var grid = new Backgrid.Grid({
  columns: columns,
  collection: self.collection
});
var paginator = new Backgrid.Extension.Paginator({
        collection: self.collection
      });
$("#managementtable").append(grid.render().el);
$("#paginator").append(paginator.render().$el);
appRouter.employeegrid = grid;

            return this;    
  },

    removeoraddAll: function()
  {
      this.collection.reset();
      this.$el.empty();

  },

}); 