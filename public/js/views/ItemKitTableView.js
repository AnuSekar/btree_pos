var admin = admin || {};

admin.ItemKitTableView = Backbone.View.extend({

  initialize: function(options) {
      console.log('Initialize itemTableView');
    var self = this;
        this.pageNo = 1;
       limit =2;
        this.recIndex = 1;
        this.options = options;
      console.log(options.mode);
       $('#itemtable').empty();
      $('#itemtable').unbind();
      $( '#paginator' ).empty();
      $( '#paginator' ).unbind(); 
      if(options.mode == 'normal'){
        this.collection = new admin.ItemKitCollection();    
      }else if(options.mode == 'trash'){
        this.collection = new admin.ItemKitTrashCollection();    
      }
      this.collection.fetch({       
        success:function(data){
          console.log("success");

          console.log(JSON.stringify(data));
          self.render();


        },
        error:function(data){
          /*try{
            var errData = JSON.parse(data.responseText);
            if ( errData.errCode == 550) {
              window.location.href = '/sessionExpired';
          } else {
            if (errData.errMsg.length > 0) {
                var failureMsg = errData.errMsg;  
              } else {
                  var failureMsg = "Error occurred while fetching the item Table View. Please Contact Administrator.";  
              }
              $( "div.failure").html(failureMsg);
                    $( "div.failure" ).fadeIn( 300 ).delay( 3500 ).fadeOut( 800 );        
           }
          }catch(e){
          window.location.href = '/sessionExpired';
        }*/
        }
      }); 
  },


  render: function() {

    var self = this;
    if(this.options.mode == 'normal'){
        this.edittemplate = '<span style="display:inline-block; width: 20px;"><a id="edititem" data-toggle="tooltip" data-placement="top" title="Edit">'+
      '<i class="glyphicon glyphicon-pencil"></i></a></span>'+
      '<span style="display:inline-block; width: 20px;"><a id="deleteitem" data-toggle="tooltip" data-placement="top" title="Delete">'+
      '<i class="glyphicon glyphicon-minus-sign"></i></a></span>';    
      this.edittext = 'Delete';
      }else if(this.options.mode == 'trash'){
        this.edittemplate = '<span style="display:inline-block; width: 20px;"><a id="deleteitem" data-toggle="tooltip" data-placement="top" title="Delete">'+
      '<i class="glyphicon glyphicon-minus-sign"></i></a></span>';  
      this.edittext = 'Restore';
      }

    var DeleteCell = Backgrid.Cell.extend({
    template: _.template(self.edittemplate),
    events: {
      "click #edititem": "editRow",
      "click #deleteitem": "deleteRow",
    },
    deleteRow: function (e) {
      console.log("Hello");
      e.preventDefault();
      modelvalue = this.model;
      var itemmodal= new BackboneBootstrapModals.ConfirmationModal({
          label: "Delete Item",
          text: "Are You want to Delete this item?",
          onConfirm: function(e) {
            e.preventDefault();
            var itemmodal = modelvalue;
            itemmodal.destroy({
                success: function (model, respose, options) {
                    console.log("The model has deleted the server");
                    $('#itemtable').empty();
                    $('#itemtable').unbind();
                    $( '#paginator' ).empty();
                    $( '#paginator' ).unbind(); 
                    appRouter.currentView.itemSearch();
                    return true;
                },
                error: function (model, xhr, options) {
                    console.log("Something went wrong while deleting the model");
                    return false;
                }
            });
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    itemmodal.render();
    },
    editRow:function(e){
      var self= this;
      e.preventDefault();
      var itemkitid = this.model.get('item_kit_id');
      console.log(this.model.get('item_kit_id'));
      var currentView = new admin.ItemKitsFormPageView({
                template: _.template($('#itemkitEditFormTpl').html()),
                mode: 'edit',
                id :itemkitid,
            }); 

    var itemmodal= new BackboneBootstrapModals.ConfirmationModal({
          label: "Edit Item",
          text: currentView.el,
          onConfirm: function(e) {
            console.log("action confirmed");
            return self.renderedititem();
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    itemmodal.render();
    },
    renderedititem:function(){
      var self = this;
    var itemmodel = new admin.ItemKitModel();
 var item = {};
 var itemarray = [];
 var itemdata = { 
  item_kit_id : this.model.get('item_kit_id'),
 item_kit_name:$.trim($('#item_name_kit').val()),
 item_kit_discount:$.trim($('#item_kit_discount').val()),
 };
  $('#list_item_kit').find('tr').each(function(){
     item.id =  $(this).attr('iit');
     item.quantity =  $(this).find('td #quantity').val();

   itemarray.push({"item_id":item.id,
   "item_quantity":item.quantity
   });
  });
  itemdata.itemarray = itemarray;
itemmodel.set(itemdata);
 if(itemmodel.isValid()){
      itemmodel.save(null,{ 
        success: function () {
          $('#itemtable').empty();
          $('#itemtable').unbind();
          $( '#paginator' ).empty();
          $( '#paginator' ).unbind(); 
          appRouter.currentView.itemSearch();
            console.log('success!');
            
        },
        error: function (model, error) {
            console.log(error);
            return false;
        }});
    }else{
      _.each(itemmodel.validationError, function (error) {
           $('#' + error.name).text(error.message);
        });
    return false;
    }
     return true;
    },
      
    render: function () {
      this.$el.html(this.template());
      this.delegateEvents();
      return this;
    }

});
    var columns = [{
    name: "", 
    cell: "select-row",
    headerCell: "select-all",
  },
    {
    name: "item_kit_name",
    label: "Item Kit Name",
     editable: false,
    cell: "string" 
  }, 
  {
    name: "item_kit_discount",
    label: "Item Kit Discount",
     editable: false,
    cell: "string" 
  },
  {
        name: "delete",
        label: "Action",
        cell: DeleteCell
    }];

var grid = new Backgrid.Grid({
  columns: columns,
  collection: self.collection
});
var paginator = new Backgrid.Extension.Paginator({
        collection: self.collection
      });
$("#itemtable").append(grid.render().el);
$("#paginator").append(paginator.render().$el);
appRouter.itemgrid = grid;
            return this;  
  },

    removeoraddAll: function()
  {
      this.collection.reset();
      this.$el.empty();

  },

  renderItemItem:function(item,recIndex){
        
    var itemItemView = new admin.ItemRowView({
      model: item
    });
      this.$el.append(itemItemView.render().el );
  }
});
