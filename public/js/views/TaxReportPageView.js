var admin = admin || {};

admin.TaxReportPageView = Backbone.View.extend({
  
  template : $('#taxReportTpl').html(),
   
	initialize: function(options) {
        this.template = options.template;
        console.log(appRouter.reporttype);
        if(appRouter.reporttype == 'normal') {
          this.render(); 
        }else{
          this.graphrender(); 
        }
        
      
   
	},

	events:{
          

	},

	render: function() {
  var self = this;			
   this.$el.html(this.template);
    var columns = [{
    name: "name",
    label: "Tax Name",
     editable: false,
    cell: "string" 
  },
  {
    name: "tax_rate",
    label: "Tax Percent",
     editable: false,
    cell: "string" 
  },
  {
    name: "sales_amount",
    label: "Sub Total",
     editable: false,
    cell: "string" 
  },
  {
    name: "item_total_cost",
    label: "Total",
     editable: false,
    cell: "string" 
  },
  {
    name: "tax_amount",
    label: "Tax",
     editable: false,
    cell: "string" 
  },
  {
    name: "count",
    label: "Number of Item Sold",
     editable: false,
    cell: "string" 
  }];
console.log(appRouter.collection);
var grid = new Backgrid.Grid({
  columns: columns,
  collection: appRouter.collection,
  rendertype : 'report'
});
var paginator = new Backgrid.Extension.Paginator({
        collection: appRouter.collection
      });
$("#taxreport").append(grid.render().el);
$("#taxpaginator").append(paginator.render().$el);
return this;
	},
	graphrender:function(){
     var arr = [];
    appRouter.collection.each(function(item){
      arr.push({label : item.get('company_name'), value : item.get('item_cost_price') });
    });
    console.log(arr);
    this.$el.html(this.template);
    
    var chartData = {
          "chart": {
            "caption": "Company Report List",
            "subCaption": "Date specified",
            "xAxisName": "Company",
            "yAxisName": "Sales Amount",
            "numberPrefix": "rs",
            "theme": "carbon",
            "bgcolor": "#DCEAFC",
            "canvasbgcolor": "#DCEAFC",
            "canvasbgalpha": "10",
            "rotateValues": "1",
            "exportEnabled": "1",
            "transposeAnimation": "1",
            "pieRadius": "70",
          },

          "data": arr
        };
        revenueChartColumn = new FusionCharts({
          type: 'column2d',
          width: '420',
          height: '350',
          dataFormat: 'json',
          id: 'companyreport1',
          dataSource: chartData
        });
       revenueChartColumn.render("companyreport");

  }
});
