var admin = admin || {};

admin.MgntExpenseCollectionView = Backbone.View.extend({

  initialize: function(options) {
      console.log('Initialize MgntExpenseCollectionView');
      console.log('faqCollection');
        console.log(options.expCollection);
      this.collection = new Backbone.Collection(options.expCollection);

    _.bindAll(this, "renderExpense");
    _.bindAll(this, "render");
    this.render();
  },

  render: function() {
      console.log('Rendering Expense collection');
      if( this.collection )
      {       
         // this.$el.html( '<option value="">Select</option>' );
         this.collection.each(function(item) {
            console.log(item.get('expense_category_id'));
            console.log(item.get('expense_category_name'));
        this.renderExpense(item);
      }, this );
    }
      return this;
  },

  renderExpense: function( item ) {
    var expItemView = new admin.MgntExpenseItemView({
      model: item
    });
      this.$el.append( expItemView.render().el );
  },

});
