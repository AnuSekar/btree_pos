var admin = admin || {};

admin.MgntCompanyItemView = Backbone.View.extend({

    tagName: 'option',

    initialize: function(){
      console.log('init');
      _.bindAll(this, 'render');
    },

    render: function(){
       console.log("veiws :: " + this.model.get('company_name'));
      $(this.el).attr('value',
      this.model.get('company_id')).html(this.model.get('company_name'));
      return this;
    }

});
