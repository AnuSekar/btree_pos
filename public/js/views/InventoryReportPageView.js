var admin = admin || {};

admin.InventoryReportPageView = Backbone.View.extend({
  
  template : $('#inventoryReportTpl').html(),
   
	initialize: function(options) {
        this.template = options.template;
        console.log(appRouter.reporttype);
        if(appRouter.reporttype == 'normal') {
          this.render(); 
        }else{
          this.graphrender(); 
        }     
   
	},

	render: function() {
  var self = this;			
   this.$el.html(this.template);
    var columns = [{
    name: "inventory_name",
    label: "Inventory Name",
     editable: false,
    cell: "string" 
  },
  {
    name: "tax_amount",
    label: "Tax",
     editable: false,
    cell: "string" 
  },
  {
    name: "quantity_purchased",
    label: "Number of Item Sold",
     editable: false,
    cell: "string" 
  }];
console.log(appRouter.collection);
var grid = new Backgrid.Grid({
  columns: columns,
  collection: appRouter.collection,
  rendertype : 'report'
});
var paginator = new Backgrid.Extension.Paginator({
        collection: appRouter.collection
      });
$("#inventoryreport").append(grid.render().el);
$("#inventorypaginator").append(paginator.render().$el);
return this;
	},
	graphrender:function(){
  this.collection = new admin.InventoryGraphReportCollection();
     var arr = [];
     appRouter.collection.each(function(item){
      arr.push({label : item.get('inventory_name'), value : item.get('sales_amount') });
    });
    this.$el.html(this.template);
    
    var chartData = {
          "chart": {
            "caption": "Inventory Report List",
            "subCaption": "Date specified",
            "xAxisName": "Inventory",
            "yAxisName": "Sales Amount",
            "numberPrefix": "rs",
            "theme": "carbon",
            "bgcolor": "#DCEAFC",
            "canvasbgcolor": "#DCEAFC",
            "canvasbgalpha": "10",
            "rotateValues": "1",
            "exportEnabled": "1",
            "transposeAnimation": "1",
            "pieRadius": "70",
          },

          "data": arr
        };
        revenueChartColumn = new FusionCharts({
          type: 'column2d',
          width: '420',
          height: '350',
          dataFormat: 'json',
          id: 'inventoryreport1',
          dataSource: chartData
        });
       revenueChartColumn.render("inventoryreport");

  }
});
