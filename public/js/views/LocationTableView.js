var admin = admin || {};

admin.LocationTableView = Backbone.View.extend({

  initialize: function(options) {
      console.log('Initialize locationTableView');
    var self = this;
        this.pageNo = 1;
       limit =2;
        this.recIndex = 1;
        this.options = options;
      console.log(options.mode);
       $('#locationtable').empty();
      $('#locationtable').unbind();
      $( '#paginator' ).empty();
      $( '#paginator' ).unbind(); 
      if(options.mode == 'normal'){
        this.collection = new admin.LocationCollection();    
      }else if(options.mode == 'trash'){
        this.collection = new admin.LocationTrashCollection();    
      }

      this.collection.fetch({
        success:function(data){
          console.log("success");

          console.log(JSON.stringify(data));
          self.render();


        },
      }); 
  },


  render: function() {          
    var self = this;
    if(this.options.mode == 'normal'){
        this.edittemplate = '<span style="display:inline-block; width: 20px;"><a id="editlocation" data-toggle="tooltip" data-placement="top" title="Edit">'+
      '<i class="glyphicon glyphicon-pencil"></i></a></span>'+
      '<span style="display:inline-block; width: 20px;"><a id="deletelocation" data-toggle="tooltip" data-placement="top" title="Delete">'+
      '<i class="glyphicon glyphicon-minus-sign"></i></a></span>';    
      this.edittext = 'Delete';
      }else if(this.options.mode == 'trash'){
        this.edittemplate = '<span style="display:inline-block; width: 20px;"><a id="deletelocation" data-toggle="tooltip" data-placement="top" title="Delete">'+
      '<i class="glyphicon glyphicon-minus-sign"></i></a></span>';  
      this.edittext = 'Restore';
      }
    var DeleteCell = Backgrid.Cell.extend({
    template: _.template(self.edittemplate),
    events: {
      "click #editlocation": "editRow",
      "click #deletelocation": "deleteRow",
    },
    deleteRow: function (e) {
      console.log("Hello");
      e.preventDefault();
      modelvalue = this.model;
      var locationmodal= new BackboneBootstrapModals.ConfirmationModal({
          label: "Delete Location",
          text: "Are You want to Delete this location?",
          onConfirm: function(e) {
            e.preventDefault();
            var locationmodal = modelvalue;
            locationmodal.destroy({
                success: function (model, respose, options) {
                    console.log("The model has deleted the server");
                    $('#locationtable').empty();
                    $('#locationtable').unbind();
                    $( '#paginator' ).empty();
                    $( '#paginator' ).unbind(); 
                    appRouter.currentView.locationSearch();
                    return true;
                },
                error: function (model, xhr, options) {
                    console.log("Something went wrong while deleting the model");
                    return false;
                }
            });
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    locationmodal.render();
    },
    editRow:function(e){
      var self= this;
       console.log("Hello",this.model);
      e.preventDefault();
      var currentView = new admin.FormPageView({
                template: _.template($('#locationEditFormTpl').html()),
                mode: 'edit',
                model:self.model.attributes
            }); 
    var locationmodal= new BackboneBootstrapModals.ConfirmationModal({
          label: "Edit Location",
          text: currentView.el,
          onConfirm: function(e) {
            console.log("action confirmed");
            return self.rendereditlocation();
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    locationmodal.render();
    },
    rendereditlocation:function(){
      var self = this;
     var locationmodel = new admin.LocationModel();
     var locationdata = { location_id:self.model.attributes.location_id,
      location_name:$.trim($('#location_name').val()) 
       };
    locationmodel.set(locationdata );
     if(locationmodel.isValid()){
      locationmodel.save(null,{ 
        success: function () {
          $('#locationtable').empty();
          $('#locationtable').unbind();
          $( '#paginator' ).empty();
          $( '#paginator' ).unbind(); 
          appRouter.currentView.locationSearch();
            console.log('success!');
            
        },
        error: function (model, error) {
            console.log(error);
            return false;
        }});
    }else{
      _.each(locationmodel.validationError, function (error) {
           $('#' + error.name).text(error.message);
        });
    return false;
    }
     return true;
    },
      
    render: function () {
      this.$el.html(this.template());
      this.delegateEvents();
      return this;
    }

});
    var columns = [{
    name: "", 
    cell: "select-row",
    headerCell: "select-all",

  }, 
    {
    name: "location_name",
    label: "Location Name",
     editable: false,
    cell: "string" 
  }, 
  {
        name: "delete",
        label: "Action",
        cell: DeleteCell
    }];

var grid = new Backgrid.Grid({
  columns: columns,
  collection: self.collection
});
var paginator = new Backgrid.Extension.Paginator({
        collection: self.collection
      });
$("#locationtable").append(grid.render().el);
$("#paginator").append(paginator.render().$el);
appRouter.locationgrid = grid;
            return this;  
  },

    removeoraddAll: function()
  {
      this.collection.reset();
      this.$el.empty();

  },

});
