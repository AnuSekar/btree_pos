var admin = admin || {};

admin.LocationPageView = Backbone.View.extend({
    template: $( '#locationPageTpl' ).html(),
  initialize: function() {
    appRouter.paginationtype = "";
    this.render();    
  },

  events:{
       'click #create_location' : 'locationCreate',
       'click #location_deleteall'    :'restoreordeleteAll',
       'click #location_deleteselect'    :'addordeleteSelect',
       'input #location_search'  : 'locationSearch',
       'click #location_restoreall'    :'restoreordeleteAll',
       'click #location_addselect'    :'addordeleteSelect',

  },

  render: function() {          
      var _self = this;
      this.$el.html(this.template);
      $("#switch1").switchbutton({
      checkedLabel: 'YES',
      uncheckedLabel: 'NO',
      classes: 'ui-switchbutton-ios5'
    }).change(function(e){
     var toggledel = $(this).prop("checked") ? "checked" : "unchecked";
      _self.toggle(toggledel);
    });

    var toggleval = $("#switch1").prop("checked") ? "checked" : "unchecked";
    this.toggle(toggleval);
    return this;
    
  },
  toggle:function(toggledel){
  console.log(toggledel);
  if(toggledel == 'checked'){
    this.locationListView = new admin.LocationTableView({el: $( '#locationtable'),
      mode:'normal'});
     $('#create_location').show();
     $('#location_deleteselect').show();
     $('#location_deleteall').show();
     $('#location_search').show();
     $('#location_search').val("");
     $('#location_restoreall').hide();
     $('#location_addselect').hide();
     this.adddeletelabel = "Delete Selected Location";
     this.adddeletetext = "Do You want to Delete Selected location???";
     this.adddeleteurl = "/btree/admin/delete_selectlocation";
     this.searchurl = "/btree/admin/search_location";
     this.restoredeletelabel = "Delete All Location";
     this.restoredeletetext = "Do You want to Delete all this location???";
     this.restoredeleteurl = "/btree/admin/delete_alllocation";
  }else if(toggledel == 'unchecked'){
    this.locationListView = new admin.LocationTableView({el: $( '#locationtable' ),
      mode:'trash'});
     $('#create_location').hide();
     $('#location_deleteselect').hide();
     $('#location_deleteall').hide();
     $('#location_search').show();
     $('#location_search').val("");
     $('#location_restoreall').show();
     $('#location_addselect').show();
     this.adddeletelabel = "Restore Selected Location";
     this.adddeletetext = "Do You want to Restore Selected location???";
     this.adddeleteurl = "/btree/admin/add_selectlocation";
     this.searchurl = "/btree/admin/search_trashlocation";
     this.restoredeletelabel = "Restore All Location";
     this.restoredeletetext = "Do You want to Restore all this location???";
     this.restoredeleteurl = "/btree/admin/restore_alllocation";

  }
  },

  locationCreate:function(e){
        e.preventDefault();
        var self = this;
    var currentView = new admin.FormPageView({
                template: _.template($('#locationCreateFormTpl').html()),
                mode: 'create'
            }); 
    var locationmodal= new BackboneBootstrapModals.ConfirmationModal({
          label: "Create Location",
          text: currentView.el,
          onConfirm: function(e) {
            console.log("action confirmed");
            return self.rendersavelocation();
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    locationmodal.render();
  },

  restoreordeleteAll:function(e)
  {
          e.preventDefault();
        var self = this;
        var locationselect= new BackboneBootstrapModals.ConfirmationModal({
          label: self.restoredeletelabel,
          text: self.restoredeletetext,
          onConfirm: function(e) {
             $.ajax({
                    type: 'POST',
                    url: self.restoredeleteurl,
                     contentType:'application/json',
                    cache:false,
                    success: function(data) {
                      self.locationListView.removeoraddAll();
                      $( "div.success").html("All location are Deleted Successfully.");
                      $( "div.success" ).fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );   
                    },
                    error: function(data) {
                      
                    }
             });
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    locationselect.render();   
  },

   locationSearch : function()
   {
    console.log("locationSearch");
    $('#findStatus').html("");
     //$('#findStatus').html(alert("No Records Found"));
    var search_location = $('#location_search').val();
    var data = {};
    self=this;
    if(search_location.length >=1 )
    {
        console.log("more 2");
        data.location_name = search_location;

        console.log(" search data "+JSON.stringify(data));

        this.locationListView.collection.fetch({url:'/btree/admin/search_location',reset: true, data:data, processData: true,
            success: function (coll) {
                $( '#locationtable' ).empty();
                $( '#locationtable' ).unbind();
                $( '#paginator' ).empty();
                $( '#paginator' ).unbind();                         
                self.locationListView.render();
            },
            error: function(err) {
                $( "div.failure").html(" Error while searching the location . Contact Administrator.");
                $( "div.failure" ).fadeIn( 300 ).delay( 3500 ).fadeOut( 800 );                      
            }

        });
        $("#location_load_more").hide();     
    }
    if(search_location.length == 0)
    {            
        console.log("empty");
        skip = 0;
         this.locationListView.collection.fetch({reset: true, data, processData: true,
            success: function (coll) {
                $( '#locationtable' ).empty();
                $( '#locationtable' ).unbind();
                $( '#paginator' ).empty();
                $( '#paginator' ).unbind();                         
                self.locationListView.render();   
            },       

        }); 
        $("#location_load_more").show();         
    }
},
rendersavelocation:function(){
  var self = this;
 var locationmodel = new admin.LocationModel();
 var locationdata = { location_name:$.trim($('#location_name').val())
 };
locationmodel.set(locationdata);
if(locationmodel.isValid()){
  locationmodel.save(null,{ 
    success: function () {
      $( "div.success").html("Location Successfully Created.");
        $( "div.success" ).fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );   
        self.render();
        $('div.modal').modal('hide');
         return true;
    },
    error: function (model, error) {
        $( "div.failure").html(error.responseJSON.errors.location_name[0]);
        $( "div.failure" ).fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );   
        return false;
    }});
}else{
  _.each(locationmodel.validationError, function (error) {
       $('#' + error.name).text(error.message);
    });
return false;
}
 return false;
},


addordeleteSelect:function(e){
  e.preventDefault();
  var self = this;
  var selectedModels = appRouter.locationgrid.getSelectedModels();
  console.log(selectedModels);
  var selectlocation = [];
for(i=0;i<selectedModels.length;i++){
  selectlocation.push(selectedModels[i].attributes.location_id);
}
    var locationselect= new BackboneBootstrapModals.ConfirmationModal({
          label: self.adddeletelabel,
          text: self.adddeletetext,
          onConfirm: function(e) {
            $('#locationtable').empty();
            $('#locationtable').unbind();
            $( '#paginator' ).empty();
            $( '#paginator' ).unbind(); 
            $.ajax({
              type:'POST',
              url:self.adddeleteurl,
              data:JSON.stringify(selectlocation),
              contentType:'application/json',
              cache:false,
              success:function(){
                self.render();
              },
              error:function(){
                return false;
              }
            });
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    locationselect.render();
},
});
