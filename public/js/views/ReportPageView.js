var admin = admin || {};

admin.ReportPageView = Backbone.View.extend({
  
  template : $('#ReportTpl').html(),
   
	initialize: function() {
         appRouter.listreport = null;
        this.render(); 
      
   
	}, 

	events:{
      
       'click #admin_report_company' : 'ReportCompany',
       'click #admin_report_product' : 'ReportProduct',  
       'click #admin_report_customer' : 'ReportCustomer',  
       'click #admin_report_supplier' : 'ReportSupplier',  
       'click #admin_report_item' : 'ReportItem',  
       'click #admin_report_expense_category' : 'ReportExpenseCategory',  
       'click #admin_report_sales' : 'ReportSales',
       'click #admin_report_suspendedsales' : 'ReportSuspendedSales',
       'click #admin_report_receiving' : 'ReportReceiving',    
       'click #admin_report_discount' : 'ReportDiscount',    
       'click #admin_report_payment' : 'ReportPayment', 
       'click #admin_report_tax' : 'ReportTax', 
       'click #admin_report_employee' : 'ReportEmployee',
       'click #admin_report_commission' : 'ReportCommission',
       'click #admin_report_itemkits' : 'ReportItemKits',
       'click #admin_report_graph_company' : 'ReportGraphCompany', 
       'click #admin_report_graph_product' : 'ReportGraphProduct', 
       'click #admin_report_graph_customer' : 'ReportGraphCustomer', 
       'click #admin_report_graph_supplier' : 'ReportGraphSupplier', 
       'click #admin_report_graph_item' : 'ReportGraphItem', 
       'click #admin_report_graph_expense' : 'ReportGraphExpense', 
       'click #admin_report_graph_sales' : 'ReportGraphSales', 
       'click #admin_report_graph_suspendedsales' : 'ReportGraphSuspendedSales', 
       'click #admin_report_graph_receiving' : 'ReportGraphReceiving', 
       'click #admin_report_graph_discount' : 'ReportGraphDiscount', 
       'click #admin_report_graph_employee' : 'ReportGraphEmployee', 
       'click #admin_report_graph_commission' : 'ReportGraphCommission', 
       'click #admin_report_graph_itemkits' : 'ReportGraphItemKits', 
       'click #admin_report_detail_sales' : 'ReportDetailSale', 
       'click #admin_report_detail_discount' : 'ReportDetailDiscount', 
       'click #admin_report_detail_customer' : 'ReportDetailCustomer', 
       'click #admin_report_detail_supplier' : 'ReportDetailSupplier', 
       'click #admin_report_detail_employee' : 'ReportDetailEmployee', 
       'click #admin_report_low_inventory' : 'ReportLowInventory', 
       'click #admin_report_inventory_summary' : 'ReportInventorySummary', 
       'click #admin_report_dead_item' : 'ReportDeadItem', 

	},
 
	render: function() {					
	    this.$el.html(this.template);
		  return this;
	},

  ReportCompany:function(){
    console.log("inside");
    appRouter.listreport = 'company';
    appRouter.reporttype = 'normal';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportProduct:function(){
    console.log("inside");
    appRouter.listreport = 'product';
    appRouter.reporttype = 'normal';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportCustomer:function(){
    console.log("inside");
    appRouter.listreport = 'customer';
    appRouter.reporttype = 'normal';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportSupplier:function(){
    console.log("inside");
    appRouter.listreport = 'supplier';
    appRouter.reporttype = 'normal';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportItem:function(){
    console.log("inside");
    appRouter.listreport = 'item';
    appRouter.reporttype = 'normal';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportExpenseCategory:function(){
    console.log("inside");
    appRouter.listreport = 'expensecategory';
    appRouter.reporttype = 'normal';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportSales:function(){
    console.log("inside");
    appRouter.listreport = 'sales';
    appRouter.reporttype = 'normal';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportSuspendedSales:function(){
    console.log("inside");
    appRouter.listreport = 'suspendedsales';
    appRouter.reporttype = 'normal';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportReceiving:function(){
    console.log("inside");
    appRouter.listreport = 'receiving';
    appRouter.reporttype = 'normal';
    appRouter.navigate("reportList", {trigger: true});
  },
  
  ReportDiscount:function(){
    console.log("inside");
    appRouter.listreport = 'discount';
    appRouter.reporttype = 'normal';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportPayment:function(){
    console.log("inside");
    appRouter.listreport = 'payment';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportItemkits:function(){
    console.log("inside");
    appRouter.listreport = 'itemkits';
    appRouter.reporttype = 'normal';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportInventory:function(){
    console.log("inside");
    appRouter.listreport = 'inventory';
    appRouter.reporttype = 'normal';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportCommission:function(){
    console.log("inside");
    appRouter.listreport = 'commission';
    appRouter.reporttype = 'normal';
    appRouter.navigate("commissionreportList", {trigger: true});
  },

  ReportGraphInventory:function(){
    appRouter.listreport = 'inventory';
    appRouter.reporttype = 'graph';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportGraphCommission:function(){
    appRouter.listreport = 'commission';
    appRouter.reporttype = 'graph';
    appRouter.navigate("commissionreportList", {trigger: true});
  },

  ReportGraphItemKits:function(){
    appRouter.listreport = 'itemkits';
    appRouter.reporttype = 'graph';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportGraphCompany:function(){
    appRouter.listreport = 'company';
    appRouter.reporttype = 'graph';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportGraphProduct:function(){
    appRouter.listreport = 'product';
    appRouter.reporttype = 'graph';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportGraphCustomer:function(){
    appRouter.listreport = 'customer';
    appRouter.reporttype = 'graph';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportGraphSupplier:function(){
    appRouter.listreport = 'supplier';
    appRouter.reporttype = 'graph';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportGraphItem:function(){
    appRouter.listreport = 'item';
    appRouter.reporttype = 'graph';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportGraphExpense:function(){
    appRouter.listreport = 'expensecategory';
    appRouter.reporttype = 'graph';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportGraphSales:function(){
    appRouter.listreport = 'sales';
    appRouter.reporttype = 'graph';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportGraphSuspendedSales:function(){
    appRouter.listreport = 'suspendedsales';
    appRouter.reporttype = 'graph';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportGraphReceiving:function(){
    appRouter.listreport = 'receiving';
    appRouter.reporttype = 'graph';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportGraphDiscount:function(){
    appRouter.listreport = 'discount';
    appRouter.reporttype = 'graph';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportGraphEmployee:function(){
    appRouter.listreport = 'employee';
    appRouter.reporttype = 'graph';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportTax:function(){
    console.log("inside");
    appRouter.listreport = 'tax';
    appRouter.reporttype = 'normal';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportEmployee:function(){
    console.log("inside");
    appRouter.listreport = 'employee';
    appRouter.reporttype = 'normal';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportDetailSale:function(){
    console.log("inside");
    appRouter.listreport = 'sales';
    appRouter.reporttype = 'details';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportDetailDiscount:function(){
    console.log("inside");
    appRouter.listreport = 'discount';
    appRouter.reporttype = 'details';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportDetailCustomer:function(){
    console.log("inside");
    appRouter.listreport = 'customer';
    appRouter.reporttype = 'details';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportDetailSupplier:function(){
    console.log("inside");
    appRouter.listreport = 'supplier';
    appRouter.reporttype = 'details';
    appRouter.navigate("reportList", {trigger: true});
  },

  ReportLowInventory:function(){
    console.log("inside");
    appRouter.listreport = 'lowinventory';
    appRouter.reporttype = 'details';
    appRouter.navigate("lowinventory", {trigger: true});
  },
  ReportInventorySummary:function(){
    console.log("inside");
    appRouter.listreport = 'inventorysummary';
    appRouter.reporttype = 'details';
    appRouter.navigate("reportList", {trigger: true});
  },
  ReportDeadItem:function(){
    console.log("inside");
    appRouter.listreport = 'deaditem';
    appRouter.reporttype = 'details';
    appRouter.navigate("reportList", {trigger: true});
  },
  
});
