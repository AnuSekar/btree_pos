var admin = admin || {};

admin.PaymentReportPageView = Backbone.View.extend({
  
  template : $('#paymentReportTpl').html(),
   
	initialize: function(options) {
        this.template = options.template;

        this.render(); 
      
   
	},

	render: function() {
  var self = this;			
   this.$el.html(this.template);
    var columns = [
  {
    name: "payment_type",
    label: "Payment Type",
     editable: false,
    cell: "string" 
  },
  {
    name: "sales_amount",
    label: "Sub Total",
     editable: false,
    cell: "string" 
  },
  {
    name: "count",
    label: "Number of Item Sold",
     editable: false,
    cell: "string" 
  }];
console.log(appRouter.collection);
var grid = new Backgrid.Grid({
  columns: columns,
  collection: appRouter.collection
});
var paginator = new Backgrid.Extension.Paginator({
        collection: appRouter.collection
      });
$("#paymentreport").append(grid.render().el);
$("#paymentpaginator").append(paginator.render().$el);
return this;
	},
	
 
});
