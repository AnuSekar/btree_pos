var admin = admin || {};

admin.ReceivingReportPageView = Backbone.View.extend({
  
  template : $('#receivingReportTpl').html(),
   
  initialize: function(options) {
        this.template = options.template;
        this.template = options.template;
        if(appRouter.reporttype == 'normal') {
          this.render(); 
        }else{
          this.graphrender(); 
        }
      
   
  },

 render: function() {
  var self = this;      
   this.$el.html(this.template);
    var columns = [{
    name: "receiving_id",
    label: "Receiving ID",
     editable: false,
    cell: "string" 
  },
  {
    name: "payment_type",
    label: "Payment Type",
     editable: false,
    cell: "string"
  },
  {
    name: "item_cost_price",
    label: "Total",
     editable: false,
    cell: "string"
  },
  {
    name: "receiving_quantity",
    label: "Quantity Received",
     editable: false,
    cell: "string"
  },
  {
    name: "receiving_time",
    label: "Received On",
     editable: false,
    cell: "string"
  }]; 
 
console.log(appRouter.collection);
var grid = new Backgrid.Grid({
  columns: columns,
  collection: appRouter.collection
});
var paginator = new Backgrid.Extension.Paginator({
        collection: appRouter.collection
      });
$("#receivingreport").append(grid.render().el);
$("#receivingpaginator").append(paginator.render().$el);
return this;
  },

  graphrender:function(){
     var arr = [];
    appRouter.collection.each(function(item){
      arr.push({label : item.get('item_name'), value : item.get('receiving_quantity') });
    });
    console.log(arr);
    this.$el.html(this.template);
    
    var chartData = {
          "chart": {
            "caption": "Receiving Report List",
            "subCaption": "Data specified",
            "xAxisName": "Item",
            "yAxisName": "Quantity Received",
            "numberPrefix": "",
            "theme": "carbon",
            "bgcolor": "#DCEAFC",
            "canvasbgcolor": "#DCEAFC",
            "canvasbgalpha": "10",
            "rotateValues": "1",
            "exportEnabled": "1",
            "transposeAnimation": "1",
            "pieRadius": "70",
          },

          "data": arr
        };
        revenueChartColumn = new FusionCharts({
          type: 'column2d',
          width: '420',
          height: '350',
          dataFormat: 'json',
          id: 'receivingreport1',
          dataSource: chartData
        });
       revenueChartColumn.render("receivingreport");

  }
  
 
});
  
  