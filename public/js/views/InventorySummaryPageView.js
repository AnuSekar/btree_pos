var admin = admin || {};

admin.InventorySummaryPageView = Backbone.View.extend({
  
  template : $('#inventorysummaryReportTpl').html(),
   
  initialize: function(options) {
        this.template = options.template;
        this.render();
      
  },

  
 render: function() {
  var self = this;      
   this.$el.html(this.template);
    var columns = [{
    name: "item_name",
    label: "Item Name",
     editable: false,
    cell: "string" 
  },
  {
    name: "item_number",
    label: "Item Number",
     editable: false,
    cell: "string" 
  },
  {
    name: "reorder_level",
    label: "Order Level",
     editable: false,
    cell: "string" 
  },
  {
    name: "quantity",
    label: "Remaining quantity",
     editable: false,
    cell: "string" 
  },
  {
    name: "cost_price",
    label: "Cost Price",
     editable: false,
    cell: "string" 
  },
  {
    name: "unit_price",
    label: "Unit Price ",
     editable: false,
    cell: "string" 
  },
  {
    name: "sales_total_value",
    label: "Sub Total ",
     editable: false,
    cell: "string" 
  }];
console.log(appRouter.collection);
var grid = new Backgrid.Grid({
  columns: columns,
  collection: appRouter.collection,
  rendertype : 'report'
});
var paginator = new Backgrid.Extension.Paginator({
        collection: appRouter.collection
      });
$("#inventorysummaryreport").append(grid.render().el);
$("#inventorysummarypaginator").append(paginator.render().$el);
return this;
  },
  

 
});
  
  

 