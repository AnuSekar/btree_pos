var admin = admin || {};

admin.ExpenseCategoryReportPageView = Backbone.View.extend({
  
  template : $('#expenseReportTpl').html(),
   
  initialize: function(options) {
        this.template = options.template;
        this.template = options.template;
        console.log(appRouter.reporttype);
        if(appRouter.reporttype == 'normal') {
          this.render(); 
        }else{
          this.graphrender(); 
        }
        
      
   
  },

  
 render: function() {
  var self = this;      
   this.$el.html(this.template);
    var columns = [{
    name: "expense_category_name",
    label: "Expense Name",
     editable: false,
    cell: "string" 
  },
  {
  	name: "amount",
    label: "Amount",
     editable: false,
    cell: "string"
  },
  {
  	name: "count",
    label: "Count",
     editable: false,
    cell: "string"
  }];
console.log(appRouter.collection);
var grid = new Backgrid.Grid({
  columns: columns,
  collection: appRouter.collection
});
var paginator = new Backgrid.Extension.Paginator({
        collection: appRouter.collection
      });
$("#expensereport").append(grid.render().el);
$("#expensepaginator").append(paginator.render().$el);
return this;
  },

  graphrender:function(){
      this.collection = new admin.ExpenseGraphReportCollection();
     var arr = [];
    appRouter.collection.each(function(item){
      arr.push({label : item.get('expense_category_name'), value : item.get('amount') });
    });
    console.log(arr);
    this.$el.html(this.template);
    
    var chartData = {
          "chart": {
            "caption": "Expense Report List",
            "subCaption": "Date specified",
            "xAxisName": "Expense Category",
            "yAxisName": "Expense Amount",
            "numberPrefix": "rs",
            "theme": "carbon",
            "bgcolor": "#DCEAFC",
            "canvasbgcolor": "#DCEAFC",
            "canvasbgalpha": "10",
            "rotateValues": "1",
            "exportEnabled": "1",
            "transposeAnimation": "1",
            "pieRadius": "70",
          },

          "data": arr
        };
        revenueChartColumn = new FusionCharts({
          type: 'column2d',
          width: '420',
          height: '350',
          dataFormat: 'json',
          id: 'expensereport1',
          dataSource: chartData
        });
       revenueChartColumn.render("expensereport");

  }
  
 
});
  
  

 