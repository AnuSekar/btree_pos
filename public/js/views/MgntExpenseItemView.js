var admin = admin || {};

admin.MgntExpenseItemView = Backbone.View.extend({

    tagName: 'option',

    initialize: function(){
      console.log('init');
      _.bindAll(this, 'render');
    },

    render: function(){
       console.log("veiws :: " + this.model.get('expense_category_name'));
      $(this.el).attr('value',
      this.model.get('expense_category_id')).html(this.model.get('expense_category_name'));
      return this;
    }

});
