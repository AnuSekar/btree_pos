var admin = admin || {};

admin.ItemkitsReportPageView = Backbone.View.extend({
  
  template : $('#itemkitsReportTpl').html(),
   
	initialize: function(options) {
        this.template = options.template;
        console.log(appRouter.reporttype);
        if(appRouter.reporttype == 'normal') {
          this.render(); 
        }else{
          this.graphrender(); 
        }     
   
	},

	render: function() {
  var self = this;			
   this.$el.html(this.template);
    var columns = [{
    name: "tax_amount",
    label: "Tax",
     editable: false,
    cell: "string" 
  },
  {
    name: "quantity_purchased",
    label: "Number of Item Sold",
     editable: false,
    cell: "string" 
  }];
console.log(appRouter.collection);
var grid = new Backgrid.Grid({
  columns: columns,
  collection: appRouter.collection,
  rendertype : 'report'
});
var paginator = new Backgrid.Extension.Paginator({
        collection: appRouter.collection
      });
$("#itemkitsreport").append(grid.render().el);
$("#itemkitspaginator").append(paginator.render().$el);
return this;
	},
	graphrender:function(){
  this.collection = new admin.ItemkitsGraphReportCollection();
     var arr = [];
     appRouter.collection.each(function(item){
      arr.push({label : item.get('itemkits_name'), value : item.get('sales_amount') });
    });
    this.$el.html(this.template);
    
    var chartData = {
          "chart": {
            "caption": "Itemkits Report List",
            "subCaption": "Date specified",
            "xAxisName": "Itemkits",
            "yAxisName": "Sales Amount",
            "numberPrefix": "rs",
            "theme": "carbon",
            "bgcolor": "#DCEAFC",
            "canvasbgcolor": "#DCEAFC",
            "canvasbgalpha": "10",
            "rotateValues": "1",
            "exportEnabled": "1",
            "transposeAnimation": "1",
            "pieRadius": "70",
          },

          "data": arr
        };
        revenueChartColumn = new FusionCharts({
          type: 'column2d',
          width: '420',
          height: '350',
          dataFormat: 'json',
          id: 'itemkitsreport1',
          dataSource: chartData
        });
       revenueChartColumn.render("itemkitsreport");

  }
});
