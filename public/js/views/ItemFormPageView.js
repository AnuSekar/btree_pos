var admin = admin || {};

admin.ItemFormPageView = Backbone.View.extend({

initialize: function(options) {
      this.mode = options.mode;
      this.template = options.template;
      this.model = options.model;
      var self = this;
      console.log ('mode: ' + this.mode + ' model: ' + this.model);
               $.when(
              $.ajax({
              url: "/btree/admin/get_supplier_list_all",
                  type: "GET",
                  contentType:'application/json',
                  cache:false,
                  success: function(data) {
                    //alert(JSON.stringify(data));
                    console.log(data);

                     self.catdata = data;
                  },
                  error: function(data) {
                      // try{
                      //     var errData = JSON.parse(data.responseText);
                      //     if ( errData.errCode == 550) {
                      //         window.location.href = '/sessionExpired';
                      //     } else {
                      //         if (errData.errMsg.length > 0) {
                      //           var failureMsg = errData.errMsg; 
                      //         } else {
                      //             var failureMsg = "Error while Edit Faq form. Please Contact Administrator."; 
                      //           }
                      //           $( "div.failure").html(failureMsg);
                      //           $( "div.failure" ).fadeIn( 300 ).delay( 3500 ).fadeOut( 800 ); 
                      //       }
                      // }catch(e) {
                      //     window.location.href = '/sessionExpired';
                      //  }          
                  }
                }),
              $.ajax({
              url: "/btree/admin/get_product_list",
                  type: "GET",
                  contentType:'application/json',
                  cache:false,
                  success: function(data) {
                    //alert(JSON.stringify(data));
                    console.log(data);

                     self.prodata = data;
                  },
                  error: function(data) {
                      // try{
                      //     var errData = JSON.parse(data.responseText);
                      //     if ( errData.errCode == 550) {
                      //         window.location.href = '/sessionExpired';
                      //     } else {
                      //         if (errData.errMsg.length > 0) {
                      //           var failureMsg = errData.errMsg; 
                      //         } else {
                      //             var failureMsg = "Error while Edit Faq form. Please Contact Administrator."; 
                      //           }
                      //           $( "div.failure").html(failureMsg);
                      //           $( "div.failure" ).fadeIn( 300 ).delay( 3500 ).fadeOut( 800 ); 
                      //       }
                      // }catch(e) {
                      //     window.location.href = '/sessionExpired';
                      //  }          
                  }
                }),
             $.ajax({
              url: "/btree/admin/get_company_list",
                  type: "GET",
                  contentType:'application/json',
                  cache:false,
                  success: function(data) {
                    //alert(JSON.stringify(data));
                    console.log(data);

                     self.comdata = data;
                  },
                  error: function(data) {
                      // try{
                      //     var errData = JSON.parse(data.responseText);
                      //     if ( errData.errCode == 550) {
                      //         window.location.href = '/sessionExpired';
                      //     } else {
                      //         if (errData.errMsg.length > 0) {
                      //           var failureMsg = errData.errMsg; 
                      //         } else {
                      //             var failureMsg = "Error while Edit Faq form. Please Contact Administrator."; 
                      //           }
                      //           $( "div.failure").html(failureMsg);
                      //           $( "div.failure" ).fadeIn( 300 ).delay( 3500 ).fadeOut( 800 ); 
                      //       }
                      // }catch(e) {
                      //     window.location.href = '/sessionExpired';
                      //  }          
                  }
                }),
                ).done(function(){
                self.render();
                
                });
      
     },

    events: {
     
  },

    render: function() {
    if (new String(this.mode).valueOf() == new String('create').valueOf()) {
      var self = this;
      this.$el.html(this.template);
       this.catCollectionView = new admin.MgntSupplierCollectionView({
        el: $( '#item_supplier' ),
        catCollection: self.catdata
         });
         this.proCollectionView = new admin.MgntProductCollectionView({
        el: $( '#item_product' ),
        proCollection: self.prodata
         });
          this.comCollectionView = new admin.MgntCompanyCollectionView({
        el: $( '#item_company' ),
        comCollection: self.comdata
         });
      } else { //edit
        var self = this;
        console.log(this.model);
        var tmp = this.model;
        console.log(tmp);
        var taxname = tmp.taxname;
        console.log(taxname);
        var splittax = taxname.split(',');
        tmp.tax_name1 = splittax[0];
        tmp.tax_name2 = splittax[1];
        tmp.tax_name3 = splittax[2];
        var taxpercent = tmp.percent;
        var splitpercent = taxpercent.split(',');  
        tmp.tax_percent1 = splitpercent[0];
        tmp.tax_percent2 = splitpercent[1];
        tmp.tax_percent3 = splitpercent[2]; 
        this.$el.html(this.template(tmp));
        this.catCollectionView = new admin.MgntSupplierCollectionView({
        el: $( '#item_supplier' ),
        catCollection: self.catdata
         });
         this.proCollectionView = new admin.MgntProductCollectionView({
        el: $( '#item_product' ),
        proCollection: self.prodata
         });
          this.comCollectionView = new admin.MgntCompanyCollectionView({
        el: $( '#item_company' ),
        comCollection: self.comdata
         });
          $('#item_supplier').val(this.model.supplier_id);
          $('#item_product').val(this.model.product_id);
          $('#item_company').val(this.model.company_id);

    }           
        return this;
  }
});
