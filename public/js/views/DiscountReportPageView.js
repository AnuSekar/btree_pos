var admin = admin || {};

admin.DiscountReportPageView = Backbone.View.extend({
  
  template : $('#discountReportTpl').html(),
   
	initialize: function(options) {
        this.template = options.template;
        if(appRouter.reporttype == 'normal') {
          this.render(); 
        }else if(appRouter.reporttype == 'details') {
          this.detailsrender(); 
        }else{
          this.graphrender(); 
        }   
	},

	
	render: function() {
  var self = this;			
   this.$el.html(this.template);
    var columns = [{
    name: "discount_percent",
    label: "Discount Percent",
     editable: false,
    cell: "string" 
  },
  {
    name: "count",
    label: "Count",
     editable: false,
    cell: "string" 
  }];
console.log(appRouter.collection);
var grid = new Backgrid.Grid({
  columns: columns,
  collection: appRouter.collection
});
var paginator = new Backgrid.Extension.Paginator({
        collection: appRouter.collection
      });
$("#discountreport").append(grid.render().el);
$("#discountpaginator").append(paginator.render().$el);
return this;
	},
detailsrender:function() {
   var self = this;      
   this.$el.html(this.template);
   
    var columns = [{
    name: "sale_time",
    label: "Date",
     editable: false,
    cell: "string" 
  },
  {
    name: "item_purchase",
    label: "Quantity",
     editable: false,
    cell: "string"
  },
  {
    name: "soldby",
    label: "sold by",
     editable: false,
    cell: "string"
  },
  {
    name: "soldto",
    label: "sold to ",
     editable: false,
    cell: "string"
  },
  {
    name: "sales_amount",
    label: "Sub Total",
     editable: false,
    cell: "string"
  },
  {
    name: "item_total_cost",
    label: "Total",
     editable: false,
    cell: "string"
  },
  {
    name: "tax_amount",
    label: "Tax",
     editable: false,
    cell: "string"
  },
  {
    name: "payment_type",
    label: "payment type",
     editable: false,
    cell: "string"
  }];
console.log(appRouter.collection);
var grid = new Backgrid.Grid({
  columns: columns,
  collection: appRouter.collection,
  rendertype : 'report'
});
var paginator = new Backgrid.Extension.Paginator({
        collection: appRouter.collection
      });

$("#discountreport").append(grid.render().el);
$("#discountpaginator").append(paginator.render().$el);
return this;
  },
  graphrender:function(){
     var arr = [];
    appRouter.collection.each(function(item){
      arr.push({label : item.get('item_name'), value : item.get('discount_percent') });
    });
    console.log(arr);
    this.$el.html(this.template);
    
    var chartData = {
          "chart": {
            "caption": "Company Report List",
            "subCaption": "Date specified",
            "xAxisName": "Item",
            "yAxisName": "Discount",
            "numberPrefix": "%",
            "theme": "carbon",
            "bgcolor": "#DCEAFC",
            "canvasbgcolor": "#DCEAFC",
            "canvasbgalpha": "10",
            "rotateValues": "1",
            "exportEnabled": "1",
            "transposeAnimation": "1",
            "pieRadius": "70",
          },

          "data": arr
        };
        revenueChartColumn = new FusionCharts({
          type: 'column2d',
          width: '420',
          height: '350',
          dataFormat: 'json',
          id: 'discountreport1',
          dataSource: chartData
        });
       revenueChartColumn.render("discountreport");

  }
	
 
});
