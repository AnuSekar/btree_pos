var admin = admin || {};

admin.FormPageView = Backbone.View.extend({

    initialize: function(options) {
       this.mode = options.mode;
       this.template = options.template;
       this.model = options.model;
       var self = this;
       console.log ('mode: ' + this.mode + ' model: ' + this.model);

          this.render();
      
     },

    events: {
     
  },

    render: function() {
    if (new String(this.mode).valueOf() == new String('create').valueOf()) {
      this.$el.html(this.template);
      } else { //edit
        console.log(this.model);
        this.$el.html(this.template(this.model));
    }           
        return this;
  }
});
