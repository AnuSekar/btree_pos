var admin = admin || {};

admin.ExpenseCategoryTableView = Backbone.View.extend({

  initialize: function(options) {
      console.log('Initialize expense_categoryTableView');
      var self = this;
      this.pageNo = 1;
      limit =2;
      this.recIndex = 1;
      this.options = options;
      console.log(options.mode);
      $('#expense_categorytable').empty();
      $('#expense_categorytable').unbind();
      $( '#paginator' ).empty();
      $( '#paginator' ).unbind(); 
      if(options.mode == 'normal'){
        this.collection = new admin.ExpenseCategoryCollection();    
      }else if(options.mode == 'trash'){
        this.collection = new admin.ExpenseCategoryTrashCollection();    
      }
      this.collection.fetch({ 
        success:function(data){
          console.log("success");

          console.log(JSON.stringify(data));
          self.render();


        },
        error:function(data){
          /*try{
            var errData = JSON.parse(data.responseText);
            if ( errData.errCode == 550) {
              window.location.href = '/sessionExpired';
          } else {
            if (errData.errMsg.length > 0) {
                var failureMsg = errData.errMsg;  
              } else {
                  var failureMsg = "Error occurred while fetching the expense_category Table View. Please Contact Administrator.";  
              }
              $( "div.failure").html(failureMsg);
?????                    $( "div.failure" ).fadeIn( 300 ).delay( 3500 ).fadeOut( 800 );        
           }
          }catch(e){
          window.location.href = '/sessionExpired';
        }*/
        }
      }); 
  },

  render: function() {          
    var self = this;
    if(this.options.mode == 'normal'){
        this.edittemplate = '<span style="display:inline-block; width: 20px;"><a id="editexpense_category" data-toggle="tooltip" data-placement="top" title="Edit">'+
      '<i class="glyphicon glyphicon-pencil"></i></a></span>'+
      '<span style="display:inline-block; width: 20px;"><a id="deleteexpense_category" data-toggle="tooltip" data-placement="top" title="Delete">'+
      '<i class="glyphicon glyphicon-minus-sign"></i></a></span>';    
      this.edittext = 'Delete';
      }else if(this.options.mode == 'trash'){
        this.edittemplate = '<span style="display:inline-block; width: 20px;"><a id="deleteexpense_category" data-toggle="tooltip" data-placement="top" title="Delete">'+
      '<i class="glyphicon glyphicon-minus-sign"></i></a></span>';  
      this.edittext = 'Restore';
      }
    var DeleteCell = Backgrid.Cell.extend({
    template: _.template(self.edittemplate),
    events: {
      "click #editexpense_category": "editRow",
      "click #deleteexpense_category": "deleteRow",
    },
    deleteRow: function (e) {
      console.log("Hello");
      e.preventDefault();
      modelvalue = this.model;
      var expense_categorymodal= new BackboneBootstrapModals.ConfirmationModal({
          label: "Delete ExpenseCategory",
          text: "Are You want to Delete this expense_category?",
          onConfirm: function(e) {
            e.preventDefault();
            var expense_categorymodal = modelvalue;
            expense_categorymodal.destroy({
                success: function (model, respose, options) {
                    console.log("The model has deleted the server");
                    $('#expense_categorytable').empty();
                    $('#expense_categorytable').unbind();
                    $( '#paginator' ).empty();
                    $( '#paginator' ).unbind(); 
                    appRouter.currentView.expense_categorySearch();
                    return true;
                },
                error: function (model, xhr, options) {
                    console.log("Something went wrong while deleting the model");
                    return false;
                }
            });
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    expense_categorymodal.render();
    },
    editRow:function(e){
      var self= this;
       console.log("Hello",this.model);
      e.preventDefault();
      var currentView = new admin.FormPageView({
                template: _.template($('#expense_categoryEditFormTpl').html()),
                mode: 'edit',
                model:self.model.attributes
            }); 
    var expense_categorymodal= new BackboneBootstrapModals.ConfirmationModal({
          label: "Edit ExpenseCategory",
          text: currentView.el,
          onConfirm: function(e) {
            console.log("action confirmed");
            return self.rendereditexpense_category();
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    expense_categorymodal.render();
    },
    rendereditexpense_category:function(){
      var self = this;
     var expense_categorymodel = new admin.ExpenseCategoryModel();
     var expense_categorydata = { expense_category_id:self.model.attributes.expense_category_id,
      expense_category_name:$.trim($('#expense_category_name').val()),
      expense_category_description:$.trim($('#expense_category_description').val()) 
     };
    expense_categorymodel.set(expense_categorydata );
     if(expense_categorymodel.isValid()){
      expense_categorymodel.save(null,{ 
        success: function () {
          $('#expense_categorytable').empty();
          $('#expense_categorytable').unbind();
          $( '#paginator' ).empty();
          $( '#paginator' ).unbind(); 
          appRouter.currentView.expense_categorySearch();
            console.log('success!');
            
        },
        error: function (model, error) {
            console.log(error);
            return false;
        }});
    }else{
      _.each(expense_categorymodel.validationError, function (error) {
           $('#' + error.name).text(error.message);
        });
    return false;
    }
     return true;
    },
      
    render: function () {
      this.$el.html(this.template());
      this.delegateEvents();
      return this;
    }

});
    var columns = [{
    name: "",
    cell: "select-row",
    headerCell: "select-all",

  }, {
    name: "expense_category_name",
    label: "Expense Name",
    editable: false,
    cell: "string" 
  }, {
    name: "expense_category_description",
    label: "Expense Description",
    editable: false,
    cell: "string" 
  },
  {
        name: "delete",
        label: "Action",
        cell: DeleteCell
    }];

var grid = new Backgrid.Grid({
  columns: columns,
  collection: self.collection
});
var paginator = new Backgrid.Extension.Paginator({
        collection: self.collection
      });
$("#expense_categorytable").append(grid.render().el);
$("#paginator").append(paginator.render().$el);
appRouter.expense_categorygrid = grid;

            return this;  
  },

    removeoraddAll: function()
  {
      this.collection.reset();
      this.$el.empty();

  },

});
