var admin = admin || {};

admin.ExpenseCategoryPageView = Backbone.View.extend({
    template: $( '#expense_categoryPageTpl' ).html(),
  initialize: function() {
    appRouter.paginationtype = "";
    this.render();    
  },

  events:{
       'click #create_expense_category' : 'expense_categoryCreate',
       'click #expense_category_deleteall'    :'restoreordeleteAll',
       'click #expense_category_deleteselect'    :'addordeleteSelect',
       'input #expense_category_search'  : 'expense_categorySearch',
       'click #expense_category_restoreall'    :'restoreordeleteAll',
       'click #expense_category_addselect'    :'addordeleteSelect',
  },

  render: function() {          
      var _self = this;
      this.$el.html(this.template);
      $("#switch1").switchbutton({
      checkedLabel: 'YES',
      uncheckedLabel: 'NO',
      classes: 'ui-switchbutton-ios5'
    }).change(function(e){
     var toggledel = $(this).prop("checked") ? "checked" : "unchecked";
      _self.toggle(toggledel);
    });
    
    var toggleval = $("#switch1").prop("checked") ? "checked" : "unchecked";
    this.toggle(toggleval);
    return this;
  },
  toggle:function(toggledel){
  console.log(toggledel);
  if(toggledel == 'checked'){
    this.expense_categoryListView = new admin.ExpenseCategoryTableView({el: $( '#expense_category-list'),
      mode:'normal'});
     $('#create_expense_category').show();
     $('#expense_category_deleteselect').show();
     $('#expense_category_deleteall').show();
     $('#expense_category_search').show();
     $('#expense_category_search').val("");
     $('#expense_category_restoreall').hide();
     $('#expense_category_addselect').hide();
     this.adddeletelabel = "Delete Selected ExpenseCategory";
     this.adddeletetext = "Do You want to Delete Selected expense_category???";
     this.adddeleteurl = "/btree/admin/delete_selectexpense_category";
     this.searchurl = "/btree/admin/search_expense_category";
     this.restoredeletelabel = "Delete All ExpenseCategory";
     this.restoredeletetext = "Do You want to Delete all this expense_category???";
     this.restoredeleteurl = "/btree/admin/delete_allexpense_category";
  }else if(toggledel == 'unchecked'){
    this.expense_categoryListView = new admin.ExpenseCategoryTableView({el: $( '#expense_category-list' ),
      mode:'trash'});
     $('#create_expense_category').hide();
     $('#expense_category_deleteselect').hide();
     $('#expense_category_deleteall').hide();
     $('#expense_category_search').show();
     $('#expense_category_search').val("");
     $('#expense_category_restoreall').show();
     $('#expense_category_addselect').show();
     this.adddeletelabel = "Restore Selected ExpenseCategory";
     this.adddeletetext = "Do You want to Restore Selected expense_category???";
     this.adddeleteurl = "/btree/admin/add_selectexpense_category";
     this.searchurl = "/btree/admin/search_trashexpense_category";
     this.restoredeletelabel = "Restore All ExpenseCategory";
     this.restoredeletetext = "Do You want to Restore all this expense_category???";
     this.restoredeleteurl = "/btree/admin/restore_allexpense_category";
  
  }
  },

  expense_categoryCreate:function(e){
        e.preventDefault();
        var self = this;
    var currentView = new admin.FormPageView({
                template: _.template($('#expense_categoryCreateFormTpl').html()),
                mode: 'create'
            }); 
    var expense_categorymodal= new BackboneBootstrapModals.ConfirmationModal({
          label: "Create Expense",
          text: currentView.el,
          onConfirm: function(e) {
            console.log("action confirmed");
            return self.rendersaveexpense_category();
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    expense_categorymodal.render();
  },

  restoreordeleteAll:function(e)
  {
          e.preventDefault();
        var self = this;
        var expense_categoryselect= new BackboneBootstrapModals.ConfirmationModal({
          label: self.restoredeletelabel,
          text: self.restoredeletetext,
          onConfirm: function(e) {
             $.ajax({
                    type: 'POST',
                    url: self.restoredeleteurl,
                     contentType:'application/json',
                    cache:false,
                    success: function(data) {
                      self.expense_categoryListView.removeoraddAll();
                      $( "div.success").html("All expense_category are Deleted Successfully.");
                      $( "div.success" ).fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );   
                    },
                    error: function(data) {
                      
                    }
             });
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    expense_categoryselect.render();   
  },
  
   expense_categorySearch : function()
   {
    $('#findStatus').html("");
     //$('#findStatus').html(alert("No Records Found"));
    var search_expense_category = $('#expense_category_search').val();
    var data = {};
    self=this;
    if(search_expense_category.length >=1 )
    {
        console.log("more 2");
        data.expense_category_name = search_expense_category;

        console.log(" search data "+JSON.stringify(data));

        this.expense_categoryListView.collection.fetch({url:'/btree/admin/search_expense_category',reset: true, data:data, processData: true,
            success: function (coll) {
                $( '#expense_categorytrashtable' ).empty();
                $( '#expense_categorytrashtable' ).unbind();
                $( '#paginator' ).empty();
                $( '#paginator' ).unbind();                         
                self.expense_categoryListView.render();
            },
            error: function(err) {
                $( "div.failure").html(" Error while searching the expense_category . Contact Administrator.");
                $( "div.failure" ).fadeIn( 300 ).delay( 3500 ).fadeOut( 800 );                      
            }

        });
        $("#expense_category_load_more").hide();     
    }
    if(search_expense_category.length == 0)
    {            
        console.log("empty");
        skip = 0;
         this.expense_categoryListView.collection.fetch({reset: true, data, processData: true,
            success: function (coll) {
                $( '#expense_categorytable' ).empty();
                $( '#expense_categorytable' ).unbind();
                $( '#paginator' ).empty();
                $( '#paginator' ).unbind();                         
                self.expense_categoryListView.render();   
            },       

        }); 
        $("#expense_category_load_more").show();         
    }
},
rendersaveexpense_category:function(){
  var self = this;
 var expense_categorymodel = new admin.ExpenseCategoryModel();
 var expense_categorydata = { expense_category_name:$.trim($('#expense_category_name').val()),
  expense_category_description:$.trim($('#expense_category_description').val())
 };
expense_categorymodel.set(expense_categorydata);
if(expense_categorymodel.isValid()){
  expense_categorymodel.save(null,{ 
    success: function () {
      $( "div.success").html("Expense Successfully Created.");
        self.render();
        return true;
    },
    error: function (model, error) {
        console.log(error);
        return false;
    }});
}else{
  _.each(expense_categorymodel.validationError, function (error) {
       $('#' + error.name).text(error.message);
    });
return false;
}
 return false;
},

addordeleteSelect:function(e){
  e.preventDefault();
  var self = this;
  var selectedModels = appRouter.expense_categorygrid.getSelectedModels();
  console.log(selectedModels);
  var selectexpense_category = [];
for(i=0;i<selectedModels.length;i++){
  selectexpense_category.push(selectedModels[i].attributes.expense_category_id);
}
    var expense_categoryselect= new BackboneBootstrapModals.ConfirmationModal({
          label: self.adddeletelabel,
          text: self.adddeletetext,
          onConfirm: function(e) {
            $('#expense_categorytable').empty();
            $('#expense_categorytable').unbind();
            $( '#paginator' ).empty();
            $( '#paginator' ).unbind(); 
            $.ajax({
              type:'POST',
              url:self.adddeleteurl,
              data:JSON.stringify(selectexpense_category),
              contentType:'application/json',
              cache:false,
              success:function(){
                self.render();
              },
              error:function(){
                return false;
              }
            });
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    expense_categoryselect.render();
},
});
