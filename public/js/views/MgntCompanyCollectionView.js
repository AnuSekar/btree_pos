var admin = admin || {};

admin.MgntCompanyCollectionView = Backbone.View.extend({

	initialize: function(options) {
    	console.log('Initialize MgntCompanyCollectionView');
    	console.log('faqCollection');
	    	console.log(options.comCollection);
			this.collection = new admin.CompanyCollection(options.comCollection);

		_.bindAll(this, "renderCompany");
		_.bindAll(this, "render");
		this.render();
	},

	events:{
	},

	render: function() {
    	console.log('Rendering Company collection');
    	if( this.collection )
    	{    		
    	   // this.$el.html( '<option value="">Select</option>' );
			   this.collection.each(function(item) {
		    	  console.log(item.get('company_id'));
		        console.log(item.get('company_name'));
				this.renderCompany(item);
			}, this );
		}
    	return this;
	},

	renderCompany: function( item ) {
		var comItemView = new admin.MgntCompanyItemView({
			model: item
		});
    	this.$el.append( comItemView.render().el );
	},

});
