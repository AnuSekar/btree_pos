var admin = admin || {};

admin.ReportListPageView = Backbone.View.extend({
  
  template : $('#ReportListTpl').html(),
   
	initialize: function() {
        this.customercol= new Backbone.Collection();
        this.suppliercol= new Backbone.Collection();
        this.render(); 
      
   
	},

	events:{
          'click #submit_company':'rendercompany',
          'click #submit_product':'renderproduct',
          'click #submit_customer':'rendercustomer',
          'click #submit_supplier':'rendersupplier',
          'click #submit_item':'renderitem',
          'click #submit_expense_category':'renderexpensecategory',
          'click #submit_sales':'rendersales',
          'click #submit_suspendedsales':'rendersuspendedsales',
          'click #submit_receiving':'renderreceiving',
          'click #submit_discount':'renderdiscount',
          'click #submit_payment':'renderpayment',
          'click #submit_tax':'rendertax',
          'click #submit_employee':'renderemployee',
          'click #submit_commission':'rendercommission',
          'click #submit_itemkits':'renderitemkits',
          'click #submit_inventorysummary':'renderinventorysummary',
          'click #submit_deaditem':'renderdeaditem',
          'change #date_type' : 'renderdatepicker',
          'input #customer_name' : 'renderCustomerName',
          'input #employee_name' : 'renderEmployeeName',


	},

	render: function() {		
        appRouter.paginationtype = "report";	
        appRouter.dateval = '';
        var tmpl = _.template( this.template );		
	    this.$el.html(tmpl({reporttype:appRouter.listreport, rendertype : appRouter.reporttype}));
	    $('.input-daterange').hide();
		  return this;
	},
	rendercompany:function(){

	var type = $('#date_type').val();
  var transtype = $('#transaction_type').val();
	console.log(type);
	var dateval = {};
	if(type == 11){
		if($('#start_date').val() == '' || $('#start_date').val() == null){
			$('#datepicker_error').text('please enter start date');
			return false;
		}else if($('#end_date').val() == '' || $('#end_date').val() == null){
			$('#datepicker_error').text('please enter end date');
			return false;
		}
	   dateval.type = type;
     dateval.transtype = transtype;
     dateval.start_date = $('#start_date').val();
	   dateval.end_date = $('#end_date').val();
	}else{
		dateval.type = type;
    dateval.transtype = transtype;
  }
  
  if(appRouter.reporttype == 'normal') {
     this.collection = new admin.CompanyReportCollection();
      dateval.reporttype = appRouter.reporttype;
    }else{
      this.collection = new admin.CompanyGraphReportCollection();
      dateval.reporttype = 'graph';
    } 
  appRouter.dateval = dateval; 
  this.collection.fetch({
  success:function(data){
  appRouter.collection = data;
  console.log(data);
  appRouter.navigate("companyreportList", {trigger: true});
  },
  error:function(){

  }});
	
	},

	renderproduct:function(){
  	var type = $('#date_type').val();
    var transtype = $('#transaction_type').val();
  	console.log(type);
  	var dateval = {};
  	if(type == 11){
    	if($('#start_date').val() == '' || $('#start_date').val() == null){
      		$('#datepicker_error').text('please enter start date');
      		return false;
    	}else if($('#end_date').val() == '' || $('#end_date').val() == null){
      		$('#datepicker_error').text('please enter end date');
      		return false;
    	}
    dateval.type = type;
    dateval.transtype = transtype;
    dateval.start_date = $('#start_date').val();
    dateval.end_date = $('#end_date').val();
  	}else{
    	dateval.type = type;
      dateval.transtype = transtype;
  	}
    if(appRouter.reporttype == 'normal') {
     this.collection = new admin.ProductReportCollection();
      dateval.reporttype = appRouter.reporttype;
    }else{
      this.collection = new admin.ProductGraphReportCollection();
      dateval.reporttype = 'graph';
    } 
    appRouter.dateval = dateval;
    this.collection.fetch({
    success:function(data){
  	appRouter.collection =data;
  	console.log(this.collection);
    appRouter.navigate("productreportList", {trigger: true});
  },

  error:function(){

}});
  
  },

  rendercustomer:function(){

    var type = $('#date_type').val();
    var transtype = $('#transaction_type').val();
    console.log(type);
    var dateval = {};
    var personid = $('#person_cust_id').val();
    if(type == 11){
      if($('#start_date').val() == '' || $('#start_date').val() == null){
          $('#datepicker_error').text('please enter start date');
          return false;
      }else if($('#end_date').val() == '' || $('#end_date').val() == null){
          $('#datepicker_error').text('please enter end date');
          return false;
      }
    dateval.type = type;
     dateval.transtype = transtype;
    dateval.start_date = $('#start_date').val();
    dateval.end_date = $('#end_date').val();
    }else{
      dateval.type = type;
       dateval.transtype = transtype;
    }
    if(appRouter.reporttype == 'normal') {
     this.collection = new admin.CustomerReportCollection();
     dateval.reporttype = appRouter.reporttype;
    }else if(appRouter.reporttype == 'details'){
      this.collection = new admin.CustomerDetailsReportCollection();
    }
    else{
      this.collection = new admin.CustomerGraphReportCollection();
      dateval.reporttype = 'graph';
    }

    if(appRouter.reporttype == 'details'){
      if(personid == "" || personid == null || personid == undefined){
        alert("please select customer name");
        return false;
      }
      dateval.personid = personid;
    }
    appRouter.dateval = dateval;
    this.collection.fetch({
    success:function(data){
    appRouter.collection = data;
    console.log(this.collection);
    appRouter.navigate("customerreportList", {trigger: true});
  },

  error:function(){

}});
  
  },

  rendersupplier:function(){

    var type = $('#date_type').val();
    var transtype = $('#transaction_type').val();
    console.log(type);
    var dateval = {};
   
    if(type == 11){
      if($('#start_date').val() == '' || $('#start_date').val() == null){
          $('#datepicker_error').text('please enter start date');
          return false;
      }else if($('#end_date').val() == '' || $('#end_date').val() == null){
          $('#datepicker_error').text('please enter end date');
          return false;
      }
    dateval.type = type;
     dateval.transtype = transtype;
    dateval.start_date = $('#start_date').val();
    dateval.end_date = $('#end_date').val();
    }else{
      dateval.type = type;
       dateval.transtype = transtype;
    }
    if(appRouter.reporttype == 'normal') {
     this.collection = new admin.SupplierReportCollection();
     dateval.reporttype = appRouter.reporttype;
    }else if(appRouter.reporttype == 'details'){
      this.collection = new admin.SupplierDetailsReportCollection();
    }
    else{
      this.collection = new admin.SupplierGraphReportCollection();
      dateval.reporttype = 'graph';
    }
    
    appRouter.dateval = dateval;
    this.collection.fetch({
    success:function(data){
    appRouter.collection = data;
    console.log(this.collection);
    appRouter.navigate("supplierreportList", {trigger: true});
  },

  error:function(){

}});
  
  },

  renderitem:function(){
    
    var type = $('#date_type').val();
    var transtype = $('#transaction_type').val();
    console.log(type);
    var dateval = {};
    if(type == 11){
      if($('#start_date').val() == '' || $('#start_date').val() == null){
          $('#datepicker_error').text('please enter start date');
          return false;
      }else if($('#end_date').val() == '' || $('#end_date').val() == null){
          $('#datepicker_error').text('please enter end date');
          return false;
      }
    dateval.type = type;
     dateval.transtype = transtype;
    dateval.start_date = $('#start_date').val();
    dateval.end_date = $('#end_date').val();
    }else{
      dateval.type = type;
       dateval.transtype = transtype;
    }
    if(appRouter.reporttype == 'normal') {
     this.collection = new admin.ItemReportCollection();
      dateval.reporttype = appRouter.reporttype;
    }else{
      this.collection = new admin.ItemGraphReportCollection();
      dateval.reporttype = 'graph';
    } 
    appRouter.dateval = dateval;
    this.collection.fetch({
    success:function(data){
    appRouter.collection = data;
    console.log(this.collection);
    appRouter.navigate("itemreportList", {trigger: true});
  },

  error:function(){

}});
  
  },

  renderexpensecategory:function(){
     this.collection = new admin.ExpenseCategoryReportCollection();
    var type = $('#date_type').val();
    var transtype = '';
    console.log(type);
    var dateval = {};
    if(type == 11){
      if($('#start_date').val() == '' || $('#start_date').val() == null){
          $('#datepicker_error').text('please enter start date');
          return false;
      }else if($('#end_date').val() == '' || $('#end_date').val() == null){
          $('#datepicker_error').text('please enter end date');
          return false;
      }
    dateval.type = type;
     dateval.transtype = transtype;
    dateval.start_date = $('#start_date').val();
    dateval.end_date = $('#end_date').val();
    }else{
      dateval.type = type;
       dateval.transtype = transtype;
    }
    appRouter.dateval = dateval;
    this.collection.fetch({
    success:function(data){
    appRouter.collection = data;
    console.log(this.collection);
    appRouter.navigate("expensecategoryreportList", {trigger: true});
  },

  error:function(){

}});
  
  },

  rendersales:function(){
    
    var type = $('#date_type').val();
    var transtype = $('#transaction_type').val();
    console.log(type);
    var dateval = {};
    if(type == 11){
      if($('#start_date').val() == '' || $('#start_date').val() == null){
          $('#datepicker_error').text('please enter start date');
          return false;
      }else if($('#end_date').val() == '' || $('#end_date').val() == null){
          $('#datepicker_error').text('please enter end date');
          return false;
      }
    dateval.type = type;
     dateval.transtype = transtype;
    dateval.start_date = $('#start_date').val();
    dateval.end_date = $('#end_date').val();
    }else{
      dateval.type = type;
       dateval.transtype = transtype;
    }
    appRouter.dateval = dateval;
    if(appRouter.reporttype == 'normal') {
     this.collection = new admin.SalesReportCollection();
     dateval.reporttype = appRouter.reporttype;
    }else if(appRouter.reporttype == 'details'){
      this.collection = new admin.SalesDetailsReportCollection();
    }
    else {
      this.collection = new admin.SalesGraphReportCollection();
      dateval.reporttype = 'graph';
    }
    this.collection.fetch({
    success:function(data){
    appRouter.collection = data;
    console.log(this.collection);
    appRouter.navigate("salesreportList", {trigger: true});
  },

  error:function(){

}});
  
  },
  rendersuspendedsales:function(){
    if(appRouter.reporttype == 'normal') {
     this.collection = new admin.SuspendedSalesReportCollection();
    }else{
      this.collection = new admin.SuspendedSalesGraphReportCollection();
    }
    var type = $('#date_type').val();
    var transtype = $('#transaction_type').val();
    console.log(type);
    var dateval = {};
    if(type == 11){
      if($('#start_date').val() == '' || $('#start_date').val() == null){
          $('#datepicker_error').text('please enter start date');
          return false;
      }else if($('#end_date').val() == '' || $('#end_date').val() == null){
          $('#datepicker_error').text('please enter end date');
          return false;
      }
    dateval.type = type;
     dateval.transtype = transtype;
    dateval.start_date = $('#start_date').val();
    dateval.end_date = $('#end_date').val();
    }else{
      dateval.type = type;
       dateval.transtype = transtype;
    }
    appRouter.dateval = dateval;
    this.collection.fetch({
    success:function(data){
    appRouter.collection = data;
    console.log(this.collection);
    appRouter.navigate("suspendedsalesreportList", {trigger: true});
  },

  error:function(){

}});
  
  },

  renderreceiving:function(){
    if(appRouter.reporttype == 'normal') {
     this.collection = new admin.ReceivingReportCollection();
    }else{
      this.collection = new admin.ReceivingGraphReportCollection();
    }
    var type = $('#date_type').val();
    var transtype = $('#transaction_type').val();
    console.log(type);
    var dateval = {};
    if(type == 11){
      if($('#start_date').val() == '' || $('#start_date').val() == null){
          $('#datepicker_error').text('please enter start date');
          return false;
      }else if($('#end_date').val() == '' || $('#end_date').val() == null){
          $('#datepicker_error').text('please enter end date');
          return false;
      }
    dateval.type = type;
     dateval.transtype = transtype;
    dateval.start_date = $('#start_date').val();
    dateval.end_date = $('#end_date').val();
    }else{
      dateval.type = type;
       dateval.transtype = transtype;
    }
    appRouter.dateval = dateval;
    this.collection.fetch({
    success:function(data){
    appRouter.collection = data;
    console.log(this.collection);
    appRouter.navigate("receivingreportList", {trigger: true});
  },

  error:function(){

}});
  
  },

  renderdiscount:function(){
  if(appRouter.reporttype == 'normal') {
     this.collection = new admin.DiscountReportCollection();
    }else if(appRouter.reporttype == 'details'){
      this.collection = new admin.DiscountDetailsReportCollection();
    }else{
      this.collection = new admin.DiscountGraphReportCollection();
    }
  var type = $('#date_type').val();
  var transtype = $('#transaction_type').val();
  var discountrate = $('#discountrate').val();
  console.log(type);
  var dateval = {};
  if(type == 11){
    if($('#start_date').val() == '' || $('#start_date').val() == null){
      $('#datepicker_error').text('please enter start date');
      return false;
    }else if($('#end_date').val() == '' || $('#end_date').val() == null){
      $('#datepicker_error').text('please enter end date');
      return false;
    }
     dateval.type = type;
     dateval.discountrate = discountrate;
      dateval.transtype = transtype;
       dateval.start_date = $('#start_date').val();
     dateval.end_date = $('#end_date').val();
  }else{
    dateval.type = type;
     dateval.discountrate = discountrate;
     dateval.transtype = transtype;
  }
  appRouter.dateval = dateval;
  console.log(dateval);
  this.collection.fetch({
    success:function(data){
    appRouter.collection = data;
    console.log(this.collection);
    appRouter.navigate("discountreportList", {trigger: true});
  },

  error:function(){

}});
  
  },
  

  renderpayment:function(){
     this.collection = new admin.PaymentReportCollection();
    
  var type = $('#date_type').val();
  var transtype = $('#transaction_type').val();
  console.log(type);
  var dateval = {};
  if(type == 11){
    if($('#start_date').val() == '' || $('#start_date').val() == null){
      $('#datepicker_error').text('please enter start date');
      return false;
    }else if($('#end_date').val() == '' || $('#end_date').val() == null){
      $('#datepicker_error').text('please enter end date');
      return false;
    }
     dateval.type = type;
      dateval.transtype = transtype;
       dateval.start_date = $('#start_date').val();
     dateval.end_date = $('#end_date').val();
  }else{
    dateval.type = type;
     dateval.transtype = transtype;
  }
  appRouter.dateval = dateval;
  this.collection.fetch({
   success:function(data){
    appRouter.collection = data;
    console.log(data);
    appRouter.navigate("paymentreportList", {trigger: true});
},
error:function(){

}});
   
  },
  rendertax:function(){
    if(appRouter.reporttype == 'normal') {
     this.collection = new admin.TaxReportCollection();
    }else{
      this.collection = new admin.TaxGraphReportCollection();
    }
  
  var type = $('#date_type').val();
  var transtype = $('#transaction_type').val();
  console.log(type);
  var dateval = {};
  if(type == 11){
    if($('#start_date').val() == '' || $('#start_date').val() == null){
      $('#datepicker_error').text('please enter start date');
      return false;
    }else if($('#end_date').val() == '' || $('#end_date').val() == null){
      $('#datepicker_error').text('please enter end date');
      return false;
    }
     dateval.type = type;
     dateval.transtype = transtype;
     dateval.start_date = $('#start_date').val();
     dateval.end_date = $('#end_date').val();
  }else{
    dateval.type = type;
    dateval.transtype = transtype;
  }
   appRouter.dateval = dateval;
  this.collection.fetch({
success:function(data){
  appRouter.collection =data;
  console.log(data);
  appRouter.navigate("taxreportList", {trigger: true});
},
error:function(){

}});
  
  },
  renderemployee:function(){
    
  var type = $('#date_type').val();
  var transtype = $('#transaction_type').val();
  console.log(type);
  var dateval = {};
   var personid = $('#person_emp_id').val();
  if(type == 11){
    if($('#start_date').val() == '' || $('#start_date').val() == null){
      $('#datepicker_error').text('please enter start date');
      return false;
    }else if($('#end_date').val() == '' || $('#end_date').val() == null){
      $('#datepicker_error').text('please enter end date');
      return false;
    }
     dateval.type = type;
     dateval.transtype = transtype;
     dateval.start_date = $('#start_date').val();
     dateval.end_date = $('#end_date').val();
  }else{
    dateval.type = type;
    dateval.transtype = transtype;
  }
  if(appRouter.reporttype == 'normal') {
     this.collection = new admin.EmployeeReportCollection();
     dateval.reporttype = appRouter.reporttype;
    }else if(appRouter.reporttype == 'details'){
      this.collection = new admin.EmployeeDetailsReportCollection();
    }
    else{
      this.collection = new admin.EmployeeGraphReportCollection();
      dateval.reporttype = 'graph';
    }
  if(appRouter.reporttype == 'details'){
      if(personid == "" || personid == null || personid == undefined){
        alert("please select customer name");
        return false;
      }
      dateval.personid = personid;
    }
   appRouter.dateval = dateval;
  this.collection.fetch({
success:function(data){
  appRouter.collection =data;
  console.log(data);
  appRouter.navigate("employeereportList", {trigger: true});
},
error:function(){

}});
  
  },

  rendercommission:function(){
  if(appRouter.reporttype == 'normal') {
     this.collection = new admin.CommissionReportCollection();
    }else{
      this.collection = new admin.CommissionGraphReportCollection();
    }  
  var type = $('#date_type').val();
  var transtype = $('#transaction_type').val();
  console.log(type);
  var dateval = {};
  if(type == 11){
    if($('#start_date').val() == '' || $('#start_date').val() == null){
      $('#datepicker_error').text('please enter start date');
      return false;
    }else if($('#end_date').val() == '' || $('#end_date').val() == null){
      $('#datepicker_error').text('please enter end date');
      return false;
    }
     dateval.type = type;
     dateval.transtype = transtype;
     dateval.start_date = $('#start_date').val();
     dateval.end_date = $('#end_date').val();
  }else{
    dateval.type = type;
    dateval.transtype = transtype;
  }
  appRouter.dateval = dateval;
  this.collection.fetch({
  success:function(data){
  appRouter.collection =data;
  console.log(data);
  appRouter.navigate("commissionreportList", {trigger: true});
  },
  error:function(){

  }});
  
  },

  renderinventorysummary:function(){
      console.log("inside");
     this.collection = new admin.InventorySummaryReportCollection();
  var type = $('#transaction_type').val();
  var dateval = {};
  dateval.type = type;
  appRouter.dateval = dateval;
  this.collection.fetch({
  success:function(data){
  appRouter.collection =data;
  console.log(data);
  appRouter.navigate("inventorysummaryreportList", {trigger: true});
  },
  error:function(){

  }});
  
  },
  renderdeaditem:function(){
  
       this.collection = new admin.DeadItemReportCollection();
      
  var type = $('#date_type').val();
  var transtype = '';
  console.log(type);
  var dateval = {};
  if(type == 11){
    if($('#start_date').val() == '' || $('#start_date').val() == null){
      $('#datepicker_error').text('please enter start date');
      return false;
    }else if($('#end_date').val() == '' || $('#end_date').val() == null){
      $('#datepicker_error').text('please enter end date');
      return false;
    }
     dateval.type = type;
     dateval.transtype = transtype;
     dateval.start_date = $('#start_date').val();
     dateval.end_date = $('#end_date').val();
  }else{
    dateval.type = type;
    dateval.transtype = transtype;
  }
  appRouter.dateval = dateval;
  this.collection.fetch({
  success:function(data){
  appRouter.collection =data;
  console.log(data);
  appRouter.navigate("deaditemreportList", {trigger: true});
  },
  error:function(){

  }});
  
  },
  renderitemkits:function(){
  if(appRouter.reporttype == 'normal') {
     this.collection = new admin.ItemkitsReportCollection();
    }else{
      this.collection = new admin.ItemkitsGraphReportCollection();
    }  
  var type = $('#date_type').val();
  var transtype = $('#transaction_type').val();
  console.log(type);
  var dateval = {};
  if(type == 11){
    if($('#start_date').val() == '' || $('#start_date').val() == null){
      $('#datepicker_error').text('please enter start date');
      return false;
    }else if($('#end_date').val() == '' || $('#end_date').val() == null){
      $('#datepicker_error').text('please enter end date');
      return false;
    }
     dateval.type = type;
     dateval.transtype = transtype;
     dateval.start_date = $('#start_date').val();
     dateval.end_date = $('#end_date').val();
  }else{
    dateval.type = type;
    dateval.transtype = transtype;
  }
  appRouter.dateval = dateval;
  this.collection.fetch({
  success:function(data){
  appRouter.collection =data;
  console.log(data);
  appRouter.navigate("itemkitsreportList", {trigger: true});
  },
  error:function(){

  }});
  
  },
  
	renderdatepicker:function(){
	var type = $('#date_type').val();
	console.log(type);
	$('.input-daterange').hide();
	if(type == 11){
		$('.input-daterange').show();
		$('#datepicker').datepicker({});
	}
	},

  renderCustomerName:function(){
          var self = this;
          var searchdata = $('#customer_name').val();
          var data = {};
          if(searchdata.length > 0){
          data.searchcustomer = searchdata;
          $.when(
            this.customercol.fetch({
            url:"/btree/admin/get_customerbysales",
            reset:true,
            data:data,
            contentType: "application/json",
         })
          ).done(function(){
            console.log(self.customercol);
          var customerNames = self.customercol.map(function (model) {
          return _.pick(model.toJSON(), ["first_name", "last_name","label"]); 
            });
          console.log(customerNames);
            $("#customer_name").autocomplete({ 
            source : customerNames,
            minLength : 0,
            select: function(event, ui){ 
              var selectedModel =self.customercol.where({first_name: ui.item.value})[0];
              console.log(selectedModel);
              $('#person_cust_id').val(selectedModel.attributes.person_id);
            }
          })
       .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
        console.log(item);
          return $( "<li>" )
            .append( "<a>" + item.first_name + " " + item.last_name + "</a>")
            .appendTo( ul );
    }
        });

    }      
},
renderEmployeeName:function(){
          var self = this;
          var searchdata = $('#employee_name').val();
          var data = {};
          if(searchdata.length > 0){
          data.searchsupplier = searchdata;
          $.when(
            this.suppliercol.fetch({
            url:"/btree/admin/get_employeebysales",
            reset:true,
            data:data,
            contentType: "application/json",
         })
          ).done(function(){
            console.log(self.suppliercol);
          var supplierNames = self.suppliercol.map(function (model) {
          return _.pick(model.toJSON(), ["first_name", "last_name","label"]);
            });
            $("#employee_name").autocomplete({ 
            source : supplierNames,
            minLength : 0,
            select: function(event, ui){ 
              var selectedModel =self.suppliercol.where({first_name: ui.item.value})[0];
              console.log(selectedModel);
              $('#person_emp_id').val(selectedModel.attributes.person_id);
            }
          })
       .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
        console.log(item);
          return $( "<li>" )
            .append( "<a>" + item.first_name + " " + item.last_name + "</a>")
            .appendTo( ul );
    }
        })
    }      
},
});
