var admin = admin || {};

admin.CompanyTableView = Backbone.View.extend({

  initialize: function(options) {
      console.log('Initialize companyTableView');
      var self = this;
      this.pageNo = 1;
      limit =2;
      this.recIndex = 1;
      this.options = options;
      console.log(options.mode);
       $('#com_protable').empty();
      $('#com_protable').unbind();
      $( '#paginator' ).empty();
      $( '#paginator' ).unbind(); 
      if(options.mode == 'normal'){
        this.collection = new admin.CompanyCollection();    
      }else if(options.mode == 'trash'){
        this.collection = new admin.CompanyTrashCollection();    
      }
      
      this.collection.fetch({ 
        success:function(data){
          console.log(JSON.stringify(data));
          self.render();
        },
        error:function(data){
          
        }
      }); 
  },


  render: function() {          
    var self = this;
    if(this.options.mode == 'normal'){
        this.edittemplate = '<span style="display:inline-block; width: 20px;"><a id="editcompany" data-toggle="tooltip" data-placement="top" title="Edit">'+
      '<i class="glyphicon glyphicon-pencil"></i></a></span>'+
      '<span style="display:inline-block; width: 20px;"><a id="deletecompany" data-toggle="tooltip" data-placement="top" title="Delete">'+
      '<i class="glyphicon glyphicon-minus-sign"></i></a></span>';    
      this.edittext = 'Delete';
      }else if(this.options.mode == 'trash'){
        this.edittemplate = '<span style="display:inline-block; width: 20px;"><a id="deletecompany" data-toggle="tooltip" data-placement="top" title="Delete">'+
      '<i class="glyphicon glyphicon-minus-sign"></i></a></span>';  
      this.edittext = 'Restore';
      }
    var DeleteCell = Backgrid.Cell.extend({
    template: _.template(self.edittemplate),
    events: {
      "click #editcompany": "editRow",
      "click #deletecompany": "deleteRow",
    },
    deleteRow: function (e) {
      console.log("Hello");
      e.preventDefault();
      modelvalue = this.model;
      var companymodal= new BackboneBootstrapModals.ConfirmationModal({
          label: self.edittext+" Company",
          text: "Are You want to "+self.edittext+" this company?",
          onConfirm: function(e) {
            e.preventDefault();
            var companymodal = modelvalue;
            companymodal.destroy({
                success: function (model, respose, options) {
                    console.log("The model has deleted the server");
                    $('#com_protable').empty();
                    $('#com_protable').unbind();
                    $( '#paginator' ).empty();
                    $( '#paginator' ).unbind(); 
                    appRouter.currentView.Search();
                    return true;
                },
                error: function (model, xhr, options) {
                    console.log("Something went wrong while deleting the model");
                    return false;
                }
            });
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    companymodal.render();
    },
    editRow:function(e){
      var self= this;
       console.log("Hello",this.model);
      e.preventDefault();
      var currentView = new admin.FormPageView({
                template: _.template($('#companyEditFormTpl').html()),
                mode: 'edit',
                model:self.model.attributes
            }); 
    var companymodal= new BackboneBootstrapModals.ConfirmationModal({
          label: "Edit Company",
          text: currentView.el,
          onConfirm: function(e) {
            console.log("action confirmed");
            return self.rendereditcompany();
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    companymodal.render();
    },
    rendereditcompany:function(){
      var self = this;
     var companymodel = new admin.CompanyModel();
     var companydata = { company_id:self.model.attributes.company_id,
      company_name:$.trim($('#company_name').val()),
      company_description:$.trim($('#company_description').val())
     };
    companymodel.set(companydata );
     if(companymodel.isValid()){
      companymodel.save(null,{ 
        success: function () {
          $('#com_protable').empty();
          $('#com_protable').unbind();
          $( '#paginator' ).empty();
          $( '#paginator' ).unbind(); 
          appRouter.currentView.Search();
            console.log('success!');
            
        },
        error: function (model, error) {
            console.log(error);
            return false;
        }});
    }else{
      _.each(companymodel.validationError, function (error) {
           $('#' + error.name).text(error.message);
        });
    return false;
    }
     return true;
    },
      
    render: function () {
      this.$el.html(this.template());
      this.delegateEvents();
      return this;
    }

});
    var columns = [{
    name: "", 
    cell: "select-row",
    headerCell: "select-all",

  }, {
    name: "company_name",
    label: "Company Name",
    editable: false,
    cell: "string" 
  }, {
    name: "company_description",
    label: "Company Description",
    editable: false,
    cell: "string" 
  },
  {
        name: "delete",
        label: "Action",
        cell: DeleteCell
    }];

var grid = new Backgrid.Grid({
  columns: columns,
  collection: self.collection
});
var paginator = new Backgrid.Extension.Paginator({
        collection: self.collection
});

$("#com_protable").append(grid.render().el);
$("#paginator").append(paginator.render().$el);
appRouter.companygrid = grid;
return this;  
  },

    removeoraddAll: function()
  {
      this.collection.reset();
      this.$el.empty();

  },

});


admin.ProductTableView = Backbone.View.extend({

  initialize: function(options) {
      console.log('Initialize productTableView');
    var self = this;
        this.pageNo = 1;
       limit =2;
        this.recIndex = 1;
        this.options = options;
      console.log(options.mode);
       $('#com_protable').empty();
      $('#com_protable').unbind();
      $( '#paginator' ).empty();
      $( '#paginator' ).unbind(); 
      if(options.mode == 'normal'){
        this.collection = new admin.ProductCollection();    
      }else if(options.mode == 'trash'){
        this.collection = new admin.ProductTrashCollection();    
      }

      this.collection.fetch({ 
        success:function(data){
          console.log("success");

          console.log(JSON.stringify(data));
          self.render();


        },
        error:function(data){
          
        }
      }); 
  },

 
  render: function() {
    var self = this;
    if(this.options.mode == 'normal'){
        this.edittemplate = '<span style="display:inline-block; width: 20px;"><a id="editproduct" data-toggle="tooltip" data-placement="top" title="Edit">'+
      '<i class="glyphicon glyphicon-pencil"></i></a></span>'+
      '<span style="display:inline-block; width: 20px;"><a id="deleteproduct" data-toggle="tooltip" data-placement="top" title="Delete">'+
      '<i class="glyphicon glyphicon-minus-sign"></i></a></span>';    
      this.edittext = 'Delete';
      }else if(this.options.mode == 'trash'){
        this.edittemplate = '<span style="display:inline-block; width: 20px;"><a id="deleteproduct" data-toggle="tooltip" data-placement="top" title="Delete">'+
      '<i class="glyphicon glyphicon-minus-sign"></i></a></span>';  
      this.edittext = 'Restore';
      }
    var DeleteCell = Backgrid.Cell.extend({
    template: _.template(self.edittemplate),
    events: {
      "click #editproduct": "editRow",
      "click #deleteproduct": "deleteRow",
    },
    deleteRow: function (e) {
      console.log("Hello");
      e.preventDefault();
      modelvalue = this.model;
      var productmodal= new BackboneBootstrapModals.ConfirmationModal({
          label: "Delete Product",
          text: "Are You want to Delete this product?",
          onConfirm: function(e) {
            e.preventDefault();
            var productmodal = modelvalue;
            productmodal.destroy({
                success: function (model, respose, options) {
                    console.log("The model has deleted the server");
                    return true;
                },
                error: function (model, xhr, options) {
                    console.log("Something went wrong while deleting the model");
                    return false;
                }
            });
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    productmodal.render();
    },
    editRow:function(e){
      var self= this;
       console.log("Hello",this.model);
      e.preventDefault();
      var currentView = new admin.FormPageView({
                template: _.template($('#productEditFormTpl').html()),
                mode: 'edit',
                model:self.model.attributes
            }); 
    var productmodal= new BackboneBootstrapModals.ConfirmationModal({
          label: "Edit Product",
          text: currentView.el,
          onConfirm: function(e) {
            console.log("action confirmed");
            return self.rendereditproduct();
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    productmodal.render();
    },
    rendereditproduct:function(){
      var self = this;
     var productmodel = new admin.ProductModel();
     var productdata = { product_id:self.model.attributes.product_id,
      product_name:$.trim($('#product_name').val()),
      product_description:$.trim($('#product_description').val()) 
     };
    productmodel.set(productdata );
    if(productmodel.isValid()){
      productmodel.save(null,{ 
        success: function () {
          $('#com_protable').empty();
          $('#com_protable').unbind();
          $( '#paginator' ).empty();
          $( '#paginator' ).unbind(); 
          appRouter.currentView.Search();
            console.log('success!');
        },
        error: function (model, error) {
            console.log(error);
            return false;
        }});
    }else{
      _.each(productmodel.validationError, function (error) {
           $('#' + error.name).text(error.message);
        });
    return false;
    }
     return true;
    },
      
    render: function () {
      this.$el.html(this.template());
      this.delegateEvents();
      return this;
    }

});
    var columns = [{
    name: "", 
    cell: "select-row",
    headerCell: "select-all",

  }, 
  {
    name: "product_name",
    label: "Product Name",
     editable: false,
    cell: "string" 
  }, {
    name: "product_description",
    label: "Product Description",
     editable: false,
    cell: "string" 
  },
  {
        name: "delete",
        label: "delete",
        cell: DeleteCell
    }];

var grid = new Backgrid.Grid({
  columns: columns,
  collection: self.collection
});

var paginator = new Backgrid.Extension.Paginator({
        collection: self.collection
      });
$("#com_protable").append(grid.render().el);
$("#paginator").append(paginator.render().$el);
appRouter.productgrid = grid;
      return this;  
  
  },

    removeoraddAll: function()
  {
      this.collection.reset();
      this.$el.empty();

  },

});

