var admin = admin || {};

admin.ProductReportPageView = Backbone.View.extend({
  
  template : $('#productReportTpl').html(),
   
  initialize: function(options) {
        //this.template = options.template;
        if(appRouter.reporttype == 'normal') {
          this.render(); 
        }else{
          this.graphrender(); 
        }  
  },

  render: function() {
  var self = this; 
   var tmpl = _.template( this.template );    
 this.$el.html(tmpl({exporttype:'product_report',type:appRouter.dateval.type, transtype:appRouter.dateval.transtype, start_date:appRouter.dateval.start_date, end_date:appRouter.dateval.end_date}));      
    var columns = [{
    name: "product_name",
    label: "Product Name",
     editable: false,
    cell: "string" 
  },
  {
    name: "sales_amount",
    label: "Sub Total",
     editable: false,
    cell: "string" 
  },
  {
    name: "item_total_cost",
    label: "Total",
     editable: false,
    cell: "string" 
  },
  {
    name: "tax_amount",
    label: "Tax",
     editable: false,
    cell: "string" 
  },
  {
    name: "quantity_purchased",
    label: "Number of Item Sold",
     editable: false,
    cell: "string" 
  }];
console.log(appRouter.collection);
var grid = new Backgrid.Grid({
  columns: columns,
  collection: appRouter.collection,
   rendertype : 'report'
});
var paginator = new Backgrid.Extension.Paginator({
        collection: appRouter.collection
      });
$("#productreport").append(grid.render().el);
$("#productpaginator").append(paginator.render().$el);
return this;
  },

  graphrender:function(){
     var arr = [];
    appRouter.collection.each(function(item){
      arr.push({label : item.get('product_name'), value : item.get('sales_amount') });
    });
    console.log(arr);
    this.$el.html(this.template);
    $('#download').hide();
    var chartData = {
          "chart": {
            "caption": "Product Report List",
            "subCaption": "Data specified",
            "xAxisName": "Product",
            "yAxisName": "Sales Amount",
            "numberPrefix": "rs",
            "theme": "carbon",
            "bgcolor": "#DCEAFC",
            "canvasbgcolor": "#DCEAFC",
            "canvasbgalpha": "10",
            "rotateValues": "1",
            "exportEnabled": "1",
            "transposeAnimation": "1",
            "pieRadius": "70",
          },

          "data": arr
        };
        revenueChartColumn = new FusionCharts({
          type: 'column2d',
          width: '420',
          height: '350',
          dataFormat: 'json',
          id: 'productreport1',
          dataSource: chartData
        });
       revenueChartColumn.render("productreport");

  }

  
 
});
