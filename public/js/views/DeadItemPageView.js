var admin = admin || {};

admin.DeadItemPageView = Backbone.View.extend({
  
  template : $('#deaditemReportTpl').html(),
   
  initialize: function(options) {
        this.template = options.template;
        this.render();
      
  },

  
 render: function() {
  var self = this;      
   this.$el.html(this.template);
    var columns = [{
    name: "item_name",
    label: "Item Name",
     editable: false,
    cell: "string" 
  },
  {
    name: "item_number",
    label: "Item Number",
     editable: false,
    cell: "string" 
  },
  {
    name: "cost_price",
    label: "Cost Price",
     editable: false,
    cell: "string" 
  },
  {
    name: "unit_price",
    label: "Unit Price ",
     editable: false,
    cell: "string" 
  },
  {
    name: "quantity",
    label: "Quantity ",
     editable: false,
    cell: "string" 
  }
  ];
console.log(appRouter.collection);
var grid = new Backgrid.Grid({
  columns: columns,
  collection: appRouter.collection,
  rendertype : 'report'
});
var paginator = new Backgrid.Extension.Paginator({
        collection: appRouter.collection
      });
$("#deaditemreport").append(grid.render().el);
$("#deaditempaginator").append(paginator.render().$el);
return this;
  },
  

 
});
  
  

 