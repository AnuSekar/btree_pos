var admin = admin || {};

admin.SupplierReportPageView = Backbone.View.extend({
  
  template : $('#supplierReportTpl').html(),
   
  initialize: function(options) {
        //this.template = options.template;
        if(appRouter.reporttype == 'normal') {
          this.render(); 
        }else if(appRouter.reporttype == 'details'){
          this.detailsrender();
        }
        else{
          this.graphrender(); 
        }
      
   
  },

  
 render: function() {
  var self = this;      
 var tmpl = _.template( this.template );    
 this.$el.html(tmpl({exporttype:'supplier_report',type:appRouter.dateval.type, transtype:appRouter.dateval.transtype, start_date:appRouter.dateval.start_date, end_date:appRouter.dateval.end_date}));  
    var columns = [{
     name: "name",
    label: "Supplier Name",
     editable: false,
    cell: "string" 
  },
  {
    name: "sales_amount",
    label: "Sub Total",
     editable: false,
    cell: "string" 
  },
  {
    name: "item_total_cost",
    label: "Total",
     editable: false,
    cell: "string" 
  },
  {
    name: "tax_amount",
    label: "Tax",
     editable: false,
    cell: "string" 
  },
  {
    name: "sales_whole_amount",
    label: "Whole Price",
     editable: false,
    cell: "string" 
  },
  {
    name: "count",
    label: "Count",
     editable: false,
    cell: "string" 
  }];
console.log(appRouter.collection);
var grid = new Backgrid.Grid({
  columns: columns,
  collection: appRouter.collection,
  rendertype : 'report'
});
var paginator = new Backgrid.Extension.Paginator({
        collection: appRouter.collection
      });
$("#supplierreport").append(grid.render().el);
$("#supplierpaginator").append(paginator.render().$el);
return this;
  },

  graphrender:function(){
     var arr = [];
    appRouter.collection.each(function(item){
      arr.push({label : item.get('name'), value : item.get('sales_amount') });
    });
    console.log(arr);
    this.$el.html(this.template);
    $('#download').hide();
    var chartData = {
          "chart": {
            "caption": "Product Report List",
            "subCaption": "Data specified",
            "xAxisName": "Supplier",
            "yAxisName": "Sales Amount",
            "numberPrefix": "rs",
            "theme": "carbon",
            "bgcolor": "#DCEAFC",
            "canvasbgcolor": "#DCEAFC",
            "canvasbgalpha": "10",
            "rotateValues": "1",
            "exportEnabled": "1",
            "transposeAnimation": "1",
            "pieRadius": "70",
          },

          "data": arr
        };
        revenueChartColumn = new FusionCharts({
          type: 'column2d',
          width: '420',
          height: '350',
          dataFormat: 'json',
          id: 'supplierreport1',
          dataSource: chartData
        });
       revenueChartColumn.render("supplierreport");

  },

  detailsrender:function(){
    var self = this;      
   this.$el.html(this.template);

    var columns = [{
    name: "name",
    label: "Supplier Name",
     editable: false,
    cell: "string" 
  },
  {
    name: "item_purchase",
    label: "item purchased",
     editable: false,
    cell: "string"
  },
  {
    name: "soldby",
    label: "sold by",
     editable: false,
    cell: "string"
  },
  {
    name: "sales_amount",
    label: "Sub Total",
     editable: false,
    cell: "string"
  },
  {
    name: "item_total_cost",
    label: "Total",
     editable: false,
    cell: "string"
  },
  {
    name: "tax_amount",
    label: "Tax",
     editable: false,
    cell: "string"
  },
  {
    name: "payment_type",
    label: "payment type",
     editable: false,
    cell: "string"
  },
  {
    name: "sale_time",
    label: "Date",
     editable: false,
    cell: "string" 
  }];
console.log(appRouter.collection);
var grid = new Backgrid.Grid({
  columns: columns,
  collection: appRouter.collection,
  rendertype : 'report'
});
var paginator = new Backgrid.Extension.Paginator({
        collection: appRouter.collection
      });
$("#supplierreport").append(grid.render().el);
$("#supplierpaginator").append(paginator.render().$el);
return this;
  },

  
 
});
  
  

 