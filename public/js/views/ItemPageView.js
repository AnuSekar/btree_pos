var admin = admin || {};

admin.ItemPageView = Backbone.View.extend({
    template: $( '#itemPageTpl' ).html(),
  initialize: function() {
    appRouter.paginationtype = "";
    this.render();    
  },

  events:{
       'click #create-item' : 'itemCreate',
       'click #item_deleteall'    :'restoreordeleteAll',
       'click #item_deleteselect'    :'addordeleteSelect',
       'input #item_search'  : 'itemSearch',
       'click #item_restoreall'    :'restoreordeleteAll',
       'click #item_addselect'    :'addordeleteSelect',
  },

  render: function() {          
      var _self = this;
      this.$el.html(this.template);
      $("#switch1").switchbutton({
      checkedLabel: 'YES',
      uncheckedLabel: 'NO',
      classes: 'ui-switchbutton-ios5'
    }).change(function(e){
     var toggledel = $(this).prop("checked") ? "checked" : "unchecked";
      _self.toggle(toggledel);
    });
    
    var toggleval = $("#switch1").prop("checked") ? "checked" : "unchecked";
    this.toggle(toggleval);
    return this;
  },
  toggle:function(toggledel){
  console.log(toggledel);
  if(toggledel == 'checked'){
    this.itemListView = new admin.ItemTableView({el: $( '#item-list'),
      mode:'normal'});
     $('#create_item').show();
     $('#item_deleteselect').show();
     $('#item_deleteall').show();
     $('#item_search').show();
     $('#item_search').val("");
     $('#item_restoreall').hide();
     $('#item_addselect').hide();
     this.adddeletelabel = "Delete Selected Item";
     this.adddeletetext = "Do You want to Delete Selected item???";
     this.adddeleteurl = "/btree/admin/delete_selectitem";
     this.searchurl = "/btree/admin/search_item";
     this.restoredeletelabel = "Delete All Item";
     this.restoredeletetext = "Do You want to Delete all this item???";
     this.restoredeleteurl = "/btree/admin/delete_allitem";
  }else if(toggledel == 'unchecked'){
    this.itemListView = new admin.ItemTableView({el: $( '#item-list' ),
      mode:'trash'});
     $('#create_item').hide();
     $('#item_deleteselect').hide();
     $('#item_deleteall').hide();
     $('#item_search').show();
     $('#item_search').val("");
     $('#item_restoreall').show();
     $('#item_addselect').show();
     this.adddeletelabel = "Restore Selected Item";
     this.adddeletetext = "Do You want to Restore Selected item???";
     this.adddeleteurl = "/btree/admin/add_selectitem";
     this.searchurl = "/btree/admin/search_trashitem";
     this.restoredeletelabel = "Restore All Item";
     this.restoredeletetext = "Do You want to Restore all this item???";
     this.restoredeleteurl = "/btree/admin/restore_allitem";
  }
  },

  itemCreate:function(e){
        var self = this;
    var currentView = new admin.ItemFormPageView({
                template: _.template($('#itemCreateFormTpl').html()),
                mode: 'create'
            }); 
    var itemmodal= new BackboneBootstrapModals.ConfirmationModal({
          label: "Create Item",
          text: currentView.el,
          onConfirm: function(e) {
            console.log("action confirmed");
            return self.rendersaveitem();
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    itemmodal.render();
  },

  restoreordeleteAll:function(e)
  {
          e.preventDefault();
        var self = this;
        var itemselect= new BackboneBootstrapModals.ConfirmationModal({
          label: self.restoredeletelabel,
          text: self.restoredeletetext,
          onConfirm: function(e) {
             $.ajax({
                    type: 'POST',
                    url: self.restoredeleteurl,
                     contentType:'application/json',
                    cache:false,
                    success: function(data) {
                      self.itemListView.removeoraddAll();
                      $( "div.success").html("All item are Deleted Successfully.");
                      $( "div.success" ).fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );   
                    },
                    error: function(data) {
                      
                    }
             });
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    itemselect.render();   
  },  

  
  itemSearch:function () {
  
    console.log("itemSearch");
    $('#findStatus').html("");
    var search_item = $('#item_search').val();
    var data = {};
    self=this;
    if(search_item.length >=1 )
    {
        console.log("more 2");
        data.item_name = search_item;

        console.log(" search data "+JSON.stringify(data));

        this.itemListView.collection.fetch({url:'/btree/admin/search_item',reset: true, data:data, processData: true,
            success: function (coll) {
                $( '#itemtable' ).empty();
                $( '#itemtable' ).unbind();
                $( '#paginator' ).empty();
                $( '#paginator' ).unbind();                         
                self.itemListView.render();
            },
            error: function(err) {
                $( "div.failure").html(" Error while searching the item . Contact Administrator.");
                $( "div.failure" ).fadeIn( 300 ).delay( 3500 ).fadeOut( 800 );                      
            }

        });
        $("#item_load_more").show();     
    }
    if(search_item.length == 0)
    {            
        console.log("empty");
        skip = 0;
         this.itemListView.collection.fetch({reset: true, data, processData: true,
            success: function (coll) {
                $( '#itemtable' ).empty();
                $( '#itemtable' ).unbind();
                $( '#paginator' ).empty();
                $( '#paginator' ).unbind();                     
                self.itemListView.render();   
            },       

        }); 
        $("#item_load_more").show();         
    }
},

rendersaveitem:function(){
  var self = this;
 var itemmodel = new admin.ItemModel();
 var itemdata = { item_name:$.trim($('#item_name').val()),
 item_number:$.trim($('#item_number').val()),
 company_id:$.trim($('#item_company').val()),
  product_id:$.trim($('#item_product').val()),
  supplier_id:$.trim($('#item_supplier').val()),
  cost_price:$.trim($('#item_cost_price').val()),
  unit_price:$.trim($('#item_unit_price').val()),
  reorder_level:$.trim($('#item_reorder_level').val()),
  receiving_quantity:$.trim($('#item_receiving_quantity').val()),
  description:$.trim($('#item_description').val()) ,
  tax_1 : $.trim($('#item_tax_name1').val()),
  tax_2 : $.trim($('#item_tax_name2').val()),
  tax_3 : $.trim($('#item_tax_name3').val()),
  commission : $.trim($('#item_commission').val()),
 };
  if( $('#item_tax_name1').val() != '' && $('#item_percent1').val() != ''){                                   
  itemdata.tax1 = {
       "tax_name" : $('#item_tax_name1').val(),
       "percent" : $('#item_percent1').val(),
    };
  }
  if( $('#item_tax_name2').val() != '' && $('#item_percent2').val() != ''){
    itemdata.tax2 = {
       "tax_name" : $('#item_tax_name2').val(),
       "percent" : $('#item_percent2').val(),
    };
  }
    if( $('#item_tax_name3').val() != '' && $('#item_percent3').val() != '' ){
    itemdata.tax3 = {
       "tax_name" : $('#item_tax_name3').val(),
       "percent" : $('#item_percent3').val(),
       "t3error" : 2,
    };
  }
itemmodel.set(itemdata);
if(itemmodel.isValid()){
  itemmodel.save(null,{ 
    success: function () {
      $( "div.success").html("Item Successfully Created.");
        self.render();
        $('div.modal').modal('hide');
        return true;
    },
    error: function (model, error) {
        $( "div.failure").html(error.responseJSON.errors.item_number[0]);
        $( "div.failure").html(error.responseJSON.errors.item_name[0]);
        $( "div.failure" ).fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
        return false;
    }});
}else{
  _.each(itemmodel.validationError, function (error) {
       $('#' + error.name).text(error.message);
    });
return false;
}
 return false;
},

addordeleteSelect:function(e){
  e.preventDefault();
  var self = this;
  var selectedModels = appRouter.itemgrid.getSelectedModels();
  console.log(selectedModels);
  var selectitem = [];
for(i=0;i<selectedModels.length;i++){
  selectitem.push(selectedModels[i].attributes.item_id);
}
    var itemselect= new BackboneBootstrapModals.ConfirmationModal({
          label: self.adddeletelabel,
          text: self.adddeletetext,
          onConfirm: function(e) {
            $('#itemtable').empty();
            $('#itemtable').unbind();
            $( '#paginator' ).empty();
            $( '#paginator' ).unbind(); 
            $.ajax({
              type:'POST',
              url:self.adddeleteurl,
              data:JSON.stringify(selectitem),
              contentType:'application/json',
              cache:false,
              success:function(){
                self.render();
              },
              error:function(){
                return false;
              }
            });
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    itemselect.render();
},

});
