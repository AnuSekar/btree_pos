var admin = admin || {};

admin.CustomerTableView = Backbone.View.extend({

  initialize: function(options) {
      console.log('Initialize customerTableView');
    var self = this;
        this.pageNo = 1;
       limit =2;
        this.recIndex = 1;
        this.options = options;
      console.log(options.mode);
       $('#managementtable').empty();
      $('#managementtable').unbind();
      $( '#paginator' ).empty();
      $( '#paginator' ).unbind(); 
      if(options.mode == 'normal'){
        this.collection = new admin.CustomerCollection();    
      }else if(options.mode == 'trash'){
        this.collection = new admin.CustomerTrashCollection();    
      }
      this.collection.fetch({       
        success:function(data){
          console.log(JSON.stringify(data));
           self.render();
        },
        error:function(data){
        }
      }); 
  },

  render: function() {

    var self = this;
    if(this.options.mode == 'normal'){
        this.edittemplate = '<span style="display:inline-block; width: 20px;"><a id="editcustomer" data-toggle="tooltip" data-placement="top" title="Edit">'+
      '<i class="glyphicon glyphicon-pencil"></i></a></span>'+
      '<span style="display:inline-block; width: 20px;"><a id="deletecustomer" data-toggle="tooltip" data-placement="top" title="Delete">'+
      '<i class="glyphicon glyphicon-minus-sign"></i></a></span>';    
      this.edittext = 'Delete';
      var redempoints = '<span style="display:inline-block; width: 20px;"><a id="redempoint" data-toggle="tooltip" data-placement="top" title="Delete">'+
      '<%=points%></a></span>';
      var detailCell; 
      console.log(detailCell);
     detailCell = Backgrid.Cell.extend({
    template: _.template(redempoints),
    events: {
      "click #redempoint": "redemPoint",
    },
    redemPoint: function (e) {
      console.log("Hello");
      e.preventDefault();
     console.log(this.model);
     var self = this;
     $('#claimModal').modal("show");
     $('#showpoints_error').text(" ");
     $('#showpoints').text(" ");
     $('#showpoints').show();
     $('#claimptsdiv').show();
     $("#submit_claim").show();
     if(this.model.get('points') <= 0 ){
      $('#showpoints_error').text("You have O points. please Purchase item and try again.");
      $('#claimptsdiv').hide();
      $("#submit_claim").hide();
     }else{
      $('#showpoints').text("You have "+this.model.get('points')+" points");
     }
     $("#submit_claim").on("click",function(){
        var claimpts = $('#claimpoints').val();
        if(claimpts == null || claimpts == undefined || claimpts == ""){
          $("#claimpoints_error").text("Please Enter Claim Points");
          return false;
        }else if(self.model.get('points') < claimpts){
          $("#claimpoints_error").text("You have insufficient Points.");
          return false;
        }
        $('#claimpoints').val(" ");
        var person_id = self.model.get('person_id');
        var id = {};
        id.person_id = person_id;
        id.claimpts =claimpts;        
     this.collection = new admin.ClaimPointsReportCollection();
     this.collection.fetch({
      data:id,
      reset: true, 
      success:function(data){
        console.log(data);
        $('#claimModal').modal("hide");
        $('#submit_claim').unbind();
        appRouter.currentView.Search();
      },
      error:function(){
        $('#claimModal').unbind();
      }
     });
       
     });
     $("#close_claim").on("click",function(){
      $('#submit_claim').unbind();
      $('#claimModal').modal("hide");
     });
     },
     render: function () {
      console.log(this.model.get('points'));
      this.$el.html(this.template({points:this.model.get('points')}));
      this.delegateEvents();
      return this;
    }
    });

      }else if(this.options.mode == 'trash'){
        this.edittemplate = '<span style="display:inline-block; width: 20px;"><a id="deletecustomer" data-toggle="tooltip" data-placement="top" title="Delete">'+
      '<i class="glyphicon glyphicon-minus-sign"></i></a></span>';  
      this.edittext = 'Restore';
      }
    var DeleteCell = Backgrid.Cell.extend({
    template: _.template(self.edittemplate),
    events: {
      "click #editcustomer": "editRow",
      "click #deletecustomer": "deleteRow",
    },
    deleteRow: function (e) {
      console.log("Hello");
      e.preventDefault();
      modelvalue = this.model;
      var customermodal= new BackboneBootstrapModals.ConfirmationModal({
          label: "Delete Customer",
          text: "Are You want to Delete this customer?",
          onConfirm: function(e) {
            e.preventDefault();
            var customermodal = modelvalue;
            customermodal.destroy({
                success: function (model, respose, options) {
                    console.log("The model has deleted the server");
                    $('#managementtable').empty();
                    $('#managementtable').unbind();
                    $( '#paginator' ).empty();
                    $( '#paginator' ).unbind(); 
                    appRouter.currentView.Search();
                    return true;
                },
                error: function (model, xhr, options) {
                    console.log("Something went wrong while deleting the model");
                    return false;
                }
            });
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    customermodal.render();
    },
    editRow:function(e){
      var self= this;
       console.log("Hello",this.model);
      e.preventDefault();
      var currentView = new admin.FormPageView({
                template: _.template($('#customerEditFormTpl').html()),
                mode: 'edit',
                model:self.model.attributes
            }); 
    var customermodal= new BackboneBootstrapModals.ConfirmationModal({
          label: "Edit Customer",
          text: currentView.el,
          onConfirm: function(e) {
            console.log("action confirmed");
            return self.rendereditcustomer();
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    customermodal.render();
    },
    rendereditcustomer:function(){
      var self = this;
     var customermodel = new admin.CustomerModel();
     var customerdata = { person_id:self.model.attributes.person_id,
      first_name:$.trim($('#customer_first_name').val()),
  last_name:$.trim($('#customer_last_name').val()),
  phone_number:$.trim($('#customer_phone_number').val()),
  email:$.trim($('#customer_email').val()),
  address_1:$.trim($('#customer_address_1').val()),
  address_2:$.trim($('#customer_address_2').val()),
  city:$.trim($('#customer_city').val()),
  state:$.trim($('#customer_state').val()),
  zip:$.trim($('#customer_zip').val()),
  country:$.trim($('#customer_country').val())
     };
    customermodel.set(customerdata);
    if(customermodel.isValid()){
      customermodel.save(null,{ 
        success: function () {
          $('#managementtable').empty();
          $('#managementtable').unbind();
          $( '#paginator' ).empty();
          $( '#paginator' ).unbind(); 
          appRouter.currentView.Search();
            console.log('success!');
            
        },
        error: function (model, error) {
            console.log(error);
            return false;
        }});
    }else{
      _.each(customermodel.validationError, function (error) {
           $('#' + error.name).text(error.message);
        });
    return false;
    }
     return true;
    },
      
    render: function () {
      this.$el.html(this.template());
      this.delegateEvents();
      return this;
    }

});
    
    var columns = [{
    name: "", 
    cell: "select-row",
    headerCell: "select-all",

  },
    {
    name: "first_name",
    label: "First Name",
     editable: false,
    cell: "string" 
  },
  {
    name: "last_name",
    label: "Last Name",
     editable: false,
    cell: "string" 
  },  
  {
    name: "email",
    label: "Email",
     editable: false,
    cell: "string" 
  }, {
    name: "phone_number",
    label: "Phone Number",
     editable: false,
    cell: "string" 
  }, 
  {
    name: "address_1",
    label: "Address",
     editable: false,
    cell: "string" 
  }, 
  {
        name: "points",
        label: "Points Earned",
        cell: detailCell
    }, 
  {
        name: "delete",
        label: "Action",
        cell: DeleteCell
    }];

var grid = new Backgrid.Grid({
  columns: columns,
  collection: self.collection
});
var paginator = new Backgrid.Extension.Paginator({
        collection: self.collection
      });
$("#managementtable").append(grid.render().el);
$("#paginator").append(paginator.render().$el);
appRouter.customergrid = grid;
            return this;  
  },

    removeoraddAll: function()
  {
      this.collection.reset();
      this.$el.empty();

  },

});
