var admin = admin || {};

admin.CustomerPageView = Backbone.View.extend({
    template1: $( '#customerPageTpl' ).html(),
    template2: $( '#employeePageTpl' ).html(),
    template3: $( '#supplierPageTpl' ).html(),
  initialize: function() {
    appRouter.paginationtype = "";
    this.render();    
  },

  events:{

       'click #create-customer' : 'customerCreate',
       'click #create-employee' : 'employeeCreate',
       'click #create-supplier' : 'supplierCreate',
       'click #deleteall'    :'restoreordeleteAll',
       'click #deleteselect'    :'addordeleteSelect',
       'input #search'  : 'Search',
       'click #restoreall'    :'restoreordeleteAll',
       'click #addselect'    :'addordeleteSelect',
  },

  render: function() {          
      var _self = this;
      if(appRouter.setvalue == 'customer'){
      this.$el.html(this.template1);
      }
      else if(appRouter.setvalue == 'employee'){
      this.$el.html(this.template2);
      }
      else if(appRouter.setvalue == 'supplier'){
      this.$el.html(this.template3);
      }
      $("#switch1").switchbutton({
      checkedLabel: 'YES',
      uncheckedLabel: 'NO',
      classes: 'ui-switchbutton-ios5'
    }).change(function(e){
     var toggledel = $(this).prop("checked") ? "checked" : "unchecked";
      _self.toggle(toggledel);
    });
    
    var toggleval = $("#switch1").prop("checked") ? "checked" : "unchecked";
    this.toggle(toggleval);
    return this;
  },
  toggle:function(toggledel){
  console.log(toggledel);
  if(toggledel == 'checked'){
   
     $('#deleteselect').show();
     $('#deleteall').show();
     $('#search').val("");
     $('#search').show();
     $('#restoreall').hide();
     $('#addselect').hide();

     if(appRouter.setvalue == 'customer'){
      $('#create-customer').show();
      this.ListView = new admin.CustomerTableView({el: $( '#managementtable'),
      mode:'normal'});
      this.adddeletelabel = "Delete Selected Customer";
      this.adddeletetext = "Do You want to Delete Selected customer???";
      this.adddeleteurl = "/btree/admin/delete_selectcustomer";
      this.searchurl = "/btree/admin/search_customer";
      this.restoredeletelabel = "Delete All Customer";
      this.restoredeletetext = "Do You want to Delete all this customer???";
      this.restoredeleteurl = "/btree/admin/delete_allcustomer";
     }
     else if(appRouter.setvalue == 'employee'){
      $('#create-employee').show();
      this.ListView = new admin.EmployeeTableView({el: $( '#managementtable'),
      mode:'normal'});
      this.adddeletelabel = "Delete Selected Employee";
      this.adddeletetext = "Do You want to Delete Selected employee???";
      this.adddeleteurl = "/btree/admin/delete_selectemployee";
      this.searchurl = "/btree/admin/search_employee";
      this.restoredeletelabel = "Delete All Employee";
      this.restoredeletetext = "Do You want to Delete all this employee???";
      this.restoredeleteurl = "/btree/admin/delete_allemployee";
     }
     else if(appRouter.setvalue == 'supplier'){
      $('#create-supplier').show();
      this.ListView = new admin.SupplierTableView({el: $( '#managementtable'),
      mode:'normal'});
      this.adddeletelabel = "Delete Selected Supplier";
      this.adddeletetext = "Do You want to Delete Selected supplier???";
      this.adddeleteurl = "/btree/admin/delete_selectsupplier";
      this.searchurl = "/btree/admin/search_supplier";
      this.restoredeletelabel = "Delete All Supplier";
      this.restoredeletetext = "Do You want to Delete all this supplier???";
      this.restoredeleteurl = "/btree/admin/delete_allsupplier";
     }
     
  }else if(toggledel == 'unchecked'){
     $('#deleteselect').hide();
     $('#deleteall').hide();
     $('#search').show();
     $('#search').val("");
     $('#restoreall').show();
     $('#addselect').show();

     if(appRouter.setvalue == 'customer'){
      this.ListView = new admin.CustomerTableView({el: $( '#managementtable' ),
      mode:'trash'});
      $('#create-customer').hide();
      this.adddeletelabel = "Restore Selected Customer";
      this.adddeletetext = "Do You want to Restore Selected customer???";
      this.adddeleteurl = "/btree/admin/add_selectcustomer";
      this.searchurl = "/btree/admin/search_trashcustomer";
      this.restoredeletelabel = "Restore All Customer";
      this.restoredeletetext = "Do You want to Restore all this customer???";
      this.restoredeleteurl = "/btree/admin/restore_allcustomer";
     }
     else if(appRouter.setvalue == 'employee'){
      this.ListView = new admin.EmployeeTableView({el: $( '#managementtable' ),
      mode:'trash'});
      $('#create-employee').hide();
      this.adddeletelabel = "Restore Selected Employee";
      this.adddeletetext = "Do You want to Restore Selected employee???";
      this.adddeleteurl = "/btree/admin/add_selectemployee";
      this.searchurl = "/btree/admin/search_trashemployee";
      this.restoredeletelabel = "Restore All Employee";
      this.restoredeletetext = "Do You want to Restore all this employee???";
      this.restoredeleteurl = "/btree/admin/restore_allemployee";
     }
     else if(appRouter.setvalue == 'supplier'){
      this.ListView = new admin.SupplierTableView({el: $( '#managementtable' ),
      mode:'trash'});
      $('#create-supplier').hide();
      this.adddeletelabel = "Restore Selected Supplier";
      this.adddeletetext = "Do You want to Restore Selected supplier???";
      this.adddeleteurl = "/btree/admin/add_selectsupplier";
      this.searchurl = "/btree/admin/search_trashsupplier";
      this.restoredeletelabel = "Restore All Supplier";
      this.restoredeletetext = "Do You want to Restore all this supplier???";
      this.restoredeleteurl = "/btree/admin/restore_allsupplier";
     }
     
  }
  },

  customerCreate:function(e){
       e.preventDefault();
        var self = this;
    var currentView = new admin.FormPageView({
                template: _.template($('#customerCreateFormTpl').html()),
            }); 
    var customermodal= new BackboneBootstrapModals.ConfirmationModal({
          label: "Create Customer",
          text: currentView.el,
          onConfirm: function(e) {
            console.log("action confirmed");
            return self.rendersavecustomer();
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    customermodal.render();
  },

  employeeCreate:function(e){
    e.preventDefault();
    var self = this;
    var currentView = new admin.FormPageView({
                template: _.template($('#employeeCreateFormTpl').html()),
                mode: 'create'
            }); 
    var employeemodal= new BackboneBootstrapModals.ConfirmationModal({
          label: "Create Employee",
          text: currentView.el,
          onConfirm: function(e) {
            console.log("action confirmed");
            return self.rendersaveemployee();
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    employeemodal.render();
  },

  supplierCreate:function(e){
    e.preventDefault();
    var self = this;
    var currentView = new admin.FormPageView({
                template: _.template($('#supplierCreateFormTpl').html()),
                mode: 'create'
            }); 
    var suppliermodal= new BackboneBootstrapModals.ConfirmationModal({
          label: "Create Supplier",
          text: currentView.el,
          onConfirm: function(e) {
            console.log("action confirmed");
            return self.rendersavesupplier();
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    suppliermodal.render();
  },

  restoreordeleteAll:function(e)
  {
        e.preventDefault();
        var self = this;
        var select= new BackboneBootstrapModals.ConfirmationModal({
          label: self.restoredeletelabel,
          text: self.restoredeletetext,
          onConfirm: function(e) {
             $.ajax({
                    type: 'POST',
                    url: self.restoredeleteurl,
                     contentType:'application/json',
                    cache:false,
                    success: function(data) {
                      self.ListView.removeoraddAll();
                      $( "div.success").html("All customer are Deleted Successfully.");
                      $( "div.success" ).fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );   
                    },
                    error: function(data) {
                      
                    }
             });
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    select.render();   
  },
  
   Search : function()
   {
    $('#findStatus').html("");
    var search = $('#search').val();
    var data = {};
    self=this;
    if(appRouter.setvalue == 'customer'){
      data.customer_name = search;
    }
    else if(appRouter.setvalue == 'employee'){
      data.employee_name = search;
    }
    else if(appRouter.setvalue == 'supplier'){
      data.supplier_name = search;
    }
  
    if(search.length >=1 )
    {
        this.ListView.collection.fetch({url:this.searchurl,reset: true, data:data, processData: true,
            success: function (coll) {
                $( '#managementtable' ).empty();
                $( '#managementtable' ).unbind(); 
                $( '#paginator' ).empty();
                $( '#paginator' ).unbind();                      
                self.ListView.render();
            },
            error: function(err) {
                $( "div.failure").html(" Error while searching.. Contact Administrator.");
                $( "div.failure" ).fadeIn( 300 ).delay( 3500 ).fadeOut( 800 );                      
            }
        });   
    }
    if(search.length == 0)
    {            
        console.log("empty");
        skip = 0;
         this.ListView.collection.fetch({reset: true, data, processData: true,
            success: function (coll) {
                $( '#managementtable' ).empty();
                $( '#managementtable' ).unbind();
                $( '#paginator' ).empty();
                $( '#paginator' ).unbind();                       
                self.ListView.render();   
            },       
        });        
    }
},

rendersavecustomer:function(){
  var self = this;
 var customermodel = new admin.CustomerModel();
 var customerdata = { first_name:$.trim($('#customer_first_name').val()),
  last_name:$.trim($('#customer_last_name').val()),
  phone_number:$.trim($('#customer_phone_number').val()),
  email:$.trim($('#customer_email').val()),
  address_1:$.trim($('#customer_address_1').val()),
  address_2:$.trim($('#customer_address_2').val()),
  city:$.trim($('#customer_city').val()),
  state:$.trim($('#customer_state').val()),
  zip:$.trim($('#customer_zip').val()),
  country:$.trim($('#customer_country').val())
 };
customermodel.set(customerdata);
if(customermodel.isValid()){
  customermodel.save(null,{ 
    success: function () {
      $( "div.success").html("Customer Successfully Created.");
        $( "div.success" ).fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );   
        self.render();
        $('div.modal').modal('hide');
         return true;
    },
    error: function (model, error) {
        $( "div.failure").html(error.responseJSON.errors.phone_number[0]);
        $( "div.failure").html(error.responseJSON.errors.email[0]);
        $( "div.failure").html(error.responseJSON.errors.address_1[0]);
        $( "div.failure" ).fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
        return false;
    }});
}else{
  _.each(customermodel.validationError, function (error) {
       $('#' + error.name).text(error.message);
    });
return false;
}
 return false;
},

rendersaveemployee:function(){
  var self = this;
 var employeemodel = new admin.EmployeeModel();
 var employeedata = { 
  first_name:$.trim($('#employee_first_name').val()),
  last_name:$.trim($('#employee_last_name').val()),
  username:$.trim($('#employee_username').val()),
  password:$.trim($('#employee_password').val()),
  phone_number:$.trim($('#employee_phone_number').val()),
  email:$.trim($('#employee_email').val()),
  address_1:$.trim($('#employee_address_1').val()),
  address_2:$.trim($('#employee_address_2').val()),
  city:$.trim($('#employee_city').val()),
  state:$.trim($('#employee_state').val()),
  zip:$.trim($('#employee_zip').val()),
  country:$.trim($('#employee_country').val())
 };
employeemodel.set(employeedata);
if(employeemodel.isValid()){
  employeemodel.save(null,{ 
    success: function () {
        console.log('success!');
        $( "div.success").html("Employee Successfully Created.");
        self.render();
        $('div.modal').modal('hide');
        return true;
    },
    error: function (model, error) {
        $( "div.failure").html(error.responseJSON.errors.phone_number[0]);
        $( "div.failure").html(error.responseJSON.errors.email[0]);
        $( "div.failure").html(error.responseJSON.errors.username[0]);
        $( "div.failure").html(error.responseJSON.errors.password[0]);
        $( "div.failure").html(error.responseJSON.errors.address_1[0]);
        $( "div.failure" ).fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
        return false; 
    }});
}else{
  _.each(employeemodel.validationError, function (error) {
       $('#' + error.name).text(error.message);
    });
return false;
}
 return false;
},

rendersavesupplier:function(){
  var self = this;
 var suppliermodel = new admin.SupplierModel();
 var supplierdata = { first_name:$.trim($('#supplier_first_name').val()),
  last_name:$.trim($('#supplier_last_name').val()),
  phone_number:$.trim($('#supplier_phone_number').val()),
  email:$.trim($('#supplier_email').val()),
  address_1:$.trim($('#supplier_address_1').val()),
  address_2:$.trim($('#supplier_address_2').val()),
  city:$.trim($('#supplier_city').val()),
  state:$.trim($('#supplier_state').val()),
  zip:$.trim($('#supplier_zip').val()),
  country:$.trim($('#supplier_country').val()),
  company_name:$.trim($('#supplier_company_name').val()),
  agency_name:$.trim($('#supplier_agency_name').val()),
  account_number:$.trim($('#supplier_account_number').val())

 };
suppliermodel.set(supplierdata);
if(suppliermodel.isValid()){
  suppliermodel.save(null,{ 
    success: function () {
      $( "div.success").html("Supplier Successfully Created.");
        self.render();
        $('div.modal').modal('hide');
        return true;
    },
    error: function (model, error) {
        $( "div.failure").html(error.responseJSON.errors.phone_number[0]);
        $( "div.failure").html(error.responseJSON.errors.email[0]);
        $( "div.failure").html(error.responseJSON.errors.address_1[0]);
        $( "div.failure" ).fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
        return false; 
    }});
}else{
  _.each(suppliermodel.validationError, function (error) {
       $('#' + error.name).text(error.message);
    });
return false;
}
 return false;
},

addordeleteSelect:function(e){
  e.preventDefault();
  var self = this;
  if(appRouter.setvalue == 'customer'){
    var selectedModels = appRouter.customergrid.getSelectedModels();
    var selectmanage = [];
    for(i=0;i<selectedModels.length;i++){
      selectmanage.push(selectedModels[i].attributes.person_id);
      }
  }
  else if(appRouter.setvalue == 'employee'){
    var selectedModels = appRouter.employeegrid.getSelectedModels();
    var selectmanage = [];
    for(i=0;i<selectedModels.length;i++){
      selectmanage.push(selectedModels[i].attributes.person_id);
      }
  }
  else if(appRouter.setvalue == 'supplier'){
    var selectedModels = appRouter.suppliergrid.getSelectedModels();
    var selectmanage = [];
    for(i=0;i<selectedModels.length;i++){
      selectmanage.push(selectedModels[i].attributes.person_id);
      }
  }
    var manageselect= new BackboneBootstrapModals.ConfirmationModal({
          label: self.adddeletelabel,
          text: self.adddeletetext,
          onConfirm: function(e) {
            $('#managementtable').empty();
            $('#managementtable').unbind();
            $( '#paginator' ).empty();
            $( '#paginator' ).unbind(); 
            $.ajax({
              type:'POST',
              url:self.adddeleteurl,
              data:JSON.stringify(selectmanage),
              contentType:'application/json',
              cache:false,
              success:function(){
                self.render();
              },
              error:function(){
                return false;
              }
            });
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    manageselect.render();
},
});
