var admin = admin || {};
 
admin.CompanyPageView = Backbone.View.extend({
    template1: $( '#companyPageTpl' ).html(),
    template2: $( '#productPageTpl' ).html(),
  initialize: function(options) {
    appRouter.paginationtype = "";
    this.render();    
  }, 

  events:{
       'click #create_company' : 'companyCreate',
       'click #create_product' : 'productCreate',
       'click #deleteall'    :'restoreordeleteAll',
       'click #deleteselect'    :'addordeleteSelect',
       'input #search'  : 'Search', 
       'click #restoreall'    :'restoreordeleteAll',
       'click #addselect'    :'addordeleteSelect',
  },

  render: function() {  
    var _self = this;
    console.log(appRouter.setvalue);
    if(appRouter.setvalue == 'company'){
      this.$el.html(this.template1);
    }
    else if(appRouter.setvalue == 'product'){
      this.$el.html(this.template2);
    }
      
      $("#switch1").switchbutton({
      checkedLabel: 'YES',
      uncheckedLabel: 'NO',
      classes: 'ui-switchbutton-ios5'
    }).change(function(e){
     var toggledel = $(this).prop("checked") ? "checked" : "unchecked";
      _self.toggle(toggledel);
    });
    
    var toggleval = $("#switch1").prop("checked") ? "checked" : "unchecked";
    this.toggle(toggleval);
    return this;
  },
  toggle:function(toggledel){
  console.log(toggledel);
  if(toggledel == 'checked'){
     $('#deleteselect').show();
     $('#deleteall').show();
     $('#restoreall').hide();
     $('#addselect').hide();
     $('#search').show();
     $('#search').val("");

     if(appRouter.setvalue == 'company'){
      $('#create_company').show();
      
      this.ListView = new admin.CompanyTableView({el: $( '#com_protable'),
      mode:'normal'});
      this.adddeletelabel = "Delete Selected Company";
      this.adddeletetext = "Do You want to Delete Selected company???";
      this.adddeleteurl = "/btree/admin/delete_selectcompany";
      this.searchurl = "/btree/admin/search_company";
      this.restoredeletelabel = "Delete All Company";
      this.restoredeletetext = "Do You want to Delete all this company???";
      this.restoredeleteurl = "/btree/admin/delete_allcompany";
     }
     else if(appRouter.setvalue == 'product'){
      $('#create_product').show();
      this.ListView = new admin.ProductTableView({el: $( '#com_protable'),
      mode:'normal'});
      this.adddeletelabel = "Delete Selected Product";
      this.adddeletetext = "Do You want to Delete Selected product???";
      this.adddeleteurl = "/btree/admin/delete_selectproduct";
      this.searchurl = "/btree/admin/search_product";
      this.restoredeletelabel = "Delete All Product";
      this.restoredeletetext = "Do You want to Delete all this product???";
      this.restoredeleteurl = "/btree/admin/delete_allproduct";
     }
  }else if(toggledel == 'unchecked'){
     $('#deleteselect').hide();
     $('#deleteall').hide();
     $('#restoreall').show();
     $('#addselect').show();
     $('#search').show();
     $('#search').val("");

     if (appRouter.setvalue == 'company'){
      this.ListView = new admin.CompanyTableView({el: $( '#company-list' ),
      mode:'trash'});
      $('#create_company').hide();
      this.adddeletelabel = "Restore Selected Company";
      this.adddeletetext = "Do You want to Restore Selected company???";
      this.adddeleteurl = "/btree/admin/add_selectcompany";
      this.searchurl = "/btree/admin/search_trashcompany";
      this.restoredeletelabel = "Restore All Company";
      this.restoredeletetext = "Do You want to Restore all this company???";
      this.restoredeleteurl = "/btree/admin/restore_allcompany";
     }
     else if(appRouter.setvalue == 'product'){
      this.ListView = new admin.ProductTableView({el: $( '#producttable' ),
      mode:'trash'});
      $('#create_product').hide();
      this.adddeletelabel = "Restore Selected Product";
      this.adddeletetext = "Do You want to Restore Selected product???";
      this.adddeleteurl = "/btree/admin/add_selectproduct";
      this.searchurl = "/btree/admin/search_trashproduct";
      this.restoredeletelabel = "Restore All Product";
      this.restoredeletetext = "Do You want to Restore all this product???";
      this.restoredeleteurl = "/btree/admin/restore_allproduct";
     }
  }
  },
  companyCreate:function(e){
        e.preventDefault();
        var self = this;
    var currentView = new admin.FormPageView({
                template: _.template($('#companyCreateFormTpl').html()),
                mode: 'create'
            }); 
    var companymodal= new BackboneBootstrapModals.ConfirmationModal({
          label: "Create Company",
          text: currentView.el,
          onConfirm: function(e) {
            console.log("action confirmed");
            return self.rendersavecompany();
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    companymodal.render();
  },

  productCreate:function(e){
        e.preventDefault();
        var self = this;
    var currentView = new admin.FormPageView({
                template: _.template($('#productCreateFormTpl').html()),
                mode: 'create'
            }); 
    var productmodal= new BackboneBootstrapModals.ConfirmationModal({
          label: "Create Product",
          text: currentView.el,
          onConfirm: function(e) {
            console.log("action confirmed");
            return self.rendersaveproduct();
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    productmodal.render();
  },


  restoreordeleteAll:function(e)
  {
        e.preventDefault();
        var self = this;
          var select= new BackboneBootstrapModals.ConfirmationModal({
          label: self.restoredeletelabel,
          text: self.restoredeletetext,
          onConfirm: function(e) {
             $.ajax({
                    type: 'POST',
                    url: self.restoredeleteurl,
                     contentType:'application/json',
                    cache:false,
                    success: function(data) {
                      self.ListView.removeoraddAll();
                      $( "div.success").html("All company are Deleted Successfully.");
                      $( "div.success" ).fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );   
                    },
                    error: function(data) {
                      
                    }
             });
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
          select.render();
  },
  
  Search:function () {

    $('#findStatus').html("");
    var search = $('#search').val();
    var data = {};
    self=this;
    if(appRouter.setvalue == 'company'){
      data.company_name = search;
    }
    else if(appRouter.setvalue == 'product'){
      data.product_name = search;
    }
  
    if(search.length >=1 )
    {
        this.ListView.collection.fetch({url:self.searchurl, reset: true, data:data, processData: true,
            success: function (coll) {
                $( '#com_protable' ).empty();
                $( '#com_protable' ).unbind();
                $( '#paginator' ).empty();
                $( '#paginator' ).unbind();                         
                self.ListView.render();
            },
            error: function(err) {
                $( "div.failure").html(" Error while searching the company . Contact Administrator.");
                $( "div.failure" ).fadeIn( 300 ).delay( 3500 ).fadeOut( 800 );                      
            }

        });
    }
    if(search.length == 0)
    {            
        console.log("empty");
        skip = 0;
         this.ListView.collection.fetch({reset: true, data, processData: true,
            success: function (coll) {
                $( '#com_protable' ).empty();
                $( '#com_protable' ).unbind();
                $( '#paginator' ).empty();
                $( '#paginator' ).unbind();                         
                self.ListView.render();   
            },       

        }); 
    }
},

rendersavecompany:function(){
  var self = this;
 var companymodel = new admin.CompanyModel();
 var companydata = { company_name:$.trim($('#company_name').val()),
  company_description:$.trim($('#company_description').val())
 };
companymodel.set(companydata);
if(companymodel.isValid()){
  companymodel.save(null,{ 
    success: function () {
        console.log('success!');
        $( "div.success").html("Company Successfully Created.");
        $( "div.success" ).fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );   
        self.render();
        $('div.modal').modal('hide');
         return true;
    },
    error: function (model, error) {
        $( "div.failure").html(error.responseJSON.errors.company_name[0]);
        $( "div.failure" ).fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );   
        return false;
    }});
}else{
  _.each(companymodel.validationError, function (error) {
       $('#' + error.name).text(error.message);
    });
return false;
}
return false;
},

rendersaveproduct:function(){
  var self = this;
 var productmodel = new admin.ProductModel();
 var productdata = { product_name:$.trim($('#product_name').val()),
  product_description:$.trim($('#product_description').val())
 };
productmodel.set(productdata);
if(productmodel.isValid()){
  productmodel.save(null,{ 
    success: function () {
        console.log('success!');
        $( "div.success").html("Product Successfully Created.");
        $( "div.success" ).fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
        self.render();
        $('div.modal').modal('hide');
        return true;
    },
    error: function (model, error) {
        $( "div.failure").html(error.responseJSON.errors.product_name[0]);
        $( "div.failure" ).fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );   
        return false;
    }});
}else{
  _.each(productmodel.validationError, function (error) {
       $('#' + error.name).text(error.message);
    });
return false;
}
 return false;
},

addordeleteSelect:function(e){
  e.preventDefault();
  var self = this;
  if(appRouter.setvalue == 'company'){
    var selectedModels = appRouter.companygrid.getSelectedModels();
    var selectcompro = [];
    for(i=0;i<selectedModels.length;i++){
    selectcompro.push(selectedModels[i].attributes.company_id);
    } 
  }
    else if(appRouter.setvalue == 'product'){
      var selectedModels = appRouter.productgrid.getSelectedModels();
      var selectcompro = [];
      for(i=0;i<selectedModels.length;i++){
      selectcompro.push(selectedModels[i].attributes.product_id);
    }
  }
    var comproselect= new BackboneBootstrapModals.ConfirmationModal({
          label: self.adddeletelabel,
          text: self.adddeletetext,
          onConfirm: function(e) {
            $('#com_protable').empty();
            $('#com_protable').unbind();
            $( '#paginator' ).empty();
            $( '#paginator' ).unbind(); 
            $.ajax({
              type:'POST',
              url:self.adddeleteurl,
              data:JSON.stringify(selectcompro),
              contentType:'application/json',
              cache:false,
              success:function(){
                self.render();
              },
              error:function(){
                return false;
              }
            });
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    comproselect.render();
}
}); 
