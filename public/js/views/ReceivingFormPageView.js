var admin = admin || {};
admin.ReceivingFormPageView = Backbone.View.extend({
      template : $('#receiving').html(),
    
    initialize: function(options) {
      appRouter.paginationtype = "";
           $('.popover-content').hide();
           this.itemsearch = new admin.SalesItemCollection();
           this.suppliercol= new Backbone.Collection();
           this.selecteditem = [];
           this.render();
         
  },
  events:{
    'input #item_name'  : 'ItemNameSearch',
    'click #remove_all_item' : 'removeAllSales',
    'input #supplier_name' : 'renderSupplierName',
    'click #delete_item' : 'renderDeleteItem',
    'click #add_receiving' : 'renderAddReceiving'
   },
render:function(){
  console.log("inside render");
  this.$el.html(this.template);
  $('#add_receiving').attr('disabled','disabled');
  $('#remove_all_item').attr('disabled','disabled');
  return this;
},

ItemNameSearch:function(){
          var self = this;
          $('#add_receiving').removeAttr('disabled');
          $('#remove_all_item').removeAttr('disabled');
          var searchdata = $('#item_name').val();
          var data = {};
          var indexitem =-2;
          if(searchdata.length > 0){
          data.searchitem = searchdata;
          $.when(
            this.itemsearch.fetch({
            url:"/btree/admin/get_salesitem_list",
            reset:true,
            data:data,
            contentType: "application/json",
         })
          ).done(function(){
          var itemNames = self.itemsearch.map(function (model) {
          return _.pick(model.toJSON(), ["item_name", "item_number","label","item_id"]);
            });
          console.log(itemNames);
            $("#item_name").autocomplete({ 
            source : itemNames,
            minLength : 0,
            select: function(event, ui){ 
              var selectedModel =self.itemsearch.where({item_name: ui.item.value})[0];
              console.log("inside",selectedModel);
              this.value='';
               indexitem = self.selecteditem.indexOf(ui.item.item_id);
              if(indexitem == -1){
                self.selecteditem.push(ui.item.item_id);
               this.receivingitemadd = new admin.ReceivingAddItemPageView({
                el: $( '#receiving_item_list' ),
                template: _.template( $( '#receivingitemTemplate' ).html() ),
                model:selectedModel,
              });
              }else{
              self.renderFocusItem(ui.item.item_id);
              }
             
               return false;
            }
          })
       .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
        
          return $( "<li>" )
            .append( "<a>" + item.item_name + 
                "<br><span style='font-size: 80%;'>Desc: " + item.item_number + "</span></a>")
            .appendTo( ul );
    };
        })
    }      
},
  removeAllSales:function(){
    $('#add_receiving').attr('disabled','disabled');
    $('#receiving_item_list').empty();
    $('#receiving_total').val("");
    $('#receiving_sub_total').val("");
    $('#remove_all_item').attr('disabled','disabled');
  },

renderSupplierName:function(){
          var self = this;
          var searchdata = $('#supplier_name').val();
          var data = {};
          if(searchdata.length > 0){
          data.searchsupplier = searchdata;
          $.when(
            this.suppliercol.fetch({
            url:"/btree/admin/get_supplier_list",
            reset:true,
            data:data,
            contentType: "application/json",
         })
          ).done(function(){
            console.log(self.suppliercol);
          var supplierNames = self.suppliercol.map(function (model) {
          return _.pick(model.toJSON(), ["first_name", "last_name","label"]);
            });
            $("#supplier_name").autocomplete({ 
            source : supplierNames,
            minLength : 0,
            select: function(event, ui){ 
              var selectedModel =self.suppliercol.where({first_name: ui.item.value})[0];
              console.log(selectedModel);
              $('#person_sup_id').val(selectedModel.attributes.person_id);
            }
          })
       .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
        console.log(item);
          return $( "<li>" )
            .append( "<a>" + item.first_name + " " + item.last_name + "</a>")
            .appendTo( ul );
    };
        })
    }      
},

renderDeleteItem:function(e){
  console.log($(e.target).attr('val'));
 var id =  $(e.target).attr('val');
 $('#'+id).remove();
 indexitem = this.selecteditem.indexOf(id);
 this.selecteditem.splice(indexitem,1);
 console.log(this.selecteditem);
 if($('#sales_item_list tr').length==0){
    $('#add_receiving').attr('disabled','disabled');
    $('#sales_amount_due').val("");
    $('#sales_total').val("");
    $('#sales_sub_total').val("");
    $('#remove_all_item').attr('disabled','disabled');
 }
},
renderAddReceiving:function(){
  console.log('inside');
  var itemarray = [];
  var receivingarray = {};
  var item = {};
  $('#receiving_item_list').find('tr').each(function(){
     item.id =  $(this).attr('id');
     item.discount = $(this).find('td #discount').val();
     item.quantity =  $(this).find('td #quantity').val();
    itemarray.push({"item_id":item.id,
   "item_discount":item.discount,
   "item_quantity":item.quantity
   });
  });
  receivingarray.item = itemarray;
 // receivingarray.supplier= $('#person_sup_id').val();
 receivingarray.supplier= '1';
  receivingarray.payment_type = $('#paymenttype').val();
  receivingarray.paymentamount = $('#receiving_total').val();
  receivingarray.receivings_type = $('#receivings_type').val();
  console.log(JSON.stringify(receivingarray));
  if( $('#receivings_type').val() ==1){
    $.ajax({
    url:"/btree/admin/create_receivingitem",
    type:"POST",
    data:JSON.stringify(receivingarray),
    contentType:'json',
    cache:false,
    success:function(data){
      console.log(data);
    },
    error:function(data){

    }
  });
  }else if($('#receivings_type').val() == 2){
    $.ajax({
    url:"/btree/admin/return_receivingitem",
    type:"POST",
    data:JSON.stringify(receivingarray),
    contentType:'json',
    cache:false,
    success:function(data){
      console.log(data);
    },
    error:function(data){

    }
  });
  }
  
  
},
renderFocusItem:function(itemvalue){
console.log(this.selecteditem);
$('#receiving_item_list #'+itemvalue).find('#quantity').focus();
}

});