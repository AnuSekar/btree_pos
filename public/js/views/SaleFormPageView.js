var admin = admin || {};

admin.SaleFormPageView = Backbone.View.extend({
      template : $('#sales').html(),
    
    initialize: function(options) {
      appRouter.paginationtype = "";
      this.mode = options.mode;
      this.id = options.id;
           $('.popover-content').hide();
           this.itemsearch = new admin.SalesItemCollection();
           this.customercol= new Backbone.Collection();
           this.selecteditem = [];
           this.suspendedsalesitem = new admin.SuspendedSalesRenderItemCollection();
           appRouter.sales_item = new admin.SalesItemCollection();
           appRouter.sales_tax = {};
          this.render();
           
         
  },
  events:{
    'input #item_name'  : 'ItemNameSearch',
    'click #remove_all_sales' : 'removeAllSales',
    'click #discount_percent' : 'DiscountPercent',
    'click #discount_amount' : 'DiscountAmount',
    'input #customer_name' : 'renderCustomerName',
    'click #sales_amount_due' : 'DiscountAmount',
    'click #delete_item' : 'renderDeleteItem',
    'click #add_sales' : 'renderAddSales',
   'click #suspended_sales' :'renderAddSuspendedSales',
   'click #list_suspended_sales' :'renderListSuspendedSales',
   
   },
render:function(){
  console.log("inside render");
  this.$el.html(this.template);
  $('#add_sales').attr('disabled','disabled');
  $('#suspended_sales').attr('disabled','disabled');
  $('#remove_all_sales').attr('disabled','disabled');
 if(this.mode == 'suspendedsales'){
     $('#list_suspended_sales').hide();
    this.suspendedrender();
 }
  return this;
},

ItemNameSearch:function(){
          var self = this;
          $('#add_sales').removeAttr('disabled');
          $('#suspended_sales').removeAttr('disabled');
          $('#remove_all_sales').removeAttr('disabled');
          var searchdata = $('#item_name').val();
          var data = {};
          var indexitem =-2;
          if(searchdata.length > 0){
          data.searchitem = searchdata;
          $.when(
            this.itemsearch.fetch({
            url:"/btree/admin/get_salesitem_list",
            reset:true,
            data:data,
            contentType: "application/json",
         })
          ).done(function(){
          var itemNames = self.itemsearch.map(function (model) {
          return _.pick(model.toJSON(), ["item_name", "item_number","label","id"]);
            });
          console.log(itemNames);
            $("#item_name").autocomplete({ 
            source : itemNames,
            minLength : 0,
            select: function(event, ui){ 
              selectedModel =self.itemsearch.where({label: ui.item.value});
              
              console.log("inside",selectedModel);
              selectedModel1 = new admin.SalesItemCollection(selectedModel);
              this.value='';
              i=0;
              selectedModel1.each(function(item){
                console.log("inside",item);
                  indexitem = self.selecteditem.indexOf(item.get('item_id'));
                  if(indexitem == -1){
                    self.selecteditem.push(item.get('item_id'));
                    appRouter.sales_item.add(self.itemsearch.where({label: ui.item.value}));
                   this.salesitemadd = new admin.SaleAddItemPageView({
                    el: $( '#sales_item_list' ),
                    template: _.template( $( '#saleitemTemplate' ).html() ),
                   model:item,
                    mode:"sales"
                  });
                 }else{
                  self.renderFocusItem(item.get('item_id'));
                  }
                  i++;
              });
              
              
               return false;
            }
          })
       .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
          return $( "<li>" )
            .append( "<a>" + item.label + 
                "<br><span style='font-size: 80%;'>Desc: " + item.item_number + "</span></a>")
            .appendTo( ul );
    };
        })
    }      
},
  removeAllSales:function(){
  	this.selecteditem = [];
    $('#add_sales').attr('disabled','disabled');
    $('#sales_item_list').empty();
    $('#sales_amount_due').val("");
    $('#sales_total').val("");
   $('#sales_sub_total').val("");
    $('#discount_amount').val("");
    $('#discount_percent').val("");
    $('#tax_amount_list').text("");
    $('#suspended_sales').attr('disabled','disabled');
    $('#remove_all_sales').attr('disabled','disabled');
  },
  DiscountPercent:function(e){
    console.log($(e.target).val());
    var disp = parseFloat($(e.target).val());
    if(!isNaN(disp)){
    if(confirm("Added Discount to Customer?")){
   var sst = 0;
      $('#sales_item_list').find('tr #price_item').each(function(){
        sst = sst + parseFloat($(this).text());
      });
    var disto = sst * (disp/100);
    var dpst = sst - disto;
    $('#sales_total').val(dpst);
    }
  }
  },
  DiscountAmount:function(e){
    var disp = parseFloat($(e.target).val());
    if(!isNaN(disp)){
    if(confirm("Added Discount to Customer?")){
    var sst = 0;
      $('#sales_item_list').find('tr #price_item').each(function(){
        sst = sst + parseFloat($(this).text());
      });
    var dpst = sst - disp;
    $('#sales_total').val(dpst);
  }
}
},
renderCustomerName:function(){
          var self = this;
          var searchdata = $('#customer_name').val();
          var data = {};
          if(searchdata.length > 0){
          data.searchcustomer = searchdata;
          $.when(
            this.customercol.fetch({
            url:"/btree/admin/get_customerbysales",
            reset:true,
            data:data,
            contentType: "application/json",
         })
          ).done(function(){
            console.log(self.customercol);
          var customerNames = self.customercol.map(function (model) {
          return _.pick(model.toJSON(), ["first_name", "last_name","label"]);
            });
          console.log(customerNames);
            $("#customer_name").autocomplete({ 
            source : customerNames,
            minLength : 0,
            select: function(event, ui){ 
              var selected =self.customercol.where({first_name: ui.item.value})[0];
              console.log(selected);
              $('#person_cust_id').val(selected.attributes.person_id);
            }
          })
       .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
        console.log(item);
          return $( "<li>" )
            .append( "<a>" + item.first_name + " " + item.last_name + "</a>")
            .appendTo( ul );
    };
        })
    }      
},

renderDeleteItem:function(e){
  console.log($(e.target).attr('val'));
 var id =  $(e.target).attr('val');
 $('#'+id).remove();
 indexitem = this.selecteditem.indexOf(id);
 this.selecteditem.splice(indexitem,1);
 console.log(this.selecteditem);
 if($('#sales_item_list tr').length==0){
    $('#add_sales').attr('disabled','disabled');
    $('#sales_amount_due').val("");
    $('#sales_total').val("");
    $('#sales_sub_total').val("");
    $('#discount_amount').val("");
    $('#discount_percent').val("");
    $('#suspended_sales').attr('disabled','disabled');
    $('#remove_all_sales').attr('disabled','disabled');
 }
},
renderAddSales:function(){
  console.log('inside');
  var itemarray = [];
  var salesarray = {};
  var item = {};
  var self = this;
  $('#sales_item_list').find('tr').each(function(){
     item.id =  $(this).attr('id');
     item.discount = $(this).find('td #discount').val();
     item.quantity =  $(this).find('td #quantity').val();
    itemarray.push({"item_id":item.id,
  "item_discount":item.discount,
   "item_quantity":item.quantity
   });
  });
  salesarray.item = itemarray;
  salesarray.customer= $('#person_cust_id').val();
  salesarray.paymenttype = $('#paymenttype').val();
  salesarray.paymentamount = $('#sales_total').val();
  salesarray.sales_type = $('#sales_type').val();
  salesarray.sales_tax = appRouter.sales_tax;
  console.log(JSON.stringify(salesarray));
  if($('#sales_type').val() == 1){
    $.ajax({
    url:"/btree/admin/create_salesitem",
    type:"POST",
    data:JSON.stringify(salesarray),
    contentType:'json',
    cache:false,
    success:function(data){
      console.log(data);
      self.removeAllSales();
    },
    error:function(data){

    }
     });
   }else if($('#sales_type').val() == 2){
     $.ajax({
    url:"/btree/admin/return_salesitem",
    type:"POST",
    data:JSON.stringify(salesarray),
    contentType:'json',
    cache:false,
    success:function(data){
      console.log(data);
    },
    error:function(data){

    }
     });
   }
 
},
renderAddSuspendedSales:function(){
  console.log('inside');
  var itemarray = [];
  var salesarray = {};
  var item = {};
  var self = this;
  $('#sales_item_list').find('tr').each(function(){
     item.id =  $(this).attr('id');
     item.discount = $(this).find('td #discount').val();
     item.quantity =  $(this).find('td #quantity').val();
   itemarray.push({"item_id":item.id,
   "item_discount":item.discount,
   "item_quantity":item.quantity
   });
  });
  salesarray.item = itemarray;
  salesarray.customer= $('#person_cust_id').val();
  salesarray.paymenttype = $('#paymenttype').val();
  salesarray.paymentamount = $('#sales_total').val();
  salesarray.sales_type = $('#sales_type').val();
  salesarray.sales_tax = appRouter.sales_tax;
  if(this.mode == 'suspendedsales'){
    salesarray.update = 'updatesales';
    salesarray.salesid = this.id;
  }
  console.log(JSON.stringify(salesarray));
    $.ajax({
    url:"/btree/admin/create_suspendeditem",
    type:"POST",
    data:JSON.stringify(salesarray),
    contentType:'json',
    cache:false,
    success:function(data){
      console.log(data);
      self.removeAllSales();
    }, 
    error:function(data){

    }
     });
 
},
renderFocusItem:function(itemvalue){
console.log(this.selecteditem);
$('#sales_item_list #'+itemvalue).find('#quantity').focus();
},

renderListSuspendedSales:function(e){
  e.preventDefault();
$('#salesModal').modal("show");
var tablehtml = '<table class="table"><thead><tr><th>Sales Id</th><th>Customer Name</th><th>Date</th><th>Action</th></tr></thead><tbody>';
this.collection = new admin.SuspendedSalesItemCollection();
     this.collection.fetch({
      success:function(data){
        console.log(data);
        data.each(function(model){
          if(model.get("customer_name") == null){
            model.set({customer_name:'-'});
          }
          tablehtml+= '<tr><td>'+ model.get("sales_id") +'</td><td>' + model.get("customer_name") +  '</td><td>' + model.get("date") + '</td><td><a href="#suspendedsales/'+model.get("sales_id")+'">Unsuspended</a></td></tr>';
          
        });
        tablehtml += '</tbody></table>';
          $('#listsales').html(tablehtml);

      }
      });
},

suspendedrender:function(){
  $('#add_sales').removeAttr('disabled');
  $('#suspended_sales').removeAttr('disabled');
  var data = {};
  var self = this;
  data.id = this.id;
this.suspendedsalesitem.fetch({
  data:data,
  success:function(data){
    console.log(data);
    data.each(function(item){
      appRouter.sales_item.add(item);
      self.selecteditem.push(item.get('item_id'));
         this.salesitemadd = new admin.SaleAddItemPageView({
          el: $( '#sales_item_list' ),
          template: _.template( $( '#saleitemTemplate' ).html() ),
          model:item,
          mode:"suspendedsales"
        });  
    });
    console.log(this.selecteditem);
  }
});

}

});
