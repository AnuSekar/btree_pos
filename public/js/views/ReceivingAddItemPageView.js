var admin = admin || {};

admin.ReceivingAddItemPageView = Backbone.View.extend({

      initialize:function(options){
        this.template = options.template;
        this.model = options.model;
        this.price = 0;
        this.render();
        this.renderSubTotal();
      },
      events:{
        'input #quantity' : 'renderQuantity',
        'input #discount' : 'renderDiscount',
        'input #price' : 'renderPrice'
      },

      render: function() {

        console.log(this.model.attributes);
        var tax =  parseFloat(this.model.attributes.percent);
        var cost_price =  parseFloat(this.model.attributes.cost_price);
        var taxamount = (tax/100)*cost_price;
        var price =  cost_price + taxamount ; 
        console.log(price);
        this.model.set({total_price:price.toFixed(3)});
        console.log(this.model);
        this.$el.append(this.template(this.model.toJSON()));
        this.price
        
        //this.$el.find('#price_item').append(price.toFixed(3));
        return this;
      },

      renderQuantity:function(e){
        console.log($(e.target).val());
        quantity = parseFloat($(e.target).val());
        var iid = parseInt($(e.target).attr('iid'));
        var price = parseFloat($('#'+iid).find('#price').val());
        var discount = parseFloat($('#'+iid).find('#discount').val());
        var tax = parseFloat($('#'+iid).find('#tax').text());
        console.log(tax,quantity,iid,discount,price);
        this.renderTotalCalculation(iid,price,tax,quantity,discount);

      },

      renderDiscount:function(e){
        console.log($(e.target).val());
        discount = parseFloat($(e.target).val());
        var iid = parseInt($(e.target).attr('iid'));
        var price = parseFloat($('#'+iid).find('#price').val());
        var  quantity= parseFloat($('#'+iid).find('#quantity').val());
        var tax = parseFloat($('#'+iid).find('#tax').text());
        this.renderTotalCalculation(iid,price,tax,quantity,discount);
        

      },

      renderPrice:function(e){
        console.log($(e.target).val());
        price = parseFloat($(e.target).val());
        var iid = parseInt($(e.target).attr('iid'));
        var quantity = parseFloat($('#'+iid).find('#quantity').val());
        var discount = parseFloat($('#'+iid).find('#discount').val());
        var tax = parseFloat($('#'+iid).find('#tax').text());
        this.renderTotalCalculation(iid,price,tax,quantity,discount);
        
        

      },
    renderTotalCalculation:function(iid,price,tax,quantity,discount){
        if(isNaN(discount)){
         discount = 0;
        }

        var taxtotal = (tax/100)+price;
        var itemprice = taxtotal + price;
        var  totalprice = itemprice * quantity;
        totalprice = totalprice-discount;
        console.log(tax,quantity,iid,discount,price);
        console.log(totalprice.toFixed(3));

        $('#'+iid).find('#price_item').text(totalprice.toFixed(3));
        this.renderSubTotal();
        

    },
    renderSubTotal:function(){
      $('#receiving_sub_total').val("");
      var sst = 0;
      $('#receiving_item_list tr').find('#price_item').each(function(){
        sst = sst + parseFloat($(this).text());
      });
        console.log(sst);
          $('#receiving_sub_total').val(sst);
          this.renderTotal();
    },
    renderTotal:function(){
      $('#receiving_total').val("");
      var st = 0;
      $('#receiving_item_list tr').find('#price_item').each(function(){
        st = st + parseFloat($(this).text());
      });
        console.log(st);
          $('#receiving_total').val(st);
    },


});
