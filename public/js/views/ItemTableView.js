var admin = admin || {};

admin.ItemTableView = Backbone.View.extend({

  initialize: function(options) {
      console.log('Initialize itemTableView');
    var self = this;
        this.pageNo = 1;
       limit =2;
        this.recIndex = 1;
        this.options = options;
      console.log(options.mode);
       $('#itemtable').empty();
      $('#itemtable').unbind();
      $( '#paginator' ).empty();
      $( '#paginator' ).unbind(); 
      if(options.mode == 'normal'){
        this.collection = new admin.ItemCollection();    
      }else if(options.mode == 'trash'){
        this.collection = new admin.ItemTrashCollection();    
      }
      this.collection.fetch({       
        success:function(data){
          console.log("success");

          console.log(JSON.stringify(data));
          self.render();


        },
        error:function(data){
          /*try{
            var errData = JSON.parse(data.responseText);
            if ( errData.errCode == 550) {
              window.location.href = '/sessionExpired';
          } else {
            if (errData.errMsg.length > 0) {
                var failureMsg = errData.errMsg;  
              } else {
                  var failureMsg = "Error occurred while fetching the item Table View. Please Contact Administrator.";  
              }
              $( "div.failure").html(failureMsg);
                    $( "div.failure" ).fadeIn( 300 ).delay( 3500 ).fadeOut( 800 );        
           }
          }catch(e){
          window.location.href = '/sessionExpired';
        }*/
        }
      }); 
  },


  render: function() {

    var self = this;
    if(this.options.mode == 'normal'){
        this.edittemplate = '<span style="display:inline-block; width: 20px;"><a id="edititem" data-toggle="tooltip" data-placement="top" title="Edit">'+
      '<i class="glyphicon glyphicon-pencil"></i></a></span>'+
      '<span style="display:inline-block; width: 20px;"><a id="deleteitem" data-toggle="tooltip" data-placement="top" title="Delete">'+
      '<i class="glyphicon glyphicon-minus-sign"></i></a></span>';    
      this.edittext = 'Delete';
      }else if(this.options.mode == 'trash'){
        this.edittemplate = '<span style="display:inline-block; width: 20px;"><a id="deleteitem" data-toggle="tooltip" data-placement="top" title="Delete">'+
      '<i class="glyphicon glyphicon-minus-sign"></i></a></span>';  
      this.edittext = 'Restore';
      }

    var DeleteCell = Backgrid.Cell.extend({
    template: _.template(self.edittemplate),
    events: {
      "click #edititem": "editRow",
      "click #deleteitem": "deleteRow",
    },
    deleteRow: function (e) {
      console.log("Hello");
      e.preventDefault();
      modelvalue = this.model;
      var itemmodal= new BackboneBootstrapModals.ConfirmationModal({
          label: "Delete Item",
          text: "Are You want to Delete this item?",
          onConfirm: function(e) {
            e.preventDefault();
            var itemmodal = modelvalue;
            itemmodal.destroy({
                success: function (model, respose, options) {
                    console.log("The model has deleted the server");
                    $('#itemtable').empty();
                    $('#itemtable').unbind();
                    $( '#paginator' ).empty();
                    $( '#paginator' ).unbind(); 
                    appRouter.currentView.itemSearch();
                    return true;
                },
                error: function (model, xhr, options) {
                    console.log("Something went wrong while deleting the model");
                    return false;
                }
            });
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    itemmodal.render();
    },
    editRow:function(e){
      var self= this;
       console.log("Hello",this.model);
      e.preventDefault();
      var currentView = new admin.ItemFormPageView({
                template: _.template($('#itemEditFormTpl').html()),
                mode: 'edit',
                model:self.model.attributes
            }); 

    var itemmodal= new BackboneBootstrapModals.ConfirmationModal({
          label: "Edit Item",
          text: currentView.el,
          onConfirm: function(e) {
            console.log("action confirmed");
            return self.renderedititem();
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    itemmodal.render();
    },
    renderedititem:function(){
      var self = this;
     var itemmodel = new admin.ItemModel();
     console.log(this.model.attributes.item_id);
     var itemdata = { item_id:this.model.attributes.item_id,
      item_name:$.trim($('#item_name').val()), 
      item_number:$.trim($('#item_number').val()),
      company_id:$.trim($('#item_company').val()),
      product_id:$.trim($('#item_product').val()),
      supplier_id:$.trim($('#item_supplier').val()),
      cost_price:$.trim($('#item_cost_price').val()),
      unit_price:$.trim($('#item_unit_price').val()),
      reorder_level:$.trim($('#item_reorder_level').val()),
      receiving_quantity:$.trim($('#item_receiving_quantity').val()),
      description:$.trim($('#item_description').val()),
      tax1 : $.trim($('#item_tax_name1').val()),
      tax2 : $.trim($('#item_tax_name2').val()),
      tax3 : $.trim($('#item_tax_name3').val()),
      commission : $.trim($('#item_commission').val()),
     };
       if( $('#item_tax_name1').val() != '' && $('#item_percent1').val() != ''){                                   
      itemdata.tax1 = {
         "tax_name" : $('#item_tax_name1').val(),
         "percent" : $('#item_percent1').val(),
      };
    }
    if( $('#item_tax_name2').val() != '' && $('#item_percent2').val() != '' ){
      itemdata.tax2 = {
         "tax_name" : $('#item_tax_name2').val(),
         "percent" : $('#item_percent2').val(),
      };
    }
      if( $('#item_tax_name3').val() != '' && $('#item_percent3').val() != '' ){
      itemdata.tax3 = {
         "tax_name" : $('#item_tax_name3').val(),
         "percent" : $('#item_percent3').val(),
      };
    }
    itemmodel.set(itemdata );
     if(itemmodel.isValid()){
      itemmodel.save(null,{ 
        success: function () {
          $('#itemtable').empty();
          $('#itemtable').unbind();
          $( '#paginator' ).empty();
          $( '#paginator' ).unbind(); 
          appRouter.currentView.itemSearch();
            console.log('success!');
            
        },
        error: function (model, error) {
            console.log(error);
            return false;
        }});
    }else{
      _.each(itemmodel.validationError, function (error) {
           $('#' + error.name).text(error.message);
        });
    return false;
    }
     return true;
    },
      
    render: function () {
      this.$el.html(this.template());
      this.delegateEvents();
      return this;
    }

});
    var columns = [{
    name: "", 
    cell: "select-row",
    headerCell: "select-all",
  },
    {
    name: "item_name",
    label: "Item Name",
     editable: false,
    cell: "string" 
  }, 
  {
    name: "item_number",
    label: "Item Number",
     editable: false,
    cell: "string" 
  },
  {
    name: "description",
    label: "Item Description",
     editable: false,
    cell: "string" 
  },
  {
    name: "company_name",
    label: "Company",
     editable: false,
    cell: "string" 
  },
  {
    name: "product_name",
    label: "Product",
     editable: false,
    cell: "string" 
  },
  {
    name: "supplier_name",
    label: "Supplier",
     editable: false,
    cell: "string" 
  },
  {
    name: "unit_price",
    label: "Unit Price",
     editable: false,
    cell: "string" 
  },
  {
    name: "cost_price",
    label: "Cost Price",
     editable: false,
    cell: "string" 
  },
  {
        name: "delete",
        label: "Action",
        cell: DeleteCell
    }];

var grid = new Backgrid.Grid({
  columns: columns,
  collection: self.collection
});
var paginator = new Backgrid.Extension.Paginator({
        collection: self.collection
      });
$("#itemtable").append(grid.render().el);
$("#paginator").append(paginator.render().$el);
appRouter.itemgrid = grid;
            return this;  
  },

    removeoraddAll: function()
  {
      this.collection.reset();
      this.$el.empty();

  },

  renderItemItem:function(item,recIndex){
        
    var itemItemView = new admin.ItemRowView({
      model: item
    });
      this.$el.append(itemItemView.render().el );
  }
});
