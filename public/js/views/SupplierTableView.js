var admin = admin || {};

admin.SupplierTableView = Backbone.View.extend({

  initialize: function(options) {
      console.log('Initialize supplierTableView');
    var self = this;
        this.pageNo = 1;
       limit =2;
        this.recIndex = 1;
        this.options = options;
      console.log(options.mode);
       $('#managementtable').empty();
      $('#managementtable').unbind();
      $( '#paginator' ).empty();
      $( '#paginator' ).unbind(); 
      if(options.mode == 'normal'){
        this.collection = new admin.SupplierCollection();    
      }else if(options.mode == 'trash'){
        this.collection = new admin.SupplierTrashCollection();    
      }

      this.collection.fetch({       
        success:function(data){
          console.log("success");

          console.log(JSON.stringify(data));
           self.render();


        },
        error:function(data){
          /*try{
            var errData = JSON.parse(data.responseText);
            if ( errData.errCode == 550) {
              window.location.href = '/sessionExpired';
          } else {
            if (errData.errMsg.length > 0) {
                var failureMsg = errData.errMsg;  
              } else {
                  var failureMsg = "Error occurred while fetching the supplier Table View. Please Contact Administrator.";  
              }
              $( "div.failure").html(failureMsg);
                    $( "div.failure" ).fadeIn( 300 ).delay( 3500 ).fadeOut( 800 );        
           }
          }catch(e){
          window.location.href = '/sessionExpired';
        }*/
        }
      }); 
  },

  render: function() {
    var self = this;
    if(this.options.mode == 'normal'){
        this.edittemplate = '<span style="display:inline-block; width: 20px;"><a id="editsupplier" data-toggle="tooltip" data-placement="top" title="Edit">'+
      '<i class="glyphicon glyphicon-pencil"></i></a></span>'+
      '<span style="display:inline-block; width: 20px;"><a id="deletesupplier" data-toggle="tooltip" data-placement="top" title="Delete">'+
      '<i class="glyphicon glyphicon-minus-sign"></i></a></span>';    
      this.edittext = 'Delete';
      }else if(this.options.mode == 'trash'){
        this.edittemplate = '<span style="display:inline-block; width: 20px;"><a id="deletesupplier" data-toggle="tooltip" data-placement="top" title="Delete">'+
      '<i class="glyphicon glyphicon-minus-sign"></i></a></span>';  
      this.edittext = 'Restore';
      }
    var DeleteCell = Backgrid.Cell.extend({
    template: _.template(self.edittemplate),
    events: {
      "click #editsupplier": "editRow",
      "click #deletesupplier": "deleteRow",
    },
    deleteRow: function (e) {
      console.log("Hello");
      e.preventDefault();
      modelvalue = this.model;
      var suppliermodal= new BackboneBootstrapModals.ConfirmationModal({
          label: "Delete Supplier",
          text: "Are You want to Delete this supplier?",
          onConfirm: function(e) {
            e.preventDefault();
            var suppliermodal = modelvalue;
            suppliermodal.destroy({
                success: function (model, respose, options) {
                    console.log("The model has deleted the server");
                    $('#managementtable').empty();
                    $('#managementtable').unbind();
                    $( '#paginator' ).empty();
                    $( '#paginator' ).unbind(); 
                    appRouter.currentView.Search();
                    return true;
                },
                error: function (model, xhr, options) {
                    console.log("Something went wrong while deleting the model");
                    return false;
                }
            });
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    suppliermodal.render();
    },
    editRow:function(e){
      var self= this;
       console.log("Hello",this.model);
      e.preventDefault();
      var currentView = new admin.FormPageView({
                template: _.template($('#supplierEditFormTpl').html()),
                mode: 'edit',
                model:self.model.attributes
            }); 
    var suppliermodal= new BackboneBootstrapModals.ConfirmationModal({
          label: "Edit Supplier",
          text: currentView.el,
          onConfirm: function(e) {
            console.log("action confirmed");
            return self.rendereditsupplier();
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    suppliermodal.render();
    },
    rendereditsupplier:function(){
      var self = this;
     var suppliermodel = new admin.SupplierModel();
     var supplierdata = { person_id:self.model.attributes.person_id,
      first_name:$.trim($('#supplier_first_name').val()),
  last_name:$.trim($('#supplier_last_name').val()),
  phone_number:$.trim($('#supplier_phone_number').val()),
  email:$.trim($('#supplier_email').val()),
  address_1:$.trim($('#supplier_address_1').val()),
  address_2:$.trim($('#supplier_address_2').val()),
  city:$.trim($('#supplier_city').val()),
  state:$.trim($('#supplier_state').val()),
  zip:$.trim($('#supplier_zip').val()),
  country:$.trim($('#supplier_country').val()),
  company_name:$.trim($('#supplier_company_name').val()),
  agency_name:$.trim($('#supplier_agency_name').val())
     };
    suppliermodel.set(supplierdata);
    if(suppliermodel.isValid()){
      suppliermodel.save(null,{ 
        success: function () {
          $('#managementtable').empty();
          $('#managementtable').unbind();
          $( '#paginator' ).empty();
          $( '#paginator' ).unbind(); 
          appRouter.currentView.Search();
            console.log('success!');
            
        },
        error: function (model, error) {
            console.log(error);
            return false;
        }});
    }else{
      _.each(suppliermodel.validationError, function (error) {
           $('#' + error.name).text(error.message);
        });
    return false;
    }
     return true;
    },
      
    render: function () {
      this.$el.html(this.template());
      this.delegateEvents();
      return this;
    }

});
    
    var columns = [{
    name: "", 
    cell: "select-row",
    headerCell: "select-all",

  }, 
    {
    name: "first_name",
    label: "First Name",
     editable: false,
    cell: "string" 
  },
  {
    name: "last_name",
    label: "Last Name",
     editable: false,
    cell: "string" 
  },  
  {
    name: "email",
    label: "Email",
     editable: false,
    cell: "string" 
  }, {
    name: "phone_number",
    label: "Phone Number",
     editable: false,
    cell: "string" 
  }, 
  {
    name: "address_1",
    label: "Address",
     editable: false,
    cell: "string" 
  }, 
  {
        name: "delete",
        label: "Action",
        cell: DeleteCell
    }];

var grid = new Backgrid.Grid({
  columns: columns,
  collection: self.collection
});
var paginator = new Backgrid.Extension.Paginator({
        collection: self.collection
      });
$("#managementtable").append(grid.render().el);
$("#paginator").append(paginator.render().$el);
appRouter.suppliergrid = grid;
            return this;  
  },

    removeoraddAll: function()
  {
      this.collection.reset();
      this.$el.empty();

  },

  
});
