var admin = admin || {};

admin.CommissionReportPageView = Backbone.View.extend({
  
  template : $('#commissionReportTpl').html(),
   
	initialize: function(options) {
        this.template = options.template;
        console.log(appRouter.reporttype);
        var self = this;
        if(appRouter.reporttype == 'normal') {
         this.collection = new admin.CommissionReportCollection();
         this.collection.fetch({
        success:function(data){
          self.render();
        },
        error:function(){

        }
       });
        }else{
          this.collection = new admin.CommissionGraphReportCollection();
          this.collection.fetch({
        success:function(data){
          self.graphrender();
        },
        error:function(){

        }
       });
        }  
	},

	render: function() {
  var self = this;			
   this.$el.html(this.template);
    var columns = [{
    name: "name",
    label: "Name",
     editable: false,
    cell: "string" 
  },
  {
    name: "earned",
    label: "Earned",
     editable: false,
    cell: "string" 
  },
  {
    name: "claimpts",
    label: "Used",
     editable: false,
    cell: "string" 
  }];
console.log(this.collection);
var grid = new Backgrid.Grid({
  columns: columns,
  collection: this.collection,
  rendertype : 'report'
});
var paginator = new Backgrid.Extension.Paginator({
        collection: this.collection
      });
$("#commissionreport").append(grid.render().el);
$("#commissionpaginator").append(paginator.render().$el);
return this;
	},
	graphrender:function(){
     var arr = [];
     this.collection.each(function(item){
      arr.push({label : item.get('name'), value : item.get('earned') });
    });
    this.$el.html(this.template);
    
    var chartData = {
          "chart": {
            "caption": "Commission Report List",
            "subCaption": "Date specified",
            "xAxisName": "Commission",
            "yAxisName": "Sales Amount",
            "numberPrefix": "rs",
            "theme": "carbon",
            "bgcolor": "#DCEAFC",
            "canvasbgcolor": "#DCEAFC",
            "canvasbgalpha": "10",
            "rotateValues": "1",
            "exportEnabled": "1",
            "transposeAnimation": "1",
            "pieRadius": "70",
          },

          "data": arr
        };
        revenueChartColumn = new FusionCharts({
          type: 'column2d',
          width: '420',
          height: '350',
          dataFormat: 'json',
          id: 'commissionreport1',
          dataSource: chartData
        });
       revenueChartColumn.render("commissionreport");

  }
});
