var admin = admin || {};

admin.ExpenseTableView = Backbone.View.extend({

  initialize: function(options) {
      console.log('Initialize expenseTableView');
    var self = this;
        this.pageNo = 1;
       limit =2;
        this.recIndex = 1;
        this.options = options;
      console.log(options.mode);
       $('#expensetable').empty();
      $('#expensetable').unbind();
      $( '#paginator' ).empty();
      $( '#paginator' ).unbind(); 
      if(options.mode == 'normal'){
        this.collection = new admin.ExpenseCollection();    
      }else if(options.mode == 'trash'){
        this.collection = new admin.ExpenseTrashCollection();    
      }
      this.collection.fetch({       
        success:function(data){
          console.log("success");

          console.log(JSON.stringify(data));
          self.render();


        },
        error:function(data){
          /*try{
            var errData = JSON.parse(data.responseText);
            if ( errData.errCode == 550) {
              window.location.href = '/sessionExpired';
          } else {
            if (errData.errMsg.length > 0) {
                var failureMsg = errData.errMsg;  
              } else {
                  var failureMsg = "Error occurred while fetching the expense Table View. Please Contact Administrator.";  
              }
              $( "div.failure").html(failureMsg);
                    $( "div.failure" ).fadeIn( 300 ).delay( 3500 ).fadeOut( 800 );        
           }
          }catch(e){
          window.location.href = '/sessionExpired';
        }*/
        }
      }); 
  },


  render: function() {

    var self = this;
    if(this.options.mode == 'normal'){
        this.edittemplate = '<span style="display:inline-block; width: 20px;"><a id="editexpense" data-toggle="tooltip" data-placement="top" title="Edit">'+
      '<i class="glyphicon glyphicon-pencil"></i></a></span>'+
      '<span style="display:inline-block; width: 20px;"><a id="deleteexpense" data-toggle="tooltip" data-placement="top" title="Delete">'+
      '<i class="glyphicon glyphicon-minus-sign"></i></a></span>';    
      this.edittext = 'Delete';
      }else if(this.options.mode == 'trash'){
        this.edittemplate = '<span style="display:inline-block; width: 20px;"><a id="deleteexpense" data-toggle="tooltip" data-placement="top" title="Delete">'+
      '<i class="glyphicon glyphicon-minus-sign"></i></a></span>';  
      this.edittext = 'Restore';
      }
    var DeleteCell = Backgrid.Cell.extend({
    template: _.template(self.edittemplate),
    events: {
      "click #editexpense": "editRow",
      "click #deleteexpense": "deleteRow",
    },
    deleteRow: function (e) {
      console.log("Hello");
      e.preventDefault();
      modelvalue = this.model;
      var expensemodal= new BackboneBootstrapModals.ConfirmationModal({
          label: "Delete Expense",
          text: "Are You want to Delete this expense?",
          onConfirm: function(e) {
            e.preventDefault();
            var expensemodal = modelvalue;
            expensemodal.destroy({
                success: function (model, respose, options) {
                    console.log("The model has deleted the server");
                    $('#expensetable').empty();
                    $('#expensetable').unbind();
                    $( '#paginator' ).empty();
                    $( '#paginator' ).unbind(); 
                    appRouter.currentView.expenseSearch();
                    return true;
                },
                error: function (model, xhr, options) {
                    console.log("Something went wrong while deleting the model");
                    return false;
                }
            });
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    expensemodal.render();
    },
    editRow:function(e){
      var self= this;
       console.log("Hello",this.model);
      e.preventDefault();
      var currentView = new admin.ExpenseFormPageView({
                template: _.template($('#expenseEditFormTpl').html()),
                mode: 'edit',
                model:self.model.attributes
            }); 

    var expensemodal= new BackboneBootstrapModals.ConfirmationModal({
          label: "Edit Expense",
          text: currentView.el,
          onConfirm: function(e) {
            console.log("action confirmed");
            return self.rendereditexpense();
          },
          onCancel: function() {
            console.log("action canceled");
          }
        });
    expensemodal.render();
    },
    rendereditexpense:function(){
      var self = this;
     var expensemodel = new admin.ExpenseModel();
     console.log(this.model.attributes.expense_id);
     var expensedata = { expense_id:this.model.attributes.expense_id,
      expense_category_id:$('#expense_category_name').val(),
      expense_description:$.trim($('#expense_description').val()),
      amount:$.trim($('#amount').val()),
      payment_type:$.trim($('#payment_type').val()),
      supplier_name:$.trim($('#supplier_name').val())
      };
      expensemodel.set(expensedata);
      if(expensemodel.isValid()){
        expensemodel.save(null,{ 
           success: function () {
            $('#expensetable').empty();
            $('#expensetable').unbind();
            $( '#paginator' ).empty();
            $( '#paginator' ).unbind(); 
          appRouter.currentView.expenseSearch();
              console.log('success!');
              self.render();
        },
      error: function (model, error) {
        console.log(error);
        return false;
     }});
      
    }else{
      _.each(expensemodel.validationError, function (error) {
           $('#' + error.name).text(error.message);
        });
    return false;
    }
     return true;
    },
      
    render: function () {
      this.$el.html(this.template());
      this.delegateEvents();
      return this;
    }

});
    var columns = [{
    name: "", 
    cell: "select-row",
    headerCell: "select-all",
    },
    {
    
    name: "expense_category_name",
    label: "Expense Category Name",
     editable: false,
    cell: "string" 
  }, 

  {
    name: "expense_description",
    label: "Expense Description",
     editable: false,
    cell: "string" 
  },

  {
    name: "amount",
    label: "Amount",
     editable: false,
    cell: "string" 
  },

  {
    name: "payment_type",
    label: "Payment Type",
     editable: false,
    cell: "string" 
  },

  {
    name: "supplier_name",
    label: "Supplier Name",
     editable: false,
    cell: "string" 
  },

  {
        name: "delete",
        label: "Action",
        cell: DeleteCell
    }];

var grid = new Backgrid.Grid({
  columns: columns,
  collection: self.collection
});
var paginator = new Backgrid.Extension.Paginator({
        collection: self.collection
      });
$("#expensetable").append(grid.render().el);
$("#paginator").append(paginator.render().$el);
appRouter.expensegrid = grid;

            return this;  
  },

    removeoraddAll: function()
  {
      this.collection.reset();
      this.$el.empty();

  },

});
