var admin = admin || {};

admin.MgntSupplierItemView = Backbone.View.extend({

    tagName: 'option',

    initialize: function(){
      console.log('init');
      _.bindAll(this, 'render');
    },

    render: function(){
       console.log("veiws :: " + this.model.get('supplier_name'));
      $(this.el).attr('value',
      this.model.get('person_id')).html(this.model.get('supplier_name'));
      return this;
    }

});
