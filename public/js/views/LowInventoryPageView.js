var admin = admin || {};

admin.LowInventoryPageView = Backbone.View.extend({
  
  template : $('#lowinventoryReportTpl').html(),
   
  initialize: function(options) {
        var self = this;
       this.collection = new admin.LowInventoryReportCollection();
       this.collection.fetch({
        success:function(data){
          self.render();
        },
        error:function(){

        }
       });
      
  },

  
 render: function() {
  var self = this;      
   this.$el.html(this.template);
    var columns = [{
    name: "item_name",
    label: "Item Name",
     editable: false,
    cell: "string" 
  },
  {
    name: "item_number",
    label: "Item Number",
     editable: false,
    cell: "string" 
  },
  {
    name: "reorder_level",
    label: "Order Level",
     editable: false,
    cell: "string" 
  },
  {
    name: "quantity",
    label: "Remaining quantity",
     editable: false,
    cell: "string" 
  }];
console.log(this.collection);
var grid = new Backgrid.Grid({
  columns: columns,
  collection: this.collection,
  rendertype : 'report'
});
var paginator = new Backgrid.Extension.Paginator({
        collection: this.collection
      });
$("#lowinventoryreport").append(grid.render().el);
$("#lowinventorypaginator").append(paginator.render().$el);
return this;
  },
  

 
});
  
  

 