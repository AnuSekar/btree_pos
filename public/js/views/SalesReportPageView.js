var admin = admin || {};

admin.SalesReportPageView = Backbone.View.extend({
  
  template : $('#salesReportTpl').html(),
   
  initialize: function(options) {
        //this.template = options.template;
        if(appRouter.reporttype == 'normal') {
          this.render(); 
        }else if(appRouter.reporttype == 'details'){
          this.detailsrender(); 
        } else{
          this.graphrender(); 
        } 
      
   
  },

  
 render: function() {
  var self = this;      
  var tmpl = _.template( this.template );    
 this.$el.html(tmpl({exporttype:'sale_report',type:appRouter.dateval.type, transtype:appRouter.dateval.transtype, start_date:appRouter.dateval.start_date, end_date:appRouter.dateval.end_date}));      
      
    var columns = [{
    name: "sale_time",
    label: "Date",
     editable: false,
    cell: "string" 
  },
  {
    name: "sales_amount",
    label: "Sub Total",
     editable: false,
    cell: "string"
  },
  {
    name: "item_total_cost",
    label: "Total",
     editable: false,
    cell: "string"
  },
  {
    name: "tax_amount",
    label: "Tax",
     editable: false,
    cell: "string"
  },
  {
    name: "count",
    label: "Count",
     editable: false,
    cell: "string"
  }];
console.log(appRouter.collection);
var grid = new Backgrid.Grid({
  columns: columns,
  collection: appRouter.collection,
  rendertype : 'report'
});
var paginator = new Backgrid.Extension.Paginator({
        collection: appRouter.collection
      });
$("#salesreport").append(grid.render().el);
$("#salespaginator").append(paginator.render().$el);
return this;
  },

  graphrender:function(){
     var arr = [];
    appRouter.collection.each(function(item){
      arr.push({label : item.get('item_name'), value : item.get('sales_amount') });
    });
    this.$el.html(this.template);
    $('#download').hide();
    var chartData = {
          "chart": {
            "caption": "Sales Report List",
            "subCaption": "Data specified",
            "xAxisName": "Sales",
            "yAxisName": "Quantity Purchased",
            "numberPrefix": "",
            "theme": "carbon",
            "bgcolor": "#DCEAFC",
            "canvasbgcolor": "#DCEAFC",
            "canvasbgalpha": "10",
            "rotateValues": "1",
            "exportEnabled": "1",
            "transposeAnimation": "1",
            "pieRadius": "70",
          },

          "data": arr
        };
        revenueChartColumn = new FusionCharts({
          type: 'column2d',
          width: '420',
          height: '350',
          dataFormat: 'json',
          id: 'salesreport1',
          dataSource: chartData
        });
       revenueChartColumn.render("salesreport");

  },

  detailsrender:function(){
    var self = this;      
   this.$el.html(this.template);
   var detailitem = '<span style="display:inline-block; width: 20px;"><a id="detailitem" data-toggle="tooltip" data-placement="top" title="Delete">'+
      'Details</a></span>'; 
      var detailCell = Backgrid.Cell.extend({
    template: _.template(detailitem),
    events: {
      "click #detailitem": "detailItem",
    },
    detailItem: function (e) {
      console.log("Hello");
      e.preventDefault();
     console.log(this.model);
     var self = this;
     var sale_id = this.model.get('sale_ids');
     var id = {};
     id.sale_id = sale_id;
     id.sales_status = 0;
     $('#detailItemModal').modal("show");
     var tablehtml = '<table class="table"><thead><tr><th>Item Name</th><th>Amount</th><th>Tax Amount</th><th>Quantity</th></tr></thead><tbody>';
     this.collection = new admin.SalesDetailItemReportCollection();
     this.collection.fetch({
      data:id,
      success:function(data){
        console.log(data);
        data.each(function(model){
          tablehtml+= '<tr><td>'+ model.get("item_name") +'</td><td>' + model.get("sales_amount") +  '</td><td>' + model.get("tax_amount") + '</td><td>' + model.get("quantity_purchased") + '</td></tr>';
          quantity = model.get("quantity_purchased");
          
          
          tax_amount = model.get("tax_amount");

        });
        tablehtml += '</tbody></table>';
        $('#listItemsales').html(tablehtml);
      },
      error:function(){

      }
     });
     },
     render: function () {
      this.$el.html(this.template());
      this.delegateEvents();
      return this;
    }
    });

    var columns = [{
    name: "sale_time",
    label: "Date",
     editable: false,
    cell: "string" 
  },
  {
    name: "item_purchase",
    label: "item purchased",
     editable: false,
    cell: "string"
  },
  {
    name: "soldby",
    label: "sold by",
     editable: false,
    cell: "string"
  },
  {
    name: "soldto",
    label: "sold to ",
     editable: false,
    cell: "string"
  },
  {
    name: "sales_amount",
    label: "Sub Total",
     editable: false,
    cell: "string"
  },
  {
    name: "item_total_cost",
    label: "Total",
     editable: false,
    cell: "string"
  },
  {
    name: "tax_amount",
    label: "Tax",
     editable: false,
    cell: "string"
  },
  {
    name: "payment_type",
    label: "payment type",
     editable: false,
    cell: "string"
  },
  {
    name: "",
    label: "details of item",
     editable: false,
    cell: detailCell
  }];
console.log(appRouter.collection);
var grid = new Backgrid.Grid({
  columns: columns,
  collection: appRouter.collection,
  rendertype : 'report'
});
var paginator = new Backgrid.Extension.Paginator({
        collection: appRouter.collection
      });
$("#salesreport").append(grid.render().el);
$("#salespaginator").append(paginator.render().$el);
return this;
  },

  
 
});
  
  

 