var admin = admin || {};

admin.MgntSupplierCollectionView = Backbone.View.extend({

	initialize: function(options) {
    	console.log('Initialize MgntSupplierCollectionView');
    	console.log('faqCollection');
	    	console.log(options.catCollection);
			this.collection = new Backbone.Collection(options.catCollection);

		_.bindAll(this, "renderSupplier");
		_.bindAll(this, "render");
		this.render();
	},

	events:{
	},

	render: function() {
    	console.log('Rendering Supplier collection');
    	if( this.collection )
    	{    		
    	   // this.$el.html( '<option value="">Select</option>' );
			   this.collection.each(function(item) {
		    	  console.log(item.get('person_id'));
		        console.log(item.get('supplier_name'));
				this.renderSupplier(item);
			}, this );
		}
    	return this;
	},

	renderSupplier: function( item ) {
		var catItemView = new admin.MgntSupplierItemView({
			model: item
		});
    	this.$el.append( catItemView.render().el );
	},

});
