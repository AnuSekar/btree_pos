var admin = admin || {};
admin.ExpenseCategoryModel = Backbone.Model.extend({
  defaults: {
    "expense_category_id":null,
    "expense_category_name":null,
    "expense_category_description":null,
    "deleted":0
  },
  idAttribute: 'expense_category_id',

  validate: function (attrs,options) {
        var errors = [];
        
        console.log(attrs);
        if (!attrs.expense_category_name || attrs.expense_category_name == null) {
             errors.push({name: 'expense_category_name_error', message: 'Please enter Expense Name field.'}); 
        }else if(!regex.test(attrs.expense_category_name)){
            errors.push({name: 'expense_category_name_error', message: 'Please enter Valid Expense Name field.'}); 
        }
        if (!attrs.expense_category_description || attrs.expense_category_description == null) {
             errors.push({name: 'expense_category_description_error', message: 'Please enter Expense Description field.'}); 
        }
        return errors.length > 0 ? errors : false;
    },

   getCustomUrl: function (method) {
        switch (method) {
          case 'read':
                return '/btree/admin/get_expense_category_by_id/' + this.id;
                break;
            case 'create':
                return '/btree/admin/create_expense_category';
                break;
            case 'update':
                return '/btree/admin/update_expense_category/' + this.id;
                break;
            case 'delete':
                return '/btree/admin/delete_expense_category/' + this.id;
                break;
        }
    },
    sync: function (method, model, options) {
        options || (options = {});
        options.url = this.getCustomUrl(method.toLowerCase());
        return Backbone.sync.apply(this, arguments);
    }
});

