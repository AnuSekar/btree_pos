var admin = admin || {};
admin.ItemModel = Backbone.Model.extend({
	defaults: {
		'item_id' : null,
		"item_name" : null,
		"company_id" : null,
		"product_id" : null,
		"supplier_id": null,	
		"deleted" : '0',	
		"item_number" : null,
		"description" : null,
		"cost_price" : null,
		"unit_price" : null,
		"reorder_level" : null,
		"receiving_quantity" : null,	
		"pic_id" : null,
		"allow_alt_description" : null,
		"is_serialized" : null,
        "tax_1" : null,
        "tax_2" : null,
        "tax_3" : null,
		"percent" : null,
        "commission":null,
	},
	idAttribute: 'item_id',

	validate: function (attrs,options) {
        var errors = [];
        console.log(attrs);
        if (!attrs.item_name || attrs.item_name == null) {
             errors.push({name: 'item_name_error', message: 'Please enter Item Name field.'}); 
        }else if(!regex.test(attrs.item_name)){
            errors.push({name: 'item_name_error', message: 'Please enter Valid Item Name field.'}); 
        }
        if (!attrs.company_id || attrs.company_id == null) {
             errors.push({name: 'item_company_error', message: 'Please select Company Name '}); 
        }
        if (!attrs.product_id || attrs.product_id == null) {
             errors.push({name: 'item_product_error', message: 'Please select Product '}); 
        }
        if (!attrs.supplier_id || attrs.supplier_id == null) {
             errors.push({name: 'item_supplier_error', message: 'Please select Supplier'}); 
        }
        // if(!regex.test(attrs.tax_1)){
        //     errors.push({name: 'item_tax_name1_error', message: 'Please enter Valid  field.'}); 
        // }else if(attrs.tax_2 == attrs.tax_1 || attrs.tax_1 == attrs.tax_3 ){
        //     errors.push({name: 'item_tax_name1_error', message: 'Please enter New Tax'}); 
        // }
        // if(attrs.tax_2 == attrs.tax_1 || attrs.tax_2 == attrs.tax_3 ){
        //     errors.push({name: 'item_tax_name2_error', message: 'Please enter New Tax'}); 
        // }else if(!regex.test(attrs.tax_2)){
        //     errors.push({name: 'item_tax_name2_error', message: 'Please enter Valid Tax Name'}); 
        // }
        /*if(attrs.tax_3 == attrs.tax_1 || attrs.tax_3 == attrs.tax_2 ){
            errors.push({name: 'item_tax_name3_error', message: 'Please enter New Tax'}); 
        }else if(!regex.test(attrs.tax_3)){
            errors.push({name: 'item_tax_name3_error', message: 'Please enter Valid Tax Name'}); 
        }*/
        
        return errors.length > 0 ? errors : false;        
        
    },

	 getCustomUrl: function (method) {
        switch (method) {
        	case 'read':
                return '/btree/admin/get_item_by_id/' + this.id;
                break;
            case 'create':
                return '/btree/admin/create_item';
                break;
            case 'update':
                return '/btree/admin/update_item/' + this.id;
                break;
            case 'delete':
                return '/btree/admin/delete_item/' + this.id;
                break;
        }
    },
    sync: function (method, model, options) {
        options || (options = {});
        options.url = this.getCustomUrl(method.toLowerCase());
        return Backbone.sync.apply(this, arguments);
    }
});

admin.ItemKitModel = Backbone.Model.extend({
    defaults: {
        'item_kit_id' : null,
        "item_kit_name" : null,
        "item_kit_discount" : null,
        "itemarray" : null
    },
    idAttribute: 'item_kit_id',

    validate: function (attrs,options) {
        var errors = [];
        console.log(attrs);
        if (!attrs.item_kit_name || attrs.item_kit_name == null) {
             errors.push({name: 'item_name_kit_error', message: 'Please enter Item Kit Name field.'}); 
        }else if(!regex.test(attrs.item_name)){
            errors.push({name: 'item_name_kit_error', message: 'Please enter Valid Item Name field.'}); 
        }
        if (!attrs.item_kit_discount || attrs.item_kit_discount == null) {
             errors.push({name: 'item_kit_discount_error', message: 'Please Enter Discount '}); 
        }
       

        return errors.length > 0 ? errors : false;        
        
    },

     getCustomUrl: function (method) {
        switch (method) {
            case 'read':
                return '/btree/admin/get_itemkit_by_id/' + this.id;
                break;
            case 'create':
                return '/btree/admin/create_itemkit';
                break;
            case 'update':
                return '/btree/admin/update_itemkit/' + this.id;
                break;
            case 'delete':
                return '/btree/admin/delete_itemkit/' + this.id;
                break;
        }
    },
    sync: function (method, model, options) {
        options || (options = {});
        options.url = this.getCustomUrl(method.toLowerCase());
        return Backbone.sync.apply(this, arguments);
    }
});