var admin = admin || {};
admin.SalesItemModel = Backbone.Model.extend({
	defaults: {
		'item_id' : null,
		"item_name" : null,
		"item_number" : null,
		"cost_price" : null,
		"unit_price" : null,
		"percent" : null,
		"label" : null,
		"quantity":1,
		"discount":0
	},
	idAttribute: 'sales_id'
});


admin.SuspendedSalesItemModel = Backbone.Model.extend({
	defaults: {
		'sales_id' : null,
		"customer_name" : null,
		"date" : null,
	},
	idAttribute: 'sales_id'
});

