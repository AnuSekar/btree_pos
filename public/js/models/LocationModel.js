var admin = admin || {};
admin.LocationModel = Backbone.Model.extend({
  defaults: {
    "location_name":null,
    "deleted":0
  },
  idAttribute: 'location_id',

  validate: function (attrs,options) {
        var errors = [];
        
        console.log(attrs);
        if (!attrs.location_name || attrs.location_name == null) {
             errors.push({name: 'location_name_error', message: 'Please enter Location Name field.'}); 
        }else if(!regex.test(attrs.location_name)){
            errors.push({name: 'location_name_error', message: 'Please enter Valid Location Name field.'}); 
        }
        
        return errors.length > 0 ? errors : false;
    },

   getCustomUrl: function (method) {
        switch (method) {
          case 'read':
                return '/btree/admin/get_location_by_id/' + this.id;
                break;
            case 'create':
                return '/btree/admin/create_location';
                break;
            case 'update':
                return '/btree/admin/update_location/' + this.id;
                break;
            case 'delete':
                return '/btree/admin/delete_location/' + this.id;
                break;
        }
    },
    sync: function (method, model, options) {
        options || (options = {});
        options.url = this.getCustomUrl(method.toLowerCase());
        return Backbone.sync.apply(this, arguments);
    }
});

