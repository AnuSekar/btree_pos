var admin = admin || {};
admin.SupplierModel = Backbone.Model.extend({
  defaults: {
       // "person_id" : null,
    "first_name" : null,
        "last_name" : null, 
    "deleted" : 0,  
    "gender" : null,
    "phone_number" : null,
    "email" : null,
    "address_1" : null, 
    "address_2" : null,
    "city" : null,
    "state" : null,
    "zip" : null,
    "company_name" : null,
    "agency_name" : null,
    "country" : null,
    "label" : null
        
  },
  idAttribute: 'person_id',
  validate: function (attrs,options) {
        var errors = [];
        
        console.log(attrs);
        if (!attrs.first_name || attrs.first_name == null) {
             errors.push({name: 'supplier_first_name_error', message: 'Please enter First_name field.'}); 
        }else if(!regex.test(attrs.first_name)){
            errors.push({name: 'supplier_first_name_error', message: 'Please enter Valid First_name field.'}); 
        }
        if (!attrs.last_name || attrs.last_name == null) {
             errors.push({name: 'supplier_last_name_error', message: 'Please enter  field.'}); 
        }else if(!regex.test(attrs.last_name)){
            errors.push({name: 'supplier_last_name_error', message: 'Please enter Valid Last_name field.'}); 
        }
        if (!attrs.phone_number || attrs.phone_number == null) {
             errors.push({name: 'supplier_phone_number_error', message: 'Please enter Phone nuMber field.'}); 
        }else if(!regex1.test(attrs.phone_number)){
            errors.push({name: 'supplier_phone_number_error', message: 'Please enter Valid Phone nuMber field.'}); 
        }
        if (!attrs.address_1 || attrs.address_1 == null) {
             errors.push({name: 'supplier_address_1_error', message: 'Please enter Address field.'}); 
        }
        if (!attrs.address_2 || attrs.address_2 == null) {
             errors.push({name: 'supplier_address_2_error', message: 'Please enter Address field.'}); 
        }
        if (!attrs.company_name || attrs.company_name == null) {
             errors.push({name: 'supplier_company_name_error', message: 'Please enter Company Name field.'}); 
        }else if(!regex.test(attrs.company_name)){
            errors.push({name: 'supplier_company_name_error', message: 'Please enter Valid Company Name field.'}); 
        }
        if (!attrs.agency_name || attrs.agency_name == null) {
             errors.push({name: 'supplier_agency_name_error', message: 'Please enter Agency Name field.'}); 
        }else if(!regex.test(attrs.agency_name)){
            errors.push({name: 'supplier_agency_name_error', message: 'Please enter Valid Agency Name field.'}); 
        }
        if (!attrs.city || attrs.city == null) {
             errors.push({name: 'supplier_city_error', message: 'Please enter City field.'}); 
        }else if(!regex.test(attrs.city)){
            errors.push({name: 'supplier_city_error', message: 'Please enter Valid City field.'}); 
        }
        if (!attrs.state || attrs.state == null) {
             errors.push({name: 'supplier_state_error', message: 'Please enter State field.'}); 
        }else if(!regex.test(attrs.state)){
            errors.push({name: 'supplier_state_error', message: 'Please enter Valid State field.'}); 
        }
        if (!attrs.country || attrs.country == null) {
             errors.push({name: 'supplier_country_error', message: 'Please enter Country field.'}); 
        }else if(!regex.test(attrs.country)){
            errors.push({name: 'supplier_country_error', message: 'Please enter Valid Country field.'}); 
        }
        if (!attrs.email || attrs.email == null) {
             errors.push({name: 'supplier_email_error', message: 'Please enter Email field.'}); 
        }
        if (!attrs.zip || attrs.zip == null) {
             errors.push({name: 'supplier_zip_error', message: 'Please enter Zip field.'}); 
        }else if(!regex1.test(attrs.zip)){
            errors.push({name: 'supplier_zip_error', message: 'Please enter Valid Zip field.'}); 
        }
        return errors.length > 0 ? errors : false;
    },

   getCustomUrl: function (method) {
        switch (method) {
          case 'read':
                return '/btree/admin/get_supplier_by_id/' + this.id;
                break;
            case 'create':
                return '/btree/admin/create_supplier';
                break;
            case 'update':
                return '/btree/admin/update_supplier/' + this.id;
                break;
            case 'delete':
                return '/btree/admin/delete_supplier/' + this.id;
                break;
        }
    },
    sync: function (method, model, options) {
        options || (options = {});
        options.url = this.getCustomUrl(method.toLowerCase());
        return Backbone.sync.apply(this, arguments);
    }

});

