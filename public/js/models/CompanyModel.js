var admin = admin || {};
admin.CompanyModel = Backbone.Model.extend({
	defaults: {
		"company_id":null,
		"company_name":null,
		"company_description":null,
		"deleted":0
	},
	idAttribute: 'company_id',

	validate: function (attrs,options) {
        var errors = [];
        
        console.log(attrs);
        if (!attrs.company_name || attrs.company_name == null) {
             errors.push({name: 'company_name_error', message: 'Please enter Company Name field.'}); 
        }else if(!regex.test(attrs.company_name)){
            errors.push({name: 'company_name_error', message: 'Please enter Valid Company Name field.'}); 
        }
        if (!attrs.company_description || attrs.company_description == null) {
             errors.push({name: 'company_description_error', message: 'Please enter Company Description field.'}); 
        }
        return errors.length > 0 ? errors : false;
    },

	 getCustomUrl: function (method) {
        switch (method) {
        	case 'read':
                return '/btree/admin/get_company_by_id/' + this.id;
                break;
            case 'create':
                return '/btree/admin/create_company';
                break;
            case 'update':
                return '/btree/admin/update_company/' + this.id;
                break;
            case 'delete':
                return '/btree/admin/delete_company/' + this.id;
                break;
        }
    },
    sync: function (method, model, options) {
        options || (options = {});
        options.url = this.getCustomUrl(method.toLowerCase());
        return Backbone.sync.apply(this, arguments);
    }
});

