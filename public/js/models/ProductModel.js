var admin = admin || {};
admin.ProductModel = Backbone.Model.extend({
	defaults: {
		"product_id":null,
		"product_name":null,
		"product_description":null,
		"deleted":0
	},
	idAttribute: 'product_id',
    
	validate: function (attrs,options) {
        var errors = [];
        console.log(attrs);
        if (!attrs.product_name || attrs.product_name == null) {
             errors.push({name: 'product_name_error', message: 'Please enter Product Name field.'}); 
        }
        if (!attrs.product_description || attrs.product_description == null) {
             errors.push({name: 'product_description_error', message: 'Please enter Product Description field.'}); 
        }
        return errors.length > 0 ? errors : false;
    },

	 getCustomUrl: function (method) {
        switch (method) {
        	case 'read':
                return '/btree/admin/get_product_by_id/' + this.id;
                break;
            case 'create':
                return '/btree/admin/create_product';
                break;
            case 'update':
                return '/btree/admin/update_product/' + this.id;
                break;
            case 'delete':
                return '/btree/admin/delete_product/' + this.id;
                break;
        }
    },
    sync: function (method, model, options) {
        options || (options = {});
        options.url = this.getCustomUrl(method.toLowerCase());
        return Backbone.sync.apply(this, arguments);
    }
});

