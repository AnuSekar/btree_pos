var admin = admin || {};
admin.CompanyReportModel = Backbone.Model.extend({
	defaults: {
		"company_id":null,
		"company_name":null,
    "sales_amount":null,
    "item_total_cost":null,
    "tax_amount":null,
    "quantity_purchased":null,
    
	},
	idAttribute: 'company_id',
});

admin.ProductReportModel = Backbone.Model.extend({
  defaults: {
    "product_id":null,
    "product_name":null,
    "sales_amount":null,
    "item_total_cost":null,
    "tax_amount":null,
    "quantity_purchased":null,
    
  },
  idAttribute: 'product_id',
});

admin.CustomerReportModel = Backbone.Model.extend({
  defaults: {
    "preson_id":null,
    "name":null,
    "sales_amount":null,
    "item_total_cost":null,
    'tax_amount' : null,
    "sales_whole_amount" : null,
    "count" : null,
  },
  idAttribute: 'preson_id',
});

admin.CustomerDetailsReportModel = Backbone.Model.extend({
  defaults: {
    "sales_amount":null,
    "item_total_cost":null,
    'tax_amount' : null,
    "Date" : null,
    "count" : null,
    "item_purchase" : null,
    "soldby" : null,
    "soldto" : null,
    "payment_type":null
  },
  idAttribute: 'sale_ids',
});

admin.SupplierReportModel = Backbone.Model.extend({
  defaults: {
    "preson_id":null,
    "name":null,
    "sales_amount":null,
    "item_total_cost":null,
    'tax_amount' : null,
    "sales_whole_amount" : null,
    "count" : null,
  },
  idAttribute: 'preson_id',

});

admin.SupplierDetailsReportModel = Backbone.Model.extend({
  defaults: {
    "sales_amount":null,
    "item_total_cost":null,
    'tax_amount' : null,
    "Date" : null,
    "count" : null,
    "item_purchase" : null,
    "soldby" : null,
    "soldto" : null,
    "payment_type":null
  },
  idAttribute: 'sale_ids',
});

admin.EmployeeReportModel = Backbone.Model.extend({
  defaults: {
    "preson_id":null,
    "name":null,
    "sales_amount":null,
    "item_total_cost":null,
    'tax_amount' : null,
    "sales_whole_amount" : null,
    "count" : null,
  },
  idAttribute: 'preson_id',
});

admin.EmployeeDetailsReportModel = Backbone.Model.extend({
  defaults: {
    "sales_amount":null,
    "item_total_cost":null,
    'tax_amount' : null,
    "Date" : null,
    "count" : null,
    "item_purchase" : null,
    "soldby" : null,
    "soldto" : null,
    "payment_type":null
  },
  idAttribute: 'sale_ids',
});

admin.ItemReportModel = Backbone.Model.extend({
  defaults: {
    "item_id":null,
    "item_name":null,
    "quantity_purchased":null,
    "deleted":0,
    'discount_percent' : null,
    "item_cost_price" : null,
  },
  idAttribute: 'item_id',
});

admin.ExpenseReportModel = Backbone.Model.extend({
  defaults: {
    "expense_category_id":null,
    "expense_category_name":null,
    "amount" : null,
  },
  idAttribute: 'expense_category_id',
});

admin.SalesReportModel = Backbone.Model.extend({
  defaults: {
    "sales_amount":null,
    "item_total_cost":null,
    'tax_amount' : null,
    "Date" : null,
    "count" : null,
  },
  idAttribute: 'Date',
});

admin.SalesDetailsReportModel = Backbone.Model.extend({
  defaults: {
    "sales_amount":null,
    "item_total_cost":null,
    'tax_amount' : null,
    "Date" : null,
    "count" : null,
    "item_purchase" : null,
    "soldby" : null,
    "soldto" : null,
    "payment_type":null
  },
  idAttribute: 'sale_ids',
});

admin.SalesDetailsItemReportModel = Backbone.Model.extend({
  defaults: {
    "sales_amount" : null,
    "quantity_purchased" : null,
    "item_name" : null,
    "tax_amount" : null,
  },
  idAttribute: 'item_id',
});

admin.SuspendedSalesReportModel = Backbone.Model.extend({
  defaults: {
    "sales_amount":null,
    "item_total_cost":null,
    'tax_amount' : null,
    "Date" : null,
    "count" : null,
    "item_purchase" : null,
    "soldby" : null,
    "soldto" : null,
  },
  idAttribute: 'sale_ids',
});

admin.ReceivingReportModel = Backbone.Model.extend({
  defaults: {
    "receiving_id":null,
    "item_name":null,
    "payment_type" : null,
    "payment_amount":null,
    "item_cost_price" : null,
    "company_name" : null,
    "receiving_quantity" : null,
  },
  idAttribute: 'receiving_id',
});

admin.DiscountReportModel = Backbone.Model.extend({
  defaults: {
    "count":null,
    "discount_percent":null   
  },
  idAttribute: 'discount_percent',
});

admin.ExpenseCategoryReportModel = Backbone.Model.extend({
  defaults: {
    "count":null,
    "expense_category_name":null,
    "amount":null,   
  },
  idAttribute: 'expense_category_id',
});

admin.DiscountDetailsReportModel = Backbone.Model.extend({
  defaults: {
    "sales_amount":null,
    "item_total_cost":null,
    'tax_amount' : null,
    "Date" : null,
    "count" : null,
    "item_purchase" : null,
    "soldby" : null,
    "soldto" : null,
    "payment_type":null,
  },
  idAttribute: 'sale_ids',
});

admin.PaymentReportModel = Backbone.Model.extend({
  defaults: {
    "payment_type" : null,
    "count":null,
    "sales_amount":null,
  },
  idAttribute: 'payment_type',

});

admin.TaxReportModel = Backbone.Model.extend({
  defaults: {
    "tax_name" : null,
    "item_total_cost":null,
    "count":null,
    "tax_amount":null,
    "name":null,
    "tax_rate":null,
    "sales_amount":null,
  },
  idAttribute: 'tax_name',
});

var admin = admin || {};
admin.CommissionReportModel = Backbone.Model.extend({
  defaults: {
    "commission_id":null,    
  },
  idAttribute: 'commission_id',
});

var admin = admin || {};
admin.ItemkitsReportModel = Backbone.Model.extend({
  defaults: {
    "itemkits_id":null,
  },
  idAttribute: 'itemkits_id',
});

var admin = admin || {};
admin.InventoryReportModel = Backbone.Model.extend({
  defaults: {
    "inventory_id":null,
  },
  idAttribute: 'inventory_id',
});

