
'use strict';

var appRouter;
var regex=/^[a-zA-Z\s]+$/;
var  regex1=/^[0-9/.]+$/;
var regex2= /^[0-9]{1,2}(?:\.[0-9]{1,3})?$/;
var regex3 = /^[a-zA-Z0-9\s]+$/;

    function initializeTabMenu(){
       $('#form-horizontal').steps({
        headerTag: 'h3',
        bodyTag: 'fieldset',
        transitionEffect: 'slide'
      });
      $('#form-vertical').steps({
        headerTag: 'h3',
        bodyTag: 'fieldset',
        transitionEffect: 'slide',
        stepsOrientation: 'vertical'
      });
      $('#form-tabs').steps({
        headerTag: 'h3',
        bodyTag: 'fieldset',
        transitionEffect: 'slideLeft',
        enableFinishButton: false,
        enablePagination: false,
        enableAllSteps: true,
        titleTemplate: '#title#',
        cssClass: 'tabcontrol'
      });  
    }

    function initializePopover(){
        $('[data-toggle=\'popover\']').popover({
         placement : 'top',
         html : true});
    }

$(document).ready(function() {

    $("#admin_employee").on('click',function(){
      $('.popover-content').hide();
         appRouter.navigate("employeemanagement",{trigger: true});
    });

    $("#admin_supplier").on('click',function(){
      $('.popover-content').hide();
         appRouter.navigate("suppliermanagement",{trigger: true});
    });

    $("#admin_customer").on('click',function(){
      $('.popover-content').hide();
         appRouter.navigate("customermanagement",{trigger: true});
    });

    $("#admin_item").on('click',function(){
      $('.popover-content').hide();
         appRouter.navigate("itemmanagement",{trigger: true});
    });
    $("#admin_item_kits").on('click',function(){
      $('.popover-content').hide();
         appRouter.navigate("itemkitmanagement",{trigger: true});
    });

    $("#admin_sales").on('click',function(){
      $('.popover-content').hide();
         appRouter.navigate("sales",{trigger: true});
    });
    $("#admin_company").on('click',function(){
      $('.popover-content').hide();
         appRouter.navigate("companymanagement",{trigger: true});
    });
    $("#admin_location").on('click',function(){
      $('.popover-content').hide();
         appRouter.navigate("locationmanagement",{trigger: true});
    });
     $("#admin_product").on('click',function(){
      $('.popover-content').hide();
         appRouter.navigate("productmanagement",{trigger: true});
    });
     $("#admin_expense_category").on('click',function(){
      $('.popover-content').hide();
         appRouter.navigate("expensecategorymanagement",{trigger: true});
    });
     $("#admin_expense").on('click',function(){
      $('.popover-content').hide();
         appRouter.navigate("expensemanagement",{trigger: true});
    });
    $("#admin_receiving").on('click',function(){
      $('.popover-content').hide();
         appRouter.navigate("receiving",{trigger: true});
    });

     $("#admin_suspended_sales").on('click',function(){
      $('.popover-content').hide();
         appRouter.navigate("suspendedsales",{trigger: true});
    });
     $("#report").on('click',function(){
      $('.popover-content').hide();
         appRouter.navigate("reportpage",{trigger: true});
    });

 console.log('getting ready');
    appRouter = new admin.AdminAppRouter();
    Backbone.history.start();
    console.log('View is Ready');
    
});