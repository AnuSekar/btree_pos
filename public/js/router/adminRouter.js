
var admin = admin || {};

admin.AdminAppRouter = Backbone.Router.extend({

    routes:{
       "employeemanagement" : "EmployeeManagement",
       "suppliermanagement" : "SupplierManagement",
       "customermanagement" : "CustomerManagement",
       "itemmanagement" : "ItemManagement",
       "itemkitmanagement" : "ItemKitManagement",
       "sales" : "Sales",
       "receiving" : "Receiving",
       "suspendedsales/:id" : "SuspendedSales",
       "companymanagement" : "Company",
       "expensecategorymanagement" : "ExpenseCategory",
       "productmanagement" : "Product",
       "locationmanagement" : "Location",
       "expensemanagement" : "Expense",
       "reportpage" : "Report",
       "reportList" : "ReportList",
       "companyreportList": "CompanyReportList",
       "productreportList": "ProductReportList",
       "customerreportList": "CustomerReportList",
       "supplierreportList": "SupplierReportList",
       "itemreportList": "ItemReportList",
       "expensecategoryreportList": "ExpenseCategoryReportList",
       "salesreportList": "SalesReportList",
       "suspendedsalesreportList": "SuspendedSalesReportList",
       "receivingreportList": "ReceivingReportList",
       "discountreportList": "DiscountReportList",
       "paymentreportList": "PaymentReportList",
       "taxreportList": "TaxReportList",
       "employeereportList":"EmployeeReportList",
       "lowinventory":"LowInvetory",
       "inventorysummaryreportList":"InventorySummary",
       "deaditemreportList":"DeadItem",
       "commissionreportList":"CommissionReportList",

       
    },

    initialize: function () {

    },			    


    EmployeeManagement:function () {
        appRouter.setvalue = 'employee';
        console.log("inside routing:fqlist");
        if (this.currentView) {
            console.log('currentView exist');
            this.currentView.$el.empty();
            this.currentView.$el.unbind();
        }
        this.currentView = new admin.CustomerPageView({el: $( '#page-section' )});
    },

    SupplierManagement:function () {
        appRouter.setvalue = 'supplier';
        console.log("inside routing:fqlist");
        if (this.currentView) {
            console.log('currentView exist');
            this.currentView.$el.empty();
            this.currentView.$el.unbind();
        }
        this.currentView = new admin.CustomerPageView({el: $( '#page-section' )});
    },

     CustomerManagement:function () {
        appRouter.setvalue = 'customer';
        console.log("inside routing:fqlist");
        if (this.currentView) {
            console.log('currentView exist');
            this.currentView.$el.empty();
            this.currentView.$el.unbind();
        }
        this.currentView = new admin.CustomerPageView({el: $( '#page-section' )});
    },

    ItemManagement:function(){
         console.log("inside routing:fqlist");
        if (this.currentView) {
            console.log('currentView exist');
            this.currentView.$el.empty();
            this.currentView.$el.unbind();
        }
        this.currentView = new admin.ItemPageView({el: $( '#page-section' )});
    },

    ItemKitManagement:function(){
         console.log("inside routing:fqlist");
        if (this.currentView) {
            console.log('currentView exist');
            this.currentView.$el.empty();
            this.currentView.$el.unbind();
        }
        this.currentView = new admin.ItemKitsPageView({el: $( '#page-section' )});
    },
    Sales:function(){
       if (this.currentView) {
            console.log('currentView exist');
            this.currentView.$el.empty();
            this.currentView.$el.unbind();
        }
        this.currentView = new admin.SaleFormPageView({el: $( '#page-section' ),
        mode:'sales'}); 
    },

    Receiving:function(){
       if (this.currentView) {
            console.log('currentView exist');
            this.currentView.$el.empty();
            this.currentView.$el.unbind();
        }
        this.currentView = new admin.ReceivingFormPageView({el: $( '#page-section' )}); 
    },

    SuspendedSales:function(id){
       if (this.currentView) {
            console.log('currentView exist');
            this.currentView.$el.empty();
            this.currentView.$el.unbind();
        }
        this.currentView = new admin.SaleFormPageView({el: $( '#page-section' ),
            mode:'suspendedsales',
            id: id}); 
    },

    Company:function(){
        appRouter.setvalue = 'company';
       if (this.currentView) {
            console.log('currentView exist');
            this.currentView.$el.empty();
            this.currentView.$el.unbind();
        }
        this.currentView = new admin.CompanyPageView({el: $( '#page-section' ) }); 
        
    },

    Product:function(){
        appRouter.setvalue = 'product';
       if (this.currentView) {
            console.log('currentView exist');
            this.currentView.$el.empty();
            this.currentView.$el.unbind();
        }
        this.currentView = new admin.CompanyPageView({el: $( '#page-section' )}); 
    },

    Location:function(){
       if (this.currentView) {
            console.log('currentView exist');
            this.currentView.$el.empty();
            this.currentView.$el.unbind();
        }
        this.currentView = new admin.LocationPageView({el: $( '#page-section' )}); 
    },

    ExpenseCategory:function(){
       if (this.currentView) {
            console.log('currentView exist');
            this.currentView.$el.empty();
            this.currentView.$el.unbind();
        }
        this.currentView = new admin.ExpenseCategoryPageView({el: $( '#page-section' )}); 
    },

    Expense:function(){
         console.log("inside routing:fqlist");
        if (this.currentView) {
            console.log('currentView exist');
            this.currentView.$el.empty();
            this.currentView.$el.unbind();
        }
        this.currentView = new admin.ExpensePageView({el: $( '#page-section' )});
    },

    Report: function(){

           if (this.currentView) {
                    console.log('currentView exist');
                    this.currentView.$el.empty();
                    this.currentView.$el.unbind();
            }

            this.currentView = new admin.ReportPageView({
                el: $( '#page-section' ),
                template: _.template( $( '#ReportTpl' ).html() )
            });  
    },

   ReportList : function(){

           if (this.currentView) {
                    console.log('currentView exist');
                    this.currentView.$el.empty();
                    this.currentView.$el.unbind();
            }

            this.currentView = new admin.ReportListPageView({
                el: $( '#page-section' ),
                template: _.template( $( '#ReportListTpl' ).html() )
            });  
    },

    CompanyReportList:function(){

           if (this.currentView) {
                    console.log('currentView exist');
                    this.currentView.$el.empty();
                    this.currentView.$el.unbind();
            }

            this.currentView = new admin.CompanyReportPageView({
                el: $( '#page-section' ),
                template: _.template( $( '#companyReportTpl' ).html() )
            });  
    },

    ProductReportList:function(){

           if (this.currentView) {
                    console.log('currentView exist');
                    this.currentView.$el.empty();
                    this.currentView.$el.unbind();
            }

            this.currentView = new admin.ProductReportPageView({
                el: $( '#page-section' ),
                template: _.template( $( '#productReportTpl' ).html() )
            });  
    },

    CustomerReportList:function(){

           if (this.currentView) {
                    console.log('currentView exist');
                    this.currentView.$el.empty();
                    this.currentView.$el.unbind();
            }

            this.currentView = new admin.CustomerReportPageView({
                el: $( '#page-section' ),
                template: _.template( $( '#customerReportTpl' ).html() )
            });  
    },

    SupplierReportList:function(){

           if (this.currentView) {
                    console.log('currentView exist');
                    this.currentView.$el.empty();
                    this.currentView.$el.unbind();
            }

            this.currentView = new admin.SupplierReportPageView({
                el: $( '#page-section' ),
                template: _.template( $( '#supplierReportTpl' ).html() )
            });  
    },

    ItemReportList:function(){

           if (this.currentView) {
                    console.log('currentView exist');
                    this.currentView.$el.empty();
                    this.currentView.$el.unbind();
            }

            this.currentView = new admin.ItemReportPageView({
                el: $( '#page-section' ),
                template: _.template( $( '#itemReportTpl' ).html() )
            });  
    },

    ExpenseCategoryReportList:function(){

           if (this.currentView) {
                    console.log('currentView exist');
                    this.currentView.$el.empty();
                    this.currentView.$el.unbind();
            }

            this.currentView = new admin.ExpenseCategoryReportPageView({
                el: $( '#page-section' ),
                template: _.template( $( '#expenseReportTpl' ).html() )
            });  
    },

    SalesReportList:function(){

           if (this.currentView) {
                    console.log('currentView exist');
                    this.currentView.$el.empty();
                    this.currentView.$el.unbind();
            }

            this.currentView = new admin.SalesReportPageView({
                el: $( '#page-section' ),
                template: _.template( $( '#salesReportTpl' ).html() )
            });  
    },

    SuspendedSalesReportList:function(){

           if (this.currentView) {
                    console.log('currentView exist');
                    this.currentView.$el.empty();
                    this.currentView.$el.unbind();
            }

            this.currentView = new admin.SuspendedSalesReportPageView({
                el: $( '#page-section' ),
                template: _.template( $( '#suspendedsalesReportTpl' ).html() )
            });  
    },

    ReceivingReportList:function(){

           if (this.currentView) {
                    console.log('currentView exist');
                    this.currentView.$el.empty();
                    this.currentView.$el.unbind();
            }

            this.currentView = new admin.ReceivingReportPageView({
                el: $( '#page-section' ),
                template: _.template( $( '#receivingReportTpl' ).html() )
            });  
    },

    DiscountReportList:function(){

           if (this.currentView) {
                    console.log('currentView exist');
                    this.currentView.$el.empty();
                    this.currentView.$el.unbind();
            }

            this.currentView = new admin.DiscountReportPageView({
                el: $( '#page-section' ),
                template: _.template( $( '#discountReportTpl' ).html() )
            });  
    },

    PaymentReportList:function(){

           if (this.currentView) {
                    console.log('currentView exist');
                    this.currentView.$el.empty();
                    this.currentView.$el.unbind();
            }

            this.currentView = new admin.PaymentReportPageView({
                el: $( '#page-section' ),
                template: _.template( $( '#paymentReportTpl' ).html() )
            });  
    },
    TaxReportList:function(){

           if (this.currentView) {
                    console.log('currentView exist');
                    this.currentView.$el.empty();
                    this.currentView.$el.unbind();
            }

            this.currentView = new admin.TaxReportPageView({
                el: $( '#page-section' ),
                template: _.template( $( '#taxReportTpl' ).html() )
            });  
    },
    EmployeeReportList:function(){

           if (this.currentView) {
                    console.log('currentView exist');
                    this.currentView.$el.empty();
                    this.currentView.$el.unbind();
            }

            this.currentView = new admin.EmployeeReportPageView({
                el: $( '#page-section' ),
                template: _.template( $( '#employeeReportTpl' ).html() )
            });  
    },
    LowInvetory:function(){

           if (this.currentView) {
                    console.log('currentView exist');
                    this.currentView.$el.empty();
                    this.currentView.$el.unbind();
            }

            this.currentView = new admin.LowInventoryPageView({
                el: $( '#page-section' ),
                template: _.template( $( '#lowinventoryReportTpl' ).html() )
            });  
    },
    InventorySummary:function(){
         if (this.currentView) {
                    console.log('currentView exist');
                    this.currentView.$el.empty();
                    this.currentView.$el.unbind();
            }

            this.currentView = new admin.InventorySummaryPageView({
                el: $( '#page-section' ),
                template: _.template( $( '#inventorysummaryReportTpl' ).html() )
            });   
    },
    DeadItem:function(){
         if (this.currentView) {
                    console.log('currentView exist');
                    this.currentView.$el.empty();
                    this.currentView.$el.unbind();
            }

            this.currentView = new admin.DeadItemPageView({
                el: $( '#page-section' ),
                template: _.template( $( '#deaditemReportTpl' ).html() )
            });   
    },
    CommissionReportList:function(){
        console.log("inside");
         if (this.currentView) {
                    console.log('currentView exist');
                    this.currentView.$el.empty();
                    this.currentView.$el.unbind();
            }

            this.currentView = new admin.CommissionReportPageView({
                el: $( '#page-section' ),
                template: _.template( $( '#commissionReportTpl' ).html() )
            });   
    },

});


