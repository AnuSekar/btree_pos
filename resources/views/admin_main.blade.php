<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Btree - Admin Console</title>

   <link rel="stylesheet" type="text/css" href="plugins/bootstrap/dist/css/bootstrap.css">
    <!-- Ionicons-->
    <link rel="stylesheet" type="text/css" href="plugins/Ionicons/css/ionicons.min.css">
    <!-- Malihu Scrollbar-->
    <link rel="stylesheet" type="text/css" href="plugins/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css">
    <!-- Animo.js-->
    <link rel="stylesheet" type="text/css" href="plugins/animo.js/animate-animo.min.css">

    <!-- Bootstrap Progressbar-->
    <link rel="stylesheet" type="text/css" href="plugins/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css">
    <!-- jQuery MiniColors-->
    <link rel="stylesheet" type="text/css" href="plugins/jquery-minicolors/jquery.minicolors.css">
    <!-- Bootstrap TouchSpin-->
    <link rel="stylesheet" type="text/css" href="plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css">

    <!-- Flag Icons-->
    <link rel="stylesheet" type="text/css" href="plugins/flag-icon-css/css/flag-icon.min.css">
    <!-- Jvector Map-->
    <link rel="stylesheet" type="text/css" href="plugins/jvectormap/jquery-jvectormap-2.0.3.css">
    <!-- SpinKit-->
    <link rel="stylesheet" type="text/css" href="plugins/SpinKit/css/spinners/2-double-bounce.css">
    <!-- DataTables-->
    <link rel="stylesheet" type="text/css" href="plugins/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="plugins/datatables.net-buttons-bs/css/buttons.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="plugins/datatables.net-colreorder-bs/css/colReorder.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="plugins/datatables.net-responsive-bs/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="plugins/jquery-ui/themes/black-tie/jquery-ui.css">

    <link rel="stylesheet" type="text/css" href="plugins/bootstrap/less/button-groups.less">

         <!-- Bootstrap TouchSpin-->
    <link rel="stylesheet" type="text/css" href="plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css">
     <!-- Bootstrap Date Range Picker-->
    <link rel="stylesheet" type="text/css" href="plugins/bootstrap-daterangepicker/daterangepicker.css">
    <!-- Bootstrap DatePicker-->
    <link rel="stylesheet" type="text/css" href="plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">




 <link rel="stylesheet" type="text/css" href="plugins/PACE/themes/blue/pace-theme-flash.css">
    <script type="text/javascript" src="plugins/PACE/pace.min.js"></script>
    <!-- Ionicons-->
    <link rel="stylesheet" type="text/css" href="plugins/Ionicons/css/ionicons.min.css">
    <!-- Malihu Scrollbar-->
    <link rel="stylesheet" type="text/css" href="plugins/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css">
    <!-- Animo.js-->
    <link rel="stylesheet" type="text/css" href="plugins/animo.js/animate-animo.min.css">
    <!-- Bootstrap Progressbar-->
    <link rel="stylesheet" type="text/css" href="plugins/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css">
    <!-- jQuery Steps-->
    <link rel="stylesheet" type="text/css" href="plugins/jquery.steps/demo/css/jquery.steps.css">
    <!-- Core CSS-->
    <link rel="stylesheet" type="text/css" href="styles/first-layout.css">
    <link rel="stylesheet" type="text/css" href="styles/backgrid.css">
    <link rel="stylesheet" href="styles/backgrid-paginator.css" />
    <link rel="stylesheet" href="css/bootstrap-datepicker.min.css" />
    <link rel="stylesheet" href="css/backgrid-select-all.min.css" />

    <link rel="stylesheet" href="css/ui.switchbutton.css" />
  </head>
  <body>
   <div class="alert-box success"></div>
    <div class="alert-box failure"></div>
    <div class="alert-box warning"></div>
    <!-- Header start-->
    <header>
      <div class="search-bar closed">
      </div>


        <a href="javascript:;" role="button" class="hamburger-menu pull-left"><span></span></a>
      <form class="search-form m-10 pull-right hidden-xs">
          <a href="/logout">logout</a> 
        <!-- <div class="form-group has-feedback mb-0">
          <input type="text" aria-describedby="inputSearchFor" placeholder="Search for..." style="width: 180px" class="form-control rounded"><span aria-hidden="true" class="ion-search form-control-feedback"></span><span id="inputSearchFor" class="sr-only">(default)</span>
        </div> -->
      </form>

      <a href="#"><img src="images/logo.png" width="200"></a>
 </header>
    <!-- Header end-->
    <div class="main-container">
      <!-- Main Sidebar start-->
       <aside data-mcs-theme="minimal-dark" class="main-sidebar mCustomScrollbar" >
        <div class="media user">
          <div class="media-left">

          </div>
        </div>

        <ul class="list-unstyled navigation mb-0">
  
        <li class="panel"><a role="button" data-toggle="collapse" data-parent=".navigation" href="#collapse2" aria-expanded="false" aria-controls="collapse2" class="collapsed"><i class="ion-ios-printer-outline bg-danger"></i><span class="sidebar-title">Management</span></a>
             <ul id="collapse2" class="list-unstyled collapse">
              <li><a id="admin_employee">Employee</a></li>
              <li><a id="admin_supplier">Supplier</a></li>
              <li><a id="admin_customer">Customer</a></li>             
            </ul>
          </li>

          <li class="panel"><a role="button" data-toggle="collapse" data-parent=".navigation" href="#collapse3" aria-expanded="false" aria-controls="collapse3" class="collapsed"><i class="ion-ios-printer-outline bg-danger"></i><span class="sidebar-title">Item Info</span></a>
            <ul id="collapse3" class="list-unstyled collapse">
              <li><a id="admin_item">Item</a></li>
              <li><a id="admin_item_kits">Item Kits</a></li>
              <li><a id="admin_expense">Expense</a></li>
              <li><a id="admin_expense_category">Expense Category</a></li>
           </ul>
          </li>
            <li class="panel"><a role="button" data-toggle="collapse" data-parent=".navigation" href="#collapse4" aria-expanded="false" aria-controls="collapse4" class="collapsed"><i class="ion-ios-printer-outline bg-danger"></i><span class="sidebar-title">Master</span></a>
            <ul id="collapse4" class="list-unstyled collapse">
              <li><a id="admin_product">Product Type</a></li>
              <li><a id="admin_company">Company</a></li>
              <li><a id="admin_location">Location</a></li>
            </ul>
          </li>


           <li class="panel"><a role="button" data-toggle="collapse" data-parent=".navigation" href="#collapse5" aria-expanded="false" aria-controls="collapse5" class="collapsed"><i class="ion-ios-printer-outline bg-danger"></i><span class="sidebar-title">Purchase & Sales</span></a>
            <ul id="collapse5" class="list-unstyled collapse">
              <li><a id="admin_sales">Sales</a></li>
              <li><a id="admin_receiving">Receiving</a></li>
            </ul>
          </li>

          <li class="panel"><a id="report"><i class="ion-ios-printer-outline bg-danger"></i><span class="sidebar-title">Report</span></a></li>

          
          <!--  <li class="panel"><a role="button" data-toggle="collapse" data-parent=".navigation" href="#collapse7" aria-expanded="false" aria-controls="collapse7" class="collapsed"><i class="ion-ios-printer-outline bg-danger"></i><span class="sidebar-title">Email & SMS</span></a>
            <ul id="collapse7" class="list-unstyled collapse">

              <li><a id="admin_email">Email</a></li>
              <li><a id="admin_sms">SMS </a></li>
            </ul>
          </li> -->
        </ul>
      
      </aside> 
      <!-- Main Sidebar end-->

     <div class="page-container">
        <div class="page-content container-fluid">
          <div class="row">
            <div class="col-lg-12">
              <div class="widget no-border">
                <div id="page-section"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
     </div>



      <script type="text/template" id="employeePageTpl">

                <div class="widget">
            <div class="widget-heading">
              <h3 class="widget-title">Employee List</h3>
            </div>
            <div class="widget-body">
            <form  class="form-horizontal">
                   <fieldset>                  

                      <div class="form-group">
                            <div class="col-sm-offset-8 col-sm-12" >
                             <div class="col-sm-8 col-md-2">

                             <input id="search" name="txtFirstNameBilling" type="text" class="form-control" placeholder="Search" >
                             <input type="checkbox" class="common" id="switch1" checked="checked" />

                             <a href="/btree/admin/get_download_excel?exporttype=employeepage " download="employee">DownloadExcel</a>

                             <a href="/btree/admin/get_download_pdf?exporttype=employeepage" download="employee">DownloadPdf</a>


                            </div>
                              <button type="button" id="create-employee" name="btnSubmit" class="btn btn-raised btn-black">Add</button>
                              <button id="deleteselect" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Del Select</button>
                               <button id="deleteall" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Del All</button>
                               <button id="restoreall" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Restore All</button>
                              <button id="addselect" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Add Select</button>
                            </div>
                      </div>
                      </div>
                  <div id="managementtable"></div>
                    <div id="paginator"></div>
                 
                </fieldset>
                </form>
            </div>
          </div>

        </script>

        <script type="text/template" id="employeeCreateFormTpl">

            <div class="widget">
            <div class="widget-body">
              <form id="form" class="form-horizontal">
                
                <fieldset>
                 <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="employee_first_name" class="col-sm-3 col-md-4 control-label">First Name</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="employee_first_name" name="employee_first_name" type="text" class="form-control">
                            <div id="employee_first_name_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="employee_last_name" class="col-sm-3 col-md-4 control-label"> Last Name</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="employee_last_name" name="employee_last_name" type="text" class="form-control">
                            <div id="employee_last_name_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                    
                 <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="employee_phone_number" class="col-sm-3 col-md-4 control-label">Phone Number</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="employee_phone_number" name="employee_phone_number" type="text" class="form-control">
                            <div id="employee_phone_number_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="employee_email" class="col-sm-3 col-md-4 control-label"> Email</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="employee_email" name="employee_email" type="text" class="form-control">
                            <div id="employee_email_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="employee_address_1" class="col-sm-3 col-md-4 control-label">Address - 1</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="employee_address_1" name="employee_address_1" type="text" class="form-control">
                            <div id="employee_address_1_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="employee_address_2" class="col-sm-3 col-md-4 control-label"> Address - 2</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="employee_address_2" name="employee_address_2" type="text" class="form-control">
                            <div id="employee_address_2_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="employee_city" class="col-sm-3 col-md-4 control-label">City</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="employee_city" name="employee_city" type="text" class="form-control">
                            <div id="employee_city_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="employee_state" class="col-sm-3 col-md-4 control-label"> State</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="employee_state" name="employee_state" type="text" class="form-control">
                            <div id="employee_state_error" style="color:red"></div>
                            </div>
                      </div>

                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="employee_zip" class="col-sm-3 col-md-4 control-label">Zip</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="employee_zip" name="employee_zip" type="text" class="form-control">
                            <div id="employee_zip_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="employee_country" class="col-sm-3 col-md-4 control-label"> Country</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="employee_country" name="employee_country" type="text" class="form-control">
                            <div id="employee_country_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>  

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="employee_username" class="col-sm-3 col-md-4 control-label">Username</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="employee_username" name="employee_username" type="text" class="form-control">
                            <div id="employee_username_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="employee_password" class="col-sm-3 col-md-4 control-label"> Password</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="employee_password" name="employee_password" type="text" class="form-control">
                            <div id="employee_password_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>  
                 
                </fieldset>
              </form>
            </div>
          </div>

        </script>

        <script type="text/template" id="employeeEditFormTpl">

            <div class="widget">
            <div class="widget-body">
              <form id="form" class="form-horizontal">
                
                <fieldset>
                <input id="person_id" type="hidden" class="form-control" value="<%=person_id%>" />
                 <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="employee_first_name" class="col-sm-3 col-md-4 control-label">First Name</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="employee_first_name" autocomplete="off" value="<%=first_name%>" name="employee_first_name" type="text" class="form-control">
                            <div id="employee_first_name_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="employee_last_name" class="col-sm-3 col-md-4 control-label"> Last Name</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="employee_last_name"  value="<%=last_name%>" name="employee_last_name" type="text" class="form-control">
                            <div id="employee_last_name_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                    
                 <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="employee_phone_number" class="col-sm-3 col-md-4 control-label">Phone Number</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="employee_phone_number" value="<%=phone_number%>" name="employee_phone_number" type="text" class="form-control">
                            <div id="employee_phone_number_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="employee_email" class="col-sm-3 col-md-4 control-label"> Email</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="employee_email" value="<%=email%>" name="employee_email" type="text" class="form-control">
                            <div id="employee_email_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="employee_address_1" class="col-sm-3 col-md-4 control-label">Address - 1</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="employee_address_1" value="<%=address_1%>" name="employee_address_1" type="text" class="form-control">
                            <div id="employee_address_1_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="employee_address_2" class="col-sm-3 col-md-4 control-label"> Address - 2</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="employee_address_2"  value="<%=address_2%>" name="employee_address_2" type="text" class="form-control">
                            <div id="employee_address_2_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="employee_city" class="col-sm-3 col-md-4 control-label">City</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="employee_city" value="<%=city%>" name="employee_city" type="text" class="form-control">
                            <div id="employee_city_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="employee_state" class="col-sm-3 col-md-4 control-label"> State</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="employee_state" value="<%=state%>" name="employee_state" type="text" class="form-control">
                            <div id="employee_state_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="employee_zip" class="col-sm-3 col-md-4 control-label">Zip</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="employee_zip"  value="<%=zip%>" name="employee_zip" type="text" class="form-control">
                            <div id="employee_zip_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="employee_country" class="col-sm-3 col-md-4 control-label"> Country</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="employee_country" value="<%=country%>" name="employee_country" type="text" class="form-control">
                            <div id="employee_country_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>  

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="employee_username" class="col-sm-3 col-md-4 control-label">Username</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="employee_username" value="<%=username%>" name="employee_username" type="text" class="form-control">
                            <div id="employee_username_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="employee_password" class="col-sm-3 col-md-4 control-label"> Password</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="employee_password" value="<%=password%>" name="employee_password" type="text" class="form-control">
                            <div id="employee_password_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>  
                 
                </fieldset>
              </form>
            </div>
          </div>

        </script>

        <script type="text/template" id="customerPageTpl">

                <div class="widget">
            <div class="widget-heading">
              <h3 class="widget-title">Customer List</h3>
            </div>
            <div class="widget-body">
            <form  class="form-horizontal">
                   <fieldset>                  

                      <div class="form-group">
                            <div class="col-sm-offset-8 col-sm-12" >
                             <div class="col-sm-8 col-md-2">

                              <input id="search" name="txtFirstNameBilling" type="text" class="form-control" placeholder="Search" >
                              <input type="checkbox" class="common" id="switch1" checked="checked" />

                              <a href="/btree/admin/get_download_excel?exporttype=customerpage " download="customer">DownloadExcel</a>

                              <a href="/btree/admin/get_download_pdf?exporttype=customerpage" download="customer">DownloadPdf</a>

                            </div>
                              <button type="button" id="create-customer" name="btnSubmit" class="btn btn-raised btn-black">Add</button> <button id="deleteselect" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Del Select</button><button id="deleteall" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Del All</button>
                              <button id="restoreall" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Restore All</button>
                              <button id="addselect" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Add Select</button> 
                            </div>
                      </div>


                  <table id="example-1" cellspacing="0" width="100%" class="table table-striped table-bordered">
                    
                    <div id="findStatus"></div>
                  </table>   

                      </div>

                 <div id="managementtable"></div>
                    <div id="paginator"></div>
                </fieldset>
                </form>
            </div>
          </div>
           <div class="modal" id="claimModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Points to be redemed</h4>
        </div>
       <div class="modal-body">
          <div id="showpoints"></div>
           <div id="showpoints_error" style="color:red"></div>
          <br>
          <div class="row" id="claimptsdiv">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="employee_username" class="col-sm-3 col-md-4 control-label">Enter Points </label>
                        <div class="col-sm-5 col-md-5">
                           <input id="claimpoints" name="claimpoints" type="text"class="form-control" />
                            <div id="claimpoints_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

         
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="submit_claim">Submit</button>
          <button type="button" class="btn btn-default" id="close_claim">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
        </script>

        <script type="text/template" id="customerCreateFormTpl">

            <div class="widget">
            
            <div class="widget-body">
              <form id="form" class="form-horizontal">
                
                <fieldset>
                 <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="customer_first_name" class="col-sm-3 col-md-4 control-label">First Name</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="customer_first_name" name="customer_first_name" type="text" class="form-control">
                            <div id="customer_first_name_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="customer_last_name" class="col-sm-3 col-md-4 control-label"> Last Name</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="customer_last_name" name="customer_last_name" type="text" class="form-control">
                            <div id="customer_last_name_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                    
                 <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="customer_phone_number" class="col-sm-3 col-md-4 control-label">Phone Number</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="customer_phone_number" name="customer_phone_number" type="text" class="form-control">
                            <div id="customer_phone_number_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="customer_email" class="col-sm-3 col-md-4 control-label"> Email</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="customer_email" name="customer_email" type="text" class="form-control">
                            <div id="customer_email_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="customer_address_1" class="col-sm-3 col-md-4 control-label">Address - 1</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="customer_address_1" name="customer_address_1" type="text" class="form-control">
                            <div id="customer_address_1_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="customer_address_2" class="col-sm-3 col-md-4 control-label"> Address - 2</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="customer_address_2" name="customer_address_2" type="text" class="form-control">
                            <div id="customer_address_2_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="customer_city" class="col-sm-3 col-md-4 control-label">City</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="customer_city" name="customer_city" type="text" class="form-control">
                            <div id="customer_city_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="customer_state" class="col-sm-3 col-md-4 control-label"> State</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="customer_state" name="customer_state" type="text" class="form-control">
                            <div id="customer_state_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="customer_zip" class="col-sm-3 col-md-4 control-label">Zip</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="customer_zip" name="customer_zip" type="text" class="form-control">
                            <div id="customer_zip_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="customer_country" class="col-sm-3 col-md-4 control-label"> Country</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="customer_country" name="customer_country" type="text" class="form-control">
                            <div id="customer_country_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>  

                   <div class="row">
                    <div class="col-md-12">

                        
                    </div>
                  </div>
                 
                </fieldset>
              </form>
            </div>
          </div>

        </script>


        <script type="text/template" id="customerEditFormTpl">

            <div class="widget">
            <div class="widget-body">
              <form id="form" class="form-horizontal">
                
                <fieldset>
                 <input id="person_id" type="hidden" class="form-control" value="<%=person_id%>" />
                 <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="customer_first_name" class="col-sm-3 col-md-4 control-label">First Name</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="customer_first_name"  value="<%=first_name%>" name="customer_first_name" type="text" class="form-control">
                            <div id="customer_first_name_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="customer_last_name" class="col-sm-3 col-md-4 control-label"> Last Name</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="customer_last_name"  value="<%=last_name%>" name="customer_last_name" type="text" class="form-control">
                            <div id="customer_last_name_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                    
                 <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="customer_phone_number" class="col-sm-3 col-md-4 control-label">Phone Number</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="customer_phone_number" value="<%=phone_number%>" name="customer_phone_number" type="text" class="form-control">
                            <div id="customer_phone_number_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="customer_email" class="col-sm-3 col-md-4 control-label"> Email</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="customer_email" value="<%=email%>" name="customer_email" type="text" class="form-control">
                            <div id="customer_email_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="customer_address_1" class="col-sm-3 col-md-4 control-label">Address - 1</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="customer_address_1" value="<%=address_1%>" name="customer_address_1" type="text" class="form-control">
                            <div id="customer_address_1_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="customer_address_2" class="col-sm-3 col-md-4 control-label"> Address - 2</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="customer_address_2"  value="<%=address_2%>" name="customer_address_2" type="text" class="form-control">
                            <div id="customer_address_2_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="customer_city" class="col-sm-3 col-md-4 control-label">City</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="customer_city" value="<%=city%>" name="customer_city" type="text" class="form-control">
                            <div id="customer_city_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="customer_state" class="col-sm-3 col-md-4 control-label"> State</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="customer_state" value="<%=state%>" name="customer_state" type="text" class="form-control">
                            <div id="customer_state_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="customer_zip" class="col-sm-3 col-md-4 control-label">Zip</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="customer_zip"  value="<%=zip%>" name="customer_zip" type="text" class="form-control">
                            <div id="customer_zip_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="customer_country" class="col-sm-3 col-md-4 control-label"> Country</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="customer_country" value="<%=country%>" name="customer_country" type="text" class="form-control">
                            <div id="customer_country_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>  
                 
                </fieldset>
              </form>
            </div>
          </div>

        </script>


        <script type="text/template" id="itemPageTpl">

                <div class="widget">
            <div class="widget-heading">
              <h3 class="widget-title">Item List</h3>
            </div>
            <div class="widget-body">
            <form  class="form-horizontal">
                   <fieldset>                  

                      <div class="form-group">
                            <div class="col-sm-offset-8 col-sm-12" >
                             <div class="col-sm-8 col-md-2">

                             <input id="item_search" name="txtFirstNameBilling" type="text" class="form-control" placeholder="Search" >
                             <input type="checkbox" class="common" id="switch1" checked="checked" />

                              <a href="/btree/admin/get_download_excel?exporttype=itempage " download="item">DownloadExcel</a>

                              <a href="/btree/admin/get_download_pdf?exporttype=itempage" download="item">DownloadPdf</a>


                            </div>
                              <button type="button" id="create-item" name="btnSubmit" class="btn btn-raised btn-black">Add</button><button id="item_deleteselect" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Del Select</button>
                               <button id="item_deleteall" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Del All</button>
                               <button id="item_restoreall" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Restore All</button>
                              <button id="item_addselect" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Add Select</button> 
                            </div>
                      </div>
                    <div id="itemtable"></div>
                    <div id="paginator"></div>
                 
                </fieldset>
                </form>
            </div>
          </div>

        </script>

         <script type="text/template" id="itemTemplate">

                        <td><%=item_name%></td>
                        <td><%=company_name%></td>
                        <td><%=product_name%></td>
                        <td><%=cost_price%></td>
                        <td><%=unit_price%></td>
                       
                        <td>
                            <span style="display:inline-block; width: 20px;">
                            <a id="edit-item" data-toggle="tooltip" data-placement="top" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a> 
                            </span>
                            <span style="display:inline-block; width: 20px;">
                            <a id="del-item" data-toggle="tooltip" data-placement="top" title="Delete"><i class="glyphicon glyphicon-minus-sign"></i></a>
                            </span>
                        </td>
        </script>

        <script type="text/template" id="itemCreateFormTpl">

            <div class="widget">
            <div class="widget-body">
              <form id="form" class="form-horizontal">
                
                <fieldset>
                 <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_name" class="col-sm-3 col-md-4 control-label">Item Name</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="item_name" name="item_name" type="text" class="form-control">
                            <div id="item_name_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_number" class="col-sm-3 col-md-4 control-label">Item Number</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="item_number" name="item_number" type="text" class="form-control">
                            <div id="item_number_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                 </div>
                    
                 <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_company" class="col-sm-3 col-md-4 control-label">Company</label>
                        <div class="col-sm-5 col-md-5">
                              <select id="item_company" name="item_company" class="form-control">
                                  <option value="">--Please Select--</option>
                           </select>     
                           <div id="item_company_error" style="color:red"></div>            
                            </div>
                      </div>
                  </div>
                    
                  <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_product" class="col-sm-3 col-md-4 control-label">Product</label>
                        <div class="col-sm-5 col-md-5">
                              <select id="item_product" name="item_product" class="form-control">
                                  <option value="">--Please Select--</option>
                           </select>     
                           <div id="item_product_error" style="color:red"></div>            
                            </div>
                      </div>
                    </div>
                   
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="Supplier" class="col-sm-3 col-md-4 control-label">Supplier</label>
                        <div class="col-sm-5 col-md-5">
                              <select id="item_supplier" name="item_supplier" class="form-control">
                                  <option value="">--Please Select--</option>
                           </select>     
                           <div id="item_supplier_error" style="color:red"></div>            
                            </div>
                      </div>
                    </div>
                     <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_description" class="col-sm-3 col-md-4 control-label">Description</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="item_description" name="item_description" type="text" class="form-control">
                            <div id="item_description_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div> 

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_cost_price" class="col-sm-3 col-md-4 control-label">Cost Price</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="item_cost_price" name="item_cost_price" type="text" class="form-control">
                            <div id="item_cost_price_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_unit_price" class="col-sm-3 col-md-4 control-label"> Unit Price</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="item_unit_price" name="item_unit_price" type="text" class="form-control">
                            <div id="item_unit_price_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_reorder_level" class="col-sm-3 col-md-4 control-label">Reorder Level</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="item_reorder_level" name="item_reorder_level" type="text" class="form-control">
                            <div id="item_reorder_level_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_receiving_quantity" class="col-sm-3 col-md-4 control-label"> Receiving Quantity</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="item_receiving_quantity" name="item_receiving_quantity" type="text" class="form-control">
                            <div id="item_receiving_quantity_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_allow_alt_description" class="col-sm-3 col-md-4 control-label">Commission</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="item_commission" name="item_commission" type="text" class="form-control">
                            <div id="item_commission_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                  <!--<div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_allow_alt_description" class="col-sm-3 col-md-4 control-label">Pic</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="item_allow_alt_description" name="item_allow_alt_description" type="text" class="form-control">
                            <div id="item_allow_alt_description_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_is_serialized" class="col-sm-3 col-md-4 control-label"> Is_Serialized</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="item_is_serialized" name="item_is_serialized" type="text" class="form-control">
                            <div id="item_is_serialized_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>   -->

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_deleted" class="col-sm-3 col-md-4 control-label">Tax Name1</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="item_tax_name1" name="item_tax_name" type="text" class="form-control">
                            <div id="item_tax_name1_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_percent" class="col-sm-3 col-md-4 control-label"> Tax Percent1</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="item_percent1" name="item_percent" type="text" class="form-control">
                            <div id="item_percent1_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div> 

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_deleted" class="col-sm-3 col-md-4 control-label">Tax Name2</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="item_tax_name2" name="item_tax_name" type="text" class="form-control">
                            <div id="item_tax_name2_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_percent2" class="col-sm-3 col-md-4 control-label"> Tax Percent2</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="item_percent2" name="item_percent" type="text" class="form-control">
                            <div id="item_percent2_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div> 

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_deleted" class="col-sm-3 col-md-4 control-label">Tax Name3</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="item_tax_name3" name="item_tax_name" type="text" class="form-control">
                            <div id="item_tax_name3_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_percent3" class="col-sm-3 col-md-4 control-label"> Tax Percent3</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="item_percent" name="item_percent" type="text" class="form-control">
                            <div id="item_percent3_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div> 
                 
                </fieldset>
              </form>
            </div>
          </div>

        </script>

        <script type="text/template" id="itemkitCreateFormTpl">

            <div class="widget">
            <div class="widget-body">
              <form id="form" class="form-horizontal">
                
                <fieldset>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="item_name_kit" class="col-sm-3 col-md-4 control-label">Item Name</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="item_name_kit" name="item_name_kit" type="text" class="form-control">
                            <div id="item_name_kit_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                        <label for="item_kit_discount" class="col-sm-3 col-md-4 control-label">Discount</label>
                        <div class="col-sm-5 col-md-5">
                              <input id="item_kit_discount" name="item_kit_discount" type="text" class="form-control">    
                           <div id="item_kit_discount_error" style="color:red"></div>            
                            </div>
                      </div>
                  </div>
                  </div>

                  <div class="row">  
                  <div class="col-md-12">
                      <div class="form-group">
                        <label for="add_item" class="col-sm-3 col-md-4 control-label">Add Item</label>
                        <div class="col-sm-5 col-md-5">
                               <input id="add_item" type="text" class="form-control">   
                               <div id="item_name_search"  style="margin-top:10px;"></div> 
                           <div id="item_kit_discount_error" style="color:red"></div>            
                            </div>
                      </div>
                  </div>
                  </div>
                  <div class="row">
                    <table >
                      <tr>
                        <th>Delete</th>
                        <th>Item ID</th>
                        <th>Item Name</th>
                        <th>Quantity</th>
                      </tr>
                      <tbody id="list_item_kit"></tbody>
                    </table>
                  </div>
                </fieldset>
              </form>
            </div>
          </div>

        </script>


        <script type="text/template" id="itemkitEditFormTpl">

            <div class="widget">
            <div class="widget-body">
              <form id="form" class="form-horizontal">
                
                <fieldset>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="item_name_kit" class="col-sm-3 col-md-4 control-label">Item Name</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="item_name_kit" name="item_name_kit" value="<%=item_kit_name%>" type="text" class="form-control">
                            <div id="item_name_kit_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                        <label for="item_kit_discount" class="col-sm-3 col-md-4 control-label">Discount</label>
                        <div class="col-sm-5 col-md-5">
                              <input id="item_kit_discount" value="<%=item_kit_discount%>" name="item_kit_discount" type="text" class="form-control">    
                           <div id="item_kit_discount_error" style="color:red"></div>            
                            </div>
                      </div>
                  </div>
                  </div>

                  <div class="row">  
                  <div class="col-md-12">
                      <div class="form-group">
                        <label for="add_item" class="col-sm-3 col-md-4 control-label">Add Item</label>
                        <div class="col-sm-5 col-md-5">
                               <input id="add_item" type="text" class="form-control">   
                               <div id="item_name_search"  style="margin-top:10px;"></div> 
                           <div id="item_kit_discount_error" style="color:red"></div>            
                            </div>
                      </div>
                  </div>
                  </div>
                  <div class="row">
                    <table >
                      <tr>
                        <th>Delete</th>
                        <th>Item ID</th>
                        <th>Item Name</th>
                        <th>Quantity</th>
                      </tr>
                      <tbody id="list_item_kit"></tbody>
                    </table>
                  </div>
                </fieldset>
              </form>
            </div>
          </div>

        </script>


        <script type="text/template" id="itemEditFormTpl">

            <div class="widget">
            <div class="widget-body">
              <form id="form" class="form-horizontal">
                
                <fieldset>
                <input id="item_id" type="hidden" class="form-control" value="<%=item_id%>" />
                 <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_name" class="col-sm-3 col-md-4 control-label">Item Name</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="item_name" value="<%=item_name%>" name="item_name" type="text" class="form-control">
                            <div id="item_name_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_number" class="col-sm-3 col-md-4 control-label">Item Number</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="item_number" name="item_number"  value="<%=item_number%>"  type="text" class="form-control">
                            <div id="item_number_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                 </div>
                    
                 <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_company" class="col-sm-3 col-md-4 control-label">Company</label>
                        <div class="col-sm-5 col-md-5">
                              <select id="item_company" name="item_company" class="form-control">
                                  <option value="">--Please Select--</option>
                           </select>     
                           <div id="item_company_error" style="color:red"></div>            
                            </div>
                      </div>
                  </div>
                    
                  <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_product" class="col-sm-3 col-md-4 control-label">Product</label>
                        <div class="col-sm-5 col-md-5">
                              <select id="item_product" name="item_product" class="form-control">
                                  <option value="">--Please Select--</option>
                           </select>     
                           <div id="item_product_error" style="color:red"></div>            
                            </div>
                      </div>
                    </div>
                   
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="Supplier" class="col-sm-3 col-md-4 control-label">Supplier</label>
                        <div class="col-sm-5 col-md-5">
                              <select id="item_supplier" name="item_supplier" class="form-control">
                                  <option value="">--Please Select--</option>
                           </select>     
                           <div id="item_supplier_error" style="color:red"></div>            
                            </div>
                      </div>
                    </div>
                     <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_description" class="col-sm-3 col-md-4 control-label">Description</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="item_description" name="item_description" value="<%=description%>"  type="text" class="form-control">
                            <div id="item_description_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div> 

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_cost_price" class="col-sm-3 col-md-4 control-label">Cost Price</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="item_cost_price" value="<%=cost_price%>" name="item_cost_price" type="text" class="form-control">
                            <div id="item_cost_price_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_unit_price" class="col-sm-3 col-md-4 control-label">Unit Price</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="item_unit_price" value="<%=unit_price%>" name="item_unit_price" type="text" class="form-control">
                            <div id="item_unit_price_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_reorder_level" class="col-sm-3 col-md-4 control-label">Reorder Level</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="item_reorder_level" value="<%=reorder_level%>" name="item_reorder_level" type="text" class="form-control">
                            <div id="item_reorder_level_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_receiving_quantity" class="col-sm-3 col-md-4 control-label"> Receiving Quantity</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="item_receiving_quantity" value="<%=receiving_quantity%>" name="item_receiving_quantity" type="text" class="form-control">
                            <div id="item_receiving_quantity_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_commission" class="col-sm-3 col-md-4 control-label">Commission</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="item_commission" value="<%=commission%>" name="item_commission" type="text" class="form-control">
                            <div id="item_commission_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                  <!-- <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_allow_alt_description" class="col-sm-3 col-md-4 control-label">Zip</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="item_allow_alt_description" value="<%=allow_alt_description%>" name="item_allow_alt_description" type="text" class="form-control">
                            <div id="item_allow_alt_description_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_is_serialized" class="col-sm-3 col-md-4 control-label"> Country</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="item_is_serialized" value="<%=is_serialized%>" name="item_is_serialized" type="text" class="form-control">
                            <div id="item_is_serialized_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>   -->

                   <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_deleted" class="col-sm-3 col-md-4 control-label">Tax Name1</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="item_tax_name1" name="item_tax_name" value="<%=tax_name1%>" type="text" class="form-control">
                            <div id="item_tax_name1_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_percent" class="col-sm-3 col-md-4 control-label"> Tax Percent1</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="item_percent1" name="percent" value="<%=tax_percent1%>" type="text" class="form-control">
                            <div id="item_percent1_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div> 

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_deleted" class="col-sm-3 col-md-4 control-label">Tax Name2</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="item_tax_name2" name="item_tax_name" value="<%=tax_name2%>" type="text" class="form-control">
                            <div id="item_tax_name2_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_percent2" class="col-sm-3 col-md-4 control-label"> Tax Percent2</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="item_percent2" name="percent" value="<%=tax_percent2%>" type="text" class="form-control">
                            <div id="item_percent2_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div> 

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_deleted" class="col-sm-3 col-md-4 control-label">Tax Name3</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="item_tax_name3" name="item_tax_name" value="<%=tax_name3%>" type="text" class="form-control">
                            <div id="item_tax_name3_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="item_percent3" class="col-sm-3 col-md-4 control-label"> Tax Percent3</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="item_percent3" name="percent" value="<%=tax_percent3%>" type="text" class="form-control">
                            <div id="item_percent3_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div> 
                 
                </fieldset>
              </form>
            </div>
          </div>

        </script>


         <script type="text/template" id="supplierPageTpl">

                <div class="widget">
            <div class="widget-heading">
              <h3 class="widget-title">Supplier List</h3>
            </div>
            <div class="widget-body">
            <form  class="form-horizontal">
                   <fieldset>                  

                      <div class="form-group">
                            <div class="col-sm-offset-8 col-sm-12" >
                             <div class="col-sm-8 col-md-2">

                             <input id="search" name="txtFirstNameBilling" type="text" class="form-control" placeholder="Search" >
                             <input type="checkbox" class="common" id="switch1" checked="checked" />

                             <a href="/btree/admin/get_download_excel?exporttype=supplierpage " download="supplier">DownloadExcel</a>

                             <a href="/btree/admin/get_download_pdf?exporttype=supplierpage" download="supplier">DownloadPdf</a>


                            </div>
                              <button type="button" id="create-supplier" name="btnSubmit" class="btn btn-raised btn-black">Add</button> 
                              <button id="deleteselect" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Del Select</button><button id="deleteall" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Del All</button>
                              <button id="restoreall" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Restore All</button>
                              <button id="addselect" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Add Select</button>
                            </div>
                      </div>
                      <div id="managementtable"></div>
                    <div id="paginator"></div>
                 
                </fieldset>
                </form>
            </div>
          </div>

        </script>

         <script type="text/template" id="supplierTemplate">

                        <td><%=first_name%> &nbsp; <%=last_name%></td>
                        <td><%=phone_number%></td>
                        <td><%=city%></td>
                        <td><%=company_name%></td>
                        <td><%=email%></td>
                       
                        <td>
                            <span style="display:inline-block; width: 20px;">
                            <a id="edit-supplier" data-toggle="tooltip" data-placement="top" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a> 
                            </span>
                            <span style="display:inline-block; width: 20px;">
                            <a id="del-supplier" data-toggle="tooltip" data-placement="top" title="Delete"><i class="glyphicon glyphicon-minus-sign"></i></a>
                            </span>
                        </td>
        </script>


        <script type="text/template" id="supplierCreateFormTpl">

            <div class="widget">
            <div class="widget-body">
              <form id="form" class="form-horizontal">
                
                <fieldset>
                 <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="supplier_first_name" class="col-sm-3 col-md-4 control-label">First Name</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="supplier_first_name" name="supplier_first_name" type="text" class="form-control">
                            <div id="supplier_first_name_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="supplier_last_name" class="col-sm-3 col-md-4 control-label"> Last Name</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="supplier_last_name" name="supplier_last_name" type="text" class="form-control">
                            <div id="supplier_last_name_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                    
                 <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="supplier_phone_number" class="col-sm-3 col-md-4 control-label">Phone Number</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="supplier_phone_number" name="supplier_phone_number" type="text" class="form-control">
                            <div id="supplier_phone_number_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="supplier_email" class="col-sm-3 col-md-4 control-label"> Email</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="supplier_email" name="supplier_email" type="text" class="form-control">
                            <div id="supplier_email_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="supplier_address_1" class="col-sm-3 col-md-4 control-label">Address - 1</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="supplier_address_1" name="supplier_address_1" type="text" class="form-control">
                            <div id="supplier_address_1_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="supplier_address_2" class="col-sm-3 col-md-4 control-label"> Address - 2</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="supplier_address_2" name="supplier_address_2" type="text" class="form-control">
                            <div id="supplier_address_2_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="supplier_city" class="col-sm-3 col-md-4 control-label">City</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="supplier_city" name="supplier_city" type="text" class="form-control">
                            <div id="supplier_city_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="supplier_state" class="col-sm-3 col-md-4 control-label"> State</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="supplier_state" name="supplier_state" type="text" class="form-control">
                            <div id="supplier_state_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="supplier_zip" class="col-sm-3 col-md-4 control-label">Zip</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="supplier_zip" name="supplier_zip" type="text" class="form-control">
                            <div id="supplier_zip_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="supplier_country" class="col-sm-3 col-md-4 control-label"> Country</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="supplier_country" name="supplier_country" type="text" class="form-control">
                            <div id="supplier_country_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>  

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="supplier_username" class="col-sm-3 col-md-4 control-label">Company Name</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="supplier_company_name" name="supplier_company_name" type="text" class="form-control">
                            <div id="supplier_company_name_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="supplier_password" class="col-sm-3 col-md-4 control-label">Account Number</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="supplier_account_number" name="supplier_account_number" type="text" class="form-control">
                            <div id="supplier_account_number_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>  

                   <div class="col-md-6">
                      <div class="form-group">
                        <label for="supplier_agency_name" class="col-sm-3 col-md-4 control-label">  Agency Name</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="supplier_agency_name" name="supplier_agency_name" type="text" class="form-control">
                            <div id="supplier_agency_name_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>  
                 
                </fieldset>
              </form>
            </div>
          </div>

        </script>

        <script type="text/template" id="supplierEditFormTpl">

            <div class="widget">
           
            <div class="widget-body">
              <form id="form" class="form-horizontal">
                
                <fieldset>
                 <input id="person_id" type="hidden" class="form-control" value="<%=person_id%>" />
                 <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="supplier_first_name" class="col-sm-3 col-md-4 control-label">First Name</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="supplier_first_name"  value="<%=first_name%>" name="supplier_first_name" type="text" class="form-control">
                            <div id="supplier_first_name_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="supplier_last_name" class="col-sm-3 col-md-4 control-label"> Last Name</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="supplier_last_name"  value="<%=last_name%>" name="supplier_last_name" type="text" class="form-control">
                            <div id="supplier_last_name_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                    
                 <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="supplier_phone_number" class="col-sm-3 col-md-4 control-label">Phone Number</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="supplier_phone_number" value="<%=phone_number%>" name="supplier_phone_number" type="text" class="form-control">
                            <div id="supplier_phone_number_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="supplier_email" class="col-sm-3 col-md-4 control-label"> Email</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="supplier_email" value="<%=email%>" name="supplier_email" type="text" class="form-control">
                            <div id="supplier_email_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="supplier_address_1" class="col-sm-3 col-md-4 control-label">Address - 1</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="supplier_address_1" value="<%=address_1%>" name="supplier_address_1" type="text" class="form-control">
                            <div id="supplier_address_1_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="supplier_address_2" class="col-sm-3 col-md-4 control-label"> Address - 2</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="supplier_address_2"  value="<%=address_2%>" name="supplier_address_2" type="text" class="form-control">
                            <div id="supplier_address_2_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="supplier_city" class="col-sm-3 col-md-4 control-label">City</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="supplier_city" value="<%=city%>" name="supplier_city" type="text" class="form-control">
                            <div id="supplier_city_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="supplier_state" class="col-sm-3 col-md-4 control-label"> State</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="supplier_state" value="<%=state%>" name="supplier_state" type="text" class="form-control">
                            <div id="supplier_state_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="supplier_zip" class="col-sm-3 col-md-4 control-label">Zip</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="supplier_zip"  value="<%=zip%>" name="supplier_zip" type="text" class="form-control">
                            <div id="supplier_zip_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="supplier_country" class="col-sm-3 col-md-4 control-label"> Country</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="supplier_country" value="<%=country%>" name="supplier_country" type="text" class="form-control">
                            <div id="supplier_country_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>  

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="supplier_username" class="col-sm-3 col-md-4 control-label">Company Name</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="supplier_company_name" value="<%=company_name%>" name="supplier_company_name" type="text" class="form-control">
                            <div id="supplier_company_name_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="supplier_agency_name" class="col-sm-3 col-md-4 control-label">  Agency Name</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="supplier_agency_name" value="<%=agency_name%>" name="supplier_agency_name" type="text" class="form-control">
                            <div id="supplier_agency_name_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>  

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="supplier_password" class="col-sm-3 col-md-4 control-label">Account Number</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="supplier_account_number" value="<%=account_number%>" name="supplier_account_number" type="text" class="form-control">
                            <div id="supplier_account_number_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>  

                   
                 
                </fieldset>
              </form>
            </div>
          </div>

        </script>

        <script type="text/template" id="sales">
        
        <div class="row">
            <div class="col-lg-12">
              <div class="widget">
                <div class="widget-heading">
                  <h3 class="widget-title">Add Sales</h3>
                </div>
                <div class="widget-body">
                  <form>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="item_name">Enter Item</label>
                          <input id="item_name" type="text" class="form-control">
                          <div id="item_name_search"  style="margin-top:10px;"></div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="ddlGengeHor" class="col-sm-3 control-label">Type</label>
                          <div class="col-sm-9">
                            <select id="sales_type" class="form-control">
                              <option value="1">Sales</option>
                              <option value="2">Return</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <button type="submit" id="list_suspended_sales" class="btn btn-black">List Suspended</button>
                    </div>
                  <div class="col-md-8">  
                    <table class="table table-responsive">
                    <thead>
                      <tr>
                        <th style="width: 17%">Item Name</th>
                        <th style="width: 25%">Price</th>
                        <th style="width: 23%">Quantity</th>
                        <th style="width: 23%">Discount%</th>
                        <th style="width: 15%">Total</th>
                        <th style="width: 20%">Delete</th>
                      </tr>
                    </thead>

                    <tbody id="sales_item_list">

                    </tbody>
                  </table>
                </div>
                <div></div>
           <div class="col-md-4">
                <div class="widget-body">
                  <form class="form-horizontal">
                  <div class="row">
                    <div class="form-group">
                      <label for="customer_name" class="col-sm-3 control-label">Customer Name</label>
                      <div class="col-sm-8">
                        <input id="customer_name" type="text" class="form-control">
                        <input type="hidden" id="person_cust_id">
                      </div>
                    </div>
                   </div> <br>
                    <div class="row">
                    <div class="form-group">
                      <label for="discount_percent" class="col-sm-3 control-label">Discount percent</label>
                      <div class="col-sm-8">
                        <input id="discount_percent" type="text" class="form-control">
                      </div>
                    </div>
                    </div><br>
                     <div class="row">
                    <div class="form-group">
                      <label for="discount_amount" class="col-sm-3 control-label">Discount Amount</label>
                      <div class="col-sm-8">
                        <input id="discount_amount" type="text" class="form-control">
                      </div>
                    </div>
                    </div><br>
                     <div class="row">
                    <div class="form-group">
                      <label for="sales_sub_total" class="col-sm-3 control-label">Sub Total</label>
                      <div class="col-sm-8">
                        <input id="sales_sub_total" type="text" class="form-control">
                      </div>
                    </div>
                    </div><br>
                    <br>
                     <div class="row">
                    <div class="form-group">
                      <label for="sales_sub_total" class="col-sm-3 control-label">Tax Amount</label>
                      <div class="col-sm-8">
                        <div id="tax_amount_list"></div>
                      </div>
                    </div>
                    </div>
                     <div class="row">
                    <div class="form-group">
                      <label for="sales_total" class="col-sm-3 control-label"> Total</label>
                      <div class="col-sm-8">
                        <input id="sales_total" type="text" class="form-control">
                      </div>
                    </div>
                    </div><br>
                    <div class="row">
                    <div class="form-group">
                      <label for="sales_total" class="col-sm-3 control-label"> Payment</label>
                      <div class="col-sm-8">
                        <select id="paymenttype">
                      <option value="cash">Cash</option>
                      <option value="credit">Credit</option>
                      <option value="debit">Debit</option>
                      </select>
                      </div>
                      
                    </div>
                    </div><br>
                     <div class="row">
                    <div class="form-group">
                      <label for="sales_amount" class="col-sm-3 control-label">Amount</label>
                      <div class="col-sm-8">
                        <input id="sales_amount" type="text" class="form-control">
                      </div>
                    </div>
                    </div><br>
                    <div class="row">
                    <div class="form-group">
                      <label for="sales_amount" class="col-sm-3 control-label">Amount Due</label>
                      </div>
                    </div>
                  </form>
                </div>
              
             </div> 

            <div class="row">
               <div class="col-lg-12">
                  <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9 col-md-offset-2 col-md-10">
                      <button type="submit" id="add_sales" class="btn btn-black">Add Sales</button>
                      <button type="submit" id="remove_all_sales" class="btn btn-black">Remove All</button>
                      <button type="submit" id="suspended_sales" class="btn btn-black">Suspended Sales</button>
                    </div>
                  </div>
                </div>
              </div>   

              <div class="modal fade" id="salesModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">List of Sale Item</h4>
        </div>
       <div class="modal-body">
          <div id="listsales"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

        </script>

         <script type="text/template" id="receiving">
        
        <div class="row">
            <div class="col-lg-12">
              <div class="widget">
                <div class="widget-heading">
                  <h3 class="widget-title">Add Receiving</h3>
                </div>
                <div class="widget-body">
                  <form>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="item_name">Enter Item</label>
                          <input id="item_name" type="text" class="form-control">
                          <div id="item_name_search"  style="margin-top:10px;"></div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="ddlGengeHor" class="col-sm-3 control-label">Type</label>
                          <div class="col-sm-9">
                            <select id="receivings_type" class="form-control">
                              <option value="1">Receiving</option>
                              <option value="2">Return</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                  <div class="col-md-8">  
                    <table class="table table-responsive">
                    <thead>
                      <tr>
                        <th style="width: 17%">Item Name</th>
                        <th style="width: 25%">Price</th>
                        <th style="width: 10%">Tax</th>
                        <th style="width: 23%">Quantity</th>
                        <th style="width: 23%">Discount%</th>
                        <th style="width: 15%">Total</th>
                        <th style="width: 20%">Delete</th>
                      </tr>
                    </thead>
                    <tbody id="receiving_item_list">
                    </tbody>
                  </table>
                </div>
                <div></div>
           <div class="col-md-4">
                <div class="widget-body">
                  <form class="form-horizontal">
                  <div class="row">
                    <div class="form-group">
                      <label for="supplier_name" class="col-sm-3 control-label">Supplier Name</label>
                      <div class="col-sm-8">
                        <input id="supplier_name" type="text" class="form-control">
                        <input type="hidden" id="person_sup_id">
                      </div>
                    </div>
                   </div> <br>
                     <div class="row">
                    <div class="form-group">
                      <label for="receiving_sub_total" class="col-sm-3 control-label">Sub Total</label>
                      <div class="col-sm-8">
                        <input id="receiving_sub_total" type="text" class="form-control">
                      </div>
                    </div>
                    </div><br>
                     <div class="row">
                    <div class="form-group">
                      <label for="receiving_total" class="col-sm-3 control-label"> Total</label>
                      <div class="col-sm-8">
                        <input id="receiving_total" type="text" class="form-control">
                      </div>
                    </div>
                    </div><br>
                    <div class="row">
                    <div class="form-group">
                      <label for="sales_total" class="col-sm-3 control-label"> Payment</label>
                      <div class="col-sm-8">
                        <select id="paymenttype">
                      <option value="cash">Cash</option>
                      <option value="credit">Credit</option>
                      <option value="debit">Debit</option>
                      </select>
                      </div>
                      
                    </div>
                    </div><br>
                     <div class="row">
                    <div class="form-group">
                      <label for="sales_amount" class="col-sm-3 control-label">Amount</label>
                      <div class="col-sm-8">
                        <input id="receiving_amount" type="text" class="form-control">
                      </div>
                    </div>
                    </div><br>
                    <div class="row">
                    <div class="form-group">
                      <label for="receiving_amount" class="col-sm-3 control-label">Amount Due</label>
                      </div>
                    </div>
                  </form>
                </div>
              
             </div> 

            <div class="row">
               <div class="col-lg-12">
                  <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9 col-md-offset-2 col-md-10">
                      <button type="submit" id="add_receiving" class="btn btn-black">Add Receiving</button>
                      <button type="submit" id="remove_all_item" class="btn btn-black">Remove All</button>
                    </div>
                  </div>
                </div>
              </div>   



        </script>

        <script type="text/template" id="receivingitemTemplate">
                       <tr id="<%=item_id%>">
                        <td><%=item_name%></td>
                        <td><div class="col-lg-12"><div class="form-group">
                      <input id="price" type="text" iid="<%=item_id%>" value="<%=cost_price%>" class="form-control">
                       </div></div></td>
                        <td><div id="tax"><%=percent%></div></td>
                        <td><div class="col-lg-12"><div class="form-group">
                      <input id="quantity" type="text" iid="<%=item_id%>" value="1" class="form-control">
                    </div></div>
                    </td>
                    <td><div class="col-lg-12"><div class="form-group">
                      <input id="discount" type="text" iid="<%=item_id%>" class="form-control">
                    </div></div>
                    </td>
                    <td><div id="price_item"><%=total_price%></div></td>
                    <td id="a<%=item_id%>"><span style="display:inline-block; width: 20px;">
                    <a id="delete_item"   data-toggle="tooltip" data-placement="top" title="Delete" val="<%=item_id%>"><i class="glyphicon glyphicon-minus-sign" val="<%=item_id%>"></i></a>
                    </span></td>
                        </tr>
        </script>

        <script type="text/template" id="saleitemTemplate">
                       <tr id="<%=item_id%>">
                        <td><%=item_name%></td>
                        <td><div class="col-lg-12"><div class="form-group">
                      <input id="price" type="text" iid="<%=item_id%>" value="<%=cost_price%>" class="form-control">
                       </div></div></td>
                        <td><div class="col-lg-12"><div class="form-group">
                      <input id="quantity" type="text" iid="<%=item_id%>" value="<%=quantity%>" class="form-control">
                    </div></div>
                    </td>
                    <td><div class="col-lg-12"><div class="form-group">
                      <input id="discount" value="<%=discount%>" type="text" iid="<%=item_id%>" class="form-control">
                    </div></div>
                    </td>
                    <td><div id="price_item">0</div></td>
                    <td id="a<%=item_id%>"><span style="display:inline-block; width: 20px;">
                    <a id="delete_item"   data-toggle="tooltip" data-placement="top" title="Delete" val="<%=item_id%>"><i class="glyphicon glyphicon-minus-sign" val="<%=item_id%>"></i></a>
                    </span></td>
                        </tr>
        </script>

          <script type="text/template" id="suspendedsales">
        
        <div class="row">
            <div class="col-lg-12">
              <div class="widget">
                <div class="widget-heading">
                  <h3 class="widget-title">Add Suspended Sales</h3>
                </div>
                <div class="widget-body">
                  <form>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="item_name">Enter Item</label>
                          <input id="item_name" type="text" class="form-control">
                          <div id="item_name_search"  style="margin-top:10px;"></div>
                        </div>
                      </div>
                    </div>
                  <div class="col-md-8">  
                    <table class="table table-responsive">
                    <thead>
                      <tr>
                        <th style="width: 17%">Item Name</th>
                        <th style="width: 25%">Price</th>
                        <th style="width: 10%">Tax</th>
                        <th style="width: 23%">Quantity</th>
                        <th style="width: 23%">Discount%</th>
                        <th style="width: 15%">Total</th>
                        <th style="width: 20%">Delete</th>
                      </tr>
                    </thead>
                    <tbody id="suspended_sale_item_list">
                    </tbody>
                  </table>
                </div>
                <div></div>
           <div class="col-md-4">
                <div class="widget-body">
                  <form class="form-horizontal">
                  <div class="row">
                    <div class="form-group">
                      <label for="customer_name" class="col-sm-3 control-label">Customer Name</label>
                      <div class="col-sm-8">
                        <input id="customer_name" type="text" class="form-control">
                        <input type="hidden" id="person_cust_id">
                      </div>
                    </div>
                   </div> <br>
                     <div class="row">
                    <div class="form-group">
                      <label for="suspended_sale_sub_total" class="col-sm-3 control-label">Sub Total</label>
                      <div class="col-sm-8">
                        <input id="suspended_sale_sub_total" type="text" class="form-control">
                      </div>
                    </div>
                    </div><br>
                     <div class="row">
                    <div class="form-group">
                      <label for="suspended_sale_total" class="col-sm-3 control-label"> Total</label>
                      <div class="col-sm-8">
                        <input id="suspended_sale_total" type="text" class="form-control">
                      </div>
                    </div>
                    </div><br>
                     <div class="row">
                    <div class="form-group">
                      <label for="suspended_sale_amount" class="col-sm-3 control-label">Amount</label>
                      <div class="col-sm-8">
                        <input id="suspended_sale_amount" type="text" class="form-control">
                      </div>
                    </div>
                    </div><br>
                    <div class="row">
                    <div class="form-group">
                      <label for="sales_total" class="col-sm-3 control-label"> Payment</label>
                      <div class="col-sm-8">
                        <select id="paymenttype">
                      <option value="cash">Cash</option>
                      <option value="credit">Credit</option>
                      <option value="debit">Debit</option>
                      </select>
                      </div>
                      
                    </div>
                    </div><br>
                    <div class="row">
                    <div class="form-group">
                      <label for="suspended_sale_amount" class="col-sm-3 control-label">Amount Due</label>
                      </div>
                    </div>
                  </form>
                </div>
              
             </div> 

            <div class="row">
               <div class="col-lg-12">
                  <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9 col-md-offset-2 col-md-10">
                      <button type="submit" id="add_suspended_sale" class="btn btn-black">Add Suspended Sales</button>
                      <button type="submit" id="remove_all_item" class="btn btn-black">Remove All</button>
                    </div>
                  </div>
                </div>
              </div>   



        </script>

        <script type="text/template" id="suspendedsaleitemTemplate">
                       <tr id="<%=item_id%>">
                        <td><%=item_name%></td>
                        <td><div class="col-lg-12"><div class="form-group">
                      <input id="price" type="text" iid="<%=item_id%>" value="<%=cost_price%>" class="form-control">
                       </div></div></td>
                        <td><div id="tax"><%=percent%></div></td>
                        <td><div class="col-lg-12"><div class="form-group">
                      <input id="quantity" type="text" iid="<%=item_id%>" value="1" class="form-control">
                    </div></div>
                    </td>
                    <td><div class="col-lg-12"><div class="form-group">
                      <input id="discount" type="text" iid="<%=item_id%>" class="form-control">
                    </div></div>
                    </td>
                    <td><div id="price_item"><%=total_price%></div></td>
                    <td id="a<%=item_id%>"><span style="display:inline-block; width: 20px;">
                    <a id="delete_item"   data-toggle="tooltip" data-placement="top" title="Delete" val="<%=item_id%>"><i class="glyphicon glyphicon-minus-sign" val="<%=item_id%>"></i></a>
                    </span></td>
                        </tr>
        </script>

          <script type="text/template" id="companyPageTpl">

                <div class="widget">
            <div class="widget-heading">
              <h3 class="widget-title">Company List</h3>
            </div>
            <div class="widget-body">
            <form  class="form-horizontal">
                   <fieldset>                  

                      <div class="form-group">
                            <div class="col-sm-offset-8 col-sm-12" >
                             <div class="col-sm-8 col-md-2">

                             <input id="search" name="txtFirstNameBilling" type="text" class="form-control" placeholder="Search" >
                             <input type="checkbox" class="common" id="switch1" checked="checked" />

                             <a href="/btree/admin/get_download_excel?exporttype=companypage " download="company">DownloadExcel</a>

                             <a href="/btree/admin/get_download_pdf?exporttype=companypage" download="company">DownloadPdf</a>

                              </div>
                              <button  id="create_company" name="btnSubmit" class="btn btn-raised btn-black">Add</button><button id="deleteselect" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Del Select</button> <button id="deleteall" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Del All</button>
                              <button id="restoreall" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Restore All</button><button id="addselect" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Add Select</button> 
                            </div>
                      </div>
            
                    <div id="com_protable"></div>
                    <div id="paginator"></div>
                </fieldset>
                </form>
            </div>
          </div>

        </script>


        <script type="text/template" id="companyCreateFormTpl">

            <div class="widget">
            <div class="widget-body">
              <form id="form" class="form-horizontal">
                
                <fieldset>
                 <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="company_name" class="col-sm-3 col-md-4 control-label">Company Name</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="company_name" name="company_name" type="text" class="form-control">
                            <div id="company_name_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                   <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="company_description" class="col-sm-3 col-md-4 control-label">Description</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="company_description" name="company_description" type="text" class="form-control">
                            <div id="company_description_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                    
                    </div>
                  </div>
                 
                </fieldset>
              </form>
            </div>
          </div>

        </script>
        <script type="text/template" id="companyEditFormTpl">

            <div class="widget">
            <div class="widget-heading">
              <h3 class="widget-title">Edit company</h3>
            </div>
            <div class="widget-body">
              <form id="form" class="form-horizontal">
                
                <fieldset>
                 <input id="company_id" type="hidden" class="form-control" value="<%=company_id%>" />
                 <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="company_name" class="col-sm-3 col-md-4 control-label">company Name</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="company_name" value="<%=company_name%>" name="company_name" type="text" class="form-control">
                            <div id="company_name_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="company_description" class="col-sm-3 col-md-4 control-label">Description</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="company_description" value="<%=company_description%>" name="company_description" type="text" class="form-control">
                            <div id="company_description_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                    </div>
                  </div>
                 
                </fieldset>
              </form>
            </div>
          </div>

        </script>

         <script type="text/template" id="productPageTpl">

                <div class="widget">
            <div class="widget-heading">
              <h3 class="widget-title">Product List</h3>
            </div>
            <div class="widget-body">
            <form  class="form-horizontal">
                   <fieldset>                  

                      <div class="form-group">
                            <div class="col-sm-offset-8 col-sm-12" >
                             <div class="col-sm-8 col-md-2">

                             <input id="search" name="txtFirstNameBilling" type="text" class="form-control" placeholder="Search" >
                             <input type="checkbox" class="common" id="switch1" checked="checked" />

                             <a href="/btree/admin/get_download_excel?exporttype=productpage " download="product">DownloadExcel</a>

                             <a href="/btree/admin/get_download_pdf?exporttype=productpage" download="product">DownloadPdf</a>


                            </div>
                              <button type="button" id="create_product" name="btnSubmit" class="btn btn-raised btn-black">Add</button> <button id="deleteselect" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Del Select</button> <button id="deleteall" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Del All</button>
                              <button id="restoreall" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Restore All</button><button id="addselect" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Add Select</button> 
                            </div>
                      </div>
       </fieldset>

                      <div id="com_protable"></div>
                    <div id="paginator"></div>

                </form>
            </div>
          </div>

        </script>

        <script type="text/template" id="productCreateFormTpl">

            <div class="widget">
            <div class="widget-body">
              <form id="form" class="form-horizontal">
                
                <fieldset>
                 <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="product_name" class="col-sm-3 col-md-4 control-label">Product Name</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="product_name" name="product_name" type="text" class="form-control">
                            <div id="product_name_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                   <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="product_description" class="col-sm-3 col-md-4 control-label">Description</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="product_description" name="product_description" type="text" class="form-control">
                            <div id="product_description_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                    
                      </div>
                    </div>
                  
                </fieldset>
              </form>
            </div>
          </div>

        </script>
        <script type="text/template" id="productEditFormTpl">

            <div class="widget">
            <div class="widget-body">
              <form id="form" class="form-horizontal">
                
                <fieldset>
                 <input id="product_id" type="hidden" class="form-control" value="<%=product_id%>" />
                 <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="product_name" class="col-sm-3 col-md-4 control-label">product Name</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="product_name" value="<%=product_name%>" name="product_name" type="text" class="form-control">
                            <div id="product_name_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="company_description" class="col-sm-3 col-md-4 control-label">Description</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="product_description" value="<%=product_description%>" name="product_description" type="text" class="form-control">
                            <div id="product_description_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>

                 
                </fieldset>
              </form>
            </div>
          </div>

        </script>

        <script type="text/template" id="locationPageTpl">

                <div class="widget">
            <div class="widget-heading">
              <h3 class="widget-title">Location List</h3>
            </div>
            <div class="widget-body">
            <form  class="form-horizontal">
                   <fieldset>                  

                      <div class="form-group">
                            <div class="col-sm-offset-8 col-sm-12" >
                             <div class="col-sm-8 col-md-2">

                             <input id="location_search" name="txtFirstNameBilling" type="text" class="form-control" placeholder="Search" >
                             <input type="checkbox" class="common" id="switch1" checked="checked" />
                            </div>
                              <button  id="create_location" name="btnSubmit" class="btn btn-raised btn-black">Add</button> <button id="location_deleteselect" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Del Select</button> <button id="location_deleteall" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Del All</button>
                              <button id="location_restoreall" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Restore All</button>
                              <button id="location_addselect" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Add Select</button> 
                            </div>
                      </div>


            
                    <div id="locationtable"></div>
                    <div id="paginator"></div>
                </fieldset>
                </form>
            </div>
          </div>

        </script>

        <script type="text/template" id="locationCreateFormTpl">

            <div class="widget">
            <div class="widget-body">
              <form id="form" class="form-horizontal">
                
                <fieldset>
                 <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="location_name" class="col-sm-3 col-md-4 control-label">Location Name</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="location_name" name="location_name" type="text" class="form-control">
                            <div id="location_name_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                   
                </fieldset>
              </form>
            </div>
          </div>

        </script>
        <script type="text/template" id="locationEditFormTpl">

            <div class="widget">
            <div class="widget-body">
              <form id="form" class="form-horizontal">
                
                <fieldset>
                 <input id="location_id" type="hidden" class="form-control" value="<%=location_id%>" />
                 <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="location_name" class="col-sm-3 col-md-4 control-label">Location Name</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="location_name" value="<%=location_name%>" name="location_name" type="text" class="form-control">
                            <div id="location_name_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                    </div>
                    
                </fieldset>
              </form>
            </div>
          </div>

        </script>

        <script type="text/template" id="expense_categoryPageTpl">

                <div class="widget">
            <div class="widget-heading">
              <h3 class="widget-title">Expense List</h3>
            </div>
            <div class="widget-body">
            <form  class="form-horizontal">
                   <fieldset>                  

                      <div class="form-group">
                            <div class="col-sm-offset-8 col-sm-12" >
                             <div class="col-sm-8 col-md-2">

                             <input id="expense_category_search" name="txtFirstNameBilling" type="text" class="form-control" placeholder="Search" >
                             <input type="checkbox" class="common" id="switch1" checked="checked" />
                            </div>
                              <button  id="create_expense_category" name="btnSubmit" class="btn btn-raised btn-black">Add</button> <button id="expense_category_deleteselect" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Del Select</button><button id="expense_category_deleteall" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Del All</button>
                              <button id="expense_category_restoreall" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Restore All</button><button id="expense_category_addselect" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Add Select</button> 
                            </div>
                      </div>


            
                    <div id="expense_categorytable"></div>
                    <div id="paginator"></div>
                </fieldset>
                </form>
            </div>
          </div>

        </script>


        <script type="text/template" id="expense_categoryCreateFormTpl">

            <div class="widget">
            <div class="widget-body">
              <form id="form" class="form-horizontal">
                
                <fieldset>
                 <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="expense_category_name" class="col-sm-3 col-md-4 control-label">Expense Name</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="expense_category_name" name="expense_category_name" type="text" class="form-control">
                            <div id="expense_category_name_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                   <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="expense_category_description" class="col-sm-3 col-md-4 control-label">Description</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="expense_category_description" name="expense_category_description" type="text" class="form-control">
                            <div id="expense_category_description_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                    
                    </div>
                  </div>
                 
                </fieldset>
              </form>
            </div>
          </div>

        </script>
        <script type="text/template" id="expense_categoryEditFormTpl">

            <div class="widget">
            
            <div class="widget-body">
              <form id="form" class="form-horizontal">
                
                <fieldset>
                 <input id="expense_category_id" type="hidden" class="form-control" value="<%=expense_category_id%>" />
                 <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="expense_category_name" class="col-sm-3 col-md-4 control-label">Expense Name</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="expense_category_name" value="<%=expense_category_name%>" name="expense_category_name" type="text" class="form-control">
                            <div id="expense_category_name_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="expense_category_description" class="col-sm-3 col-md-4 control-label">Description</label>
                        <div class="col-sm-5 col-md-5">
                        <input id="expense_category_description" value="<%=expense_category_description%>" name="expense_category_description" type="text" class="form-control">
                            <div id="expense_category_description_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                    </div>
                  </div>
                 
                </fieldset>
              </form>
            </div>
          </div>

        </script>


        <script type="text/template" id="expensePageTpl">

                <div class="widget">
            <div class="widget-heading">
              <h3 class="widget-title">Expense List</h3>
            </div>
            <div class="widget-body">
            <form  class="form-horizontal">
                   <fieldset>                  

                      <div class="form-group">
                            <div class="col-sm-offset-8 col-sm-12" >
                             <div class="col-sm-8 col-md-2">

                             <input id="expense_search" name="txtFirstNameBilling" type="text" class="form-control" placeholder="Search" >
                             <input type="checkbox" class="common" id="switch1" checked="checked" />
                            </div>
                              <button  id="create_expense" name="btnSubmit" class="btn btn-raised btn-black">Add</button> <button id="expense_deleteselect" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Del Select</button><button id="expense_deleteall" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Del All</button>
                              <button id="expense_restoreall" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Restore All</button>
                              <button id="expense_addselect" type="submit" name="btnSubmit" class="btn btn-raised btn-black">Add Select</button>
                            </div>
                      </div>
    
                    <div id="expensetable"></div>
                    <div id="paginator"></div>
                </fieldset>
                </form>
            </div>
          </div>

        </script>


        <script type="text/template" id="expenseCreateFormTpl">

            <div class="widget">
            <div class="widget-body">
              <form id="form" class="form-horizontal">
                
                <fieldset>
                 
                 <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                        <label for="expense_category_name" class="col-sm-3 col-md-4 control-label">Expense Category</label>
                        <div class="col-sm-5 col-md-5">
                              <select id="expense_category_name" name="expense_category_name" class="form-control">
                                  <option value="">--Please Select--</option>
                           </select>     
                           <div id="expense_category_name_error" style="color:red"></div>            
                            </div>
                      </div>
                  </div>
                 
                 <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="expense_description" class="col-sm-3 col-md-4 control-label">Expense Description</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="expense_description" name="expense_description" type="text" class="form-control">
                            <div id="expense_description_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                 
                 <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="amount" class="col-sm-3 col-md-4 control-label">Amount</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="amount" name="amount" type="text" class="form-control">
                            <div id="amount_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                 
                 <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="payment_type" class="col-sm-3 col-md-4 control-label">Payment Type</label>
                        <div class="col-sm-5 col-md-5">
                            <select id="paymenttype">
                      <option value="cash">Cash</option>
                      <option value="credit">Credit</option>
                      <option value="debit">Debit</option>
                      </select>
                            <div id="payment_type_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="supplier_name" class="col-sm-3 col-md-4 control-label">Supplier Name</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="supplier_name" name="supplier_name" type="text" class="form-control">
                            <div id="supplier_name_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                   
                </fieldset>
              </form>
            </div>
          </div>

        </script>
        <script type="text/template" id="expenseEditFormTpl">

            <div class="widget">
            <div class="widget-body">
              <form id="form" class="form-horizontal">
                
                <fieldset>
                 <input id="expense_id" type="hidden" class="form-control" value="<%=expense_id%>" />

                 <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                        <label for="expense_category_name" class="col-sm-3 col-md-4 control-label">Expense Category</label>
                        <div class="col-sm-5 col-md-5">
                              <select id="expense_category_name" name="expense_category_name" class="form-control">
                                  <option value="">--Please Select--</option>
                           </select>     
                           <div id="expense_category_name_error" style="color:red"></div>            
                            </div>
                      </div>
                  </div>
                 
                 <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="expense_description" class="col-sm-3 col-md-4 control-label">Expense Description</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="expense_description" value="<%=expense_description%>" name="expense_description" type="text" class="form-control">
                            <div id="expense_description_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                 
                 <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="amount" class="col-sm-3 col-md-4 control-label">Amount</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="amount" name="amount" value="<%=amount%>" type="text" class="form-control">
                            <div id="amount_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                 
                 <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="payment_type" class="col-sm-3 col-md-4 control-label">Payment Type</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="payment_type" value="<%=payment_type%>" name="payment_type" type="text" class="form-control">
                            <div id="payment_type_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="supplier_name" class="col-sm-3 col-md-4 control-label">Supplier Name</label>
                        <div class="col-sm-5 col-md-5">
                            <input id="supplier_name" value="<%=supplier_name%>" name="supplier_name" type="text" class="form-control">
                            <div id="supplier_name_error" style="color:red"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                 
                    
                </fieldset>
              </form>
            </div>
          </div>

        </script>

        <script type="text/template" id="ReportTpl">
          <div class="col-lg-12">
            <div class="col-md-4">
            <ul >
              <h2>Summary Report </h2>
              <li><a id="admin_report_commission">Commission</a></li>
              <li><a id="admin_report_company">Company</a></li>
              <li><a id="admin_report_customer">Customer</a></li>
              <li><a id="admin_report_discount">Discounts</a></li>
              <li><a id="admin_report_employee">Employee</a></li>
              <li><a id="admin_report_expense_category">Expense Category</a></li>
              <li><a id="admin_report_item">Items</a></li>
              <li><a id="admin_report_itemkits">Item Kits</a></li>
              <li><a id="admin_report_payment">Payments</a></li>
              <li><a id="admin_report_product">Product</a></li>
              <li><a id="admin_report_receiving">Receiving</a></li>
              <li><a id="admin_report_sales">Sales</a></li>
              <li><a id="admin_report_supplier">Supplier</a></li>
              <li><a id="admin_report_tax">Tax</a></li>
            </ul>
          </div>
          <div class="col-md-4">
             <ul >
              <h2> Graphical Report</h2>
              <li><a id="admin_report_graph_commission">Commission</a></li>
              <li><a id="admin_report_graph_company">Company</a></li>
              <li><a id="admin_report_graph_customer">Customer</a></li>
              <li><a id="admin_report_graph_discount">Discounts</a></li>
              <li><a id="admin_report_graph_employee">Employee</a></li>
              <li><a id="admin_report_graph_expense">Expense Category</a></li>
              <li><a id="admin_report_graph_item">Items</a></li>
              <li><a id="admin_report_graph_itemkits">Item Kits</a></li>
              <li><a id="admin_report_graph_product">Product</a></li>
              <li><a id="admin_report_graph_receiving">Receiving</a></li>
              <li><a id="admin_report_graph_sales">Sales</a></li>
              <li><a id="admin_report_graph_supplier">Supplier</a></li>
              <li><a id="admin_report_graph_suspendedsales">Suspended Sales</a></li>
              <li><a id="admin_report_graph_tax">Tax</a></li>
            </ul>
          </div>
          <div class="col-md-4">
             <ul >
              <h2> Detailed Report</h2>
              <li><a id="admin_report_detail_customer">Customer</a></li>
              <li><a id="admin_report_detail_employee">Employee</a></li>
              <li><a id="admin_report_detail_sales">Sales</a></li>
              <li><a id="admin_sms">Receiving</a></li>
              <li><a id="admin_report_detail_discount">Discount</a></li>
            </ul>
            <ul>
              <h2> Inventory Report</h2>
              <li><a id="admin_report_low_inventory">Low Inventory</a></li>
              <li><a id="admin_report_inventory_summary">Inventory Summary</a></li>
              <li><a id="admin_report_dead_item">Dead Item</a></li>
            </ul>
          </div>

        </div>
       </script>

      
    <script type="text/template" id="ReportListTpl">
       <% if (reporttype != "inventorysummary") { %> 
       Date Range:<select id="date_type">
      <option value="1">Today</option>
      <option value="2">Yesterday</option>
      <option value="3">Last 7 days</option>
      <option value="4">Last 30 days</option>
      <option value="5">Last 90 days</option>
      <option value="6">Last 180 days</option>
      <option value="7">Last 1 Year</option>
      <option value="8">This Week</option>
      <option value="9">This Month</option>
      <option value="10">This Year</option>
      <option value="11">Custom Date</option>
      </select>
      <div class="input-daterange input-group" data-provide="datepicker"  data-date-format="yyyy-mm-dd">
          <input type="text" class="input-sm form-control" id="start_date" name="start" />
          <span class="input-group-addon">to</span>
          <input type="text" class="input-sm form-control" id="end_date" name="end" />
      </div>
      <div id="datepicker_error" style="color:red"></div>

      <% if (reporttype == "deaditem") { %> 
    <div class="form-group">
          <div class="col-sm-offset-4 col-sm-12">
            <button type="submit" id="submit_deaditem" name="product_save" class="btn btn-raised btn-black">Submit</button>  
          </div>
      </div>
   
    <% }  %> 

       <% if (reporttype != "expensecategory" && reporttype != "deaditem") { %> 
      Transaction Type<select id="transaction_type">
      <option value="1">All</option>
      <option value="2">Completed Sales</option>
      <option value="3">Returns</option>
      </select>
       <% }  %> 
    <% if (reporttype == "company") { %> 

    <div class="form-group">
          <div class="col-sm-offset-4 col-sm-12">
            <button type="submit" id="submit_company" name="product_save" class="btn btn-raised btn-black">Submit</button>  

          </div>
      </div>
   
    <% }  %> 

    <% if (reporttype == "product") { %> 

    <div class="form-group">
          <div class="col-sm-offset-4 col-sm-12">
            <button type="submit" id="submit_product" name="product_save" class="btn btn-raised btn-black">Submit</button>  
          </div>
      </div>
   
    <% }  %> 
    
        <% if (reporttype == "customer") { %> 

   <% if (rendertype == "details") { %> 

    <div class="form-group">
          <div class="col-sm-offset-4 col-sm-12">
            <label for="customer_name" class="col-sm-3 control-label">Customer Name</label>
                <div class="col-sm-8">
                  <input id="customer_name" type="text" class="form-control">
                  <input type="hidden" id="person_cust_id">
                </div>
          </div>
      </div>
   
    <% }  %>
    <div class="form-group">
          <div class="col-sm-offset-4 col-sm-12">
            <button type="submit" id="submit_customer" name="customer_save" class="btn btn-raised btn-black">Submit</button>  
          </div>
      </div>

 
   
    <% }  %> 

     <% if (reporttype == "supplier") { %> 

    <div class="form-group">
          <div class="col-sm-offset-4 col-sm-12">
            <button type="submit" id="submit_supplier" name="supplier_save" class="btn btn-raised btn-black">Submit</button>  
          </div>
      </div>
   
    <% }  %> 

    <% if (reporttype == "item") { %> 

    <div class="form-group">
          <div class="col-sm-offset-4 col-sm-12">
            <button type="submit" id="submit_item" name="item_save" class="btn btn-raised btn-black">Submit</button>  
          </div>
      </div>
   
    <% }  %> 


    <% if (reporttype == "expensecategory") { %> 

    <div class="form-group">
          <div class="col-sm-offset-4 col-sm-12">
            <button type="submit" id="submit_expense_category" name="expense_save" class="btn btn-raised btn-black">Submit</button>  
          </div>
      </div>
   
    <% }  %> 

<% if (reporttype == "sales") { %> 

    <div class="form-group">
          <div class="col-sm-offset-4 col-sm-12">
            <button type="submit" id="submit_sales" name="sales_save" class="btn btn-raised btn-black">Submit</button>  
          </div>
      </div>
   
    <% }  %> 


    <% if (reporttype == "suspendedsales") { %> 

    <div class="form-group">
          <div class="col-sm-offset-4 col-sm-12">
            <button type="submit" id="submit_suspendedsales" name="suspendedsales_save" class="btn btn-raised btn-black">Submit</button>  
          </div>
      </div>
   
    <% }  %> 


    <% if (reporttype == "receiving") { %> 

    <div class="form-group">
          <div class="col-sm-offset-4 col-sm-12">
            <button type="submit" id="submit_receiving" name="receiving_save" class="btn btn-raised btn-black">Submit</button>  
          </div>
      </div>
   
    <% }  %> 

    <% if (reporttype == "discount") { %> 
    <input type="text" id="discountrate" name="start" />
    <div class="form-group">
          <div class="col-sm-offset-4 col-sm-12">
            <button type="submit" id="submit_discount" name="discount_save" class="btn btn-raised btn-black">Submit</button>  
          </div>
      </div>
   
    <% }  %> 

    <% if (reporttype == "payment") { %> 

    <div class="form-group">
          <div class="col-sm-offset-4 col-sm-12">
            <button type="submit" id="submit_payment" name="payment_save" class="btn btn-raised btn-black">Submit</button>  
          </div>
      </div>
   
    <% }  %> 

    <% if (reporttype == "tax") { %> 

    <div class="form-group">
          <div class="col-sm-offset-4 col-sm-12">
            <button type="submit" id="submit_tax" name="product_save" class="btn btn-raised btn-black">Submit</button>  
          </div>
      </div>
   
    <% }  %> 

     <% if (reporttype == "employee") { %> 
     <% if (rendertype == "details") { %> 

    <div class="form-group">
          <div class="col-sm-offset-4 col-sm-12">
            <label for="employee_name" class="col-sm-3 control-label">Employee Name</label>
                <div class="col-sm-8">
                  <input id="employee_name" type="text" class="form-control">
                  <input type="hidden" id="person_emp_id">
                </div>
          </div>
      </div>
   
    <% }  %>

    <div class="form-group">
          <div class="col-sm-offset-4 col-sm-12">
            <button type="submit" id="submit_employee" name="product_save" class="btn btn-raised btn-black">Submit</button>  
          </div>
      </div>
   
    <% }  %> 

    <% if (reporttype == "commission") { %> 

    <div class="form-group">
          <div class="col-sm-offset-4 col-sm-12">
            <button type="submit" id="submit_commission" name="product_save" class="btn btn-raised btn-black">Submit</button>  

          </div>
      </div>
   
    <% }  %> 

    <% if (reporttype == "itemkits") { %> 

    <div class="form-group">
          <div class="col-sm-offset-4 col-sm-12">
            <button type="submit" id="submit_itemkits" name="product_save" class="btn btn-raised btn-black">Submit</button>  
          </div>
      </div>
   
    <% }  %> 

    
 <% }  %>
 <% if (reporttype == "inventorysummary") { %> 
    Filter Item<select id="transaction_type">
      <option value="1">All</option>
      <option value="2">zero and less</option>
      <option value="3">more than zero</option>
      </select>
    <div class="form-group">
          <div class="col-sm-offset-4 col-sm-12">
            <button type="submit" id="submit_inventorysummary" name="product_save" class="btn btn-raised btn-black">Submit</button>  
          </div>
      </div>
   
    <% }  %> 
    
    
    </script>

  <script type="text/template" id="companyReportTpl">
   <div class="widget">
            <div class="widget-heading">
              <h3 class="widget-title">Company List</h3>
            </div>
            <div class="widget-body">
            <form  class="form-horizontal">
                   <fieldset> 

                    <div class="widget-subbody" id="download">

                    <% if(type == 11) { %>
                    <a href="/btree/admin/get_download_excel?exporttype=company_report&type=<%= type %> & transtype=<%= transtype %> & start_date=<%= start_date %> & end_date=<%= end_date %> &reporttype=graph" 
                    download="company">DownloadExcel</a>
                    <a href="/btree/admin/get_download_pdf?exporttype=company_report&type=<%= type %> & transtype=<%= transtype %> & start_date=<%= start_date %> & end_date=<%= end_date %> &reporttype=graph" 
                    download="company">DownloadPdf</a>

                    <% } else { %>
                    <a href="/btree/admin/get_download_excel?exporttype=company_report&type=<%= type %> & transtype=<%= transtype %>&reporttype=graph" download="company">DownloadExcel</a>
                    <a href="/btree/admin/get_download_pdf?exporttype=company_report&type=<%= type %> & transtype=<%= transtype %>&reporttype=graph" download="company">DownloadPdf</a>

                    <% } %>

                  </div>

            
                    <div id="companyreport" class="companyreport"></div>
                    <div id="companypaginator"></div>
                </fieldset>
                </form>
            </div>
          </div>
    </script>

  <script type="text/template" id="taxReportTpl">
   <div class="widget">
            <div class="widget-heading">
              <h3 class="widget-title">Tax List</h3>
            </div>
            <div class="widget-body">
            <form  class="form-horizontal">
                   <fieldset>                 


            
                    <div id="taxreport" class="companyreport"></div>
                    <div id="taxpaginator"></div>
                </fieldset>
                </form>
            </div>
          </div>
    </script>

    <script type="text/template" id="productReportTpl">
   <div class="widget">
            <div class="widget-heading">
              <h3 class="widget-title">Product List</h3>
            </div>
            <div class="widget-body">
            <form  class="form-horizontal">
                   <fieldset>                 

                    <div class="widget-subbody" id="download">

                    <% if(type == 11) { %>
                    <a href="/btree/admin/get_download_excel?exporttype=product_report&type=<%= type %> & transtype=<%= transtype %> & start_date=<%= start_date %> & end_date=<%= end_date %> &reporttype=graph" 
                    download="product">DownloadExcel</a>
                    <a href="/btree/admin/get_download_pdf?exporttype=product_report&type=<%= type %> & transtype=<%= transtype %> & start_date=<%= start_date %> & end_date=<%= end_date %> &reporttype=graph" 
                    download="product">DownloadPdf</a>

                    <% } else { %>
                    <a href="/btree/admin/get_download_excel?exporttype=product_report&type=<%= type %> & transtype=<%= transtype %>&reporttype=graph" download="product">DownloadExcel</a>
                    <a href="/btree/admin/get_download_pdf?exporttype=product_report&type=<%= type %> & transtype=<%= transtype %>&reporttype=graph" download="product">DownloadPdf</a>

                    <% } %>

                  </div>    

            
                    <div id="productreport"></div>
                    <div id="productpaginator"></div>
                </fieldset>
                </form>
            </div>
          </div>
    </script>


     <script type="text/template" id="customerReportTpl">
     <div class="widget">
            <div class="widget-heading">
              <h3 class="widget-title">Customer Report</h3>
            </div>
            <div class="widget-body">
            <form  class="form-horizontal">
                   <fieldset>                 

                    <div class="widget-subbody" id="download">

                    <% if(type == 11) { %>
                    <a href="/btree/admin/get_download_excel?exporttype=customer_report&type=<%= type %> & transtype=<%= transtype %> & start_date=<%= start_date %> & end_date=<%= end_date %> &reporttype=graph" 
                    download="customer">DownloadExcel</a>
                    <a href="/btree/admin/get_download_pdf?exporttype=customer_report&type=<%= type %> & transtype=<%= transtype %> & start_date=<%= start_date %> & end_date=<%= end_date %> &reporttype=graph" 
                    download="customer">DownloadPdf</a>

                    <% } else { %>
                    <a href="/btree/admin/get_download_excel?exporttype=customer_report&type=<%= type %> & transtype=<%= transtype %>&reporttype=graph" download="customer">DownloadExcel</a>
                    <a href="/btree/admin/get_download_pdf?exporttype=customer_report&type=<%= type %> & transtype=<%= transtype %>&reporttype=graph" download="customer">DownloadPdf</a>

                    <% } %>

                  </div>    
            
                    <div id="customerreport"></div>
                    <div id="customerpaginator"></div>
                </fieldset>
                </form>
            </div>
          </div>
     <div class="modal fade" id="detailItemModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">List of Sale Item</h4>
        </div>
       <div class="modal-body">
          <div id="listItemsales"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
    </script>

    <script type="text/template" id="employeeReportTpl">
     <div class="widget">
            <div class="widget-heading">
              <h3 class="widget-title">Customer Report</h3>
            </div>
            <div class="widget-body">
            <form  class="form-horizontal">
                   <fieldset>                 

                    <div class="widget-subbody" id="download">

                    <% if(type == 11) { %>
                    <a href="/btree/admin/get_download_excel?exporttype=employee_report&type=<%= type %> & transtype=<%= transtype %> & start_date=<%= start_date %> & end_date=<%= end_date %> &reporttype=graph" 
                    download="employee">DownloadExcel</a>
                    <a href="/btree/admin/get_download_pdf?exporttype=employee_report&type=<%= type %> & transtype=<%= transtype %> & start_date=<%= start_date %> & end_date=<%= end_date %> &reporttype=graph" 
                    download="employee">DownloadPdf</a>

                    <% } else { %>
                    <a href="/btree/admin/get_download_excel?exporttype=employee_report&type=<%= type %> & transtype=<%= transtype %>&reporttype=graph" download="employee">Download</a>
                    <a href="/btree/admin/get_download_pdf?exporttype=employee_report&type=<%= type %> & transtype=<%= transtype %>&reporttype=graph" download="employee">DownloadPdf</a>

                    <% } %>

                  </div>    
            
                    <div id="employeereport"></div>
                    <div id="employeepaginator"></div>
                </fieldset>
                </form>
            </div>
          </div>
    <div class="modal fade" id="detailItemModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">List of Sale Item</h4>
        </div>
       <div class="modal-body">
          <div id="listItemsales"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
    </script>


    <script type="text/template" id="supplierReportTpl">
     <div class="widget">
            <div class="widget-heading">
              <h3 class="widget-title">Supplier Report</h3>
            </div>
            <div class="widget-body">
            <form  class="form-horizontal">
                   <fieldset>                 

                    <div class="widget-subbody" id="download">

                    <% if(type == 11) { %>
                    <a href="/btree/admin/get_download_excel?exporttype=supplier_report&type=<%= type %> & transtype=<%= transtype %> & start_date=<%= start_date %> & end_date=<%= end_date %> &reporttype=graph" 
                    download="supplier">DownloadExcel</a>
                    <a href="/btree/admin/get_download_pdf?exporttype=supplier_report&type=<%= type %> & transtype=<%= transtype %> & start_date=<%= start_date %> & end_date=<%= end_date %> &reporttype=graph" 
                    download="supplier">DownloadPdf</a>


                    <% } else { %>
                    <a href="/btree/admin/get_download_excel?exporttype=supplier_report&type=<%= type %> & transtype=<%= transtype %>&reporttype=graph" download="supplier">DownloadExcel</a>
                    <a href="/btree/admin/get_download_pdf?exporttype=supplier_report&type=<%= type %> & transtype=<%= transtype %>&reporttype=graph" download="supplier">DownloadPdf</a>

                    <% } %>

                  </div>    
            
                    <div id="supplierreport"></div>
                    <div id="supplierpaginator"></div>
                </fieldset>
                </form>
            </div>
          </div>
    </script>

    <script type="text/template" id="lowinventoryReportTpl">
     <div class="widget">
            <div class="widget-heading">
              <h3 class="widget-title">Low Inventory Report</h3>
            </div>
            <div class="widget-body">
            <form  class="form-horizontal">
                   <fieldset>                 


            
                    <div id="lowinventoryreport"></div>
                    <div id="lowinventorypaginator"></div>
                </fieldset>
                </form>
            </div>
          </div>
    </script>

  <script type="text/template" id="inventorysummaryReportTpl">
     <div class="widget">
            <div class="widget-heading">
              <h3 class="widget-title">Inventory Summary Report</h3>
            </div>
            <div class="widget-body">
            <form  class="form-horizontal">
                   <fieldset>                 


            
                    <div id="inventorysummaryreport"></div>
                    <div id="inventorysummarypaginator"></div>
                </fieldset>
                </form>
            </div>
          </div>
    </script>

    <script type="text/template" id="deaditemReportTpl">
     <div class="widget">
            <div class="widget-heading">
              <h3 class="widget-title">Dead Item Report</h3>
            </div>
            <div class="widget-body">
            <form  class="form-horizontal">
                   <fieldset>                 


            
                    <div id="deaditemreport"></div>
                    <div id="deaditempaginator"></div>
                </fieldset>
                </form>
            </div>
          </div>
    </script>

    <script type="text/template" id="itemReportTpl">
     <div class="widget">
            <div class="widget-heading">
              <h3 class="widget-title">Item Report</h3>
            </div>
            <div class="widget-body">
            <form  class="form-horizontal">
                   <fieldset>                 

                    <div class="widget-subbody" id="download">

                    <% if(type == 11) { %>
                    <a href="/btree/admin/get_download_excel?exporttype=item_report&type=<%= type %> & transtype=<%= transtype %> & start_date=<%= start_date %> & end_date=<%= end_date %> &reporttype=graph" 
                    download="item">DownloadExcel</a>
                    <a href="/btree/admin/get_download_pdf?exporttype=item_report&type=<%= type %> & transtype=<%= transtype %> & start_date=<%= start_date %> & end_date=<%= end_date %> &reporttype=graph" 
                    download="item">DownloadPdf</a>

                    <% } else { %>
                    <a href="/btree/admin/get_download_excel?exporttype=item_report&type=<%= type %> & transtype=<%= transtype %>&reporttype=graph" download="item">DownloadExcel</a>
                    <a href="/btree/admin/get_download_pdf?exporttype=item_report&type=<%= type %> & transtype=<%= transtype %>&reporttype=graph" download="item">DownloadPdf</a>

                    <% } %>

                  </div>    
            
                    <div id="itemreport"></div>
                    <div id="itempaginator"></div>
                </fieldset>
                </form>
            </div>
          </div>
    </script>

    <script type="text/template" id="expenseReportTpl">
     <div class="widget">
            <div class="widget-heading">
              <h3 class="widget-title">Expense Report</h3>
            </div>
            <div class="widget-body">
            <form  class="form-horizontal">
                   <fieldset>                 


            
                    <div id="expensereport"></div>
                    <div id="expensepaginator"></div>
                </fieldset>
                </form>
            </div>
          </div>
    </script>

    <script type="text/template" id="salesReportTpl">
     <div class="widget">
            <div class="widget-heading">
              <h3 class="widget-title">Sales Report</h3>
            </div>
            <div class="widget-body">
            <form  class="form-horizontal">
                   <fieldset> 

                   <div class="widget-subbody" id="download">

                    <% if(type == 11) { %>
                    <a href="/btree/admin/get_download_excel?exporttype=sale_report&type=<%= type %> & transtype=<%= transtype %> & start_date=<%= start_date %> & end_date=<%= end_date %> &reporttype=graph" 
                    download="sale">DownloadExcel</a>
                    <a href="/btree/admin/get_download_pdf?exporttype=sale_report&type=<%= type %> & transtype=<%= transtype %> & start_date=<%= start_date %> & end_date=<%= end_date %> &reporttype=graph" 
                    download="sale">DownloadPdf</a>


                    <% } else { %>
                    <a href="/btree/admin/get_download_excel?exporttype=sale_report&type=<%= type %> & transtype=<%= transtype %>&reporttype=graph" download="sale">DownloadExcel</a>
                    <a href="/btree/admin/get_download_pdf?exporttype=sale_report&type=<%= type %> & transtype=<%= transtype %>&reporttype=graph" download="sale">DownloadPdf</a>

                    <% } %>

                  </div>                
            
                    <div id="salesreport"></div>
                    <div id="salespaginator"></div>
                </fieldset>
                </form>
            </div>
          </div>
  <div class="modal fade" id="detailItemModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">List of Sale Item</h4>
        </div>
       <div class="modal-body">
          <div id="listItemsales"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
    </script>

    <script type="text/template" id="suspendedsalesReportTpl">
     <div class="widget">
            <div class="widget-heading">
              <h3 class="widget-title">Suspended Sales</h3>
            </div>
            <div class="widget-body">
            <form  class="form-horizontal">
                   <fieldset>                 


            
                    <div id="suspendedsalesreport"></div>
                    <div id="suspendedsalespaginator"></div>
                </fieldset>
                </form>
            </div>
          </div>

    <div class="modal fade" id="detailItemModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">List of Sale Item</h4>
        </div>
       <div class="modal-body">
          <div id="listItemsales"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
    </script>

    <script type="text/template" id="receivingReportTpl">
     <div class="widget">
            <div class="widget-heading">
              <h3 class="widget-title">Receiving Report</h3>
            </div>
            <div class="widget-body">
            <form  class="form-horizontal">
                   <fieldset>                 


            
                    <div id="receivingreport"></div>
                    <div id="receivingpaginator"></div>
                </fieldset>
                </form>
            </div>
          </div>
    </script>

    <script type="text/template" id="discountReportTpl">
     <div class="widget">
            <div class="widget-heading">
              <h3 class="widget-title">Discount Report</h3>
            </div>
            <div class="widget-body">
            <form  class="form-horizontal">
                   <fieldset>                 


            
                    <div id="discountreport"></div>
                    <div id="discountpaginator"></div>
                </fieldset>
                </form>
            </div>
          </div>
    </script>

    <script type="text/template" id="paymentReportTpl">
     <div class="widget">
            <div class="widget-heading">
              <h3 class="widget-title">Payment Report</h3>
            </div>
            <div class="widget-body">
            <form  class="form-horizontal">
                   <fieldset>                 


            
                    <div id="paymentreport"></div>
                    <div id="paymentpaginator"></div>
                </fieldset>
                </form>
            </div>
          </div>
    </script>

    <script type="text/template" id="itemkitsReportTpl">
   <div class="widget">
            <div class="widget-heading">
              <h3 class="widget-title">Item Kits List</h3>
            </div>
            <div class="widget-body">
            <form  class="form-horizontal">
                   <fieldset>                 


            
                    <div id="itemkitsreport" class="itemkitsreport"></div>
                    <div id="itemkitspaginator"></div>
                </fieldset>
                </form>
            </div>
          </div>
    </script>

    <script type="text/template" id="inventoryReportTpl">
   <div class="widget">
            <div class="widget-heading">
              <h3 class="widget-title">Inventory List</h3>
            </div>
            <div class="widget-body">
            <form  class="form-horizontal">
                   <fieldset>                 


            
                    <div id="inventoryreport" class="inventoryreport"></div>
                    <div id="inventorypaginator"></div>
                </fieldset>
                </form>
            </div>
          </div>
    </script>

    <script type="text/template" id="commissionReportTpl">
   <div class="widget">
            <div class="widget-heading">
              <h3 class="widget-title">Commissin</h3>
            </div>
            <div class="widget-body">
            <form  class="form-horizontal">
                   <fieldset>                 


            
                    <div id="commissionreport" class="commissionreport"></div>
                    <div id="commissionpaginator"></div>
                </fieldset>
                </form>
            </div>
          </div>
    </script>


      <!-- Right Sidebar end-->
    
 <!-- jQuery-->


    <script type="text/javascript" src="plugins/jquery/dist/jquery.min.js"></script>

     <!-- jQuery Steps-->
     <script type="text/javascript" src="js/lib/jquery.tmpl.min.js"></script>
       <script type="text/javascript" src="plugins/jquery-ui/jquery-ui.js"></script>
    <script type="text/javascript" src="plugins/jquery.steps/build/jquery.steps.min.js"></script>
    <!-- Backbone & underscore-->
    <script type="text/javascript" src="js/lib/underscore-min.js"></script>
    <script type="text/javascript" src="js/lib/backbone.js"></script>  
    <script type="text/javascript" src="js/lib/backbone-bootstrap-modals.js"></script>  
    <script type="text/javascript" src="js/lib/backbone-forms.js"></script>  
    <script type="text/javascript" src="js/lib/backbone.paginator.js"></script>   
    <script type="text/javascript" src="js/lib/backgrid.js"></script>
    <script type="text/javascript" src="js/lib/backgrid-paginator.js"></script>
    <script type="text/javascript" src="js/lib/backgrid-select-all.min.js"></script>
    <script type="text/javascript" src="js/lib/jquery.switchbutton.js"></script>
    <script type="text/javascript" src="js/lib/fusioncharts.js"></script>
    <script type="text/javascript" src="js/lib/fusioncharts.charts.js"></script>
    <script type="text/javascript" src="js/lib/fusioncharts.theme.carbon.js"></script>
    <script type="text/javascript" src="js/lib/modernizr.js"></script>
  
      <!-- Bootstrap JavaScript-->
    <script type="text/javascript" src="plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Malihu Scrollbar-->
    <script type="text/javascript" src="plugins/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- Animo.js-->
    <script type="text/javascript" src="plugins/animo.js/animo.min.js"></script>
    <!-- Bootstrap Progressbar-->
    <script type="text/javascript" src="plugins/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- jQuery Easy Pie Chart-->
    <script type="text/javascript" src="plugins/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>

     

     <!-- Bootstrap FileStyle-->
    <script type="text/javascript" src="plugins/bootstrap-filestyle/src/bootstrap-filestyle.js"></script>
    <!-- jQuery DatePicker start-->
    <script type="text/javascript" src="plugins/moment/min/moment.min.js"></script>
        <!-- Bootstrap MaxLength-->
    <script type="text/javascript" src="plugins/bootstrap-maxlength/src/bootstrap-maxlength.js"></script>
    <!-- jQuery MiniColors-->
    <script type="text/javascript" src="plugins/jquery-minicolors/jquery.minicolors.min.js"></script>
    <!-- Bootstrap TouchSpin-->
    <script type="text/javascript" src="plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>

    <script type="text/javascript" src="plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- Bootstrap DatePicker-->

    <script type="text/javascript" src="plugins/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>


    <!-- jQuery DatePicker End-->

    <!-- DataTables-->
    <script type="text/javascript" src="plugins/datatables.net/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="plugins/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="plugins/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="plugins/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="plugins/jszip/dist/jszip.min.js"></script>    
    <!-- <script type="text/javascript" src="plugins/pdfmake/build/vfs_fonts.js"></script> -->
    <script type="text/javascript" src="plugins/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="plugins/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="plugins/datatables.net-colreorder/js/dataTables.colReorder.min.js"></script>
    <script type="text/javascript" src="plugins/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="plugins/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
     <script type="text/javascript" src="js/lib/bootstrap-datepicker.min.js"></script>
      
    <!-- Models-->
    <script type="text/javascript" src="js/models/EmployeeModel.js"></script>
    <script type="text/javascript" src="js/models/SupplierModel.js"></script>
    <script type="text/javascript" src="js/models/CustomerModel.js"></script>
    <script type="text/javascript" src="js/models/ItemModel.js"></script>
    <script type="text/javascript" src="js/models/SalesItemModel.js"></script>
    <script type="text/javascript" src="js/models/CompanyModel.js"></script>
    <script type="text/javascript" src="js/models/ProductModel.js"></script>
    <script type="text/javascript" src="js/models/ReportModel.js"></script>
    <script type="text/javascript" src="js/models/LocationModel.js"></script>
    <script type="text/javascript" src="js/models/ExpenseCategoryModel.js"></script>
    <script type="text/javascript" src="js/models/ExpenseModel.js"></script>
   

    <!-- Collections-->
    <script type="text/javascript" src="js/collections/EmployeeCollection.js"></script>
    <script type="text/javascript" src="js/collections/SupplierCollection.js"></script>
    <script type="text/javascript" src="js/collections/CustomerCollection.js"></script>
    <script type="text/javascript" src="js/collections/ItemCollection.js"></script>
    <script type="text/javascript" src="js/collections/SalesItemCollection.js"></script>
    <script type="text/javascript" src="js/collections/CompanyCollection.js"></script>
    <script type="text/javascript" src="js/collections/ProductCollection.js"></script> 
    <script type="text/javascript" src="js/collections/ReportCollection.js"></script>
    <script type="text/javascript" src="js/collections/LocationCollection.js"></script>
    <script type="text/javascript" src="js/collections/ExpenseCategoryCollection.js"></script>
    <script type="text/javascript" src="js/collections/ExpenseCollection.js"></script>

    <!-- Views-->
    <!-- Employee-->
    <script type="text/javascript" src="js/views/EmployeeTableView.js"></script>
    
    <!--Supplier-->
    <script type="text/javascript" src="js/views/SupplierTableView.js"></script>
    <script type="text/javascript" src="js/views/MgntSupplierCollectionView.js"></script>
    <script type="text/javascript" src="js/views/MgntSupplierItemView.js"></script>

    <!-- Customer-->
    <script type="text/javascript" src="js/views/CustomerPageView.js"></script>
    <script type="text/javascript" src="js/views/CustomerTableView.js"></script>

    <!-- Company -->
    <script type="text/javascript" src="js/views/FormPageView.js"></script>
    <script type="text/javascript" src="js/views/CompanyPageView.js"></script>
    <script type="text/javascript" src="js/views/CompanyTableView.js"></script>
    <script type="text/javascript" src="js/views/MgntCompanyCollectionView.js"></script>
    <script type="text/javascript" src="js/views/MgntCompanyItemView.js"></script>

    <!-- Product -->
    <script type="text/javascript" src="js/views/MgntProductCollectionView.js"></script>
    <script type="text/javascript" src="js/views/MgntProductItemView.js"></script>

    <!-- Location -->
    <script type="text/javascript" src="js/views/LocationPageView.js"></script>
    <script type="text/javascript" src="js/views/LocationTableView.js"></script>

    <!-- Expense Category -->
    <script type="text/javascript" src="js/views/ExpenseCategoryPageView.js"></script>
    <script type="text/javascript" src="js/views/ExpenseCategoryTableView.js"></script>

    <!-- Expense -->
    <script type="text/javascript" src="js/views/ExpensePageView.js"></script>
    <script type="text/javascript" src="js/views/ExpenseTableView.js"></script>
    <script type="text/javascript" src="js/views/MgntExpenseCollectionView.js"></script>
    <script type="text/javascript" src="js/views/MgntExpenseItemView.js"></script>
    <script type="text/javascript" src="js/views/ExpenseFormPageView.js"></script>
   
    <!-- Item-->
    <script type="text/javascript" src="js/views/ItemPageView.js"></script>
    <script type="text/javascript" src="js/views/ItemTableView.js"></script>
    <script type="text/javascript" src="js/views/ItemFormPageView.js"></script>

    <script type="text/javascript" src="js/views/ItemKitsPageView.js"></script>
    <script type="text/javascript" src="js/views/ItemKitsFormPageView.js"></script>
    <script type="text/javascript" src="js/views/ItemKitTableView.js"></script>
    
    <!-- Report -->
    <script type="text/javascript" src="js/views/ReportPageView.js"></script>
    <script type="text/javascript" src="js/views/ReportListPageView.js"></script>
    <script type="text/javascript" src="js/views/CompanyReportPageView.js"></script>
    <script type="text/javascript" src="js/views/TaxReportPageView.js"></script>
    <script type="text/javascript" src="js/views/ProductReportPageView.js"></script>
    <script type="text/javascript" src="js/views/CustomerReportPageView.js"></script>
    <script type="text/javascript" src="js/views/SupplierReportPageView.js"></script>   
    <script type="text/javascript" src="js/views/ItemReportPageView.js"></script>   
    <script type="text/javascript" src="js/views/ExpenseCategoryReportPageView.js"></script>   
    <script type="text/javascript" src="js/views/SalesReportPageView.js"></script>   
    <script type="text/javascript" src="js/views/SuspendedSalesReportPageView.js"></script> 
    <script type="text/javascript" src="js/views/EmployeeReportPageView.js"></script>     
    <script type="text/javascript" src="js/views/ReceivingReportPageView.js"></script>   
    <script type="text/javascript" src="js/views/DiscountReportPageView.js"></script>   
    <script type="text/javascript" src="js/views/PaymentReportPageView.js"></script>   
    <script type="text/javascript" src="js/views/LowInventoryPageView.js"></script>   
    <script type="text/javascript" src="js/views/InventorySummaryPageView.js"></script>   
    <script type="text/javascript" src="js/views/DeadItemPageView.js"></script>   
    <script type="text/javascript" src="js/views/CommissionReportPageView.js"></script>   
    
    <script type="text/javascript" src="js/views/SaleFormPageView.js"></script>
    <script type="text/javascript" src="js/views/SaleAddItemPageView.js"></script>

    <script type="text/javascript" src="js/views/ReceivingFormPageView.js"></script>
    <script type="text/javascript" src="js/views/ReceivingAddItemPageView.js"></script>

    <!-- Router -->    
    <script type="text/javascript" src="js/router/adminRouter.js"></script>

    <!-- Core JS-->
       
   <script type="text/javascript" src="js/eapp.js"></script>
    <script type="text/javascript" src="js/Validation.js"></script>


   <script type="text/javascript" src="js/page-content/forms/forms-wizard.js"></script>
  <script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
  </body>
</html>
